<?php

namespace App\Models\Dosen;

use Illuminate\Database\Eloquent\Model;
use DB;

class Kegiatan extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'kegiatan';

    public function scopeNamaDosen($query,$dosen_id)
    {
        return DB::table('dosen')->where('id',$dosen_id)->first()->nama_dosen;
    }

}