<?php

namespace App\Models\Dosen;

use Illuminate\Database\Eloquent\Model;
use DB;

class P2m extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'data_p2m';

    public function scopeCountP2m($query,$jd,$ts)
    {

        $jd_all=@DB::table('jd_sp_p2m')->where('sp_id','=',$jd)->pluck('jd_id');

        $dp=@DB::table('sumber_penelitian_p2m')->whereIn('jenis_dana_id',$jd_all)->pluck('data_p2m_id');
        $count=@DB::table('data_p2m')->whereIn('id',$dp)->where('tahun_p2m','=',$ts)->count();
        if($count) {
            return $count;
        }else {
            return '&nbsp;';
        }

    }

    public function scopeDosen($query,$id)
    {
        $result=@DB::table('dosen_p2m')->join('dosen',function ($query)use($id){
            $query->on('dosen_p2m.dosen_id','=','dosen.id')->where('dosen_p2m.data_p2m_id','=',$id);
        })->select('dosen.*')->get();
        if($result)
        {
            return $result;
        }else {
            return ' ';
        }
    }

    public function scopeMhs($query,$id)
    {

        $result=@DB::table('p2m_mhs')->join('biodata_mhs',function ($query)use($id){
            $query->on('p2m_mhs.mhs_id','=','biodata_mhs.id')->where('p2m_mhs.data_p2m_id','=',$id);
        })->select('biodata_mhs.*')->get();
        return $result;
    }
}