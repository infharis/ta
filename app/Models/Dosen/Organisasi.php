<?php

namespace App\Models\Dosen;

use Illuminate\Database\Eloquent\Model;
use DB;

class Organisasi extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'organisasi';


    public function scopeNamaDosen($query,$dosen_id)
    {
        return DB::table('dosen')->where('id',$dosen_id)->first()->nama_dosen;
    }
}