<?php

namespace App\Models\Dosen;

use Illuminate\Database\Eloquent\Model;
use DB;

class Kemampuan extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $timestamps=false;
    protected $table = 'kemampuan_dosen';

    public function scopeNamaDosen($query,$dosen_id)
    {
        return DB::table('dosen')->where('id',$dosen_id)->first()->nama_dosen;
    }
}