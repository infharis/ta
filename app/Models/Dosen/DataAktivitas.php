<?php

namespace App\Models\Dosen;

use Illuminate\Database\Eloquent\Model;
use DB;

class DataAktivitas extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $timestamps=false;
    protected $table = 'data_aktivitas_dosen';

    public function scopeNamaMK($query,$id)
    {
        $result=DB::table('kurikulum')->where('id',$id)->first();
        if($result)
        {
            return $result->nama_mk;
        }else {
            return ' ';
        }
    }
    public function scopeNamaDosen($query,$dosen_id)
    {
        return DB::table('dosen')->where('id',$dosen_id)->first()->nama_dosen;
    }

    // get data aktivitas dosen tetap diluar ps
    public function scopeDataAktivitas($query,$id)
    {
        $result=DB::table('data_aktivitas_dosen')->where('dosen_id',$id)->get();
        return $result;
    }


}