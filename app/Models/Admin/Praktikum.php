<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Praktikum extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'praktikum';

    public function scopeModul($query,$id)
    {
        $result=$query->where('mk_id','=',$id)->get();
        if($result)
        {
            return $result;
        }else{
            return ' ';
        }
    }

    public function scopeModulCount($query,$id)
    {
        $result=$query->where('mk_id','=',$id)->count();
        if($result)
        {
            return $result;
        }else{
            return ' ';
        }
    }

}