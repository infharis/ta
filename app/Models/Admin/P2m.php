<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use DB;

class P2m extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'data_p2m';

    public function scopeP2mTs($query,$ts)
    {
        $result=$query->where('tahun_p2m',$ts)->get();
        if($result)
        {
            return $result;
        }else {
            return ' ';
        }
    }
}