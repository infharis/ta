<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use DB;

class Ta extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'bi';

    public function scopeCommon($query,$table,$statement)
    {
        $result=DB::table($table)->where($statement)->first();
        return $result;
    }

}