<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use DB;

class RuangKerja extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'data_ruang';

}