<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use DB;

class Kerjasama extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'kerjasama';
}