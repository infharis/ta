<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use DB;

class TenagaKependidikan extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tenaga_kependidikan';

    public function scopeCountTK($query,$id,$tenaga_id)
    {
        $statement=array('data_tenaga_kependidikan_id'=>$tenaga_id,'pendidikan_id'=>$id);
        $result=@$query->where($statement)->count();
        if($result)
        {
            return $result;
        }else {
            return ' ';
        }
    }

    public function scopeNamaPendidikan($query,$id)
    {
        $result=DB::table('daftar_pendidikan')->where('id',$id)->first();
        return $result->nama_pendidikan;
    }

    public function scopeCountPendidikan($query,$id)
    {
        $result=$query->where('pendidikan_id',$id)->count();
        if($result)
        {
            return $result;
        }else {
            return ' ';
        }
    }


}