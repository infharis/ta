<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use DB;

class Silabus extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'silabus';

    public function scopeMk($query,$id)
    {
        $result=DB::table('kurikulum')->where('id',$id)->first();
        return $result;
    }
}