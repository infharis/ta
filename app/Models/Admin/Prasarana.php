<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Prasarana extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'data_prasarana';

}