<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use DB;

class Ipk extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'ipk';

    public function scopeCommon($query,$table,$statement)
    {
        $result=DB::table($table)->where($statement)->first();
        return $result;
    }


}