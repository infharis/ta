<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use DB;

class Biodata extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'biodata_mhs';
    protected $fillable=['nim'];

    public function scopeCountKerja($query,$statement)
    {
        $result=$query->where($statement)->where('sidang','!=','0000-00-00')->count();
        if($result)
        {
            return $result;
        }else {
            return ' ';
        }
    }
    public function scopeDosen($query,$nim)
    {
        $nip=DB::table('perwalian_mhs')->where('nim_mhs',$nim)->orderBy('created_at','desc')->first();
        $result=DB::table('dosen')->where('nip',$nip->nip_dosen)->first();
        return $result;
    }
    public function scopeWaktuPekerjaan($query,$id)
    {
        $result=DB::table('daftar_waktu_pekerjaan')->where('id',$id)->first();
        if($result)
        {
            return $result->waktu;
        }else{
            return ' ';
        }
    }
    public function scopeDataMhs($query,$mhs_id)
    {
        $mhs_p2m=0;$mhs_pen=0;$mhs_ipk=0;$mhs_prestasi=0;$result=0;

        $mhs_p2m=DB::table('p2m_mhs')->where('mhs_id',$mhs_id)->count();
        $mhs_pen=DB::table('penelitian_mhs')->where('mhs_id',$mhs_id)->count();
        $mhs_prestasi=DB::table('prestasi_mhs')->where('mhs_id',$mhs_id)->count();
        $mhs_ipk=DB::table('ipk')->where('mhs_id',$mhs_id)->count();
        $result=$mhs_p2m+$mhs_pen+$mhs_prestasi+$mhs_ipk;
        if($result)
        {
            return $result;
        }else {
            $result='kosong';
            return $result;
        }
    }
    public function scopeCommon($query,$table,$statement)
    {
        $result=DB::table($table)->where($statement)->first();
        return $result;
    }

    public function scopeAkademik($query,$dosen_id)
    {
        $result=DB::table('biodata_mhs')->where('dosen_id',$dosen_id)->count();
        if($result)
        {
            return $result;
        }else {
            return ' ';
        }
    }

    public function  scopeTa($query,$dosen_id)
    {
        $doping1=0;
        $doping2=0;
        $doping1=DB::table('biodata_mhs')->where('doping1',$dosen_id)->count();
        $doping2=DB::table('biodata_mhs')->where('doping2',$dosen_id)->count();
        $result=$doping1+$doping2;
        if($result)
        {
            return $result;
        }else {
            return ' ';
        }

    }
}