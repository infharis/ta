<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class PrasaranaLain extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'data_prasarana_lain';

}