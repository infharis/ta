<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use DB;

class Dosen extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'dosen';

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    // get sumber dana penelitian
    public function scopeSumberDana($query,$pd)
    {
        $result=@DB::table('sumber_penelitian_p2m')->join('jenis_dana',function($join)use($pd){
                $join->on('sumber_penelitian_p2m.jenis_dana_id','=','jenis_dana.id')->where('sumber_penelitian_p2m.data_penelitian_id','=',$pd);
        })->select('jenis_dana.*')->get();
        return $result;
    }
}