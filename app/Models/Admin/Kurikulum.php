<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use DB;

class Kurikulum extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'kurikulum';
    public $fillable=['kode_mk'];

    public function scopeNamaMK($query,$id)
    {
        $result=$query->where('semester_id',$id)->get();
        if($result)
        {
            return $result;
        }else {
            return ' ';
        }

    }

    public function scopeNamaMKCount($query,$id)
    {
        $result=$query->where('semester_id',$id)->count();
        if($result)
        {
            return $result;
        }else {
            return ' ';
        }
    }
    public function scopeCountWajib($query)
    {
        $result=$query->sum('bobot_sks');
        if($result)
        {
            return $result;
        }else {
            return ' ';
        }
    }
    public function scopeCountPilihan($query,$table)
    {
        $result=DB::table('mk_pilihan')->sum('bobot_sks');
        if($result)
        {
            return $result;
        }else {
            return ' ';
        }
    }
}