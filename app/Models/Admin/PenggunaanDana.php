<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use DB;

class PenggunaanDana extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'jenis_penggunaan';

    public function scopePresentase($query,$id,$ts,$tsall)
    {

        $jd_all=@DB::table('jd_jp')->where('jp_id','=',$id)->pluck('jd_id');

        // get all dana penelitian
        $dp=@DB::table('sumber_penelitian_p2m')->whereIn('jenis_dana_id',$jd_all)->pluck('data_penelitian_id');
        // get all ts
        $sum_all=@DB::table('data_penelitian')->whereIn('id',$dp)->whereIn('tahun_penelitian',$tsall)->sum('jumlah_dana');
        // one ts
        $sum=@DB::table('data_penelitian')->whereIn('id',$dp)->where('tahun_penelitian','=',$ts)->sum('jumlah_dana');

        if(!$sum)
        {
            // get all dana p2m
            $dp=@DB::table('sumber_penelitian_p2m')->whereIn('jenis_dana_id',$jd_all)->pluck('data_p2m_id');
            // get all ts
            $sum_all=@DB::table('data_p2m')->whereIn('id',$dp)->whereIn('tahun_p2m',$tsall)->sum('jumlah_dana');
            // one ts
            $sum=@DB::table('data_p2m')->whereIn('id',$dp)->where('tahun_p2m','=',$ts)->sum('jumlah_dana');
        }

        if($sum) {
            $result=($sum/$sum_all)*100;
            return $result.' %';
        }else {
            return '&nbsp;';
        }
    }

}