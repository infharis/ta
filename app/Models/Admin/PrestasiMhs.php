<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use DB;

class PrestasiMhs extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'prestasi_mhs';

    public function scopeMhs($query,$mhs_id)
    {
        $result=DB::table('biodata_mhs')->where('id',$mhs_id)->first()->nama;
        return $result;
    }
    public function scopeCountP($query,$statement)
    {
        $result=0;
        $result=$query->where($statement)->count();
        if($result)
        {
            return $result;
        }else {
            return ' ';
        }
    }
}