<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use DB;

class Sd extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sumber_dana';

    public function scopeCommon($query,$table,$statement)
    {
        $result=DB::table($table)->where($statement);
        return $result;
    }
    public function scopeSumJD($query,$jd,$ts)
    {

        $jd_all=@DB::table('sumber_penelitian_p2m')->where('jenis_dana_id','=',$jd)->pluck('data_penelitian_id');
        $sum=@DB::table('data_penelitian')->whereIn('id',$jd_all)->where('tahun_penelitian',$ts)->sum('jumlah_dana');
        if($sum) {
            return number_format($sum,2);
        }else {
            $jd_all=@DB::table('sumber_penelitian_p2m')->where('jenis_dana_id','=',$jd)->pluck('data_p2m_id');
            $sum=@DB::table('data_p2m')->whereIn('id',$jd_all)->where('tahun_p2m',$ts)->sum('jumlah_dana');
            if($sum)
            {
                return number_format($sum,2);
            }else {
                return '&nbsp;';
            }
        }
    }

}