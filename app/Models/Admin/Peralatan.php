<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Peralatan extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'peralatan_lab';


    public function scopePeralatan($query,$id)
    {
        $result=$query->where('data_lab_id',$id)->get();
        if($result)
        {
            return $result;
        }
        else{
            return ' ';
        }
    }
}