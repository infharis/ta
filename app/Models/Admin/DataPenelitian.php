<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use DB;

class DataPenelitian extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'data_penelitian';

    public function scopePenelitianTs($query,$ts)
    {
        $result=$query->where('tahun_penelitian',$ts)->get();
        if($result)
        {
            return $result;
        }else {
            return ' ';
        }
    }
    public function scopeCountDP($query,$jd,$ts)
    {

        $jd_all=@DB::table('jd_sp')->where('sp_id','=',$jd)->pluck('jd_id');

        $dp=@DB::table('sumber_penelitian_p2m')->whereIn('jenis_dana_id',$jd_all)->pluck('data_penelitian_id');
        $count=@DB::table('data_penelitian')->whereIn('id',$dp)->where('tahun_penelitian','=',$ts)->count();
        if($count) {
            return $count;
        }else {
            return '&nbsp;';
        }

    }
//    get penelitian mhs
    public function scopeMhs($query,$id)
    {
        $result=@DB::table('penelitian_mhs')->join('biodata_mhs',function ($query)use($id){
           $query->on('penelitian_mhs.mhs_id','=','biodata_mhs.id')->where('penelitian_mhs.data_penelitian_id','=',$id);
        })->select('biodata_mhs.*')->get();
        if($result)
        {
            return $result;
        }else {
            return ' ';
        }
    }
    //    get penelitian mhs
    public function scopeDosen($query,$id)
    {
        $result=@DB::table('penelitian_dosen')->join('dosen',function ($query)use($id){
           $query->on('penelitian_dosen.dosen_id','=','dosen.id')->where('penelitian_dosen.data_penelitian_id','=',$id);
        })->select('dosen.*')->get();
        if($result)
        {
            return $result;
        }else {
            return ' ';
        }
    }

}