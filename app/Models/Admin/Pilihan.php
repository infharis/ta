<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Pilihan extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'kurikulum';

    public function scopeMK($query,$id)
    {
        $result=$query->where('data_mk_pilihan_id',$id)->get();
        if($result)
        {
            return $result;
        }else {
            return ' ';
        }
    }
    public function scopeCountMK($query,$id)
    {
        $result=$query->where('data_mk_pilihan_id',$id)->count();
        if($result)
        {
            return $result;
        }else {
            return ' ';
        }
    }



}