<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use DB;

class Perwalian extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'perwalian_mhs';
    protected $fillable = ['nip_dosen','semester','tahun','semester','nim_mhs'];


}