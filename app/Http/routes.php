<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// read file e-musrenbang
Route::get('musrenbang',function (){

//    $result=\Excel::load('progkeg_olah.xlsx')->get();
//
//    $i=0;
//    foreach ($result as $value)
//    {
//
//            $result=DB::table('kegiatan')->where('nama_kegiatan',$value->keterangan)->first();
//            $result1=DB::table('program')->where('nama_program',$value->keterangan)->first();
//            if($result) {
//                DB::table('kegiatan')->where('id',$result->id)->update(['kode_kegiatan'=>$value->kode]);
//                echo $value->keterangan.'<br>';
//            }elseif($result1){
//                DB::table('program')->where('id',$result1->id)->update(['kode_program'=>$value->kode]);
//            }
//
//    }

    $url="http://ws.unsyiah.ac.id/webservice/dosen/cdosen/makulkurikulum/kdprodi/0810701/key/iuhs768hkj";
    $xml = simplexml_load_file($url);
    $i=1;
    foreach ($xml as $value)
    {
        foreach ($value as $val) {
            echo $val->kode_mata_kuliah.'<br/>';
        }
    }




});

Route::get('/', function () {
    if(Auth::user())
    {
        return redirect()->back();
    }else {
        return view('auth.login');
    }
});

Route::post('login','Auth\AuthController@login');
Route::get('logout','Auth\AuthController@logout');


/** routes admin */
Route::group(['middleware'=>'acl:admin','namespace'=>'Admin'],function(){

    Route::get('admin','LaporanController@index');
   // Route::get('laporan','IndexController@laporan');

    // laporan borang akreditasi
    Route::get('d/umpan_balik','LaporanController@umpan_balik');
    Route::get('d/mhs_reguler','LaporanController@mhs_reguler');
    Route::get('d/daftar_dosen','LaporanController@daftar_dosen');
    Route::get('d/data_penelitian','LaporanController@data_penelitian');
    Route::get('d/data_p2m','LaporanController@data_p2m');

    // login as dosen
    Route::get('login/as','LoginAsController@index');
    Route::post('login/as','LoginAsController@login_as');

    // realisasi perolehan dana
    Route::get('realisasi_dana/view','RealisasiController@index');
    Route::get('realisasi_dana/create_sd','RealisasiController@create_sd');
    Route::post('realisasi_dana/store_sd','RealisasiController@store_sd');
    Route::get('realisasi_dana/edit_sd/{id}','RealisasiController@edit_sd');
    Route::post('realisasi_dana/update_sd/{id}','RealisasiController@update_sd');

    // realisasi jenis dana
    Route::get('realisasi_dana/create_jd','RealisasiController@create_jd');
    Route::post('realisasi_dana/store_jd','RealisasiController@store_jd');
    Route::get('realisasi_dana/edit_jd/{id}','RealisasiController@edit_jd');
    Route::post('realisasi_dana/update_jd/{id}','RealisasiController@update_jd');

    // pengunaan dana
    Route::get('pengunaan_dana/view','PenggunaanDanaController@index');
    Route::get('pengunaan_dana/create','PenggunaanDanaController@create');
    Route::post('pengunaan_dana/store','PenggunaanDanaController@store');
    Route::get('pengunaan_dana/edit/{id}','PenggunaanDanaController@edit');
    Route::post('pengunaan_dana/update/{id}','PenggunaanDanaController@update');

    // jumlah judul penelitian
    Route::get('penelitian/dosen_tetap',function (){
        return view('admin.penelitian.index');
    });
    Route::get('penelitian/view','PenelitianController@index');
    Route::get('penelitian/create','PenelitianController@create');
    Route::post('penelitian/store','PenelitianController@store');
    Route::get('penelitian/edit/{id}','PenelitianController@edit');
    Route::post('penelitian/update/{id}','PenelitianController@update');

    //p2m
    Route::get('p2m/kegiatan',function (){
        return view('admin.p2m.index');
    });
    Route::get('p2m/view','P2mController@index');
    Route::get('p2m/create','P2mController@create');
    Route::post('p2m/store','P2mController@store');

    // data master mhs
    Route::get('mhs/view/data','Master_Mhs\IndexController@index');
    // biodata mhs
    Route::get('data/mhs/biodata_mhs','Master_Mhs\BiodataController@index');
    Route::get('data/mhs/biodata_mhs/create','Master_Mhs\BiodataController@create');
    Route::get('data/mhs/biodata_mhs/get_prov','Master_Mhs\BiodataController@get_prov');
    Route::post('data/mhs/biodata_mhs/store','Master_Mhs\BiodataController@store');
    Route::delete('data/mhs/biodata_mhs/destroy','Master_Mhs\BiodataController@destroy');
    Route::get('data/mhs/biodata_mhs/edit/{id}','Master_Mhs\BiodataController@edit');
    Route::post('data/mhs/biodata_mhs/update/{id}','Master_Mhs\BiodataController@update');
    // sinkronisasi data mhs dan dosen
    Route::post('sinkron_data_mhs','Master_Mhs\SinkronMhsController@get_all_mhs');
    Route::post('sinkron_data_dosen','Master_Dosen\SinkronDosenController@get_all_dosen');


    // import and export
    Route::get('mhs/view/data/format_biodata_mhs','Master_Mhs\FileController@index');
    Route::post('data/mhs/biodata_mhs/import_data_mhs','Master_Mhs\FileController@upload_data_mhs');
    // end

    // prestasi mhs
    Route::get('data/mhs/prestasi_mhs','Master_Mhs\PrestasiController@index');
    Route::get('mhs/view/prestasi_mhs','Master_Mhs\PrestasiController@index');
    Route::get('data/mhs/prestasi_mhs/create','Master_Mhs\PrestasiController@create');
    Route::post('data/mhs/prestasi_mhs/store','Master_Mhs\PrestasiController@store');
    Route::get('data/mhs/prestasi_mhs/edit/{id}','Master_Mhs\PrestasiController@edit');
    Route::post('data/mhs/prestasi_mhs/update/{id}','Master_Mhs\PrestasiController@update');
    Route::delete('data/mhs/prestasi_mhs/destroy','Master_Mhs\PrestasiController@destroy');

    // akademik mhs
    Route::get('data/mhs/akademik','Master_Mhs\IpkController@index');
    Route::get('data/mhs/akademik/create','Master_Mhs\IpkController@create');
    Route::post('data/mhs/akademik/store','Master_Mhs\IpkController@store');
    Route::get('data/mhs/akademik/edit/{id}','Master_Mhs\IpkController@edit');
    Route::post('data/mhs/akademik/update/{id}','Master_Mhs\IpkController@update');
    Route::delete('data/mhs/akademik/destroy','Master_Mhs\IpkController@destroy');

    // tugas akhir
    Route::get('data/mhs/ta','Master_Mhs\TaController@index');
    Route::get('data/mhs/ta/create','Master_Mhs\TaController@create');
    Route::post('data/mhs/ta/store','Master_Mhs\TaController@store');
    Route::get('data/mhs/ta/edit/{id}','Master_Mhs\TaController@edit');
    Route::post('data/mhs/ta/update/{id}','Master_Mhs\TaController@update');

    // alumni
    Route::get('data/mhs/data_alumni','Master_Mhs\AlumniController@index');
    Route::get('data/mhs/data_alumni/{nim}/edit','Master_Mhs\AlumniController@edit');
    Route::post('data/mhs/data_alumni/{nim}/update','Master_Mhs\AlumniController@update');

    // penelitian mhs
    Route::get('penelitian/mhs','Master_Mhs\IndexController@penelitian_mhs');

    // data master dosen
    Route::get('data/dosen/view','Master_Dosen\IndexController@index');
    Route::get('data/dosen/create','Master_Dosen\IndexController@create');
    Route::post('data/dosen/store','Master_Dosen\IndexController@store');

    // dosen tetap
    Route::get('sdm/data/dosen_tetap/view','Master_Dosen\IndexController@dosen_tetap');
    Route::get('sdm/data/dosen_tetap/perwalian_mhs/{nip}','Master_Dosen\IndexController@perwalian_mhs');
    Route::get('sdm/data/dosen_tetap/edit/{id}','Master_Dosen\IndexController@edit_dosen_tetap');
    Route::post('sdm/data/dosen_tetap/update/{id}','Master_Dosen\IndexController@update_dosen_tetap');
    Route::get('sdm/aktivitas/dosen_tetap','Master_Dosen\AktivitasDtController@aktivitas_dosen_tetap');
    Route::get('sdm/data/aktivitas_dosen_tetap','Master_Dosen\AktivitasDtController@data_aktivitas_dosen_tetap');
    Route::get('sdm/data/dosen_tetap/pembimbing_ta/{dosen_id}','Master_Dosen\AktivitasDtController@count_pembimbing_ta');
    // data dosen import
    Route::get('dosen/view/data/format_biodata_dosen','Master_Dosen\FileController@index');
    Route::post('data/dosen/biodata_dosen/import_data_dosen','Master_Dosen\FileController@upload_data_dosen');


    // dosen diluar ps
    Route::get('sdm/data/dosen_diluar_ps/view','Master_Dosen\IndexController@dosen_diluar_ps');
    Route::get('sdm/data/aktivitas_dosen_diluar_ps','Master_Dosen\AktivitasLpController@data_aktivitas_dosen_diluar_ps');
    // dosen tidak tetap
    Route::get('sdm/dosen_tidak_tetap/view','Master_Dosen\IndexController@dosen_tidak_tetap');
    Route::get('sdm/dosen_tidak_tetap/data_aktivitas','Master_Dosen\AktivitasDtController@data_aktivitas_tidak_tetap');
    // karya ilimiah dosen tetap
    Route::get('penelitian/karya_dosen','Master_Dosen\IndexController@karya_ilmiah');
    // paten
    Route::get('penelitian/paten','Master_Dosen\IndexController@paten');

    // jumlah p2m
    Route::get('p2m/view/jumlah','P2mController@jumlah_p2m');
    Route::get('p2m/kegiatan/create','P2mController@jumlah_p2m_create');
    Route::post('p2m/kegiatan/store','P2mController@jumlah_p2m_store');
    Route::get('p2m/kegiatan/edit/{id}','P2mController@jumlah_p2m_edit');
    Route::post('p2m/kegiatan/update/{id}','P2mController@jumlah_p2m_update');
    Route::get('p2m/mhs/view','P2mController@jumlah_p2m_mhs');

    // data ruang kerja
    Route::get('ruang_kerja/view','RuangKerjaController@index');
    Route::get('ruang_kerja/create','RuangKerjaController@create');
    Route::post('ruang_kerja/store','RuangKerjaController@store');
    Route::get('ruang_kerja/edit/{id}','RuangKerjaController@edit');
    Route::post('ruang_kerja/update/{id}','RuangKerjaController@update');
    Route::delete('ruang_kerja/destroy','RuangKerjaController@destroy');

    //Aksesibilitas tiap jenis data
    Route::get('aksesibilitas/view','AksesibilitasController@index');
    Route::get('aksesibilitas/create','AksesibilitasController@create');
    Route::post('aksesibilitas/store','AksesibilitasController@store');
    Route::get('aksesibilitas/edit/{id}','AksesibilitasController@edit');
    Route::post('aksesibilitas/update/{id}','AksesibilitasController@update');
    Route::delete('aksesibilitas/destroy','AksesibilitasController@destroy');

    // pustaka
    Route::get('pembiayaan/sarana',function (){
        return view('admin.pustaka.index');
    });
    Route::get('pustaka/view','PustakaController@index');
    Route::get('pustaka/create','PustakaController@create');
    Route::post('pustaka/store','PustakaController@store');
    Route::get('pustaka/edit/{id}','PustakaController@edit');
    Route::post('pustaka/update/{id}','PustakaController@update');
    Route::delete('pustaka/destroy','PustakaController@destroy');

    // data mhs reguler dan non reguler
    Route::get('mhs/view',function(){
        return view('admin.mhs.index');
    });

    // reguler
    Route::get('mhs/view/reguler','RegulerController@index');
    Route::get('mhs/view/reguler_create','RegulerController@create');
    Route::post('mhs/store/reguler_create','RegulerController@store');
    Route::get('mhs/view/reguler/addmore/{id}','RegulerController@add_more');
    Route::post('mhs/view/reguler/addmore/{id}','RegulerController@add_more_store');
    Route::get('mhs/edit/reguler/{id}','RegulerController@edit');
    Route::post('mhs/update/reguler/{id}','RegulerController@update');
    Route::delete('mhs/view/reguler/','RegulerController@destroy');

    // non reguler
    Route::get('mhs/view/non_reguler','NonRegulerController@index');
    Route::get('mhs/view/non_reguler_create','NonRegulerController@create');
    Route::post('mhs/store/non_reguler_create','NonRegulerController@store');
    Route::get('mhs/edit/non_reguler/{id}','NonRegulerController@edit');
    Route::post('mhs/update/non_reguler/{id}','NonRegulerController@update');
    Route::delete('mhs/delete/non_reguler','NonRegulerController@destroy');

    // jumlah reguler
    Route::get('mhs/view/jumlah_reguler','JumlahRegulerController@index');
    Route::get('mhs/view/create_jumlah_reguler','JumlahRegulerController@create');
    Route::post('mhs/store/create_jumlah_reguler','JumlahRegulerController@store');
    Route::get('mhs/edit/jumlah_reguler/{id}','JumlahRegulerController@edit');
    Route::post('mhs/update/jumlah_reguler/{id}','JumlahRegulerController@update');
    Route::delete('mhs/delete/jumlah_reguler','JumlahRegulerController@destroy');
    // studi pelacakan
    Route::get('mhs/evaluasi','StudiController@index');
    Route::get('mhs/evaluasi/create','StudiController@create');
    Route::post('mhs/evaluasi/store','StudiController@store');
    Route::get('mhs/evaluasi/edit/{id}','StudiController@edit');
    Route::post('mhs/evaluasi/update/{id}','StudiController@update');
    Route::delete('mhs/evaluasi/destroy','StudiController@destroy');

    // silabus/SAP
    Route::get('kurikulum/silabus/view','SilabusController@index');
    Route::get('kurikulum/silabus/create','SilabusController@create');
    Route::post('kurikulum/silabus/store','SilabusController@store');
    Route::get('kurikulum/silabus/edit/{id}','SilabusController@edit');
    Route::post('kurikulum/silabus/update/{id}','SilabusController@update');
    Route::delete('kurikulum/silabus/destroy','SilabusController@destroy');

   // kerjasama luar dan dalam negeri
    Route::get('kerjasama',function(){
        return view('admin.kerjasama.index');
    });
    Route::get('kerjasama/view','KerjasamaController@index');
    Route::get('kerjasama/create','KerjasamaController@create');
    Route::post('kerjasama/store','KerjasamaController@store');
    Route::get('kerjasama/edit/{id}','KerjasamaController@edit');
    Route::post('kerjasama/update/{id}','KerjasamaController@update');
    Route::delete('kerjasama/destroy','KerjasamaController@destroy');


    // kegiatan tenaga ahli
    Route::get('sdm/pakar/create','TenagaAhliController@create');
    Route::get('sdm/pakar/view','TenagaAhliController@index');
    Route::post('pakar/store','TenagaAhliController@store');
    Route::get('sdm/pakar/edit/{id}','TenagaAhliController@edit');
    Route::post('sdm/pakar/update/{id}','TenagaAhliController@update');
    Route::delete('sdm/pakar/delete','TenagaAhliController@destroy');

    // data tenaga kependidikan
    Route::get('sdm/kependidikan/view','DataTenagaController@index');
    Route::get('sdm/kependidikan/create','DataTenagaController@create');
    Route::post('kependidikan/store','DataTenagaController@store');
    Route::get('sdm/kependidikan/detail/{id}','DataTenagaController@detail');
    Route::get('sdm/kependidikan/edit/{id}','DataTenagaController@edit');
    Route::post('sdm/kependidikan/update/{id}','DataTenagaController@update');
    Route::delete('sdm/kependidikan/destroy','DataTenagaController@destroy');

    // dosen tetap tugas belajar
    Route::get('sdm/dosen_tetap/tugas_belajar','Master_Dosen\TugasBelajarController@index');

    // kegiatan dosen tetap
    Route::get('sdm/dosen_tetap/kegiatan','Master_Dosen\KegiatanController@index');

    // prestasi dosen
    Route::get('sdm/dosen/prestasi','Master_Dosen\PrestasiController@index');

    // organisasi dosen tetap
    Route::get('sdm/dosen_tetap/organisasi','Master_Dosen\PrestasiController@organisasi');

    // bimbingan akademik
    Route::get('bimbingan',function (){
        return view('admin.bimbingan.index');
    });
    Route::get('bimbingan/view','BimbinganController@index');
    Route::get('bimbingan/create','BimbinganController@create');
    Route::post('bimbingan/store','BimbinganController@store');

    // data prasarana
    Route::get('pembiayaan/prasarana',function (){
        return view('admin.prasarana.index');
    });
    Route::get('data/prasarana','PrasaranaController@index');
    Route::get('data/prasarana/create','PrasaranaController@create');
    Route::post('data/prasarana/store','PrasaranaController@store');
    Route::get('data/prasarana/edit/{id}','PrasaranaController@edit');
    Route::post('data/prasarana/update/{id}','PrasaranaController@update');
    Route::delete('data/prasarana/destroy','PrasaranaController@destroy');

    // data prasarana lain
    Route::get('data/prasarana_lain','PrasaranaLainController@index');
    Route::get('data/prasarana_lain/create','PrasaranaLainController@create');
    Route::post('data/prasarana_lain/store','PrasaranaLainController@store');
    Route::get('data/prasarana_lain/edit/{id}','PrasaranaLainController@edit');
    Route::post('data/prasarana_lain/update/{id}','PrasaranaLainController@update');
    Route::delete('data/prasarana_lain/destroy','PrasaranaLainController@destroy');

    // peralatan lab
    Route::get('peralatan/lab','PeralatanController@index');
    Route::get('peralatan/create_lab','PeralatanController@create_lab');
    Route::post('peralatan/create_lab','PeralatanController@store_lab');
    Route::get('peralatan/edit_lab/{id}','PeralatanController@edit_lab');
    Route::post('peralatan/update_lab/{id}','PeralatanController@update_lab');

    // data lab
    Route::get('peralatan/create_peralatan','PeralatanController@create_peralatan');
    Route::post('peralatan/create_peralatan','PeralatanController@store_peralatan');
    Route::get('peralatan/edit_peralatan/{id}','PeralatanController@edit_peralatan');
    Route::post('peralatan/update_peralatan/{id}','PeralatanController@update_peralatan');


    // data umpan balik
    Route::get('umpan_balik/view','UmpanBalikController@index');
    Route::get('umpan_balik/create','UmpanBalikController@create');
    Route::post('umpan_balik/store','UmpanBalikController@store');
    Route::get('umpan_balik/edit/{id}','UmpanBalikController@edit');
    Route::post('umpan_balik/update/{id}','UmpanBalikController@update');
    Route::delete('umpan_balik/destroy','UmpanBalikController@destroy');

    //layanan to mhs
    Route::get('mhs/layanan','LayananController@index');
    Route::get('mhs/layanan/create','LayananController@create');
    Route::post('layanan_mhs/store','LayananController@store');
    Route::get('mhs/layanan/edit/{id}','LayananController@edit');
    Route::post('mhs/layanan/update/{id}','LayananController@update');
    Route::delete('mhs/layanan/destroy','LayananController@destroy');

    // kurikulum
    Route::get('kurikulum',function (){
       return view('admin.kurikulum.index');
    });
    Route::get('kurikulum/view','KurikulumController@index');
    Route::get('kurikulum/create','KurikulumController@create');
    Route::post('kurikulum/store','KurikulumController@store');
    Route::get('kurikulum/edit/{id}','KurikulumController@edit');
    Route::post('kurikulum/update/{id}','KurikulumController@update');
    Route::post('kurikulum/sinkron/','KurikulumController@sinkron_data');
    Route::get('kurikulum/jumlah_sks',function (){
        return view('admin.kurikulum.jumlah_sks');
    });

    // mk pilihan
    Route::get('kurikulum/mk_pilihan/view','PilihanController@index');
    Route::get('mk_pilihan/create','PilihanController@create');
    Route::post('mk_pilihan/store','PilihanController@store');
    Route::get('kurikulum/mk_pilihan/edit/{id}','PilihanController@edit');
    Route::post('kurikulum/mk_pilihan/update/{id}','PilihanController@update');
    // praktikum
    Route::get('kurikulum/praktikum/view','PraktikumController@index');
    Route::get('kurikulum/praktikum/create','PraktikumController@create');
    Route::post('praktikum/store','PraktikumController@store');
    Route::get('kurikulum/praktikum/edit/{id}','PraktikumController@edit');
    Route::post('kurikulum/praktikum/update/{id}','PraktikumController@update');
    Route::delete('kurikulum/praktikum/destroy','PraktikumController@destroy');

    // upaya perbaikan
    Route::get('bimbingan/upaya/view','UpayaController@index');
    Route::get('bimbingan/upaya/create','UpayaController@create');
    Route::post('upaya/store','UpayaController@store');
    Route::get('bimbingan/upaya/edit/{id}','UpayaController@edit');
    Route::post('bimbingan/upaya/update/{id}','UpayaController@update');
    Route::delete('bimbingan/upaya/destroy','UpayaController@destroy');
    // bimbingan akademik
    Route::get('bimbingan/akademik','UpayaController@akademik');
    // tugas akhir
    Route::get('bimbingan/ta','UpayaController@ta');

    // perubahan password
    Route::get('change_password_admin','ChangePasswordController@index');
    Route::post('change_password_admin','ChangePasswordController@change_password');

    /* sumber daya manusia */
    // data dosen tetap
    Route::get('sdm/data/dosen_tetap',function (){
        return view('admin.dosen.data_dosen_tetap');
    });
    // data dosen tidak tetap
    Route::get('sdm/data_dosen_tidak_tetap',function (){
        return view('admin.dosen.data_dosen_tidak_tetap');
    });
    Route::get('sdm/upaya_peningkatan',function (){
        return view('admin.dosen.upaya_sdm');
    });

    // suasana akademik
    Route::get('suasana_akademik','IndexController@suasana_akademik');

    // alokasi dana
    Route::get('pembiayaan/alokasi_dana',function (){
        return view('admin.alokasi_dana.index');
    });

    // data penelitian
    Route::get('pembiayaan/penelitian','PenelitianController@data_penelitian');
    // data p2m
    Route::get('pembiayaan/p2m','P2mController@data_p2m');

});


/** routes dosen */
Route::group(['middleware'=>'acl:dosen','namespace'=>'Dosen'],function(){
    Route::get('dosen',function(){
       return view('dosen.data_master.index');
    });

    // login as admin
    Route::get('login/admin','IndexController@login_as_admin');
    // data master
    Route::get('dosen/data_master/show_form','IndexController@create');
    Route::post('dosen/data_master/store','DosenController@store');

    Route::get('dosen/data_master',function(){
       return view('dosen.data_master.index');
    });


    // data master dosen
//
//    Route::get('dosen/update',function(){
//        return view('operator_sarjana.dosen.update');
//    });
//    Route::get('dosen/create/dosen_tetap',function(){
//        $title='Form Input Data Dosen Tetap';
//        return view('operator_sarjana.dosen.create',compact('title'));
//    });
//    Route::get('dosen/create/dosen_luar_ps',function(){
//        $title='Form Input Data Dosen Tetap Diluar Bidang PS';
//        return view('dosen.data_master.create',compact('title'));
//    });
//
//    Route::get('dosen/dosen_tetap/aktivitas_details',function(){
//        return view('dosen.data_master.aktivitas_dosen_tetap.detail');
//    });
//
//    Route::get('dosen/create/data_aktivitas_luar_ps',function(){
//        return view('dosen.data_master.aktivitas_dosen_tetap.data_luar_ps');
//    });
//    Route::get('dosen/create/data_aktivitas_dosen_tidak',function(){
//        return view('dosen.data_master.aktivitas_dosen_tetap.dosen_tidak_tetap');
//    });
//
//    // data dosen tetap
//    Route::get('dosen/view/dosen_tetap',function(){
//        return view('dosen.data_master.dosen_tetap.view');
//    });
//    Route::get('dosen/create/dosen_tetap',function(){
//        return view('dosen.data_master.dosen_tetap.create');
//    });
//
//
//    // dosen diluar bidang ps
//    Route::get('dosen/view/diluar_ps',function(){
//        return view('dosen.data_master.diluar_ps.view');
//    });

    // view aktivitas dosen tetap
    Route::get('dosen/view/aktivitas_dosen_tetap','AktivitasController@index');
    Route::get('dosen/create/aktivitas_dosen_tetap','AktivitasController@create');
    Route::post('dosen/store/aktivitas_dosen_tetap','AktivitasController@store');
    Route::get('dosen/edit/aktivitas_dosen_tetap/{id}','AktivitasController@edit');
    Route::post('dosen/update/aktivitas_dosen_tetap/{id}','AktivitasController@update');
    Route::delete('dosen/delete/aktivitas_dosen_tetap','AktivitasController@destroy');


    // data aktivitas dosen tetap
    Route::get('dosen/view/data_aktivitas_dosen_tetap','DataAktivitasController@index');
    Route::get('dosen/create/data_aktivitas_dosen_tetap','DataAktivitasController@create');
    Route::post('dosen/store/data_aktivitas_dosen_tetap','DataAktivitasController@store');
    Route::get('dosen/edit/data_aktivitas_dosen_tetap/{id}','DataAktivitasController@edit');
    Route::post('dosen/update/data_aktivitas_dosen_tetap/{id}','DataAktivitasController@update');
    Route::delete('dosen/delete/data_aktivitas_dosen_tetap','DataAktivitasController@destroy');



    // data aktivitas dosen tetap di luar ps
    Route::get('dosen/view/data_aktivitas_luar_ps',function (){
        return view('dosen.data_master.data_aktivitas.view_luar_ps');
    });
    // data dosen tidak tetap
    Route::get('dosen/view/dosen_tidak_tetap',function(){
        return view('dosen.data_master.dosen_tidak_tetap.view');
    });
    // data aktivitas dosen tidak tetap
    Route::get('dosen/view/data_aktivitas_dosen_tidak_tetap',function(){
        return view('dosen.data_master.aktivitas_tidak_tetap.view');
    });

    //kegiatan dosen tetap
    Route::get('dosen/view/kegiatan_dosen_tetap','KegiatanController@index');
    Route::get('dosen/create/kegiatan_dosen_tetap','KegiatanController@create');
    Route::post('dosen/store/kegiatan_dosen_tetap','KegiatanController@store');
    Route::get('dosen/edit/kegiatan_dosen_tetap/{id}','KegiatanController@edit');
    Route::post('dosen/update/kegiatan_dosen_tetap/{id}','KegiatanController@update');
    Route::delete('dosen/delete/kegiatan_dosen_tetap','KegiatanController@destroy');

    // prestasi Dosen
    Route::get('dosen/view/prestasi','PrestasiController@index');
    Route::get('dosen/create/prestasi','PrestasiController@create');
    Route::post('dosen/store/prestasi','PrestasiController@store');
    Route::get('dosen/edit/prestasi/{id}','PrestasiController@edit');
    Route::post('dosen/update/prestasi/{id}','PrestasiController@update');
    Route::delete('dosen/delete/prestasi','PrestasiController@destroy');

    // organisasi
    Route::get('dosen/view/organisasi','OrganisasiController@index');
    Route::get('dosen/create/organisasi','OrganisasiController@create');
    Route::post('dosen/store/organisasi','OrganisasiController@store');
    Route::delete('dosen/delete/organisasi','OrganisasiController@destroy');

    // pembimbing akademik
    Route::get('dosen/view/pembimbing_akademik','PaController@index');
    // pembimbing tugas akhir
    Route::get('dosen/view/pembimbing_ta','TaController@index');

    // dana untuk kegiatan penelitian
    Route::get('dosen/view/dana_penelitian','PenelitianController@index');
    Route::get('dosen/create/dana_penelitian','PenelitianController@create');
    Route::post('dosen/store/dana_penelitian','PenelitianController@store');
    Route::get('dosen/edit/dana_penelitian/{id}','PenelitianController@edit');
    Route::post('dosen/update/dana_penelitian/{id}','PenelitianController@update');

    // dana untuk kegiatan p2m
    Route::get('dosen/view/dana_p2m','P2mController@index');
    Route::get('dosen/create/dana_p2m','P2mController@create');
    Route::post('dosen/store/dana_p2m','P2mController@store');
    Route::get('dosen/edit/dana_p2m/{id}','P2mController@edit');
    Route::post('dosen/update/dana_p2m/{id}','P2mController@update');
    Route::delete('dosen/destroy/dana_p2m','P2mController@destroy');


    // artikel dosen
    Route::get('dosen/view/artikel_dosen','ArtikelController@index');
    Route::get('dosen/create/artikel_dosen','ArtikelController@create');
    Route::post('dosen/store/artikel_dosen','ArtikelController@store');
    Route::delete('dosen/delete/artikel_dosen','ArtikelController@destroy');

    // peningkatan kemampuan dosen tetap
    Route::get('dosen/view/kemampuan','KemampuanController@index');
    Route::get('dosen/create/kemampuan','KemampuanController@create');
    Route::post('dosen/store/kemampuan','KemampuanController@store');
    Route::delete('dosen/delete/kemampuan','KemampuanController@destroy');

    // karya dan atau paten dosen or mhs
    Route::get('dosen/view/karya_paten','KaryaController@index');
    Route::get('dosen/create/karya_paten','KaryaController@create');
    Route::post('dosen/store/karya_paten','KaryaController@store');
    Route::delete('dosen/delete/karya_paten','KaryaController@destroy');

    // perubahan password
    Route::get('change_password_dosen','ChangePasswordController@index');
    Route::post('change_password_dosen','ChangePasswordController@change_password');

    // biodata diri
    Route::get('dosen/biodata_diri','BiodataController@index');
    Route::post('dosen/biodata_diri','BiodataController@update');

    // pendidikan dosen
    Route::get('dosen/view/pendidikan','PendidikanController@index');
    Route::get('dosen/create/pendidikan','PendidikanController@create');
    Route::post('dosen/store/pendidikan','PendidikanController@store');
    Route::get('dosen/edit/pendidikan/{id}','PendidikanController@edit');
    Route::post('dosen/update/pendidikan/{id}','PendidikanController@update');
    Route::delete('dosen/delete/pendidikan','PendidikanController@destroy');
});

