<?php
/**
 * Created by PhpStorm.
 * User: infharis
 * Date: 10/9/17
 * Time: 9:15 AM
 */

namespace App\Http\Controllers\Dosen;

use App\User;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Illuminate\Http\Request;
use App\Models\Dosen\Organisasi;



class OrganisasiController extends Controller
{
    public $dosen_id;
    public function __construct()
    {
        $this->dosen_id=Auth::user()->dosen->id;
    }

    public function index()
    {
        $result=Organisasi::where('dosen_id',$this->dosen_id)->get();
        return view('dosen.data_master.organisasi.view',compact('result'));
    }

    public function create()
    {
        return view('dosen.data_master.organisasi.create');
    }

    public function store(Request $request)
    {
        $org=new Organisasi();
        $org->dosen_id=$this->dosen_id;
        $org->nama_organisasi=$request->organisasi;
        $org->mulai=$request->start;
        $org->akhir=$request->end;
        $org->tingkat=$request->tingkat;
        $org->save();

        if($request->hasFile('file'))
        {
            $file = $request->file('file');
            $fileName = $file->getClientOriginalName();
            $request->file('file')->move("local/storage/app/organisasi", $fileName);

            Organisasi::where('id',$org->id)->update(['softcopy'=>$fileName]);
        }

        return redirect('dosen/view/organisasi')->with('success','Data Organisasi Berhasil Ditambah');
    }

    public function destroy(Request $request)
    {
        $data=Organisasi::find($request->id);

        // hapus file
        if($data->softcopy) {
            unlink('local/storage/app/organisasi/' . $data->softcopy);
        }
        if(Organisasi::destroy($request->id))
        {
            echo 1;
        }else {
            echo 0;
        }
    }
}