<?php

namespace App\Http\Controllers\Dosen;

use App\User;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Illuminate\Http\Request;
use App\Models\Admin\DataPenelitian;

class PenelitianController extends Controller
{

    public function index()
    {
        $dosen_id=Auth::user()->dosen->id;

        $result=@DB::table('data_penelitian')->join('penelitian_dosen',function($join)use($dosen_id){
            $join->on('data_penelitian.id','=','penelitian_dosen.data_penelitian_id')->where('penelitian_dosen.dosen_id','=',$dosen_id);
        })->select('data_penelitian.*')->get();
        return view('dosen.data_master.dana_penelitian.view',compact('result'));
    }

    public function create()
    {
        $ds=DB::table('dosen')->where('id','!=',Auth::user()->dosen->id)->get();
        $mhs=DB::table('biodata_mhs')->get();
        $jd=DB::table('jenis_dana')->get();
        return view('dosen.data_master.dana_penelitian.create',compact('ds','jd','mhs'));
    }
    public function edit($id)
    {
        $ds=DB::table('dosen')->where('id','!=',Auth::user()->dosen->id)->get();
        $mhs=DB::table('biodata_mhs')->get();
        $jd=DB::table('jenis_dana')->get();

        // data penelitian prev
        $ds_prev=DB::table('penelitian_dosen')->where('data_penelitian_id',$id)->get();
        $mhs_prev=DB::table('penelitian_mhs')->where('data_penelitian_id',$id)->get();
        $jd_prev=DB::table('sumber_penelitian_p2m')->where('data_penelitian_id',$id)->get();
        $result=DataPenelitian::find($id);

        // convert object to array
        $ds_prev = array_map(function ($value) {
            return $value->dosen_id;
        }, $ds_prev);
        $mhs_prev = array_map(function ($value) {
            return $value->mhs_id;
        }, $mhs_prev);
        $jd_prev = array_map(function ($value) {
            return $value->jenis_dana_id;
        }, $jd_prev);


        return view('dosen.data_master.dana_penelitian.edit',compact('jd_prev','ds','jd','mhs','result','ds_prev','mhs_prev'));
    }
    public function update($id,Request $request)
    {
        $dosen_id=Auth::user()->dosen->id;
        // penelitian
        $dp=DataPenelitian::find($id);
        $dp->judul_penelitian=$request->jp;
        $dp->jumlah_dana=$request->jumlah_dana;
        $dp->tahun_penelitian=$request->tahun;
        $dp->save();


        // jenis dana
        DB::table('sumber_penelitian_p2m')->where('data_penelitian_id',$id)->delete();
        if($request->jd) {
            foreach ($request->jd as $value) {
                DB::table('sumber_penelitian_p2m')->insert(['data_penelitian_id' =>$id, 'jenis_dana_id' => $value]);
            }
        }
        // anggota penelitian dosen
        DB::table('penelitian_dosen')->where('data_penelitian_id',$id)->delete();
        if($request->dosen) {
            $self=0;
            foreach ($request->dosen as $val) {
                if($val) {
                    DB::table('penelitian_dosen')->insert(['data_penelitian_id' => $id, 'dosen_id' => $val]);
                    $self=1;
                }
            }

        }
        DB::table('penelitian_dosen')->insert(['data_penelitian_id' => $id, 'dosen_id' => $dosen_id]);

        // anggota penelitian mhs
        DB::table('penelitian_mhs')->where('data_penelitian_id',$id)->delete();
        if($request->mhs)
        {
            foreach ($request->mhs as $mhs) {
                if($mhs) {
                    DB::table('penelitian_mhs')->insert(['data_penelitian_id' => $id, 'mhs_id' => $mhs]);
                }
            }
        }


        return redirect('dosen/view/dana_penelitian')->with('success','Data Penelitian Berhasil Diupdate');

    }
    public function store(Request $request)
    {
        $dosen_id=Auth::user()->dosen->id;
        // penelitian
        $dp=new DataPenelitian();
        $dp->judul_penelitian=$request->jp;
        $dp->jumlah_dana=$request->jumlah_dana;
        $dp->tahun_penelitian=$request->tahun;
        $dp->save();


        // jenis dana
        if($request->jd) {
            foreach ($request->jd as $value) {
                DB::table('sumber_penelitian_p2m')->insert(['data_penelitian_id' => $dp->id, 'jenis_dana_id' => $value]);
            }
        }
        // anggota penelitian dosen
        if($request->dosen) {
            $self=0;
            foreach ($request->dosen as $val) {
                if($val) {
                    DB::table('penelitian_dosen')->insert(['data_penelitian_id' => $dp->id, 'dosen_id' => $val]);
                    $self=1;
                }
            }

        }

        // dosen currently
        DB::table('penelitian_dosen')->insert(['data_penelitian_id' => $dp->id, 'dosen_id' => $dosen_id]);

        // anggota penelitian mhs
        if($request->mhs)
        {
            foreach ($request->mhs as $mhs) {
                if($mhs) {
                    DB::table('penelitian_mhs')->insert(['data_penelitian_id' => $dp->id, 'mhs_id' => $mhs]);
                }
            }
        }


        // softcopy
        if($request->hasFile('file'))
        {
            $file = $request->file('file');
            $fileName = $file->getClientOriginalName();
            $request->file('file')->move("local/storage/app/penelitian", $fileName);

            DataPenelitian::where('id',$dp->id)->update(['softcopy'=>$fileName]);
        }

        return redirect('dosen/view/dana_penelitian')->with('success','Data Penelitian Berhasil Disimpan');

    }

}