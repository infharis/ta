<?php

namespace App\Http\Controllers\Dosen;

use App\User;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Illuminate\Http\Request;
use App\Models\Dosen\Kemampuan;

class KemampuanController extends Controller
{

    public $dosen_id;

    public function __construct()
    {
        $this->dosen_id=Auth::user()->dosen->id;
    }

    public function index()
    {
        $result=Kemampuan::where('dosen_id',$this->dosen_id)->get();
        return view('dosen.data_master.kemampuan.view',compact('result'));
    }
    public function create()
    {
        return view('dosen.data_master.kemampuan.create');
    }
    public function store(Request $request)
    {
        $km=new Kemampuan();
        $km->dosen_id=$this->dosen_id;
        $km->jenjang=$request->jenjang;
        $km->bidang=$request->bidang;
        $km->perguruan=$request->perguruan;
        $km->negara=$request->negara;
        $km->tahun=$request->tahun;
        $km->save();
        return redirect('dosen/view/kemampuan')->with('success','Data Berhasil Ditambahkan');
    }

    public function destroy(Request $request)
    {
        if(Kemampuan::destroy($request->id))
        {
            echo 1;
        }else {
            echo 0;
        }
    }


}