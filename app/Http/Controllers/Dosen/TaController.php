<?php

namespace App\Http\Controllers\Dosen;

use App\User;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Illuminate\Http\Request;

class TaController extends Controller
{

    public $dosen_id;

    public function __construct()
    {
        $this->dosen_id=Auth::user()->dosen->id;
    }

    public function index()
    {
        $result=DB::table('biodata_mhs')->where('doping1',$this->dosen_id)->count();
        return view('dosen.data_master.pembimbing_ta.view',compact('result'));
    }

}