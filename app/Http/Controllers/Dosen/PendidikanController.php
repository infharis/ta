<?php

namespace App\Http\Controllers\Dosen;

use App\User;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Illuminate\Http\Request;
use App\Models\Dosen\Pendidikan;

class PendidikanController extends Controller
{

    public $dosen_id;

    public function __construct()
    {
        $this->dosen_id=Auth::user()->dosen->id;
    }

    public function index()
    {
        $result=Pendidikan::where('dosen_id',$this->dosen_id)->get();
        return view('dosen.data_master.pendidikan.index',compact('result'));
    }

    public function create()
    {
        $pd=DB::table('pendidikan_dosen')->where('dosen_id',$this->dosen_id)->get();
        $jenjang=DB::table('jenjang')->get();
        return view('dosen.data_master.pendidikan.create',compact('pd','jenjang'));
    }

    public function store(Request $request)
    {
        $pd=new Pendidikan();
        $pd->dosen_id=$this->dosen_id;
        $pd->jenjang_id=$request->jenjang;
        $pd->gelar=$request->gelar;
        $pd->pendidikan=$request->pendidikan;
        $pd->bidang=$request->bidang;
        $pd->save();


        if($request->hasFile('file'))
        {
            $file = $request->file('file');
            $fileName = $file->getClientOriginalName();
            $request->file('file')->move("local/storage/app/ijazah", $fileName);

            Pendidikan::where('id',$pd->id)->update(['softcopy'=>$fileName]);
        }

        return redirect('dosen/view/pendidikan')->with('success','Data Prestasi Dosen Berhasil Ditambah');
    }
    public function update($id,Request $request)
    {
        $pd=Pendidikan::find($id);
        $pd->dosen_id=$this->dosen_id;
        $pd->jenjang_id=$request->jenjang;
        $pd->gelar=$request->gelar;
        $pd->pendidikan=$request->pendidikan;
        $pd->bidang=$request->bidang;
        $pd->save();

        return redirect('dosen/view/pendidikan')->with('success','Data Prestasi Dosen Berhasil Diupdate');
    }

    public function edit($id)
    {
        $result=Pendidikan::find($id);
        $jenjang=DB::table('jenjang')->get();
        return view('dosen.data_master.pendidikan.edit',compact('result','jenjang'));
    }

    public function destroy(Request $request)
    {
        $data=Pendidikan::find($request->id);

        // hapus file
        if($data->softcopy) {
            unlink('local/storage/app/ijazah/' . $data->softcopy);
        }
        if(Pendidikan::destroy($request->id))
        {
            echo 1;
        }else {
            echo 0;
        }
    }


}