<?php

namespace App\Http\Controllers\Dosen;

use App\User;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Illuminate\Http\Request;
use App\Models\Dosen\P2m;


class P2mController extends Controller
{
    public $dosen_id;
    public function __construct()
    {
        $this->dosen_id=Auth::user()->dosen->id;
    }
    public function index()
    {
        $dosen=$this->dosen_id;
        $result=@DB::table('data_p2m')->join('dosen_p2m',function($query)use($dosen){
            $query->on('data_p2m.id','=','dosen_p2m.data_p2m_id')->where('dosen_p2m.dosen_id','=',$dosen);
        })->select('data_p2m.*')->get();
        return view('dosen.data_master.p2m.view',compact('result'));
    }
    public function create()
    {
        $jd=DB::table('jenis_dana')->get();
        $dosen=DB::table('dosen')->where('id','!=',$this->dosen_id)->get();
        $mhs=DB::table('biodata_mhs')->get();
        return view('dosen.data_master.p2m.create',compact('jd','dosen','mhs'));
    }
    public function edit($id)
    {
        $ds=DB::table('dosen')->where('id','!=',Auth::user()->dosen->id)->get();
        $mhs=DB::table('biodata_mhs')->get();
        $jd=DB::table('jenis_dana')->get();

        // data penelitian prev
        $ds_prev=DB::table('dosen_p2m')->where('data_p2m_id',$id)->get();
        $mhs_prev=DB::table('p2m_mhs')->where('data_p2m_id',$id)->get();
        $jd_prev=DB::table('sumber_penelitian_p2m')->where('data_p2m_id',$id)->get();
        $result=P2m::find($id);

        // convert object to array
        $ds_prev = array_map(function ($value) {
            return $value->dosen_id;
        }, $ds_prev);
        $mhs_prev = array_map(function ($value) {
            return $value->mhs_id;
        }, $mhs_prev);
        $jd_prev = array_map(function ($value) {
            return $value->jenis_dana_id;
        }, $jd_prev);

        return view('dosen.data_master.p2m.edit',compact('jd_prev','ds','jd','mhs','result','ds_prev','mhs_prev'));
    }
    public function update($id,Request $request)
    {
        $p2m =P2m::find($id);
        $p2m->judul_p2m = $request->p2m;
        $p2m->jumlah_dana = $request->jumlah_dana;
        $p2m->tahun_p2m = $request->tahun;
        $p2m->save();

        // anggota dosen
        DB::table('dosen_p2m')->where('data_p2m_id',$id)->delete();
        if(is_array($request->dosen) || is_object($request->dosen)){
            foreach ($request->dosen as $value) {
                DB::table('dosen_p2m')->insert(['data_p2m_id' => $id, 'dosen_id' => $value]);
            }
        }
        DB::table('dosen_p2m')->insert(['data_p2m_id' => $id, 'dosen_id' => $this->dosen_id]);

        // anggota mhs
        DB::table('p2m_mhs')->where('data_p2m_id',$id)->delete();
        if(is_array($request->mhs) || is_object($request->mhs)){
            foreach ($request->mhs as $va) {
                DB::table('p2m_mhs')->insert(['data_p2m_id' => $id, 'mhs_id' => $va]);
            }
        }

        // jenis dana
        DB::table('sumber_penelitian_p2m')->where('data_p2m_id',$id)->delete();
        if(is_array($request->jd) || is_object($request->jd)) {
            foreach ($request->jd as $val) {
                DB::table('sumber_penelitian_p2m')->insert(['data_p2m_id' =>$id, 'jenis_dana_id' => $val]);
                //DB::table('sumber_p2m')->insert(['data_p2m_id' => $id, 'jenis_dana_id' => $val]);
            }
        }

        return redirect('dosen/view/dana_p2m')->with('success','Data P2M Berhasil Diupdate');
    }
    public function store(Request $request)
    {
        $p2m = new P2m();
        $p2m->judul_p2m = $request->p2m;
        $p2m->jumlah_dana = $request->jumlah_dana;
        $p2m->tahun_p2m = $request->tahun;
        $p2m->save();

        // anggota dosen
        if(is_array($request->dosen) || is_object($request->dosen)){
            foreach ($request->dosen as $value) {
                DB::table('dosen_p2m')->insert(['data_p2m_id' => $p2m->id, 'dosen_id' => $value]);
            }
        }
        DB::table('dosen_p2m')->insert(['data_p2m_id' => $p2m->id, 'dosen_id' => $this->dosen_id]);

        // anggota mhs
        if(is_array($request->mhs) || is_object($request->mhs)){
            foreach ($request->mhs as $va) {
                DB::table('p2m_mhs')->insert(['data_p2m_id' => $p2m->id, 'mhs_id' => $va]);
            }
        }

        // jenis dana
        foreach($request->jd as $val)
        {
            DB::table('sumber_penelitian_p2m')->insert(['data_p2m_id' =>$p2m->id, 'jenis_dana_id' => $val]);
            //DB::table('sumber_p2m')->insert(['data_p2m_id'=>$p2m->id,'jenis_dana_id'=>$val]);
        }

        if($request->hasFile('file'))
        {
            $file = $request->file('file');
            $fileName = $file->getClientOriginalName();
            $request->file('file')->move("local/storage/app/p2m", $fileName);

            P2m::where('id',$p2m->id)->update(['softcopy'=>$fileName]);
        }

        return redirect('dosen/view/dana_p2m')->with('success','Data P2M Berhasil Ditambahkan');
    }

    public function destroy(Request $request)
    {
        $single=P2m::find($request->id);
        if($single->softcopy)
        {
            // hapus file p2m
            unlink('local/storage/app/p2m/'.$single->softcopy);
        }
        // hapus sumber dana p2m
       // DB::table('sumber_p2m')->where(['data_p2m_id'=>$single->id])->delete();
        DB::table('sumber_penelitian_p2m')->where(['data_p2m_id'=>$single->id])->delete();
        // keterlibatan dosen dan mhs
        DB::table('dosen_p2m')->where(['data_p2m_id'=>$single->id])->delete();
        DB::table('p2m_mhs')->where(['data_p2m_id'=>$single->id])->delete();

        if(P2m::destroy($single->id))
        {
            echo 1;
        }else {
            echo 0;
        }


    }


}