<?php

namespace App\Http\Controllers\Dosen;

use App\User;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Illuminate\Http\Request;
use App\Models\Dosen\Artikel;

class ArtikelController extends Controller
{

    public $dosen_id;

    public function __construct()
    {
        $this->dosen_id=Auth::user()->dosen->id;
    }

    public function index()
    {
        $dosen=$this->dosen_id;
        $result=@DB::table('artikel')->join('artikel_dosen',function($query)use($dosen){
            $query->on('artikel.id','=','artikel_dosen.artikel_id')->where('artikel_dosen.dosen_id','=',$dosen);
        })->select('artikel.*')->get();
        return view('dosen.data_master.artikel_dosen.view',compact('result'));
    }
    public function create()
    {
        $dosen=DB::table('dosen')->where('id','!=',$this->dosen_id)->get();
        return view('dosen.data_master.artikel_dosen.create',compact('dosen'));
    }

    public function store(Request $request)
    {
        $art=new Artikel();
        $art->judul=$request->judul;
        $art->dihasilkan=$request->dihasilkan;
        $art->tahun=$request->tahun;
        $art->tingkat=$request->tingkat;
        $art->jurnal=$request->jurnal;
        $art->seminar=$request->seminar;
        $art->buku=$request->buku;
        $art->save();

        // anggota
        if($request->dosen) {
            foreach ($request->dosen as $value) {
                DB::table('artikel_dosen')->insert(['artikel_id' => $art->id, 'dosen_id' => $value]);
            }
        }
        DB::table('artikel_dosen')->insert(['artikel_id'=>$art->id,'dosen_id'=>$this->dosen_id]);

        if($request->hasFile('file'))
        {
            $file = $request->file('file');
            $fileName = $file->getClientOriginalName();
            $request->file('file')->move("local/storage/app/artikel", $fileName);

            Artikel::where('id',$art->id)->update(['softcopy'=>$fileName]);
        }


        return redirect('dosen/view/artikel_dosen')->with('success','Data Artikel Berhasil Ditambahkan');
    }

    public function destroy(Request $request)
    {
        $data=Artikel::find($request->id);

        // hapus file
        if($data->softcopy) {
            unlink('local/storage/app/artikel/' . $data->softcopy);
        }
        // hapus anggota artikel
        @DB::table('artikel_dosen')->where('artikel_id','=',$data->id)->delete();

        if(Artikel::destroy($request->id))
        {
            echo 1;
        }else {
            echo 0;
        }
    }

}