<?php

namespace App\Http\Controllers\Dosen;

use App\User;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Illuminate\Http\Request;
use App\Models\Dosen\Prestasi;

class PrestasiController extends Controller
{

    public $dosen_id;

    public function __construct()
    {
        $this->dosen_id=Auth::user()->dosen->id;
    }

    public function index()
    {
        $result=Prestasi::where('dosen_id',$this->dosen_id)->get();
        return view('dosen.data_master.prestasi.view',compact('result'));
    }
    public function create()
    {
        return view('dosen.data_master.prestasi.create');
    }
    public function store(Request $request)
    {
        $ps=new Prestasi();
        $ps->dosen_id=$this->dosen_id;
        $ps->judul_prestasi=$request->prestasi;
        $ps->waktu=$request->waktu;
        $ps->tingkat=$request->tingkat;
        $ps->save();

        if($request->hasFile('file'))
        {
            $file = $request->file('file');
            $fileName = $file->getClientOriginalName();
            $request->file('file')->move("local/storage/app/prestasi_dosen", $fileName);

            Prestasi::where('id',$ps->id)->update(['softcopy'=>$fileName]);
        }

        return redirect('dosen/view/prestasi')->with('success','Data Prestasi Dosen Berhasil Ditambah');
    }
    public function update($id,Request $request)
    {
        $ps=Prestasi::find($id);
        $ps->dosen_id=$this->dosen_id;
        $ps->judul_prestasi=$request->prestasi;
        $ps->waktu=$request->waktu;
        $ps->tingkat=$request->tingkat;
        $ps->save();

        return redirect('dosen/view/prestasi')->with('success','Data Prestasi Dosen Berhasil Diupdate');
    }

    public function edit($id)
    {
        $result=Prestasi::find($id);
        return view('dosen.data_master.prestasi.edit',compact('result'));
    }
    public function destroy(Request $request)
    {
        $data=Prestasi::find($request->id);

        // hapus file
        if($data->softcopy) {
            unlink('local/storage/app/prestasi_dosen/' . $data->softcopy);
        }
        if(Prestasi::destroy($request->id))
        {
            echo 1;
        }else {
            echo 0;
        }
    }
}