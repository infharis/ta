<?php

namespace App\Http\Controllers\Dosen;

use App\User;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;
use App\Models\Admin\Dosen;
use Auth;
use App\Models\Admin\PendidikanDosen;
use App\Models\Admin\DataPenelitian;
use App\Models\Admin\P2m;

class DosenController extends Controller
{

    public $dosen=array('nama_dosen','bidang','nidn','nip','tempat','tgl_lahir','jabatan_id','pendidikan_id');
    public $akdmik=array('pendidikan','bidang','softcopy');
    public $penelitian=array('judul_penelitian','jumlah_dana');
    public $p2m=array('judul_p2m','jumlah_dana');
    public $p_id,$p_d,$p2md,$sks_id,$kg_id,$prestasi_id,$or_id;

    public $sks=array('ps_lain','pt_lain','sks_penelitian','sks_p2m','m_pt_sendiri','m_pt_lain');
    public $kegiatan=array('tempat','waktu','sebagai');
    public $prestasi=array('waktu','tingkat');
    public $organisasi=array('mulai','akhir','tingkat');

    public function store(Request $request)
    {
            // biodata dosen
          $dosen_id=Auth::user()->dosen->id;

        // organisasi dosen
        $i=0;
        $index=0;
        if($request->organisasi) {
            foreach ($request->organisasi as $or) {
                if ($i == 0 && $or['value']) {
                    $this->or_id = DB::table('organisasi')->insertGetId(['dosen_id' => $dosen_id, 'nama_organisasi' => $or['value']]);
                } elseif ($or['value']) {
                    DB::table('organisasi')->where('id', $this->or_id)->update([$this->organisasi[$index] => $or['value']]);
                    $index++;
                }
                $i++;
            }
        }

          // prestasi dosen
        $i=0;
        $index=0;
        if($request->prestasi) {
            foreach ($request->prestasi as $ps) {
                if ($i == 0 && $ps['value']) {
                    $this->prestasi_id = DB::table('prestasi_dosen')->insertGetId(['dosen_id' => $dosen_id, 'judul_prestasi' => $ps['value']]);
                } elseif ($ps['value']) {
                    DB::table('prestasi_dosen')->where('id', $this->prestasi_id)->update([$this->prestasi[$index] => $ps['value']]);
                    $index++;
                }
                $i++;
            }
        }

        /// kegiatan
            $i=0;
            $index=0;
            if(isset($request->kegiatan)) {
                foreach ($request->kegiatan as $kg) {
                    if ($i == 0 && $kg['value']) {
                        $this->kg_id = DB::table('kegiatan')->insertGetId(['dosen_id' => $dosen_id, 'jenis_kegiatan' => $kg['value']]);
                    } elseif ($kg['value']) {
                        DB::table('kegiatan')->where('id', $this->kg_id)->update([$this->kegiatan[$index] => $kg['value']]);
                        $index++;
                    }
                    $i++;
                }
            }

          //  aktivitasi dosen
          $i=0;
          $index=0;
          if($request->sks) {
              foreach ($request->sks as $v) {
                  if ($i == 0 && $v['value']) {
                      $this->sks_id = DB::table('aktivitas')->insertGetId(['ps_sendiri' => $v['value'], 'dosen_id' => $dosen_id]);
                  } elseif ($v['value']) {
                      DB::table('aktivitas')->where('id', $this->sks_id)->update([$this->sks[$index] => $v['value']]);
                      $index++;
                  }
                  $i++;
              }
          }

          // biodata dosen
          $i=0;
              foreach ($request->dosen as $value) {
                  Dosen::where('id', $dosen_id)->update([$this->dosen[$i] => $value['value']]);
                  $i++;
              }

          // pendidikan dosen
          $l=0;
          if($request->pendidikan) {
              foreach ($request->pendidikan as $value1) {
                  if ($l == 0 && $value1['value']) {
                      $pd = new PendidikanDosen();
                      $pd->gelar = $value1['value'];
                      $pd->dosen_id = $dosen_id;
                      $pd->save();

                      $this->p_id = $pd->id;
                      $l++;
                  } elseif ($l == 1 && $value1['value']) {
                      PendidikanDosen::where('id', $this->p_id)->update(['pendidikan' => $value1['value']]);
                      $l++;
                  } elseif ($l == 2 && $value1['value']) {
                      PendidikanDosen::where('id', $this->p_id)->update(['bidang' => $value1['value']]);
                      $l = 0;
                  }
              }
          }
          // penelitian
            $l=0;$index=0;
            if($request->penelitian) {
                foreach ($request->penelitian as $val) {
                    if ($l == 0 && $val['value']) {
                        $pd = new DataPenelitian();
                        $pd->tahun_penelitian = $val['value'];
                        $pd->save();

                        $this->p_d = $pd->id;
                        $l++;
                    } elseif ($val['value']) {
                        DataPenelitian::where('id', $this->p_d)->update([$this->penelitian[$index] => $val['value']]);
                        $index++;
                    }
                }
            }
            // sumber dana penelitian
            if($request->jd_penelitian) {
                foreach ($request->jd_penelitian as $jd_p) {
                    if ($jd_p['value']) {
                        DB::table('sumber_penelitian')->insert(['data_penelitian_id' => $this->p_d,
                            'jenis_dana_id' => $jd_p['value']]);
                    }
                }
            }
            // anggota penelitian
            if($request->anggota_p) {
                $self=0; // jika nilai 1 dosen penelitian tsb juga trlbat
                foreach ($request->anggota_p as $ap) {
                    if ($ap['value']) {
                        DB::table('penelitian_dosen')->insert(['data_penelitian_id' => $this->p_d,
                            'dosen_id' => $ap['value']]);
                        $self=1;
                    }
                }
                // tambahkan dosen current
                if($self==1)
                {
                    DB::table('penelitian_dosen')->insert(['data_penelitian_id' => $this->p_d,
                        'dosen_id' => $dosen_id]);
                }
            }


        // p2m
        $l=0;$index=0;
        foreach($request->p2m as $val1)
        {
            if($l==0 && $val1['value'])
            {
                $pd=new P2m();
                $pd->tahun_p2m=$val1['value'];
                $pd->save();

                $this->p2md=$pd->id;
                $l++;
            }elseif($val1['value']) {
                P2m::where('id',$this->p2md)->update([$this->p2m[$index]=>$val1['value']]);
                $index++;
            }
        }
        // sumber dana p2m
        if($request->jd_p2m) {
            foreach ($request->jd_p2m as $jd_p2m) {
                if ($jd_p2m['value']) {
                    DB::table('sumber_p2m')->insert(['data_p2m_id' => $this->p2md,
                        'jenis_dana_id' => $jd_p2m['value']]);
                }
            }
        }
        // anggota p2m
        if($request->anggota_p2m) {
            $self_p2m=0;
            foreach ($request->anggota_p2m as $ap2m) {
                if ($ap2m['value']) {
                    DB::table('dosen_p2m')->insert(['data_penelitian_id' => $this->p2md,
                        'dosen_id' => $ap2m['value']]);
                    $self_p2m=1;
                }
            }
            if($self_p2m==1)
            {
                DB::table('dosen_p2m')->insert(['data_penelitian_id' => $this->p2md,
                    'dosen_id' => $dosen_id]);
            }
        }
        echo 'Data Berhasil Disimpan';
    }

}