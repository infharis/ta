<?php

namespace App\Http\Controllers\Dosen;

use App\User;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Illuminate\Http\Request;
use App\Models\Dosen\DataAktivitas;

class DataAktivitasController extends Controller
{

    public $dosen_id;

    public function __construct()
    {
        $this->dosen_id=Auth::user()->dosen->id;
    }

    public function index()
    {
        $result=DB::table('data_aktivitas_dosen')->where('dosen_id',$this->dosen_id)->get();
        return view('dosen.data_master.data_aktivitas.view',compact('result'));
    }

    public function create()
    {
        $result=DB::table('kurikulum')->get();
        return view('dosen.data_master.data_aktivitas.create',compact('result'));
    }

    public function store(Request $request)
    {
        DB::table('data_aktivitas_dosen')->insert([
            'dosen_id'=>$this->dosen_id,
            'mk_id'=>$request->mk_id,
            'jumlah_kelas'=>$request->jumlah_kelas,
            'jumlah_pertemuan'=>$request->jumlah_pertemuan,
            'jumlah_dilaksanakan'=>$request->jumlah_dilaksanakan
            ]);

        return redirect('dosen/view/data_aktivitas_dosen_tetap')->with('success','Data Berhasil Ditambahkan');
    }
    public function update($id,Request $request)
    {
        DB::table('data_aktivitas_dosen')->where('id',$id)->update([
            'dosen_id'=>$this->dosen_id,
            'mk_id'=>$request->mk_id,
            'jumlah_kelas'=>$request->jumlah_kelas,
            'jumlah_pertemuan'=>$request->jumlah_pertemuan,
            'jumlah_dilaksanakan'=>$request->jumlah_dilaksanakan
            ]);

        return redirect('dosen/view/data_aktivitas_dosen_tetap')->with('success','Data Berhasil Diupdate');
    }
    public function edit($id)
    {
        $kur=DB::table('kurikulum')->get();
        $result=DB::table('data_aktivitas_dosen')->where('id',$id)->first();
        return view('dosen.data_master.data_aktivitas.edit',compact('kur','result'));
    }
    public function destroy(Request $request)
    {
        if(DataAktivitas::destroy($request->id))
        {
            echo 1;
        }else {
            echo 0;
        }
    }


}