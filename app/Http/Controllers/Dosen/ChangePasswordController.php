<?php

namespace App\Http\Controllers\Dosen;

use App\User;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Illuminate\Http\Request;
use Hash;

class ChangePasswordController extends Controller
{

    public $dosen_id;

    public function __construct()
    {
        $this->dosen_id=Auth::user()->dosen->id;
    }
    public function index()
    {
        return view('dosen.data_master.change_password');
    }
    public function change_password(Request $request)
    {
        $this->validate($request,['old'=>'required|min:6','password'=>'required|confirmed|min:6']);
        if(Hash::check($request->old,Auth::user()->password))
        {
            //$this->resetPassword(Auth::user(),$request->password);
            User::where('id','=',Auth::user()->id)->update(['password'=>Hash::make($request->password)]);
            return redirect('dosen')->with('success','Perubahan Password Berhasil Dibuat');

        }else {
            return redirect()->back()->with('error','Password Salah');
        }
    }


}