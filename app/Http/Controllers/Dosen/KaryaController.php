<?php

namespace App\Http\Controllers\Dosen;

use App\User;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Illuminate\Http\Request;
use App\Models\Dosen\Karya;

class KaryaController extends Controller
{

    public $dosen_id;

    public function __construct()
    {
        $this->dosen_id=Auth::user()->dosen->id;
    }

    public function index()
    {
        $result=Karya::where('dosen_id',$this->dosen_id)->get();
        return view('dosen.data_master.karya.view',compact('result'));
    }
    public function create()
    {
        return view('dosen.data_master.karya.create');
    }
    public function store(Request $request)
    {
        $kr=new Karya();
        $kr->dosen_id=$this->dosen_id;
        $kr->karya=$request->karya;
        $kr->no_hki=$request->no_hki;
        $kr->save();

        if($request->hasFile('file'))
        {
            $file = $request->file('file');
            $fileName = $file->getClientOriginalName();
            $request->file('file')->move("local/storage/app/karya", $fileName);

            Karya::where('id',$kr->id)->update(['softcopy'=>$fileName]);
        }

        return redirect('dosen/view/karya_paten')->with('success','Data Berhasil Ditambahkan');
    }


    public function destroy(Request $request)
    {
        $data=Karya::find($request->id);

        // hapus file
        if($data->softcopy) {
            unlink('local/storage/app/karya/' . $data->softcopy);
        }
        if(Karya::destroy($request->id))
        {
            echo 1;
        }else {
            echo 0;
        }
    }

}