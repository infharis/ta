<?php

namespace App\Http\Controllers\Dosen;

use Illuminate\Http\Request;
use App\User;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use App\Models\Dosen\Aktivitas;

class AktivitasController extends Controller
{

    public $dosen_id;
    public function __construct()
    {
        $this->dosen_id=Auth::user()->dosen->id;
    }

    public function index()
    {
        $result=Aktivitas::where('dosen_id','=',$this->dosen_id)->first();
        return view('dosen.data_master.aktivitas_dosen_tetap.view',compact('result'));
    }

    public function create()
    {

        return view('dosen.data_master.aktivitas_dosen_tetap.create');
    }

    public function store(Request $request)
    {
        $ak=new Aktivitas();
        $ak->dosen_id=$this->dosen_id;
        $ak->ps_sendiri=$request->ps_sendiri;
        $ak->ps_lain=$request->ps_lain;
        $ak->pt_lain=$request->pt_lain;
        $ak->sks_penelitian=$request->sks_penelitian;
        $ak->sks_p2m=$request->sks_p2m;
        $ak->m_pt_sendiri=$request->m_pt_sendiri;
        $ak->m_pt_lain=$request->m_pt_lain;
        $ak->save();

        return redirect('dosen/view/aktivitas_dosen_tetap')->with('success','Data Berhasil Ditambahkan');
    }
    public function update($id,Request $request)
    {
        $ak=Aktivitas::find($id);
        $ak->dosen_id=$this->dosen_id;
        $ak->ps_sendiri=$request->ps_sendiri;
        $ak->ps_lain=$request->ps_lain;
        $ak->pt_lain=$request->pt_lain;
        $ak->sks_penelitian=$request->sks_penelitian;
        $ak->sks_p2m=$request->sks_p2m;
        $ak->m_pt_sendiri=$request->m_pt_sendiri;
        $ak->m_pt_lain=$request->m_pt_lain;
        $ak->save();

        return redirect('dosen/view/aktivitas_dosen_tetap')->with('success','Data Berhasil Diupdate');
    }
    public function edit($id)
    {
        $result=Aktivitas::find($id);
        return view('dosen.data_master.aktivitas_dosen_tetap.update',compact('result'));
    }
    public function destroy(Request $request)
    {
        if(Aktivitas::destroy($request->id))
        {
            echo 1;
        }else {
            echo 0;
        }
    }

}