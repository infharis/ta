<?php

namespace App\Http\Controllers\Dosen;

use App\User;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Illuminate\Http\Request;
use App\Models\Dosen\Prestasi;

class BiodataController extends Controller
{

    public $dosen_id;

    public function __construct()
    {
        $this->dosen_id=Auth::user()->dosen->id;
    }

    public function index()
    {
        $jb=DB::table('jabatan_fungsional')->get();
        $pt=DB::table('daftar_pendidikan')->get();
        return view('dosen.biodata_diri',compact('jb','pt'));
    }
    public function update(Request $request)
    {
        DB::table('dosen')->where('id',$this->dosen_id)->update(['nama_dosen'=>$request->nama,
            'bidang'=>$request->bidang,'nidn'=>$request->nidn,'nip'=>$request->nip,
            'jabatan_id'=>$request->jabatan,'pendidikan_id'=>$request->pendidikan,
            'tempat'=>$request->tempat_lahir,'tgl_lahir'=>$request->tgl_lahir,
            'sertifikasi'=>$request->sertifikasi
        ]);
        return redirect('dosen')->with('success','Data Berhasil Diupdate');
    }


}