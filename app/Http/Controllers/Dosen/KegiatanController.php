<?php

namespace App\Http\Controllers\Dosen;

use App\User;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Illuminate\Http\Request;
use App\Models\Dosen\Kegiatan;

class KegiatanController extends Controller
{
    public $dosen_id;
    public function __construct()
    {
        $this->dosen_id=Auth::user()->dosen->id;
    }

    public function index()
    {

        $result=DB::table('kegiatan')->where('dosen_id',$this->dosen_id)->get();
        return view('dosen.data_master.kegiatan_dosen_tetap.view',compact('result'));
    }
    public function create()
    {
        return view('dosen.data_master.kegiatan_dosen_tetap.create');
    }
    public function store(Request $request)
    {
        $keg=new Kegiatan();
        $keg->dosen_id=$this->dosen_id;
        $keg->jenis_kegiatan=$request->jenis_kegiatan;
        $keg->tempat=$request->tempat;
        $keg->waktu=$request->waktu;
        $keg->sebagai=$request->sebagai;
        $keg->save();

        if($request->hasFile('file'))
        {
            $file = $request->file('file');
            $fileName = $file->getClientOriginalName();
            $request->file('file')->move("local/storage/app/kegiatan", $fileName);

            Kegiatan::where('id',$keg->id)->update(['softcopy'=>$fileName]);
        }
        return redirect('dosen/view/kegiatan_dosen_tetap')->with('success','Data Kegiatan Berhasil Disimpan');
    }
    public function update($id,Request $request)
    {
        $keg=Kegiatan::find($id);
        $keg->dosen_id=$this->dosen_id;
        $keg->jenis_kegiatan=$request->jenis_kegiatan;
        $keg->tempat=$request->tempat;
        $keg->waktu=$request->waktu;
        $keg->sebagai=$request->sebagai;
        $keg->save();


        return redirect('dosen/view/kegiatan_dosen_tetap')->with('success','Data Kegiatan Berhasil Diupdate');
    }

    public function edit($id)
    {
        $result=Kegiatan::find($id);
        return view('dosen.data_master.kegiatan_dosen_tetap.edit',compact('result'));
    }
    public function destroy(Request $request)
    {
        $data=Kegiatan::find($request->id);

        // hapus file
        if($data->softcopy) {
            unlink('local/storage/app/kegiatan/' . $data->softcopy);
        }
        if(Kegiatan::destroy($request->id))
        {
            echo 1;
        }else {
            echo 0;
        }
    }

}