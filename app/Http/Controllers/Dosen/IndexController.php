<?php

namespace App\Http\Controllers\Dosen;

use App\User;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Illuminate\Http\Request;

class IndexController extends Controller
{

    public function create()
    {
        $jb=DB::table('jabatan_fungsional')->get();
        $pt=DB::table('daftar_pendidikan')->get();
        $jd=DB::table('jenis_dana')->get();
        $ds=DB::table('dosen')->where('id','!=',Auth::user()->dosen->id)->get();
        return view('dosen.data_master.create',compact('jb','pt','jd','ds'));
    }

    public function login_as_admin(Request $request)
    {
        $user_id=$request->session()->get('user_prev');
        Auth::loginUsingId($user_id, true);
        // remove all session
        $request->session()->flush();


        return redirect('admin');
    }
}