<?php
/**
 * Created by PhpStorm.
 * User: infharis
 * Date: 10/9/17
 * Time: 9:15 AM
 */

namespace App\Http\Controllers\Dosen;

use App\User;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Illuminate\Http\Request;


class PaController extends Controller
{
    public $dosen_id;
    public function __construct()
    {
        $this->dosen_id=Auth::user()->dosen->id;
    }

    public function index()
    {
        $result=DB::table('biodata_mhs')->where('dosen_id',$this->dosen_id)->count();
        return view('dosen.data_master.pembimbing_akademik.view',compact('result'));
    }


}