<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;
use App\Models\Admin\JumlahReguler;


class JumlahRegulerController extends Controller
{

    public function index()
    {
        $result=JumlahReguler::all();
        return view('admin.mhs.jumlah_reguler.view',compact('result'));
    }
    public function create()
    {

        return view('admin.mhs.jumlah_reguler.create');
    }
    public function store(Request $request)
    {
        $jreg=new JumlahReguler();
        $jreg->tahun_masuk=$request->tahun_masuk;
        $jreg->ts6=$request->ts6;
        $jreg->ts5=$request->ts5;
        $jreg->ts4=$request->ts4;
        $jreg->ts3=$request->ts3;
        $jreg->ts2=$request->ts2;
        $jreg->ts1=$request->ts1;
        $jreg->save();

        return redirect('mhs/view/jumlah_reguler')->with('success','Data Berhasil Ditambahkan');
    }
    public function edit($id)
    {
        $result=JumlahReguler::find($id);
        return view('admin.mhs.jumlah_reguler.edit',compact('result'));
    }
    public function update($id,Request $request)
    {
        $jreg=JumlahReguler::find($id);
        $jreg->tahun_masuk=$request->tahun_masuk;
        $jreg->ts6=$request->ts6;
        $jreg->ts5=$request->ts5;
        $jreg->ts4=$request->ts4;
        $jreg->ts3=$request->ts3;
        $jreg->ts2=$request->ts2;
        $jreg->ts1=$request->ts1;
        $jreg->save();

        return redirect('mhs/view/jumlah_reguler')->with('success','Data Berhasil Diupdate');
    }
    public function destroy(Request $request)
    {
        if(JumlahReguler::destroy($request->id))
        {
            echo 1;
        }else {
            echo 0;
        }
    }

}