<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;
use App\Models\Admin\Aksesibilitas;


class AksesibilitasController extends Controller
{

    public function index()
    {
        $result=Aksesibilitas::all();
        return view('admin.aksesibilitas.view',compact('result'));
    }

    public function create()
    {
        return view('admin.aksesibilitas.create');
    }

    public function store(Request $request)
    {

        $ak=new Aksesibilitas();
        $ak->jenis_data=$request->jenis_data;
        $ak->manual=$request->manual;
        $ak->komputer=$request->komputer;
        $ak->lan=$request->lan;
        $ak->wan=$request->wan;
        $ak->save();

        return redirect('aksesibilitas/view')->with('success','Data Berhasil Ditambahkan');
    }
    public function update($id,Request $request)
    {

        $ak=Aksesibilitas::find($id);
        $ak->jenis_data=$request->jenis_data;
        $ak->manual=$request->manual;
        $ak->komputer=$request->komputer;
        $ak->lan=$request->lan;
        $ak->wan=$request->wan;
        $ak->save();

        return redirect('aksesibilitas/view')->with('success','Data Berhasil Diupdate');
    }

    public function edit($id)
    {
        $result=Aksesibilitas::find($id);
        return view('admin.aksesibilitas.edit',compact('result'));
    }
    public function destroy(Request $request)
    {
        if(Aksesibilitas::destroy($request->id))
        {
            echo 1;
        }else {
            echo 0;
        }
    }

}