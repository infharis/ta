<?php

namespace App\Http\Controllers\Admin\Master_Mhs;

use Illuminate\Http\Request;
use App\User;
use App\Http\Controllers\Controller;
use DB;
use App\Models\Admin\Biodata;
use App\Models\Admin\Semester;
use App\Models\Admin\Ipk;

class IpkController extends Controller
{
    public function index()
    {
        $result=Ipk::all();
        return view('admin.data_master_mhs.akademik.view',compact('result'));
    }

    public  function create()
    {
        $mhs=Biodata::all();
        $sm=Semester::all();
        return view('admin.data_master_mhs.akademik.create',compact('mhs','sm'));
    }

    public function store(Request $request)
    {
        $ipk=new Ipk();
        $ipk->semester_id=$request->semester;
        $ipk->mhs_id=$request->mhs_id;
        $ipk->ipk=$request->ipk;
        $ipk->save();
        return redirect('data/mhs/akademik')->with('success','Ipk Mahasiswa Berhasil Ditambah');
    }

    public function update($id,Request $request)
    {
        $ipk=Ipk::find($id);
        $ipk->semester_id=$request->semester;
        $ipk->ipk=$request->ipk;
        $ipk->save();
        return redirect('data/mhs/akademik')->with('success','Ipk Mahasiswa Berhasil Diupdate');
    }
    public function edit($id)
    {
        $result=Ipk::find($id);
        $sm=Semester::all();
        return view('admin.data_master_mhs.akademik.edit',compact('result','sm'));
    }
    public function destroy(Request $request)
    {
        if(Ipk::destroy($request->id))
        {
            echo 1;
        }else {
            echo 0;
        }
    }

}