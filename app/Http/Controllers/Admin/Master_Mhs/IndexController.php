<?php

namespace App\Http\Controllers\Admin\Master_Mhs;

use App\User;
use App\Http\Controllers\Controller;
use DB;

class IndexController extends Controller
{

    public function index()
    {
        return view('admin.data_master_mhs.view');
    }
    public function penelitian_mhs()
    {
        $result=@DB::table('data_penelitian')->join('penelitian_mhs',function($join){
            $join->on('data_penelitian.id','=','penelitian_mhs.data_penelitian_id');
        })->select('data_penelitian.*')->get();
        return view('admin.data_master_mhs.penelitian',compact('result'));
    }


}