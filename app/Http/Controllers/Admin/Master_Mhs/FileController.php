<?php
/**
 * Created by PhpStorm.
 * User: infharis
 * Date: 11/01/18
 * Time: 20.29
 */

namespace App\Http\Controllers\Admin\Master_Mhs;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Excel;
use App\Models\Admin\Biodata;

class FileController extends Controller
{
    public $tahun_prefix='20';
    public function index()
    {
        return view('admin.data_master_mhs.biodata.biodata_mhs_file');
    }
    public function upload_data_mhs(Request $request)
    {
        $this->validate($request,['data_mhs'=>'required|mimes:xls,xlsx']);
        $name_file=$request->file('data_mhs');
        $result=Excel::load($name_file)->get();
        foreach ($result as $value)
        {
            $bio=@Biodata::firstOrNew(['nim'=>$value->npm]);
            $tm=substr($value->npm,0);
            $bio->nama=$value->nama_mahasiswa ? $value->nama_mahasiswa : ' ';
            $bio->tahun_masuk=$this->tahun_prefix.$tm;
            $bio->save();
        }
        return redirect()->back()->with('success',' Data Berhasil Disimpan');
    }

}