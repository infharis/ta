<?php
/**
 * Created by PhpStorm.
 * User: infharis
 * Date: 15/01/18
 * Time: 12.05
 */

namespace App\Http\Controllers\Admin\Master_Mhs;

use DB;
use App\Http\Controllers\Controller;
use App\Models\Admin\Biodata;
use Illuminate\Http\Request;
//use Symfony\Component\Yaml\Tests\B;

class AlumniController extends Controller
{
    public function index()
    {
        $result=Biodata::where('sidang','!=','0000-00-00')->get();
        $list_kerja=DB::table('daftar_waktu_pekerjaan')->get();
        $daftar_tahun=Biodata::where('tahun_masuk','!=','NULL')->distinct()->orderBy('tahun_masuk')->get(['tahun_masuk']);
        return view('admin.data_master_mhs.alumni.index',compact('result','list_kerja','daftar_tahun'));
    }
    public function edit($nim)
    {
        $result=Biodata::where('nim',$nim)->first();
        $pekerjaan=DB::table('daftar_waktu_pekerjaan')->get();
        return view('admin.data_master_mhs.alumni.edit',compact('result','pekerjaan'));
    }
    public function update($id,Request $request)
    {
        $mhs=Biodata::find($id);
        $mhs->toefl=$request->toefl;
        $mhs->email=$request->email;
        $mhs->dapat_pekerjaan=$request->waktu;
        $mhs->gaji=$request->gaji;
        $mhs->pekerjaan=$request->pekerjaan;
        $mhs->instansi=$request->instansi;
        $mhs->save();
        return redirect('data/mhs/data_alumni')->with('success','Data Berhasil Diupdate');
    }

}