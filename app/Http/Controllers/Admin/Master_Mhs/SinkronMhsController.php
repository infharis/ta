<?php
/**
 * Created by PhpStorm.
 * User: infharis
 * Date: 12/01/18
 * Time: 19.16
 */

namespace App\Http\Controllers\Admin\Master_Mhs;


use App\Http\Controllers\Controller;
use App\Models\Admin\Biodata;
use App\Models\Admin\Dosen;
use App\Models\Admin\History;
use App\Models\Admin\PrestasiMhs;
use DB;
use Illuminate\Http\Request;

class SinkronMhsController extends Controller
{
    public $url='http://ws.unsyiah.ac.id/webservice/webservice1/dosen/cdosen/mhswali/npm/';
    public $perwalian_url="http://ws.unsyiah.ac.id/webservice/dosen/cdosen/listmhsdoswal/nip/";
    public $key="/key/iuhs768hkj";

    public function get_all_mhs(Request $request)
    {

            $tahun=substr($request->tahun,2);

            for($i=$request->nim_awal;$i<=$request->nim_akhir;$i++) {
                if ($i < 10) {
                    if($request->tahun=='2010') {
                        $nim = $tahun . '0810702000' . $i;
                    }else {
                        $nim = $tahun . '0810701000' . $i;
                    }
                } else {
                    if($request->tahun=='2010'){
                        $nim = $tahun . '081070200' . $i;
                    }else {
                        $nim = $tahun . '081070100' . $i;
                    }
                }
                $url = $this->url . $nim . $this->key;
                $result = simplexml_load_file($url);

                $daerah = DB::table('kab')->where('nama_kab', $result->asal_daerah)->first();
                $bd = @Biodata::firstOrNew(['nim' => $result->npm]);
                $bd->nim = $result->npm;
                $bd->nama = $result->nama;
                $bd->tahun_masuk = $result->tahun_masuk;
                $bd->jenis_kelamin = $result->jenis_kelamin;
                $bd->asal_sekolah = $result->asal_sekolah;
                $bd->prov_id = @$daerah->prov_id;
                $bd->kab_id = @$daerah->id;
                $bd->ipk = $result->ipk;
                $bd->save();

            }

            // update last record
            $history=new History();
            $history->model="mhs";
            $history->save();
           return redirect()->back()->with('success', 'Sinkronisasi Data Mahasiswa Berhasil!!!');

    }

}