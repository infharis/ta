<?php

namespace App\Http\Controllers\Admin\Master_Mhs;

use App\Models\Dosen\Prestasi;
use Illuminate\Http\Request;
use App\User;
use App\Http\Controllers\Controller;
use DB;
use App\Models\Admin\Biodata;
use App\Models\Admin\PrestasiMhs;

class PrestasiController extends Controller
{

    public function index()
    {
        $result=PrestasiMhs::all();
        $count=DB::table('prestasi_mhs')->selectRaw('count(IF(tingkat="L",1,NULL)) lokal')->
              selectRaw('count(IF(tingkat="R",1,NULL)) regional')->
               selectRaw('count(IF(tingkat="N",1,NULL)) nasional')->
              selectRaw('count(IF(tingkat="I",1,NULL)) internasional')->
        get();
        $tp=PrestasiMhs::distinct()->orderBy('tahun')->get(['tahun']);
        return view('admin.data_master_mhs.prestasi.view',compact('result','count','tp'));
    }
    public function create()
    {
        $mhs=Biodata::all();
        return view('admin.data_master_mhs.prestasi.create',compact('mhs'));
    }
    public function store(Request $request)
    {
        $this->validate($request,['tingkat'=>'required','tahun:']);
        $pres=new PrestasiMhs();
        $pres->mhs_id=$request->mhs_id;
        $pres->nama_kegiatan=$request->kegiatan;
        $pres->prestasi=$request->prestasi;
        $pres->tahun=$request->tahun;
        $pres->tingkat=$request->tingkat;
        $pres->save();

        if($request->file('file')) {
            $file = $request->file('file');
            $fileName = $file->getClientOriginalName();
            $request->file('file')->move("local/storage/app/prestasi_mhs", $fileName);

            PrestasiMhs::where('id',$pres->id)->update(['softcopy'=>$fileName]);
        }

        return redirect('data/mhs/prestasi_mhs')->with('success','Prestasi Mahasiswa Berhasil Ditambah');
    }
    public function update($id,Request $request)
    {
        $pres=PrestasiMhs::find($id);
        $pres->nama_kegiatan=$request->kegiatan;
        $pres->prestasi=$request->prestasi;
        $pres->tahun=$request->tahun;
        $pres->tingkat=$request->tingkat;
        $pres->save();

        return redirect('data/mhs/prestasi_mhs')->with('success','Prestasi Mahasiswa Berhasil Diupdate');
    }
    public function edit($id)
    {
        $result=PrestasiMhs::find($id);
        return view('admin.data_master_mhs.prestasi.edit',compact('result'));

    }
    public function destroy(Request $request)
    {
        $data=PrestasiMhs::find($request->id);

        // hapus file
        if($data->softcopy) {
            unlink('local/storage/app/prestasi_mhs/' . $data->softcopy);
        }
        if(PrestasiMhs::destroy($request->id))
        {
            echo 1;
        }else {
            echo 0;
        }
    }

}