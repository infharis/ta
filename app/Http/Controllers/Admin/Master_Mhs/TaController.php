<?php

namespace App\Http\Controllers\Admin\Master_Mhs;

use Illuminate\Http\Request;
use App\User;
use App\Http\Controllers\Controller;
use DB;
use App\Models\Admin\Biodata;
use App\Models\Admin\Semester;

class TaController extends Controller
{
    public function index()
    {
        $result=Biodata::all();
        return view('admin.data_master_mhs.ta.view',compact('result'));
    }

    public  function create()
    {
        $mhs=Biodata::all();
        return view('admin.data_master_mhs.ta.create',compact('mhs'));
    }

//    public function store(Request $request)
//    {
//        Biodata::where('nim',$request->nim)->update([
//            'proposal'=>$request->proposal,'hasil'=>$request->hasil,'sidang'=>$request->sidang,
//            'doping1'=>$request->doping1,'doping2'=>$request->doping2
//        ]);
//        return redirect('data/mhs/ta')->with('success','Bimbingan TA Berhasil Ditambah');
//    }

   public function edit($id)
   {
       $result=Biodata::find($id);
       $ds=DB::table('dosen')->get();
       return view('admin.data_master_mhs.ta.edit',compact('result','ds'));
   }

    public function update($id,Request $request)
    {
        Biodata::where('id',$id)->update([
            'proposal'=>$request->proposal,'hasil'=>$request->hasil,'sidang'=>$request->sidang,
            'doping1'=>$request->doping1,'doping2'=>$request->doping2,
            'penguji1'=>$request->penguji1,'penguji2'=>$request->penguji2,'penguji3'=>$request->penguji3
        ]);
        return redirect('data/mhs/ta')->with('success','Bimbingan TA Berhasil Diupdate');
    }

}