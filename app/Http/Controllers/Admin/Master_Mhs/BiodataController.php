<?php

namespace App\Http\Controllers\Admin\Master_Mhs;

use App\Models\Admin\PrestasiMhs;
use Illuminate\Http\Request;
use App\User;
use App\Http\Controllers\Controller;
use DB;
use App\Models\Admin\Biodata;
use App\Models\Admin\History;

class BiodataController extends Controller
{

    public function index()
    {
        $result=Biodata::all();
        $last_updated=History::where('model','mhs')->orderBy('created_at', 'desc')->first();
        return view('admin.data_master_mhs.biodata.view',compact('result','last_updated'));
    }

    public function create()
    {
        $prov=DB::table('prov')->get();
        $dosen=DB::table('dosen')->get();
        return view('admin.data_master_mhs.biodata.create',compact('prov','dosen'));
    }
    public function get_prov(Request $request)
    {
        $result=DB::table('kab')->where('prov_id',$request->id)->get();
        echo json_encode($result);
    }
    public function store(Request $request)
    {
        $this->validate($request, [
            'nim' => 'required|unique:biodata_mhs',

        ]);

        $bd=new Biodata();
        $bd->nim=$request->nim;
        $bd->dosen_id=$request->dosen;
        $bd->prov_id=$request->prov;
        $bd->kab_id=$request->kab;
        $bd->nama=ucwords($request->nama);
        $bd->asal_sekolah=$request->asal;
        $bd->tahun_masuk=$request->tahun_masuk;
        $bd->status=$request->status;
        $bd->beasiswa=$request->beasiswa;
        $bd->sumber_beasiswa=$request->sumber_beasiswa;
        $bd->save();
        return redirect('data/mhs/biodata_mhs')->with('success','Data Mahasiswa Berhasil Ditambah');
    }
    public function update($id,Request $request)
    {
        $nim=Biodata::find($id);
        if($request->nim!=$nim->nim)
        {
            $this->validate($request, [
                'nim' => 'required|unique:biodata_mhs',

            ]);
        }

        $bd=Biodata::find($id);
        $bd->nim=$request->nim;
        $bd->dosen_id=$request->dosen;
        $bd->prov_id=$request->prov;
        $bd->kab_id=$request->kab;
        $bd->nama=ucwords($request->nama);
        $bd->asal_sekolah=$request->asal;
        $bd->tahun_masuk=$request->tahun_masuk;
        $bd->tahun_keluar=$request->tahun_keluar;
        $bd->status=$request->status;
        $bd->beasiswa=$request->beasiswa;
        $bd->sumber_beasiswa=$request->sumber_beasiswa;
        $bd->save();
        return redirect('data/mhs/biodata_mhs')->with('success','Data Mahasiswa Berhasil Diupdate');
    }
    public function edit($id)
    {
        $result=Biodata::find($id);
        $prov=DB::table('prov')->get();
        $kab=DB::table('kab')->get();
        $dosen=DB::table('dosen')->get();

        return view('admin.data_master_mhs.biodata.edit',compact('result','prov','kab','dosen'));
    }
    public function destroy(Request $request)
    {
        if(Biodata::destroy($request->id))
        {
            echo 1;
        }else {
            echo 0;
        }
    }
}