<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;
use App\Models\Admin\Bimbingan;


class BimbinganController extends Controller
{

    public function index()
    {
        $result=Bimbingan::all();
        return view('admin.bimbingan.view',compact('result'));
    }

    public function create()
    {
        return view('admin.bimbingan.create');
    }

    public function store(Request $request)
    {

        $ba=new Bimbingan();
        $ba->hal=$request->hal;
        $ba->penjelasan=$request->penjelasan;
        $ba->save();

        return redirect('bimbingan/view')->with('success','Data Berhasil Ditambahkan');
    }
}