<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;
use App\Models\Admin\Peralatan;


class PeralatanController extends Controller
{

    public function index()
    {
        $result=DB::table('data_lab')->get();
        return view('admin.peralatan.view',compact('result'));
    }
    // method data lab
    public function create_lab()
    {
        return view('admin.peralatan.create_lab');
    }
    public function store_lab(Request $request)
    {
        DB::table('data_lab')->insert(['nama_lab'=>$request->nama_lab]);
        return redirect('peralatan/lab')->with('success','Data Berhasil Ditambahkan');
    }
    public function update_lab($id,Request $request)
    {
        DB::table('data_lab')->where('id',$id)->update(['nama_lab'=>$request->nama_lab]);
        return redirect('peralatan/lab')->with('success','Data Berhasil Diupdate');
    }
    public function edit_lab($id)
    {
        $result=DB::table('data_lab')->where('id',$id)->first();
        return view('admin.peralatan.edit_lab',compact('result'));
    }

    // method peralatan lab
    public function create_peralatan()
    {
        $result=DB::table('data_lab')->get();
        return view('admin.peralatan.create_peralatan',compact('result'));
    }

    public function store_peralatan(Request $request)
    {
        $p=new Peralatan();
        $p->data_lab_id=$request->data_lab_id;
        $p->jenis_peralatan=$request->jenis_peralatan;
        $p->jumlah_unit=$request->jumlah_unit;
        $p->waktu=$request->waktu;
        $p->kepemilikan=$request->kepemilikan;
        $p->kondisi=$request->kondisi;
        $p->save();

        return redirect('peralatan/lab')->with('success','Data Berhasil Ditambahkan');
    }
    public function update_peralatan($id,Request $request)
    {
        $p=Peralatan::find($id);
        $p->data_lab_id=$request->data_lab_id;
        $p->jenis_peralatan=$request->jenis_peralatan;
        $p->jumlah_unit=$request->jumlah_unit;
        $p->waktu=$request->waktu;
        $p->kepemilikan=$request->kepemilikan;
        $p->kondisi=$request->kondisi;
        $p->save();

        return redirect('peralatan/lab')->with('success','Data Berhasil Diupdate');
    }

    public function edit_peralatan($id)
    {
        $result=Peralatan::find($id);
        $data_lab=DB::table('data_lab')->get();
        return view('admin.peralatan.edit_peralatan',compact('result','data_lab'));
    }

}