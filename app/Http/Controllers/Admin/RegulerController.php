<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;
use App\Models\Admin\Reguler;


class RegulerController extends Controller
{

    public function index()
    {
        $result=Reguler::all();
        return view('admin.mhs.reguler.view',compact('result'));
    }

    public function create()
    {
        return view('admin.mhs.reguler.create');
    }

    public function add_more($id)
    {
        $single=Reguler::find($id);
        return view('admin.mhs.reguler.add_more',compact('single'));
    }
    public function add_more_store($id,Request $request)
    {
        Reguler::where('id',$id)->update(['ipk_min'=>$request->ipk_min,
            'ipk_rata'=>$request->ipk_rata,'ipk_mak'=>$request->ipk_mak,
            'p_rendah'=>$request->p_rendah,'p_sedang'=>$request->p_sedang,
            'p_tinggi'=>$request->p_tinggi
        ]);
        return redirect('mhs/view/reguler')->with('success','Data Berhasil Ditambahkan');
    }

    public function store(Request $request)
    {
        $reg=new Reguler();
        $reg->tahun_akademik=$request->tahun_akademik;
        $reg->daya_tampung=$request->daya_tampung;
        $reg->ikut_seleksi=$request->ikut_seleksi;
        $reg->lulus_seleksi=$request->lulus_seleksi;
        $reg->m_reguler=$request->m_reguler;
        $reg->m_transfer=$request->m_transfer;
        $reg->t_reguler=$request->t_reguler;
        $reg->t_transfer=$request->t_transfer;
        $reg->l_reguler=$request->l_reguler;
        $reg->l_transfer=$request->l_transfer;
        $reg->ipk_min=0;
        $reg->save();

        return redirect('mhs/view/reguler')->with('success','Data Berhasil Ditambahkan');
    }
    public function edit($id)
    {
        $result=Reguler::find($id);
        return view('admin.mhs.reguler.edit',compact('result'));
    }

    public function update($id,Request $request)
    {
        $reg=Reguler::find($id);
        $reg->tahun_akademik=$request->tahun_akademik;
        $reg->daya_tampung=$request->daya_tampung;
        $reg->ikut_seleksi=$request->ikut_seleksi;
        $reg->lulus_seleksi=$request->lulus_seleksi;
        $reg->m_reguler=$request->m_reguler;
        $reg->m_transfer=$request->m_transfer;
        $reg->t_reguler=$request->t_reguler;
        $reg->t_transfer=$request->t_transfer;
        $reg->l_reguler=$request->l_reguler;
        $reg->l_transfer=$request->l_transfer;
        $reg->save();

        return redirect('mhs/view/reguler')->with('success','Data Berhasil Diupdate');
    }
    public function destroy(Request $request)
    {
        if(Reguler::destroy($request->id))
        {
            echo 1;
        }else {
            echo 0;
        }
    }

}