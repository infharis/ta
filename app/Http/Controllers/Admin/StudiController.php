<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\PrestasiMhs;
use DB;
use Illuminate\Http\Request;
use App\Models\Admin\Pelacakan;


class StudiController extends Controller
{

    public function index()
    {
        $result=Pelacakan::all();
        return view('admin.studi_pelacakan.view',compact('result'));
    }
    public function create()
    {
        return view('admin.studi_pelacakan.create');
    }

    public function store(Request $request)
    {
        $dp=new Pelacakan();
        $dp->jenis_kemampuan=$request->jenis_kemampuan;
        $dp->sangat_baik=$request->sangat_baik;
        $dp->baik=$request->baik;
        $dp->cukup=$request->cukup;
        $dp->kurang=$request->kurang;
        $dp->rencana=$request->rencana;
        $dp->save();

        return redirect('mhs/evaluasi')->with('success','Data Berhasil Ditambahkan');

    }

    public function update($id,Request $request)
    {
        $dp=Pelacakan::find($id);
        $dp->jenis_kemampuan=$request->jenis_kemampuan;
        $dp->sangat_baik=$request->sangat_baik;
        $dp->baik=$request->baik;
        $dp->cukup=$request->cukup;
        $dp->kurang=$request->kurang;
        $dp->rencana=$request->rencana;
        $dp->save();

        return redirect('mhs/evaluasi')->with('success','Data Berhasil Diupdate');

    }

    public function edit($id)
    {
        $result=Pelacakan::find($id);
        return view('admin.studi_pelacakan.edit',compact('result'));
    }
    public function destroy(Request $request)
    {
        if(Pelacakan::destroy($request->id))
        {
            echo 1;
        }else {
            echo 0;
        }
    }

}