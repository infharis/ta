<?php

namespace App\Http\Controllers\Admin\Master_Dosen;

use App\User;
use App\Http\Controllers\Controller;
use App\Models\Dosen\Kemampuan;


class TugasBelajarController extends Controller
{

    public function index()
    {
        $result=Kemampuan::all();
        return view('admin.dosen.dosen_tetap.dosen_tugas_belajar',compact('result'));
    }

}