<?php

namespace App\Http\Controllers\Admin\Master_Dosen;

use App\User;
use App\Http\Controllers\Controller;
use App\Models\Dosen\Aktivitas;
use App\Models\Dosen\DataAktivitas;
use DB;


class AktivitasLpController extends Controller
{

    public function data_aktivitas_dosen_diluar_ps()
    {
        $dosen_diluar_ps=3;
        $result=@DB::table('dosen')->join('role_user',function($query)use($dosen_diluar_ps){
            $query->on('dosen.user_id','=','role_user.user_id')->where('role_user.role_id','=',3);
        })->select('dosen.*')->get();
        return view('admin.dosen.diluar_ps.data_aktivitas',compact('result'));
    }

}