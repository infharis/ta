<?php

namespace App\Http\Controllers\Admin\Master_Dosen;

use App\User;
use App\Http\Controllers\Controller;
use App\Models\Dosen\Aktivitas;
use App\Models\Dosen\DataAktivitas;
use DB;

class AktivitasDtController extends Controller
{

    public function aktivitas_dosen_tetap()
    {
        $result=Aktivitas::all();
        return view('admin.dosen.dosen_tetap.aktivitas',compact('result'));
    }

    public function data_aktivitas_dosen_tetap()
    {
        $result=DataAktivitas::all();
        return view('admin.dosen.dosen_tetap.data_aktivitas',compact('result'));
    }

    public function data_aktivitas_tidak_tetap()
    {
        $dosen_tidak_tetap=4;
        $result=@DB::table('dosen')->join('role_user',function($query)use($dosen_tidak_tetap){
            $query->on('dosen.user_id','=','role_user.user_id')->where('role_user.role_id','=',$dosen_tidak_tetap);
        })->select('dosen.*')->get();
        return view('admin.dosen.dosen_tidak_tetap.data_aktivitas',compact('result'));
    }

    public function count_pembimbing_ta($dosen_id)
    {

    }
}