<?php
/**
 * Created by PhpStorm.
 * User: infharis
 * Date: 22/01/18
 * Time: 22.55
 */

namespace App\Http\Controllers\Admin\Master_Dosen;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Excel;
use App\Models\Admin\Dosen;

class FileController extends Controller
{
    public function index()
    {
        return view('admin.dosen.dosen_tetap.biodata_dosen_file');
    }
    public function upload_data_dosen(Request $request)
    {
        $this->validate($request,['data_dosen'=>'required|mimes:xls,xlsx']);
        $name_file=$request->file('data_dosen');
        $result=Excel::load($name_file)->get();
        foreach ($result as $value)
        {
            $bio=@Dosen::firstOrNew(['nip'=>$value->nip]);
            $bio->nama_dosen=$value->nama_dosen ? $value->nama_dosen : ' ';
            $bio->nidn=$value->nidn ? $value->nidn : ' ';
            $bio->bidang=$value->bidang;
            $bio->save();
        }
        return redirect()->back()->with('success',' Data Berhasil Disimpan');
    }
}