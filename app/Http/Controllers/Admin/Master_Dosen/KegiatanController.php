<?php

namespace App\Http\Controllers\Admin\Master_Dosen;

use App\User;
use App\Http\Controllers\Controller;
use App\Models\Dosen\Kegiatan;


class KegiatanController extends Controller
{

    public function index()
    {
        $result=Kegiatan::all();
        return view('admin.dosen.dosen_tetap.kegiatan',compact('result'));
    }

}