<?php

namespace App\Http\Controllers\Admin\Master_Dosen;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\Dosen;
use DB;
use Mail;
use Hash;
use App\Models\Dosen\Artikel;
use App\Models\Dosen\Karya;
use App\Models\Admin\Perwalian;

class IndexController extends Controller
{
    public $password;

    public function paten()
    {
        $result=Karya::all();
        return view('admin.dosen.paten',compact('result'));
    }
    public function karya_ilmiah()
    {
        $result=Artikel::all();
        return view('admin.dosen.karya_ilmiah',compact('result'));
    }
    public function perwalian_mhs($nip)
    {
        $result=Perwalian::where('nip_dosen',$nip)->get();
        $dosen=Dosen::where('nip',$nip)->first();
        return view('admin.dosen.dosen_tetap.detail_perwalian',compact('result','dosen'));
    }

    public function p2m_dosen()
    {

    }

    /*
     * Method Dosen Tetap
     *
     * */

    public function dosen_tetap()
    {
        $result=User::all();
        return view('admin.dosen.dosen_tetap.view',compact('result'));
    }

    public function edit_dosen_tetap($id)
    {
        $result=User::find($id);
        return view('admin.dosen.dosen_tetap.edit',compact('result'));
    }

    public function update_dosen_tetap($id,Request $request)
    {
        if($request->status==5)
        {
            DB::table('dosen')->where('user_id',$id)->delete();
        }
        return redirect('data/dosen_tetap/view')->with('success','Data Berhasil Diupdate');
    }
    /*  end method dosen tetap */

    public function dosen_diluar_ps()
    {
        $result=User::all();
        return view('admin.dosen.diluar_ps.view',compact('result'));
    }
    public function dosen_tidak_tetap()
    {
        $result=User::all();
        return view('admin.dosen.dosen_tidak_tetap.view',compact('result'));
    }
    public function index()
    {
        return view('admin.dosen.index');
    }
    public function create()
    {
        return view('admin.dosen.create');
    }
    public function store(Request $request)
    {
        $this->validate($request,['nip'=>'required|unique:dosen']);
        if($request->status==5)
        {
            // data dosen
            $ds = new Dosen();
            $ds->nama_dosen = $request->nama;
            $ds->nip = $request->nip;
            $ds->nidn = $request->nidn;
            $ds->sertifikasi = 'N';
            $ds->kategori_dosen = $request->status;
            $ds->save();
            return redirect('sdm/data/dosen_tetap')->with('success', 'Data Berhasil Disimpan');

        }else {
            $this->password = $this->generator();
            // akun dosen
            $user = new User();
            $user->name = $request->nama;
            $user->email = $request->email;
            $user->password = Hash::make($this->password);
            $user->save();

            // rule dosen
            $role_id = DB::table('role_user')->insertGetId(['user_id' => $user->id, 'role_id' => $request->status]);
            DB::table('permission_role')->insert(['permission_id' => $request->status, 'role_id' => $role_id]);

            // data dosen
            $ds = new Dosen();
            $ds->user_id = $user->id;
            $ds->nama_dosen = $request->nama;
            $ds->nip = $request->nip;
            $ds->nidn = $request->nidn;
            $ds->sertifikasi = 'N';
            $ds->kategori_dosen = $request->status;
            $ds->save();

            $this->send_email($request, $this->password);
            return redirect('sdm/data/dosen_tetap')->with('success', 'Akun Telah Berhasil Dibuat!!');
        }

    }
    public function send_email($request,$password)
    {
        Mail::send('mail.akun', ['user' =>$request,'password'=>$password], function ($m)use($request){
            $m->from('zingapaleh@gmail.com', 'Informatika Unsyiah');

            $m->to($request->email,'')->subject('Informasi Akun Borang Akreditasi');
        });
    }
    public function generator(){
        $length = 10;
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}