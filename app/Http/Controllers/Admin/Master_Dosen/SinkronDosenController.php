<?php
/**
 * Created by PhpStorm.
 * User: infharis
 * Date: 12/01/18
 * Time: 19.16
 */

namespace App\Http\Controllers\Admin\Master_Dosen;


use App\Http\Controllers\Controller;
use App\Models\Admin\Dosen;
use App\Models\Admin\History;
use App\Models\Admin\PendidikanDosen;
use App\Models\Admin\Biodata;
use DB;
use App\Models\Admin\Perwalian;

class SinkronDosenController extends Controller
{
    public $url='http://ws.unsyiah.ac.id/webservice/dosen/cdosen/dosen/nip/';
    public $perwalian_url="http://ws.unsyiah.ac.id/webservice/dosen/cdosen/listmhsdoswal/nip/";

    public $key="/key/iuhs768hkj";
    public $nip_dosen,$semester,$tahun,$per;

    public function get_all_dosen()
    {
        $data_dosen=Dosen::all();
        foreach ($data_dosen as $value)
        {
            $url=$this->url.$value->nip.$this->key;
            $url_perwalian=$this->perwalian_url.$value->nip.$this->key;
            $result = @simplexml_load_file($url);
            $result_perwalian = @simplexml_load_file($url_perwalian);
            if($result!==false) {
                $sertifikat = 'N';
                if ($result->no_sertifikat_dosen) {
                    $sertifikat = 'Y';
                }
                $jbtan_fungs = DB::table('jabatan_fungsional')->where('nama_jabatan', $result->jabfung)->first();
                $dosen = Dosen::find($value->id);
                $dosen->nidn = $result->nidn;
                $dosen->tempat = $result->tempatlahir;
                $dosen->tgl_lahir = $result->tanggallahir;
                $dosen->jabatan_id = $jbtan_fungs->id;
                $dosen->sertifikasi = $sertifikat;
                $dosen->bidang = $result->bidangilmu;
                $dosen->golongan = $result->golruang_pangkat;
                $dosen->save();

                // perwalian mhs
                foreach ($result_perwalian as $val_per){
                    foreach ($val_per as $val_mhs) {
                        if($val_mhs->nip) {
                            $sub = substr($val_mhs->semester, 0, 4);
                            $semester = substr($val_mhs->semester, 4, 4);
                            $this->tahun=$sub;
                            $this->semester=$semester;
                            $this->nip_dosen=$val_mhs->nip;
                        }else {
                            $statement=array('nip_dosen'=>$this->nip_dosen,'tahun'=>$this->tahun,
                                'semester'=>$this->semester,'nim_mhs'=>$val_mhs->nama);
                            $p=@Perwalian::firstOrNew($statement);
                            $p->save();
                        }
                    }
                }

                $gelar_s2=explode(',',$result->gelar_belakang);
                $statement_s3=array('dosen_id'=>$dosen->id,'jenjang_id'=>3);
                $statement_s2=array('dosen_id'=>$dosen->id,'jenjang_id'=>2);
                $statement_s1=array('dosen_id'=>$dosen->id,'jenjang_id'=>1);
                $s3=PendidikanDosen::where($statement_s3)->first();
                $s2=PendidikanDosen::where($statement_s2)->first();
                $s1=PendidikanDosen::where($statement_s1)->first();

                // jenjang s3
                if(is_null($s3) && $result->gelar_depan=='Dr') {
                    $pd =new PendidikanDosen();
                    $pd->dosen_id = $dosen->id;
                    $pd->jenjang_id = 3;
                    $pd->pendidikan = $result->nama_perguruan_tinggi_S3;
                    $pd->bidang = $result->jurusan_perguruan_tinggi_S3;
                    $pd->gelar = $result->gelar_depan;
                    $pd->kota = $result->kota_perguruan_tinggi_S3;
                    $pd->save();
                }elseif($result->gelar_depan=='Dr'){
                    $pd =PendidikanDosen::firstOrNew(['id' => $s3->id]);
                    $pd->jenjang_id = 3;
                    $pd->pendidikan = $result->nama_perguruan_tinggi_S3;
                    $pd->bidang = $result->jurusan_perguruan_tinggi_S3;
                    $pd->gelar = $result->gelar_depan;
                    $pd->kota = $result->kota_perguruan_tinggi_S3;
                    $pd->save();
                }

                // s2
                if(is_null($s2) && isset($gelar_s2[1])) {
                    $pd =new PendidikanDosen();
                    $pd->dosen_id = $dosen->id;
                    $pd->jenjang_id = 2;
                    $pd->pendidikan = $result->nama_perguruan_tinggi_S2;
                    $pd->bidang = $result->jurusan_perguruan_tinggi_S2;
                    $pd->gelar = $gelar_s2[1];
                    $pd->kota = $result->kota_perguruan_tinggi_S2;
                    $pd->save();
                }elseif(isset($gelar_s2[1])){
                    $pd =PendidikanDosen::firstOrNew(['id' => $s2->id]);
                    $pd->jenjang_id = 2;
                    $pd->pendidikan = $result->nama_perguruan_tinggi_S2;
                    $pd->bidang = $result->jurusan_perguruan_tinggi_S2;
                    $pd->gelar = $gelar_s2[1];
                    $pd->kota = $result->kota_perguruan_tinggi_S2;
                    $pd->save();
                }

                // s1
                if(is_null($s1) && isset($gelar_s2[0])) {
                    $pd =new PendidikanDosen();
                    $pd->dosen_id = $dosen->id;
                    $pd->jenjang_id = 1;
                    $pd->pendidikan = $result->nama_perguruan_tinggi_S1;
                    $pd->bidang = $result->jurusan_perguruan_tinggi_S1;
                    $pd->gelar = $gelar_s2[0];
                    $pd->kota = $result->kota_perguruan_tinggi_S1;
                    $pd->save();
                }elseif(isset($gelar_s2[0])){
                    $pd =PendidikanDosen::firstOrNew(['id' => $s1->id]);
                    $pd->jenjang_id = 1;
                    $pd->pendidikan = $result->nama_perguruan_tinggi_S1;
                    $pd->bidang = $result->jurusan_perguruan_tinggi_S1;
                    $pd->gelar = $gelar_s2[0];
                    $pd->kota = $result->kota_perguruan_tinggi_S1;
                    $pd->save();
                }



            }
        }
        // update last record
        $history=new History();
        $history->model="dosen";
        $history->save();
        return redirect()->back()->with('success', 'Sinkronisasi Data Dosen Berhasil!!!');

    }

}