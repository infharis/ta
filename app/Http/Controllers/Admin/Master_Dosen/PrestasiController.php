<?php

namespace App\Http\Controllers\Admin\Master_Dosen;

use App\User;
use App\Http\Controllers\Controller;
use App\Models\Dosen\Prestasi;
use App\Models\Dosen\Organisasi;


class PrestasiController extends Controller
{

    public function index()
    {
        $result=Prestasi::all();
        return view('admin.dosen.prestasi',compact('result'));
    }

    public function organisasi()
    {
        $result=Organisasi::all();
        return view('admin.dosen.dosen_tetap.organisasi',compact('result'));
    }

}