<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;
use App\Models\Admin\Prasarana;


class PrasaranaController extends Controller
{

    public function index()
    {
        $result=Prasarana::all();
        return view('admin.prasarana.view',compact('result'));
    }
    public function create()
    {
        return view('admin.prasarana.create');
    }

    public function store(Request $request)
    {
        $pr=new Prasarana();
        $pr->jenis_prasarana=$request->jenis_prasarana;
        $pr->jumlah_unit=$request->jumlah_unit;
        $pr->total_luas=$request->total_luas;
        $pr->utilisasi=$request->utilisasi;
        $pr->kepemilikan=$request->kepemilikan;
        $pr->kondisi=$request->kondisi;
        $pr->save();

        return redirect('data/prasarana')->with('success','Data Berhasil Ditambahkan');

    }
    public function update($id,Request $request)
    {
        $pr=Prasarana::find($id);
        $pr->jenis_prasarana=$request->jenis_prasarana;
        $pr->jumlah_unit=$request->jumlah_unit;
        $pr->total_luas=$request->total_luas;
        $pr->utilisasi=$request->utilisasi;
        $pr->kepemilikan=$request->kepemilikan;
        $pr->kondisi=$request->kondisi;
        $pr->save();

        return redirect('data/prasarana')->with('success','Data Berhasil Diupdate');

    }

    public function edit($id)
    {
        $result=Prasarana::find($id);
        return view('admin.prasarana.edit',compact('result'));
    }
    public function destroy(Request $request)
    {
        if(Prasarana::destroy($request->id))
        {
            echo 1;
        }else {
            echo 0;
        }
    }
}