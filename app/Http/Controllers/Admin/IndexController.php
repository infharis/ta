<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Admin\UmpanBalik;
use App\Models\Dosen\Kegiatan;

class IndexController extends Controller
{

    public function laporan()
    {
        $umpan_balik=UmpanBalik::all();
        return view('admin.laporan.main',compact('umpan_balik'));
    }

    public function suasana_akademik()
    {
        $result=Kegiatan::all();
        return view('admin.suasana_akademik.index',compact('result'));
    }
}