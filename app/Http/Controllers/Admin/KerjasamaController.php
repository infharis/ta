<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;
use App\Models\Admin\Kerjasama;

class KerjasamaController extends Controller
{

    public function index()
    {
        $result=Kerjasama::all();
        return view('admin.kerjasama.view',compact('result'));
    }

    public function create()
    {
        return view('admin.kerjasama.create');
    }

    public function store(Request $request)
    {
        $kj=new Kerjasama();
        $kj->nama_instansi=$request->nama_instansi;
        $kj->jenis_kegiatan=$request->jenis_kegiatan;
        $kj->start=$request->start;
        $kj->end=$request->end;
        $kj->manfaat=$request->manfaat;
        $kj->kerjasama=$request->kerjasama;
        $kj->save();

        if($request->file('softcopy')) {
            $file = $request->file('softcopy');
            $fileName = $file->getClientOriginalName();
            $request->file('softcopy')->move("local/storage/app/kerjasama", $fileName);

            Kerjasama::where('id',$kj->id)->update(['softcopy'=>$fileName]);
        }

        return redirect('kerjasama/view')->with('success','Data Berhasil Ditambahkan');
    }
    public function update($id,Request $request)
    {
        $kj=Kerjasama::find($id);
        $kj->nama_instansi=$request->nama_instansi;
        $kj->jenis_kegiatan=$request->jenis_kegiatan;
        $kj->start=$request->start;
        $kj->end=$request->end;
        $kj->manfaat=$request->manfaat;
        $kj->kerjasama=$request->kerjasama;
        $kj->save();

        return redirect('kerjasama/view')->with('success','Data Berhasil Diupdate');
    }

    public function edit($id)
    {
        $result=Kerjasama::find($id);
        return view('admin.kerjasama.edit',compact('result'));
    }
    public function destroy(Request $request)
    {
        if(Kerjasama::destroy($request->id))
        {
            echo 1;
        }else {
            echo 0;
        }
    }
}