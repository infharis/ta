<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;
use App\Models\Admin\RuangKerja;


class RuangKerjaController extends Controller
{

    public function index()
    {
        $result=RuangKerja::all();
        return view('admin.ruang_kerja.view',compact('result'));
    }
    public function create()
    {
        return view('admin.ruang_kerja.create');
    }

    public function store(Request $request)
    {
        $rk=new RuangKerja();
        $rk->ruang_kerja=$request->ruang_kerja;
        $rk->jumlah=$request->jumlah;
        $rk->luas=$request->luas;
        $rk->save();

        return redirect('ruang_kerja/view')->with('success','Data Berhasil Ditambahkan');
    }
    public function update($id,Request $request)
    {
        $rk=RuangKerja::find($id);
        $rk->ruang_kerja=$request->ruang_kerja;
        $rk->jumlah=$request->jumlah;
        $rk->luas=$request->luas;
        $rk->save();

        return redirect('ruang_kerja/view')->with('success','Data Berhasil Diupdate');
    }
    public function edit($id)
    {
        $result=RuangKerja::find($id);
        return view('admin.ruang_kerja.edit',compact('result'));
    }
    public function destroy(Request $request)
    {
        if(RuangKerja::destroy($request->id))
        {
            echo 1;
        }else {
            echo 0;
        }
    }

}