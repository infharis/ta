<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;
use App\Models\Admin\Pustaka;


class PustakaController extends Controller
{

    public function index()
    {
        $result=Pustaka::all();
        return view('admin.pustaka.view',compact('result'));
    }

    public function create()
    {
        return view('admin.pustaka.create');
    }

    public function store(Request $request)
    {
        $ps=new Pustaka();
        $ps->jenis_pustaka=$request->jenis_pustaka;
        $ps->jumlah_judul=$request->jumlah_judul;
        $ps->jumlah_copy=$request->jumlah_copy;
        $ps->save();

        return redirect('pustaka/view')->with('success','Data Berhasil Ditambahkan');
    }
    public function update($id,Request $request)
    {
        $ps=Pustaka::find($id);
        $ps->jenis_pustaka=$request->jenis_pustaka;
        $ps->jumlah_judul=$request->jumlah_judul;
        $ps->jumlah_copy=$request->jumlah_copy;
        $ps->save();

        return redirect('pustaka/view')->with('success','Data Berhasil Diupdate');
    }

    public function edit($id)
    {
        $result=Pustaka::find($id);
        return view('admin.pustaka.edit',compact('result'));
    }

    public function destroy(Request $request)
    {
        if(Pustaka::destroy($request->id))
        {
            echo 1;
        }else {
            echo 0;
        }
    }

}