<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Dosen\Karya;
use DB;
use Illuminate\Http\Request;
use App\Models\Admin\Kurikulum;
use App\Models\Admin\History;


class KurikulumController extends Controller
{
    public $url='http://ws.unsyiah.ac.id/webservice/dosen/cdosen/makulkurikulum/kdprodi/';
    public $key="/key/iuhs768hkj";
    public $kode_prodi='0810701';

    public function index()
    {
       // $result=DB::table('semester')->get();
        $result=Kurikulum::all();
        return view('admin.kurikulum.view',compact('result'));
    }

    public function create()
    {
        $result=DB::table('semester')->get();
        return view('admin.kurikulum.create',compact('result'));
    }
    public function sinkron_data()
    {
        $url = $this->url.$this->kode_prodi.$this->key;
        $result = simplexml_load_file($url);
        foreach ($result as $value1)
        {
            foreach ($value1 as $value) {
                $kur = Kurikulum::firstOrNew(['kode_mk' => $value->kode_mata_kuliah]);
                $kur->nama_mk = $value->nama_mata_kuliah;
                $kur->bobot_sks = $value->bobot_sks;
                $kur->tahun = $value->tahun;
                $kur->save();
            }
        }

        $history=new History();
        $history->model="kurikulum";
        $history->save();
        return redirect('kurikulum/view')->with('success','Sinkronisasi Data Kurikulum Berhasil');

    }

    public function store(Request $request)
    {
        $kr=new Kurikulum();
        $kr->semester_id=$request->semester_id;
        $kr->kode_mk=$request->kode_mk;
        $kr->nama_mk=$request->nama_mk;
        $kr->bobot_sks=$request->bobot_sks;
        $kr->inti=$request->inti;
        $kr->bobot_tugas=$request->bobot_tugas;

        // deskripsi
        if($request->deskripsi) {
            $kr->deskripsi = $request->deskripsi;
        }else {
            $kr->deskripsi = 'N';
        }

        // silabus
        if($request->silabus)
        {
            $kr->silabus=$request->silabus;
        }else {
            $kr->silabus='N';
        }

        // sap
        if($request->sap)
        {
            $kr->sap=$request->sap;
        }else{
            $kr->sap='N';
        }

        $kr->unit=$request->unit;
        $kr->save();

        return redirect('kurikulum/view')->with('success','Data Berhasil Ditambahkan');
    }

    public function edit($id)
    {
        $result=Kurikulum::find($id);
        $sm=DB::table('semester')->get();
        return view('admin.kurikulum.edit',compact('result','sm'));
    }

    public function update($id,Request $request)
    {
        $kr=Kurikulum::find($id);
        $kr->semester_id=$request->semester_id;
        $kr->kode_mk=$request->kode_mk;
        $kr->nama_mk=$request->nama_mk;
        $kr->bobot_sks=$request->bobot_sks;
        $kr->inti=$request->inti;
        $kr->bobot_tugas=$request->bobot_tugas;

        // deskripsi
        if($request->deskripsi) {
            $kr->deskripsi = $request->deskripsi;
        }else {
            $kr->deskripsi = 'N';
        }

        // silabus
        if($request->silabus)
        {
            $kr->silabus=$request->silabus;
        }else {
            $kr->silabus='N';
        }

        // sap
        if($request->sap)
        {
            $kr->sap=$request->sap;
        }else{
            $kr->sap='N';
        }

        $kr->unit=$request->unit;
        $kr->save();

        return redirect('kurikulum/view')->with('success','Data Berhasil Diupdate');
    }


}