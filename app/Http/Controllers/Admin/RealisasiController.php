<?php


namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\Sd;
use DB;
use Illuminate\Http\Request;

class RealisasiController extends Controller
{

    public function index()
    {
        $result=Sd::all();
        $ts2=date("Y",strtotime("-3 year"));
        $ts1=date("Y",strtotime("-2 year"));
        $ts=date("Y",strtotime("-1 year"));
        return view('admin.realisasi.view',compact('result','ts2','ts1','ts'));
    }
    // sumber dana
    public function create_sd()
    {
        return view('admin.realisasi.create_sd');
    }
    public function store_sd(Request $request)
    {
        DB::table('sumber_dana')->insert(['nama_sumber_dana'=>$request->sd]);
        return redirect('realisasi_dana/view')->with('success','Sumber Dana Berhasil Ditambah');
    }
    public function edit_sd($id)
    {
        $result=DB::table('sumber_dana')->where('id',$id)->first();
        return view('admin.realisasi.edit_sd',compact('result'));
    }
    public function update_sd($id,Request $request)
    {
        DB::table('sumber_dana')->where('id',$id)->update(['nama_sumber_dana'=>$request->sd]);
        return redirect('realisasi_dana/view')->with('success','Sumber Dana Berhasil Diupdate');
    }


    // jenis dana
    public function create_jd()
    {
        $result=DB::table('sumber_dana')->get();
        return view('admin.realisasi.create_jd',compact('result'));
    }
    public function store_jd(Request $request)
    {
        DB::table('jenis_dana')->insert(['sumber_dana_id'=>$request->sd,'nama_jenis_dana'=>$request->jd]);
        return redirect('realisasi_dana/view')->with('success','Jenis Dana Berhasil Ditambah');
    }
    public function edit_jd($id)
    {
        $result=DB::table('jenis_dana')->where('id',$id)->first();
        $sd=DB::table('sumber_dana')->get();
        return view('admin.realisasi.edit_jd',compact('result','sd'));
    }
    public function update_jd($id,Request $request)
    {
        DB::table('jenis_dana')->where('id',$id)->update(['sumber_dana_id'=>$request->sd,'nama_jenis_dana'=>$request->jd]);
        return redirect('realisasi_dana/view')->with('success','Jenis Dana Berhasil Diupdate');
    }
}