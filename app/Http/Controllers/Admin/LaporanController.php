<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;
use App\Models\Admin\UmpanBalik;

class LaporanController extends Controller
{
    // index [0] = nama borang akreditasi, [1] = url download borang, [2] = nama file borang
    public $daftar_report=array(
        array('2.5','Umpan Balik dari dosen, mahasiswa, alumni, dan pengguna lulusan','d/umpan_balik','umpan_balik'),
        array('3.1.1',' Keikutsertaan dosen tetap dalam organisasi keilmuan atau organisasi profesi','d/mhs_reguler','mhs_reguler'),
        array('3.1.2',' Data mahasiswa non-reguler (5 tahun terakhir)',''),
        array('3.1.3','Pencapaian prestasi/reputasi mahasiswa (3 tahun terakhir)',''),
        array('3.1.4',' Data jumlah mahasiswa reguler (7 tahun terakhir)',''),
        array('3.2','Layanan kepada Mahasiswa',''),
        array('3.3.1',' Studi pelacakan',''),
        array('4.3.1','Data dosen tetap yang bidang keahliannya sesuai bidang PS Dosen prodi',''),
        array('4.3.2',' Data dosen tetap yang bidang keahliannya di luar bidang PS: Dosen non prodi',''),
        array('4.3.3',' Aktivitas dosen tetap yang bidang bidang keahliannya sesuai dengan PS dinyatakan dalam sks rata-rata per semester ',''),
        array('4.3.4',' Data aktivitas mengajar dosen tetap yang bidang keahliannya sesuai dengan PS',''),
        array('4.3.5',' Data aktivitas mengajar dosen tetap yang bidang keahliannya di luar PS',''),
        array('4.4.1','Data dosen tidak tetap pada PS',''),
        array('4.4.2',' Data aktivitas mengajar dosen tidak tetap pada satu tahun terakhir di PS ',''),
        array('4.5.1',' Kegiatan tenaga ahli/pakar sebagai pembicara dalam seminar/pelatihan, pembicara tamu, dsb, dari luar PT sendiri',''),
        array('4.5.2',' Peningkatan kemampuan dosen tetap melalui program tugas belajar dalam bidang yang sesuai dengan bidang PS',''),
        array('4.5.3',' Kegiatan dosen tetap dalam seminar ilmiah/lokakarya/penataran/workshop/ pagelaran/ pameran/peragaan 	',''),
        array('4.5.4',' Pencapaian prestasi/reputasi dosen ',''),
        array('4.5.5',' Keikutsertaan dosen tetap dalam organisasi keilmuan atau organisasi profesi.',''),
        array('4.6.1',' Data tenaga kependidikan',''),
        array('5.1.2.1','Jumlah sks PS (minimum untuk kelulusan)',''),
        array('5.1.2.2','Struktur kurikulum berdasarkan urutan mata kuliah (MK)',''),
        array('5.1.3',' Mata kuliah pilihan yang dilaksanakan (3 tahun terakhir)',''),
        array('5.1.4',' Substansi praktikum/praktek',''),
        array('5.2',' Peninjauan silabus/SAP dan buku ajar (5 tahun terakhir)',''),
        array('5.4.1','Nama dosen pembimbing akademik dan jumlah mahasiswa',''),
        array('5.4.2','Proses pembimbingan akademik',''),
        array('5.5.1','Pelaksanaan pembimbingan Tugas Akhir atau Skripsi',''),
        array('5.6','Upaya Perbaikan Pembelajaran selesai',''),
        array('6.2.1 a','Realisasi perolehan dan alokasi dana (termasuk hibah) ',''),
        array('6.2.1 b','Penggunaan dana',''),
        array('6.2.2',' Dana untuk kegiatan penelitian (3 tahun terakhir)',''),
        array('6.2.3',' Dana diperoleh dari/untuk kegiatan P2M (3 tahun terakhir)		',''),
        array('6.3.1','Data ruang kerja dosen tetap',''),
        array('6.3.2',' Data prasarana (kantor, ruang kelas, ruang laboratorium, studio, ruang perpustakaan, kebun percobaan, dsb)',''),
        array('6.3.3',' Data prasarana lain yang menunjang (tempat olah raga, ruang bersama, ruang himpunan mahasiswa, poliklinik',''),
        array('6.4.1.1',' Rekapitulasi jumlah ketersediaan pustaka ',''),
        array('6.4.1.2',' Jurnal yang tersedia/yang diterima secara teratur (lengkap), (3 tahun terakhir)',''),
        array('6.4.3',' Peralatan utama yang digunakan di laboratorium ',''),
        array('6.5.2',' Aksesibilitas tiap jenis data',''),
        array('7.1.1',' Jumlah judul penelitian (3 tahun terakhir)',''),
        array('7.1.3',' Judul artikel ilmiah/karya ilmiah/karya seni/buku dihasilkan oleh dosen tetap (3 tahun terakhir)',''),
        array('7.1.4',' Karya dosen dan atau mahasiswa (Paten)	',''),
        array('7.2.1','Jumlah kegiatan P2M (3 tahun terakhir)',''),
        array('7.3.1',' Instansi dalam negeri yang menjalin kerjasama (3 tahun terakhir)	',''),
        array('7.3.2',' Instansi luar negeri yang menjalin kerjasama (3 tahun terakhir)',''),
    );
    public function index()
    {
        $result=$this->daftar_report;
        return view('admin.index',compact('result'));
    }
    public function umpan_balik()
    {
        $sheet1=$this->daftar_report[0][0];
        $name_file=$this->daftar_report[0][3];
        $result=UmpanBalik::all();
        return view('admin.laporan.main',compact('sheet1','result','name_file'));
    }
    public function daftar_dosen()
    {
        $sheet1='data dosen';
        $name_file='Daftar Dosen Informatika';
        $result=DB::table('dosen')->where('kategori_dosen','=',2)->get();
        return view('admin.laporan.daftar_dosen',compact('sheet1','result','name_file'));
    }
    public function data_penelitian()
    {
        $sheet1='data penelitian';
        $name_file='Data penelitian';
        $ts2=date("Y",strtotime("-3 year"));
        $ts1=date("Y",strtotime("-2 year"));
        $ts=date("Y",strtotime("-1 year"));
        $result=array($ts,$ts1,$ts2);
        return view('admin.laporan.data_penelitian',compact('sheet1','result','name_file'));
    }
    public function data_p2m()
    {
        $sheet1='Data Pelayanan Dan Pengabdian Masyarakat';
        $name_file='Data p2m';
        $ts2=date("Y",strtotime("-3 year"));
        $ts1=date("Y",strtotime("-2 year"));
        $ts=date("Y",strtotime("-1 year"));
        $result=array($ts,$ts1,$ts2);
        return view('admin.laporan.data_p2m',compact('sheet1','result','name_file'));
    }
}