<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;
use App\Models\Admin\Silabus;

class SilabusController extends Controller
{
    public function index()
    {
        $result=Silabus::all();
        return view('admin.silabus.view',compact('result'));
    }

    public function create()
    {
        $mk=DB::table('kurikulum')->get();
        return view('admin.silabus.create',compact('mk'));
    }

    public function store(Request $request)
    {
        $sil=new Silabus();
        $sil->mk_id=$request->mk;
        $sil->status_mk=$request->status;
        $sil->berlaku=$request->berlaku;
        $sil->alasan=$request->alasan;
        $sil->usulan=$request->usulan;
        if($request->silabus=='Y')
        {
            $sil->silabus=$request->silabus;
        }else {
            $sil->silabus='N';
        }

        if($request->buku_ajar=='Y')
        {
            $sil->buku_ajar=$request->buku_ajar;
        }else {
            $sil->buku_ajar='N';
        }
        $sil->save();

        return redirect('kurikulum/silabus/view')->with('success','Data Berhasil Ditambahkan');
    }
    public function edit($id)
    {
        $result=Silabus::find($id);
        $mk=DB::table('kurikulum')->get();
        return view('admin.silabus.edit',compact('result','mk'));
    }

    public function update($id,Request $request)
    {
        $sil=Silabus::find($id);
        $sil->mk_id=$request->mk;
        $sil->status_mk=$request->status;
        $sil->berlaku=$request->berlaku;
        $sil->alasan=$request->alasan;
        $sil->usulan=$request->usulan;
        if($request->silabus=='Y')
        {
            $sil->silabus=$request->silabus;
        }else {
            $sil->silabus='N';
        }

        if($request->buku_ajar=='Y')
        {
            $sil->buku_ajar=$request->buku_ajar;
        }else {
            $sil->buku_ajar='N';
        }
        $sil->save();

        return redirect('kurikulum/silabus/view')->with('success','Data Berhasil Diupdate');
    }

    public function destroy(Request $request)
    {
        if(Silabus::destroy($request->id))
        {
            echo 1;
        }else {
            echo 0;
        }
    }

}