<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;
use Auth;


class LoginAsController extends Controller
{

    public function index()
    {
        $ds=DB::table('dosen')->join('users','dosen.user_id','=','users.id')->select('dosen.*')->get();
        return view('admin.login_as.index',compact('ds'));
    }

    public function login_as(Request $request)
    {
        $request->session()->put('user_prev',Auth::user()->id);
        Auth::loginUsingId($request->user, true);
        return redirect('dosen');
    }


}