<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;
use App\Models\Admin\NonReguler;

class NonRegulerController extends Controller
{

    public function index()
    {
        $result=NonReguler::all();
        return view('admin.mhs.non_reguler.view',compact('result'));
    }

    public function create()
    {
        return view('admin.mhs.non_reguler.create');
    }

    public function store(Request $request)
    {
        $reg=new NonReguler();
        $reg->tahun_akademik=$request->tahun_akademik;
        $reg->daya_tampung=$request->daya_tampung;
        $reg->ikut_seleksi=$request->ikut_seleksi;
        $reg->lulus_seleksi=$request->lulus_seleksi;
        $reg->non_reguler=$request->non_reguler;
        $reg->transfer=$request->transfer;
        $reg->t_non_reguler=$request->t_non_reguler;
        $reg->t_transfer=$request->t_transfer;
        $reg->save();

        return redirect('mhs/view/non_reguler')->with('success','Data Berhasil Ditambahkan');
    }
    public function destroy(Request $request)
    {
        if(NonReguler::destroy($request->id))
        {
            echo 1;
        }else {
            echo 0;
        }
    }

    public function edit($id)
    {
        $result=NonReguler::find($id);
        return view('admin.mhs.non_reguler.edit',compact('result'));
    }

    public function update($id,Request $request)
    {
        $reg=NonReguler::find($id);
        $reg->tahun_akademik=$request->tahun_akademik;
        $reg->daya_tampung=$request->daya_tampung;
        $reg->ikut_seleksi=$request->ikut_seleksi;
        $reg->lulus_seleksi=$request->lulus_seleksi;
        $reg->non_reguler=$request->non_reguler;
        $reg->transfer=$request->transfer;
        $reg->t_non_reguler=$request->t_non_reguler;
        $reg->t_transfer=$request->t_transfer;
        $reg->save();

        return redirect('mhs/view/non_reguler')->with('success','Data Berhasil Diupdate');
    }

}