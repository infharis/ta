<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;
use App\Models\Admin\PenggunaanDana;

class PenggunaanDanaController extends Controller
{

    public function index()
    {
        $result=PenggunaanDana::all();
        $ts2=date("Y",strtotime("-3 year"));
        $ts1=date("Y",strtotime("-2 year"));
        $ts=date("Y",strtotime("-1 year"));
        return view('admin.pengunaan_dana.view',compact('result','ts2','ts1','ts'));
    }

    public function create()
    {
        $result=DB::table('jenis_dana')->get();
        return view('admin.pengunaan_dana.create',compact('result'));
    }
    public function store(Request $request)
    {
       $jp_id=DB::table('jenis_penggunaan')->insertGetId(['nama_jenis_penggunaan'=>$request->jp]);
       foreach($request->jd as $value)
       {
           DB::table('jd_jp')->insert(['jd_id'=>$value,'jp_id'=>$jp_id]);
       }
       return redirect('pengunaan_dana/view')->with('success','Penggunaan Dana Berhasil Ditambah');
    }

    public function edit($id)
    {
        $jd=DB::table('jenis_dana')->get();
        $result=DB::table('jenis_penggunaan')->where('id',$id)->first();

        // get all jenis dana
        $jd_pg=DB::table('jd_jp')->where('jp_id',$id)->get();
        // convert object to array
        $result_jd_pg = array_map(function ($value) {
            return $value->jd_id;
        }, $jd_pg);
        return view('admin.pengunaan_dana.edit',compact('jd','result','result_jd_pg'));
    }
    public function update($id,Request $request)
    {
        // hapus data sebelumnnya
        DB::table('jd_jp')->where('jp_id',$id)->delete();

        $jp_id=DB::table('jenis_penggunaan')->where('id',$id)->update(['nama_jenis_penggunaan'=>$request->jp]);
        foreach($request->jd as $value)
        {
            DB::table('jd_jp')->insert(['jd_id'=>$value,'jp_id'=>$id]);
        }
        return redirect('pengunaan_dana/view')->with('success','Data Penggunaan Dana Berhasil Diupdate');
    }
}