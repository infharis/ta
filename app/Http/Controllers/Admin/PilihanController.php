<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;
use App\Models\Admin\Kurikulum;

class PilihanController extends Controller
{

    public function index()
    {
        $result=DB::table('data_mk_pilihan')->get();
        return view('admin.mk_pilihan.view',compact('result'));
    }

    public function create()
    {
        $result=DB::table('data_mk_pilihan')->get();
        return view('admin.mk_pilihan.create',compact('result'));
    }

    public function store(Request $request)
    {
        $pl=new Kurikulum();
        $pl->data_mk_pilihan_id=$request->data_mk_pilihan_id;
        $pl->kode_mk=$request->kode_mk;
        $pl->nama_mk=$request->nama;
        $pl->bobot_tugas=$request->bobot_tugas;
        $pl->bobot_sks=$request->bobot_sks;
        $pl->unit=$request->unit;
        $pl->save();

        return redirect('kurikulum/mk_pilihan/view')->with('success','Data Berhasil Ditambahkan');
    }

    public function edit($id)
    {
        $result=Kurikulum::find($id);
        $dt=DB::table('data_mk_pilihan')->get();
        return view('admin.mk_pilihan.edit',compact('result','dt'));
    }

    public function update($id,Request $request)
    {
        $pl=Kurikulum::find($id);
        $pl->data_mk_pilihan_id=$request->data_mk_pilihan_id;
        $pl->kode_mk=$request->kode_mk;
        $pl->nama_mk=$request->nama;
        $pl->bobot_tugas=$request->bobot_tugas;
        $pl->bobot_sks=$request->bobot_sks;
        $pl->unit=$request->unit;
        $pl->save();

        return redirect('kurikulum/mk_pilihan/view')->with('success','Data Berhasil Diupdate');

    }

}