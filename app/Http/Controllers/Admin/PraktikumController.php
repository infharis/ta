<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;
use App\Models\Admin\Praktikum;

class PraktikumController extends Controller
{

    public function index()
    {
        $result=DB::table('kurikulum')->get();
        return view('admin.praktikum.view',compact('result'));
    }

    public function create()
    {
        $result=DB::table('kurikulum')->get();
        return view('admin.praktikum.create',compact('result'));
    }

    public function store(Request $request)
    {
        $pr=new Praktikum();
        $pr->mk_id=$request->mk_id;
        $pr->judul=$request->judul;
        $pr->jam=$request->jam;
        $pr->tempat=$request->tempat;
        $pr->save();

        return redirect('kurikulum/praktikum/view')->with('success','Data Berhasil Ditambahkan');
    }
    public function update($id,Request $request)
    {
        $pr=Praktikum::find($id);
        $pr->mk_id=$request->mk_id;
        $pr->judul=$request->judul;
        $pr->jam=$request->jam;
        $pr->tempat=$request->tempat;
        $pr->save();

        return redirect('kurikulum/praktikum/view')->with('success','Data Berhasil Diupdate');
    }

    public function edit($id)
    {
        $result=Praktikum::find($id);
        $mk=DB::table('kurikulum')->get();
        return view('admin.praktikum.edit',compact('result','mk'));
    }

    public function destroy(Request $request)
    {
        if(Praktikum::destroy($request->id))
        {
            echo 1;
        }else {
            echo 0;
        }
    }

}