<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\Models\Admin\DataPenelitian;

class PenelitianController extends Controller
{

    public function index()
    {
        $result=DB::table('sumber_pembiayaan')->get();
        $ts2=date("Y",strtotime("-3 year"));
        $ts1=date("Y",strtotime("-2 year"));
        $ts=date("Y",strtotime("-1 year"));
        return view('admin.penelitian.view',compact('result','ts2','ts1','ts'));
    }
    public function create()
    {
        $result=DB::table('jenis_dana')->get();
        return view('admin.penelitian.create',compact('result'));
    }

    public function store(Request $request)
    {
        $sp_id=DB::table('sumber_pembiayaan')->insertGetId(['nama_sumber_pembiayaan'=>$request->sp]);
        if($request->jd) {
            foreach ($request->jd as $value) {
                DB::table('jd_sp')->insert(['jd_id' => $value, 'sp_id' => $sp_id]);
            }
        }

        return redirect('penelitian/view')->with('success','Sumber Pembiayaan Berhasil Ditambah');
    }
    public function update($id,Request $request)
    {
        DB::table('jd_sp')->where('sp_id',$id)->delete();

        DB::table('sumber_pembiayaan')->where('id',$id)->update(['nama_sumber_pembiayaan'=>$request->sp]);
        if($request->jd) {
            foreach ($request->jd as $value) {
                DB::table('jd_sp')->insert(['jd_id' => $value, 'sp_id' => $id]);
            }
        }

        return redirect('penelitian/view')->with('success','Sumber Pembiayaan Berhasil Diupdate');
    }
    public function edit($id)
    {
        $jd=DB::table('jenis_dana')->get();
        $result=DB::table('sumber_pembiayaan')->where('id',$id)->first();

        // get all jenis dana
        $jd_sp=DB::table('jd_sp')->where('sp_id',$id)->get();
        // convert object to array
        $result_jd_sp = array_map(function ($value) {
            return $value->jd_id;
        }, $jd_sp);
        return view('admin.penelitian.edit',compact('jd','result','result_jd_sp'));
    }
    public function data_penelitian()
    {
        $result=DataPenelitian::all();
        return view('admin.penelitian.data_penelitian',compact('result'));
    }
}