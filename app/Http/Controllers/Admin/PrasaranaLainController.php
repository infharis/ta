<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;
use App\Models\Admin\PrasaranaLain;


class PrasaranaLainController extends Controller
{

    public function index()
    {
        $result=PrasaranaLain::all();
        return view('admin.prasarana_lain.view',compact('result'));
    }
    public function create()
    {
        return view('admin.prasarana_lain.create');
    }

    public function store(Request $request)
    {
        $pr=new PrasaranaLain();
        $pr->jenis_prasarana=$request->jenis_prasarana;
        $pr->jumlah_unit=$request->jumlah_unit;
        $pr->total_luas=$request->total_luas;
        $pr->unit_pengelola=$request->unit_pengelola;
        $pr->kepemilikan=$request->kepemilikan;
        $pr->kondisi=$request->kondisi;
        $pr->save();

        return redirect('data/prasarana_lain')->with('success','Data Berhasil Ditambahkan');

    }
    public function update($id,Request $request)
    {
        $pr=PrasaranaLain::find($id);
        $pr->jenis_prasarana=$request->jenis_prasarana;
        $pr->jumlah_unit=$request->jumlah_unit;
        $pr->total_luas=$request->total_luas;
        $pr->unit_pengelola=$request->unit_pengelola;
        $pr->kepemilikan=$request->kepemilikan;
        $pr->kondisi=$request->kondisi;
        $pr->save();

        return redirect('data/prasarana_lain')->with('success','Data Berhasil Diupdate');

    }

    public function edit($id)
    {
        $result=PrasaranaLain::find($id);
        return view('admin.prasarana_lain.edit',compact('result'));
    }

    public function destroy(Request $request)
    {
        if(PrasaranaLain::destroy($request->id))
        {
            echo 1;
        }else {
            echo 0;
        }
    }
}