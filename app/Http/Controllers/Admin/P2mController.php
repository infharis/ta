<?php
/**
 * Created by PhpStorm.
 * User: infharis
 * Date: 9/29/17
 * Time: 10:40 PM
 */

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;

class P2mController extends Controller
{


    public function jumlah_p2m()
    {
        $result=DB::table('sumber_pembiayaan_p2m')->get();
        $ts2=date("Y",strtotime("-3 year"));
        $ts1=date("Y",strtotime("-2 year"));
        $ts=date("Y",strtotime("-1 year"));
        return view('admin.p2m.jumlah_p2m',compact('result','ts2','ts1','ts'));
    }
    public function jumlah_p2m_create()
    {
        $result=DB::table('jenis_dana')->get();
        return view('admin.p2m.create',compact('result'));
    }
    public function jumlah_p2m_store(Request $request)
    {
        $sp_id=DB::table('sumber_pembiayaan_p2m')->insertGetId(['nama_sumber_pembiayaan'=>$request->sp]);
        if($request->jd) {
            foreach ($request->jd as $value) {
                DB::table('jd_sp_p2m')->insert(['jd_id' => $value, 'sp_id' => $sp_id]);
            }
        }
        return redirect('p2m/view/jumlah')->with('success','Data Berhasil Ditambahkan');
    }
    public function jumlah_p2m_update($id,Request $request)
    {
        DB::table('jd_sp_p2m')->where('sp_id',$id)->delete();

        DB::table('sumber_pembiayaan_p2m')->where('id',$id)->update(['nama_sumber_pembiayaan'=>$request->sp]);
        if($request->jd) {
            foreach ($request->jd as $value) {
                DB::table('jd_sp_p2m')->insert(['jd_id' => $value, 'sp_id' => $id]);
            }
        }

        return redirect('p2m/view/jumlah')->with('success','Sumber Pembiayaan Berhasil Diupdate');
    }
    public function jumlah_p2m_edit($id)
    {
        $jd=DB::table('jenis_dana')->get();
        $result=DB::table('sumber_pembiayaan_p2m')->where('id',$id)->first();

        // get all jenis dana
        $jd_sp=DB::table('jd_sp_p2m')->where('sp_id',$id)->get();
        // convert object to array
        $result_jd_sp = array_map(function ($value) {
            return $value->jd_id;
        }, $jd_sp);
        return view('admin.p2m.jumlah_p2m_edit',compact('jd','result','result_jd_sp'));
    }

    public function jumlah_p2m_mhs()
    {
        $result=DB::table('data_p2m')->get();
        return view('admin.p2m.p2m_mhs',compact('result'));
    }
    public function index()
    {
        $result=DB::table('sumber_pembiayaan')->get();
        return view('admin.p2m.view',compact('result'));
    }
    public function data_p2m()
    {
        $result=@DB::table('data_p2m')->get();
        return view('admin.p2m.data_p2m',compact('result'));
    }
}