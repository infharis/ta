<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;
use App\Models\Admin\Upaya;


class UpayaController extends Controller
{

    public function index()
    {
        $result=Upaya::all();
        return view('admin.upaya.view',compact('result'));
    }
    public function create()
    {
        return view('admin.upaya.create');
    }
    public function store(Request $request)
    {
        $up=new Upaya();
        $up->butir=$request->butir;
        $up->tindakan=$request->tindakan;
        $up->hasil=$request->hasil;
        $up->save();

        return redirect('bimbingan/upaya/view')->with('success','Data Berhasil Ditambahkan');
    }
    public function update($id,Request $request)
    {
        $up=Upaya::find($id);
        $up->butir=$request->butir;
        $up->tindakan=$request->tindakan;
        $up->hasil=$request->hasil;
        $up->save();

        return redirect('bimbingan/upaya/view')->with('success','Data Berhasil Diupdate');
    }
    public function edit($id)
    {
        $result=Upaya::find($id);
        return view('admin.upaya.edit',compact('result'));
    }

    public function destroy(Request $request)
    {
        if(Upaya::destroy($request->id))
        {
            echo 1;
        }else {
            echo 0;
        }
    }
    public function akademik()
    {
        $result=DB::table('dosen')->where('kategori_dosen','!=',5)->get();
        return view('admin.bimbingan.akademik',compact('result'));
    }
    public function ta()
    {
        $result=DB::table('dosen')->where('kategori_dosen','!=',5)->get();
        return view('admin.bimbingan.ta',compact('result'));
    }


}