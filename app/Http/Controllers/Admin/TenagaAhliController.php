<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;
use App\Models\Admin\TenagaAhli;


class TenagaAhliController extends Controller
{

   public function create()
   {
       return view('admin.tenaga_ahli.create');
   }

   public function index()
   {
       $result=TenagaAhli::all();
       return view('admin.tenaga_ahli.view',compact('result'));
   }

   public function store(Request $request)
   {
       $ta=new TenagaAhli();
       $ta->pakar=$request->pakar;
       $ta->judul=$request->judul;
       $ta->waktu=$request->waktu;
       $ta->save();

       return redirect('sdm/pakar/view')->with('success','Data Berhasil Ditambahkan');
   }
   public function edit($id)
   {
       $result=TenagaAhli::find($id);
       return view('admin.tenaga_ahli.edit',compact('result'));
   }

    public function update($id,Request $request)
    {
        $ta=TenagaAhli::find($id);
        $ta->pakar=$request->pakar;
        $ta->judul=$request->judul;
        $ta->waktu=$request->waktu;
        $ta->save();

        return redirect('sdm/pakar/view')->with('success','Data Berhasil Diupdate');
    }
    public function destroy(Request $request)
    {
        if(TenagaAhli::destroy($request->id))
        {
            echo 1;
        }else {
            echo 0;
        }
    }

}