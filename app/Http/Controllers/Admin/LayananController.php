<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;
use App\Models\Admin\Layanan;


class LayananController extends Controller
{
    public function index()
    {
        $result=Layanan::all();
        return view('admin.layanan.view',compact('result'));
    }
    public function create()
    {
        return view('admin.layanan.create');
    }

    public function store(Request $request)
    {
        $l=new Layanan();
        $l->jenis_pelayanan=$request->jenis_pelayanan;
        $l->bentuk=$request->bentuk;
        $l->save();

        return redirect('mhs/layanan')->with('success','Data Berhasil Ditambahkan');
    }

    public function edit($id)
    {
        $result=Layanan::find($id);
        return view('admin.layanan.edit',compact('result'));
    }
    public function update($id,Request $request)
    {
        $l=Layanan::find($id);
        $l->jenis_pelayanan=$request->jenis_pelayanan;
        $l->bentuk=$request->bentuk;
        $l->save();

        return redirect('mhs/layanan')->with('success','Data Berhasil Diupdate');
    }

    public function destroy(Request $request)
    {
        if(Layanan::destroy($request->id))
        {
            echo 1;
        }else {
            echo 0;
        }
    }

}