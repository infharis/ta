<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;
use App\Models\Admin\UmpanBalik;


class UmpanBalikController extends Controller
{

    public function index()
    {
        $result=UmpanBalik::all();
        return view('admin.umpan_balik.view',compact('result'));
    }

    public function create()
    {
        return view('admin.umpan_balik.create');
    }

    public function store(Request $request)
    {
        $ub=new UmpanBalik();
        $ub->umpan_balik=$request->umpan_balik;
        $ub->isi=$request->isi;
        $ub->tindak_lanjut=$request->tindak_lanjut;
        $ub->save();

        return redirect('umpan_balik/view')->with('success','Data Berhasil Ditambahkan');
    }
    public function update($id,Request $request)
    {
        $ub=UmpanBalik::find($id);
        $ub->umpan_balik=$request->umpan_balik;
        $ub->isi=$request->isi;
        $ub->tindak_lanjut=$request->tindak_lanjut;
        $ub->save();

        return redirect('umpan_balik/view')->with('success','Data Berhasil Diupdate');
    }
    public function edit($id)
    {
        $result=UmpanBalik::find($id);
        return view('admin.umpan_balik.edit',compact('result'));
    }
    public function destroy(Request $request)
    {
        if(UmpanBalik::destroy($request->id))
        {
            echo 1;
        }else {
            echo 0;
        }
    }

}