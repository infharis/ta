<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Http\Controllers\Controller;
use DB;
use App\Models\Admin\TenagaKependidikan;
use Illuminate\Http\Request;

class DataTenagaController extends Controller
{

    public function index()
    {
        $result=DB::table('data_tenaga_kependidikan')->get();
        return view('admin.kependidikan.view',compact('result'));
    }
    public function create()
    {
        $pt=DB::table('daftar_pendidikan')->get();
        $tk=DB::table('data_tenaga_kependidikan')->get();
        return view('admin.kependidikan.create',compact('pt','tk'));
    }

    public function store(Request $request)
    {
        $tk=new TenagaKependidikan();
        $tk->data_tenaga_kependidikan_id=$request->sebagai;
        $tk->pendidikan_id=$request->pendidikan;
        $tk->nama=$request->nama;
        $tk->tempat_lahir=$request->tempat_lahir;
        $tk->tgl_lahir=$request->tgl_lahir;
        $tk->alamat=$request->alamat;
        $tk->save();

        return redirect('sdm/kependidikan/view')->with('success','Data Berhasil Ditambahkan');
    }
    public function update($id,Request $request)
    {
        $tk=TenagaKependidikan::find($id);
        $tk->data_tenaga_kependidikan_id=$request->sebagai;
        $tk->pendidikan_id=$request->pendidikan;
        $tk->nama=$request->nama;
        $tk->tempat_lahir=$request->tempat_lahir;
        $tk->tgl_lahir=$request->tgl_lahir;
        $tk->alamat=$request->alamat;
        $tk->save();

        return redirect('sdm/kependidikan/view')->with('success','Data Berhasil Diupdate');
    }
    public function detail($id)
    {
        $result=TenagaKependidikan::where('data_tenaga_kependidikan_id',$id)->get();
        $single=DB::table('data_tenaga_kependidikan')->where('id',$id)->first();
        return view('admin.kependidikan.detail',compact('result','single'));
    }

    public function edit($id)
    {
        $pt=DB::table('daftar_pendidikan')->get();
        $tk=DB::table('data_tenaga_kependidikan')->get();
        $result=TenagaKependidikan::find($id);
        return view('admin.kependidikan.edit',compact('pt','tk','result'));

    }

    public function destroy(Request $request)
    {
        if(TenagaKependidikan::destroy($request->id))
        {
            echo 1;
        }else {
            echo 0;
        }
    }

}