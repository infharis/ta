<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use DB;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /*
  |--------------------------------------------------------------------------
  | ACL Methods
  |--------------------------------------------------------------------------
  */

    /**
     * Checks a Permission
     *
     * @param  String permission Slug of a permission (i.e: manage_user)
     * @return Boolean true if has permission, otherwise false
     */
    public function cann($permission = null)
    {
        return !is_null($permission) && $this->checkPermission($permission);
    }

    /**
     * Check if the permission matches with any permission user has
     *
     * @param  String permission slug of a permission
     * @return Boolean true if permission exists, otherwise false
     */
    protected function checkPermission($perm)
    {
//        $permissions = $this->getAllPernissionsFormAllRoles();
//
//        $permissionArray = is_array($perm) ? $perm : [$perm];
//
//        return count(array_intersect($permissions, $permissionArray));

        $permissions = $this->getAllPermissionsFormAllRoles();
        $permissionArray = is_array($perm) ? $perm : [$perm];
        return count(array_intersect([$permissions[0]['permission_slug']], $permissionArray));
    }

    /**
     * Get all permission slugs from all permissions of all roles
     *
     * @return Array of permission slugs
     */
    protected function getAllPermissionsFormAllRoles()
    {
//        $permissionsArray = [];
//
//        $permissions = $this->roles->load('permissions')->fetch('permissions')->toArray();
//
//        return array_map('strtolower', array_unique(array_flatten(array_map(function ($permission) {
//
//            return array_fetch($permission, 'permission_slug');
//
//        }, $permissions))));

        $permissionsArray = [];
        return $this->roles->load('permissions')->lists('permissions')->first()->toArray();
    }

    /*
    |--------------------------------------------------------------------------
    | Relationship Methods
    |--------------------------------------------------------------------------
    */

    /**
     * Many-To-Many Relationship Method for accessing the User->roles
     *
     * @return QueryBuilder Object
     */
    public function roles()
    {
        return $this->belongsToMany('App\Role');
    }
    public function dosen()
    {
        return $this->hasOne('App\Models\Admin\Dosen');
    }
    public function scopeCommon($query,$table,$statement)
    {

        $result=DB::table($table)->where($statement)->first();
        return $result;
    }
    public function scopeJenjangDos($query,$table,$statement)
    {

        $result=DB::table($table)->where($statement)->count();
        if($result) {
            return $result;
        }else {
            return ' ';
        }
    }
    public function scopeJabatanFungsional($query,$kategori_dosen,$jabatan_fungsional)
    {
        $statement=array('jabatan_id'=>$jabatan_fungsional,'kategori_dosen'=>$kategori_dosen);
        $result=DB::table('dosen')->where($statement)->count();
        if($result)
        {
            return $result;
        }else {
            return ' ';
        }
    }
    public function scopeCountTa($query,$dosen_id)
    {
            $p1=0;$p2=0;
            $p1=@DB::table('biodata_mhs')->where('doping1',$dosen_id)->count();
            $p2=@DB::table('biodata_mhs')->where('doping2',$dosen_id)->count();
            $result=$p1+$p2;
            if($result)
            {
                return $result;
            }else {
                return ' ';
            }
    }
    public function scopeCountPenguji($query,$dosen_id)
    {
            $p1=0;$p2=0;$p3=0;
            $p1=@DB::table('biodata_mhs')->where('penguji1',$dosen_id)->count();
            $p2=@DB::table('biodata_mhs')->where('penguji2',$dosen_id)->count();
            $p3=@DB::table('biodata_mhs')->where('penguji3',$dosen_id)->count();

            $result=$p1+$p2+$p3;
            if($result)
            {
                return $result;
            }else {
                return ' ';
            }
    }
    public function scopeDosen($query,$user_id)
    {
//        $count_mhs=0;$count_pen=0;$count_p2m=0;
//        $result=DB::table('dosen')->where('user_id',$user_id)->f
    }
}
