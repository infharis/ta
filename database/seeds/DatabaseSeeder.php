<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(SemesterTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(DaerahTableSeeder::class);
        $this->call(KurikulumDosenSDJDTableSeeder::class);
        $this->call(MhsTableSeeder::class);
       // $this->call(DosenTableSeeder::class);
    }
}
