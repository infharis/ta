<?php

use Illuminate\Database\Seeder;
use Faker\Factory;
use App\Models\Admin\Biodata;

class MhsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public $daftar_pekerjaan=array('Belum Bekerja','Kurang dari 1 Bulan','2 Bulan','3 Bulan','4 Bulan','5 Bulan','6 Bulan','7 s.d 12 bulan');
    public function run()
    {
        $limit=400;
        // nim mhs
        $kode_nim='081070100';
        $kode_thn=array(10,11,12,13,14,15,16,17,18,19);

        // kab dan prov
        $start_kab=1101;
        $end_kab=9471;
        $start_prov=11;
        $end_prov=94;

//        for($i=1;$i<=$limit;$i++)
//        {
//            $faker=Factory::create();
//
//            $bio_mhs=new Biodata();
//            $bio_mhs->nim=$kode_thn[rand(0,9)].$kode_nim.$i;
//            $bio_mhs->nama=$faker->name;
//            $bio_mhs->proposal=date("Y-m-d",strtotime("+".rand(1,3)."month +".rand(1,27)."days"));
//            $bio_mhs->sidang=date("Y-m-d",strtotime("+".rand(5,10)."month"));
//            $bio_mhs->save();
//        }
        // daftar waktu pekerjaan
        for($i=0;$i<count($this->daftar_pekerjaan);$i++)
        {
            DB::table('daftar_waktu_pekerjaan')->insert(['waktu'=>$this->daftar_pekerjaan[$i]]);
        }
    }
}
