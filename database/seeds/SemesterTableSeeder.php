<?php

use Illuminate\Database\Seeder;
use App\Models\Admin\Semester;

class SemesterTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public $sm=array('I','II','III','IV','V','VI','VII','VIII');
    public $dp=array('S3','S2','S1','D4','D3','D2','D1','SMA/SMK','SMP','SD');
    public $jf=array('Guru Besar','Lektor Kepala','Lektor','Asisten Ahli','Tenaga Pengajar');
    public $dtp=array('Pustakawan','Laboran/ Teknisi/ Analis/ Operator/ Programer','Administrasi','	Lainnya...');
    public function run()
    {

        for($i=0;$i<=7;$i++)
        {
            $sm=new Semester();
            $sm->semester=$this->sm[$i];
            $sm->save();

        }

        // daftar pendidikan
        for ($i=0;$i<10;$i++)
        {
            DB::table('daftar_pendidikan')->insert(['nama_pendidikan'=>$this->dp[$i]]);
        }

        // jabatan fungsional
        for ($i=0;$i<5;$i++)
        {
            DB::table('jabatan_fungsional')->insert(['nama_jabatan'=>$this->jf[$i]]);
        }

        // data tenaga kependidikan
        for ($i=0;$i<4;$i++)
        {
            DB::table('data_tenaga_kependidikan')->insert(['nama'=>$this->dtp[$i]]);
        }

        DB::table('data_mk_pilihan')->insert(['semester'=>'Ganjil']);
        DB::table('data_mk_pilihan')->insert(['semester'=>'Genap']);

        $jenjang=array('S1','S2','S3');
        for($i=0;$i<count($jenjang);$i++)
        {
            DB::table('jenjang')->insert(['nama_jenjang'=>$jenjang[$i]]);
        }
    }
}
