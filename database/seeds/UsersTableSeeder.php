<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    public $pass='123456';
    // kode 1 = dosen tetap sesuai bidang ps
    // kode 2 = dosen tetap diluar ps
    // kode 3 = dosen tidak tetap sesuai bidang ps
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // account admin
        $user=new User;
        $user->name='administrator';
        $user->email='infharis@gmail.com';
        $user->password=bcrypt($this->pass);
        $user->save();




        DB::table('role_user')->insert(array(
            array('role_id'=>1,'user_id'=>1),
            array('role_id'=>2,'user_id'=>2),
            array('role_id'=>3,'user_id'=>3),
            array('role_id'=>4,'user_id'=>4),
        ));
        DB::table('roles')->insert(array(
            array('role_title'=>'Admin','role_slug'=>'admin'),
            array('role_title'=>'dosen1','role_slug'=>'dosen'),
            array('role_title'=>'dosen2','role_slug'=>'dosen'),
            array('role_title'=>'dosen3','role_slug'=>'dosen'),
        ));

        DB::table('permission_role')->insert(array(
            array('role_id'=>1,'permission_id'=>1),
            array('role_id'=>2,'permission_id'=>2),
            array('role_id'=>3,'permission_id'=>3),
            array('role_id'=>4,'permission_id'=>4),
        ));
        DB::table('permissions')->insert(array(
            array('permission_title'=>'Admin','permission_slug'=>'admin','permission_description'=>'area admin'),
            array('permission_title'=>'dosen1','permission_slug'=>'dosen','permission_description'=>'dosen tetap sesuai ps'),
            array('permission_title'=>'dosen2','permission_slug'=>'dosen','permission_description'=>'dosen tetap diluar ps'),
            array('permission_title'=>'dosen3','permission_slug'=>'dosen','permission_description'=>'dosen tidak tetap'),
        ));


    }
}