<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Models\Admin\Dosen;
use App\Models\Admin\Kurikulum;

class KurikulumDosenSDJDTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public $password='123456';
    public $daftar_nama_dosen=array('Viska Mutiawani, S.Sn, M.IT','Razief Perucha Fauzie Afidh, M.Sc',
                                    'Dr. Muhammad Subianto, S.Si, M.Si','Irvanizam Zamanhuri, S.Si, M.Sc',
        'Kurnia Saputra, S.T, M.Sc','Dalila Husna Yunardi, B.Sc, M.Sc','Amalia Mabrina Masbar Rus, MBIS',
        'Nazaruddin, S.Si, M.Eng.Sc','Zulfan, S.Si, M.Sc','Dr. Taufik Fuadi Abidin, M.Tech','Dr. Nizamuddin, M.Info.Sc',
        'Arie Budiansyah, M.Eng','Zahnur, M.InfoTech'
    );
    public $daftar_email_dosen=array('198008312009122003','198408062012121002','196812111994031005',
        '198103152003121003','198003262014041001','199006172015042001','198905052015042002','197202061997021001',
        '198606022015041003','197010081994031002','197108241996031001','197808152010121002','196905291994031002'
    );

    public $adm=array('lia','fitri');
    public $role_dosen=2;
    public function run()
    {

            for($i=0;$i<count($this->daftar_nama_dosen);$i++)
            {
                // data aku user
                $user = new User();
                $user->name = $this->daftar_nama_dosen[$i];
                $user->email = $this->daftar_email_dosen[$i];
                $user->password = Hash::make($this->password);
                $user->save();

                // data dosen
                $ds = new Dosen();
                $ds->user_id = $user->id;
                $ds->nama_dosen = $this->daftar_nama_dosen[$i];
                $ds->nip = $this->daftar_email_dosen[$i];
                $ds->sertifikasi = 'N';
                $ds->kategori_dosen =2;
                $ds->save();

                $user=$user->id;
                $this->role_permi($user,$this->role_dosen);

            }

            // administrator
            $user = new User();
            $user->name = 'Fitria rambe, S.E';
            $user->email = '198602072009012101';
            $user->password = Hash::make($this->password);
            $user->save();
            $this->role_permi($user->id,1);

            $user = new User();
            $user->name = 'Liya Novita, S.Kom ';
            $user->email = '198911282014012101';
            $user->password = Hash::make($this->password);
            $user->save();
            $this->role_permi($user->id,1);

            // kurikulum kuliah
//        DB::table('kurikulum')->insert(['semester_id'=>1,'kode_mk'=>'USK - 004',
//            'nama_mk'=>'Pengantar Aplikasi Komputer','bobot_sks'=>3,'inti'=>'Y','bobot_tugas'=>'Y',
//            'deskripsi'=>'Y','silabus'=>'Y','sap'=>'Y','wajib'=>'Y'
//        ]);
//            DB::table('kurikulum')->insert(['semester_id'=>1,'kode_mk'=>'MPA - 003',
//            'nama_mk'=>'Statistika Dasar','bobot_sks'=>3,'inti'=>'Y','bobot_tugas'=>'Y',
//            'deskripsi'=>'Y','silabus'=>'Y','sap'=>'Y','wajib'=>'Y'
//        ]);

            // sumber dan jenis dana
        $sd_id=DB::table('sumber_dana')->insertGetId(['nama_sumber_dana'=>'PT Sendiri']);
        DB::table('jenis_dana')->insert(['sumber_dana_id'=>$sd_id,'nama_jenis_dana'=>'SPP']);
        DB::table('jenis_dana')->insert(['sumber_dana_id'=>$sd_id,'nama_jenis_dana'=>'BJM Unsyiah']);
        DB::table('jenis_dana')->insert(['sumber_dana_id'=>$sd_id,'nama_jenis_dana'=>'Dana Sumbangan Pembangunan Institusi (SPI)']);
    }

    public function role_permi($user_id,$role)
    {
        $role_id = DB::table('role_user')->insertGetId(['user_id' => $user_id, 'role_id' => $role]);
        DB::table('permission_role')->insert(['permission_id' => $role, 'role_id' => $role_id]);

    }
}
