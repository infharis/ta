<?php

use Illuminate\Database\Seeder;
use Maatwebsite\Excel\Facades\Excel;

class DaerahTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public $prov_id;
    public $kab_id;
    public function run()
    {
        // provinsi
        DB::table('prov')->insert(['id'=>11,'nama_provinsi'=>'ACEH']);

        $provinsi=Excel::load('storage/app/data_daerah/provinces.csv', function($reader) {
            $sheet='provinces';
            $reader->each(function($sheet) {

                // Loop through all rows
                $i=1;
                foreach ($sheet as $row)
                {
                    if($i==1) {
                        $this->prov_id=DB::table('prov')->insertGetId(['id'=>$row]);
                        $i++;
                    }elseif($i==2)
                    {
                        DB::table('prov')->where('id',$this->prov_id)->update(['nama_provinsi'=>$row]);
                        $i=1;
                    }
                }

            });

        });
        // end provinsi

        // kab
        DB::table('kab')->insert(['id'=>1101,'prov_id'=>11,'nama_kab'=>'KAB. SIMEULUE']);
        $kab=Excel::load('storage/app/data_daerah/regencies.csv', function($reader) {
            $sheet='regencies';
            $reader->each(function($sheet) {

                // Loop through all rows
                $i=1;
                foreach ($sheet as $row)
                {
                    if($i==1) {
                        $this->kab_id=DB::table('kab')->insertGetId(['id'=>$row]);
                        $i++;
                    }elseif($i==2)
                    {
                        DB::table('kab')->where('id',$this->kab_id)->update(['prov_id'=>$row]);
                        $i++;
                    }elseif($i==3)
                    {
                        DB::table('kab')->where('id',$this->kab_id)->update(['nama_kab'=>$row]);
                        $i=1;
                    }
                }

            });

        });
        // end kab
    }
}
