<?php

use Illuminate\Database\Seeder;
use App\Models\Admin\Dosen;
use Faker\Factory;
use App\User;

class DosenTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public $password='123456';
    public $kategori_dosen=array(2,3,4,5);
    public function run()
    {
        $limit=400;
        for($i=1;$i<=$limit;$i++)
        {
            $faker=Factory::create();
            if($i<=100)
            {
                // data dosen
                $ds = new Dosen();
                $ds->nama_dosen = $faker->name;
                $ds->nip = $faker->creditCardNumber;
                $ds->sertifikasi = 'N';
                $ds->kategori_dosen = $this->kategori_dosen[3];
                $ds->save();

            }else {
                // akun dosen
                $nama_dosen=$faker->name;
                $user = new User();
                $user->name = $nama_dosen;
                $user->email = $faker->email;
                $user->password = Hash::make($this->password);
                $user->save();

                // rule dosen
                if($i<=200) {
                    $role=$this->kategori_dosen[0];
                    $this->role_permi($user->id,$role);
                }elseif($i<=300)
                {
                    $role=$this->kategori_dosen[1];
                    $this->role_permi($user->id,$role);
                }elseif($i<=400)
                {
                    $role=$this->kategori_dosen[2];
                    $this->role_permi($user->id,$role);
                }
                // data dosen
                $ds = new Dosen();
                $ds->user_id = $user->id;
                $ds->nama_dosen = $faker->nama_dosen;
                $ds->nip = $faker->creditCardNumber;
                $ds->sertifikasi = 'N';
                $ds->kategori_dosen = $role;
                $ds->save();

            }

        }
    }

    public function role_permi($user_id,$role)
    {
        $role_id = DB::table('role_user')->insertGetId(['user_id' => $user_id, 'role_id' => $role]);
        DB::table('permission_role')->insert(['permission_id' => $role, 'role_id' => $role_id]);

    }
}
