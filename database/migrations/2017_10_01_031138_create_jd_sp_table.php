<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJdSpTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // jenis dana, sumber pembiayaan, ('PT Bersangkutan')
        Schema::create('jd_sp', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('jd_id');
            $table->integer('sp_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('jd_sp');
    }
}
