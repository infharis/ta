<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMkPilihan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mk_pilihan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('data_mk_pilihan_id');
            $table->string('	kode_mk');
            $table->string('nama');
            $table->integer('bobot_sks');
            $table->enum('bobot_tugas',array('Y','N'));
            $table->string('unit');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mk_pilihan');
    }
}
