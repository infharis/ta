<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTenagaKependidikanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenaga_kependidikan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('data_tenaga_kependidikan_id');
            $table->integer('pendidikan_id');
            $table->string('nama',50);
            $table->string('tempat_lahir',50);
            $table->date('tgl_lahir');
            $table->string('alamat',100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tenaga_kependidikan');
    }
}
