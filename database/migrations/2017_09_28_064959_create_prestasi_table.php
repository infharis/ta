<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrestasiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prestasi_mhs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('mhs_id');
            $table->string('nama_kegiatan');
            $table->string('prestasi',50);
            $table->char('tahun',4);
            $table->string('softcopy');
            $table->enum('tingkat',array('L','R','N','I'));
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('prestasi_mhs');
    }
}
