<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSilabusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('silabus', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('mk_id');
            $table->string('status_mk');
            $table->char('berlaku',4);
            $table->enum('silabus',array('Y','N'));
            $table->enum('buku_ajar',array('Y','N'));
            $table->text('alasan');
            $table->text('usulan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('silabus');
    }
}
