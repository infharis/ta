<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataAktivitasDosen extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_aktivitas_dosen', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('dosen_id');
            $table->integer('mk_id');
            $table->integer('jumlah_kelas');
            $table->integer('jumlah_pertemuan');
            $table->integer('jumlah_dilaksanakan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('data_aktivitas_dosen');
    }
}
