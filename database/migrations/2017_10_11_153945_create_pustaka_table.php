<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePustakaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pustaka', function (Blueprint $table) {
            $table->increments('id');
            $table->string('jenis_pustaka');
            $table->integer('jumlah_judul');
            $table->integer('jumlah_copy');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pustaka');
    }
}
