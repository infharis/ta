<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDosen extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // kategori dosen
        // 2 => dosen tetap sesuai ps, 3 => dosen tetap diluar ps, 4 => dosen tidak tetap, 5 => dosen diluar prodi
        Schema::create('dosen', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('jabatan_id');
            $table->integer('pendidikan_id');
            $table->string('nidn',30);
            $table->string('nip',30);
            $table->string('bidang',50);
            $table->string('golongan',100);
            $table->string('nama_dosen');
            $table->string('tempat',50);
            $table->date('tgl_lahir');
            $table->enum('sertifikasi',array('Y','N'));
            $table->enum('kategori_dosen',array('2','3','4','5'));
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dosen');
    }
}
