<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePendidikan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pendidikan_dosen', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('dosen_id');
            $table->integer('jenjang_id');
            $table->string('gelar',20);
            $table->string('pendidikan',150);
            $table->string('bidang',150);
            $table->string('kota',150);
            $table->string('softcopy');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pendidikan_dosen');
    }
}
