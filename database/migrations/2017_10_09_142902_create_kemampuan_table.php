<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKemampuanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kemampuan_dosen', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('dosen_id');
            $table->string('jenjang');
            $table->string('bidang');
            $table->string('perguruan');
            $table->string('negara');
            $table->char('tahun',4);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('kemampuan_dosen');
    }
}
