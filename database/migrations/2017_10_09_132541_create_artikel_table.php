<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArtikelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artikel', function (Blueprint $table) {
            $table->increments('id');
            $table->text('judul');
            $table->text('dihasilkan');
            $table->char('tahun',4);
            $table->enum('tingkat',array('L','N','I'));
            $table->enum('jurnal',array('L','N','I'));
            $table->enum('seminar',array('L','N','I'));
            $table->enum('buku',array('Y','N'));
            $table->string('softcopy');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('artikel');
    }
}
