<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKurikulum extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kurikulum', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('semester_id')->default(1);
            $table->integer('data_mk_pilihan_id')->default(0);
            $table->char('tahun',4)->default(0);
            $table->char('kode_mk',15);
            $table->string('nama_mk',50);
            $table->integer('bobot_sks');
            $table->enum('inti',array('Y','N'));
            $table->enum('bobot_tugas',array('Y','N'));
            $table->enum('deskripsi',array('Y','N'));
            $table->enum('silabus',array('Y','N'));
            $table->enum('sap',array('Y','N'));
            $table->string('unit',15);
            $table->enum('wajib',array('Y','N'));
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('kurikulum');
    }
}
