<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePenelitanSumberDana extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sumber_penelitian_p2m', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('data_penelitian_id');
            $table->integer('data_p2m_id');
            $table->integer('jenis_dana_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sumber_penelitian_p2m');
    }
}
