<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrestasi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prestasi_dosen', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('dosen_id');
            $table->text('judul_prestasi');
            $table->char('waktu');
            $table->enum('tingkat',array('L','N','I'));
            $table->string('softcopy');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('prestasi_dosen');
    }
}
