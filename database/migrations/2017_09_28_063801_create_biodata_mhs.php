<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBiodataMhs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('biodata_mhs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nim',15)->unique();
            $table->integer('dosen_id');
            $table->integer('prov_id')->nullable();
            $table->integer('kab_id')->nullable();
            $table->string('nama',100);
            $table->string('jenis_kelamin',15);
            $table->string('asal_sekolah',100);
            $table->char('tahun_masuk',4)->default('NULL');
            $table->char('tahun_keluar',4)->default('NULL');
            $table->enum('status',array('R','T'));
            $table->enum('beasiswa',array('Y','N'));
            $table->string('sumber_beasiswa',100);
            $table->integer('toefl');
            $table->string('email');
            $table->integer('dapat_pekerjaan');
            $table->decimal('gaji',12,2);
            $table->string('pekerjaan');
            $table->string('instansi');
            $table->date('proposal');
            $table->date('hasil');
            $table->date('sidang');
            $table->decimal('ipk',15,2);
            $table->integer('doping1')->default(0);
            $table->integer('doping2')->default(0);
            $table->integer('penguji1')->default(0);
            $table->integer('penguji2')->default(0);
            $table->integer('penguji3')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('biodata_mhs');
    }
}
