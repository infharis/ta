<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKerjasamaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kerjasama', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama_instansi');
            $table->string('jenis_kegiatan');
            $table->char('start',4);
            $table->char('end',4);
            $table->text('manfaat');
            $table->enum('kerjasama',array('L','D'));
            $table->string('softcopy');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('kerjasama');
    }
}
