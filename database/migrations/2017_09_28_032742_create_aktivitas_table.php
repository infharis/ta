<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAktivitasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aktivitas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('dosen_id');
            $table->integer('ps_sendiri');
            $table->integer('ps_lain');
            $table->integer('pt_lain');
            $table->integer('sks_penelitian');
            $table->integer('sks_p2m');
            $table->integer('m_pt_sendiri');
            $table->integer('m_pt_lain');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('aktivitas');
    }
}
