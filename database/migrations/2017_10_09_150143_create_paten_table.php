<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paten', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('dosen_id');
            $table->text('karya');
            $table->text('no_hki');
            $table->string('softcopy');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('paten');
    }
}
