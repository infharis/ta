<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeralatanLabTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('peralatan_lab', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('data_lab_id');
            $table->string('jenis_peralatan');
            $table->integer('jumlah_unit');
            $table->enum('kepemilikan',array('SD','SW'));
            $table->enum('kondisi',array('Y','N'));
            $table->char('waktu',4);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('peralatan_lab');
    }
}
