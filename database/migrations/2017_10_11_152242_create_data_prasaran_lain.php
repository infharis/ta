<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataPrasaranLain extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_prasarana_lain', function (Blueprint $table) {
            $table->increments('id');
            $table->string('jenis_prasarana');
            $table->integer('jumlah_unit');
            $table->integer('total_luas');
            $table->enum('kepemilikan',array('SD','SW'));
            $table->enum('kondisi',array('Y','N'));
            $table->string('unit_pengelola',30);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('data_prasarana_lain');
    }
}
