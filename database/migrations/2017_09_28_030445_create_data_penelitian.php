<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataPenelitian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_penelitian', function (Blueprint $table) {
            $table->increments('id');
//            $table->integer('jumlah_mhs');
            $table->text('judul_penelitian');
            $table->float('jumlah_dana',20,2);
            $table->char('tahun_penelitian',4);
            $table->string('softcopy');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('data_penelitian');
    }
}
