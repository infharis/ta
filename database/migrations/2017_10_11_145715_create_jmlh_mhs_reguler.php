<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJmlhMhsReguler extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jumlah_mhs_reguler', function (Blueprint $table) {
            $table->increments('id');
            $table->char('tahun_masuk',4);
            $table->integer('ts6');
            $table->integer('ts5');
            $table->integer('ts4');
            $table->integer('ts3');
            $table->integer('ts2');
            $table->integer('ts1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('jumlah_mhs_reguler');
    }
}
