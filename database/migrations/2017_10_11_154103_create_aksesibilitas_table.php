<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAksesibilitasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aksesibilitas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('jenis_data');
            $table->enum('manual',array('Y','N'));
            $table->enum('komputer',array('Y','N'));
            $table->enum('lan',array('Y','N'));
            $table->enum('wan',array('Y','N'));
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('aksesibilitas');
    }
}
