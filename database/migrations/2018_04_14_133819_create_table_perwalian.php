<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePerwalian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('perwalian_mhs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nip_dosen',30);
            $table->string('nim_mhs',14);
            $table->char('tahun',4);
            $table->integer('semester');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('perwalian_mhs');
    }
}
