<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataPelacakanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_pelacakan', function (Blueprint $table) {
            $table->increments('id');
            $table->string('jenis_kemampuan');
            $table->integer('sangat_baik');
            $table->integer('baik');
            $table->integer('cukup');
            $table->integer('kurang');
            $table->string('rencana');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('data_pelacakan');
    }
}
