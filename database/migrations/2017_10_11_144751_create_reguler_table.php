<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegulerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mhs_reguler', function (Blueprint $table) {
            $table->increments('id');
            $table->char('tahun_akademik',4);
            $table->integer('daya_tampung');
            $table->integer('ikut_seleksi');
            $table->integer('lulus_seleksi');
            $table->integer('m_reguler');
            $table->integer('m_transfer');
            $table->integer('t_reguler');
            $table->integer('t_transfer');
            $table->integer('l_reguler');
            $table->integer('l_transfer');
            $table->char('ipk_min',11);
            $table->char('ipk_rata',11);
            $table->char('ipk_mak',11);
            $table->integer('p_rendah');
            $table->integer('p_sedang');
            $table->integer('p_tinggi');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mhs_reguler');
    }
}
