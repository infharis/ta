<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMhsNonRegulerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mhs_non_reguler', function (Blueprint $table) {
            $table->increments('id');
            $table->char('tahun_akademik',4);
            $table->integer('daya_tampung');
            $table->integer('ikut_seleksi');
            $table->integer('lulus_seleksi');
            $table->integer('non_reguler');
            $table->integer('transfer');
            $table->integer('t_non_reguler');
            $table->integer('t_transfer');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mhs_non_reguler');
    }
}
