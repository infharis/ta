<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateP2m extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_p2m', function (Blueprint $table) {
            $table->increments('id');
            $table->text('judul_p2m');
            $table->float('jumlah_dana',20,2);
            $table->char('tahun_p2m',4);
            $table->string('softcopy');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('data_p2m');
    }
}
