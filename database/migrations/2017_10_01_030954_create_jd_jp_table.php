<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJdJpTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // jenis dana, jenis penggunaa ('pendidikan')
        Schema::create('jd_jp', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('jd_id');
            $table->integer('jp_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('jd_jp');
    }
}
