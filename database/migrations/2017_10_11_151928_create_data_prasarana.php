<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataPrasarana extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_prasarana', function (Blueprint $table) {
            $table->increments('id');
            $table->string('jenis_prasarana');
            $table->integer('total_luas');
            $table->integer('jumlah_unit');
            $table->enum('kepemilikan',array('SD','SW'));
            $table->enum('kondisi',array('Y','N'));
            $table->string('utilisasi',50);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('data_prasarana');
    }
}
