@extends('dosen.layouts.index')
@section('css')
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/select2/css/select2.min.css') !!}" rel="stylesheet" type="text/css" />
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/select2/css/select2-bootstrap.min.css')!!}" rel="stylesheet" type="text/css" />
@stop
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">@include('notif')
        <!-- END PAGE BREADCRUMB -->
            <div class="row ">
                <div class="col-md-12">
                    <!-- BEGIN SAMPLE FORM PORTLET-->
                    <div class="portlet box purple ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-gift"></i> Form Biodata Diri
                            </div>
                            <div class="pull-right" style="padding-top: 0.5%;">
                                <a href="{!! URL::to('dosen') !!}" class="btn btn-sm yellow dropdown-toggle">Back</a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <form role="form" action="{!! URL::to('dosen/biodata_diri') !!}" method="post">
                                {!! csrf_field() !!}
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="">Nama Dosen</label>
                                        <input type="text" class="form-control" name="nama" value="{!! Auth::user()->dosen->nama_dosen !!}">
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="">Bidang Keahlian</label>
                                        <input type="text" class="form-control" name="bidang" value="{!! Auth::user()->dosen->bidang !!}">
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="">NIDN</label>
                                        <input type="text" class="form-control" name="nidn" value="{!! Auth::user()->dosen->nidn !!}">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="">NIP</label>
                                        <input type="text" class="form-control" name="nip" value="{!! Auth::user()->dosen->nip !!}">
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="">Tempat Lahir</label>
                                        <input type="text" class="form-control" name="tempat_lahir" value="{!! Auth::user()->dosen->tempat !!}">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="">Tgl Lahir</label>
                                        <input type="date" class="form-control" name="tgl_lahir"  value="{!! Auth::user()->dosen->tgl_lahir !!}">
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label> Sertifikasi</label>
                                        <div class="md-radio-inline">
                                                <div class="md-radio">
                                                    <input @if(Auth::user()->dosen->sertifikasi=='Y') checked @endif type="radio" id="checkbox1_111" name="sertifikasi" value="Y" class="md-radiobtn">
                                                    <label for="checkbox1_111">
                                                        <span></span>
                                                        <span class="check"></span>
                                                        <span class="box"></span> Yes </label>
                                                </div>
                                            <div class="md-radio">
                                                    <input @if(Auth::user()->dosen->sertifikasi=='N') checked @endif type="radio" id="checkbox1_112" name="sertifikasi" value="N" class="md-radiobtn">
                                                    <label for="checkbox1_112">
                                                        <span></span>
                                                        <span class="check"></span>
                                                        <span class="box"></span> No </label>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="">Jabatan Akademik</label>
                                            <select id="multiple" name="jabatan" class="form-control select2-multiple">
                                                <option value="0">--Pilih Jabatan Anda ---</option>
                                                @foreach($jb as $value)
                                                    @if(Auth::user()->dosen->jabatan_id==$value->id)
                                                        <option selected value="{!! $value->id !!}">{!! $value->nama_jabatan !!}</option>
                                                    @else
                                                        <option value="{!! $value->id !!}">{!! $value->nama_jabatan !!}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label> Pendidikan Terakhir</label>
                                        <div class="md-radio-inline">
                                            @foreach($pt as $val)
                                                <div class="md-radio">
                                                    <input @if(Auth::user()->dosen->pendidikan_id==$val->id) checked @endif type="radio" id="checkbox1_{!! $val->id !!}" name="pendidikan" value="{!! $val->id !!}" class="md-radiobtn">
                                                    <label for="checkbox1_{!! $val->id !!}">
                                                        <span></span>
                                                        <span class="check"></span>
                                                        <span class="box"></span> {!! $val->nama_pendidikan !!} </label>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>

                                <!-- /.row -->
                                <br>
                                <input type="submit" class="btn btn-primary" value="Simpan">
                            </form>
                        </div>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
@stop
@section('js')
    <script src="{!! URL::to('local/resources/assets/assets/global/plugins/select2/js/select2.full.min.js') !!}" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="{!! URL::to('local/resources/assets/assets/global/scripts/app.min.js')!!}" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{!! URL::to('local/resources/assets/assets/pages/scripts/components-select2.min.js')!!}" type="text/javascript"></script>

@stop