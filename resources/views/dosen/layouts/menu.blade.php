<div class="page-sidebar navbar-collapse collapse">
    <ul {!! Request::is('dosen/data_master/show_form') ? 'class="page-sidebar-menu page-sidebar-menu-hover-submenu page-sidebar-menu-closed"' : 'class="page-sidebar-menu page-sidebar-menu-hover-submenu"'!!} data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
        <li class="nav-item {!! Request::is('dosen') ? 'active open' : '' !!}">
            <a href="{!! URL::to('dosen') !!}" class="nav-link nav-toggle">
                <i class="icon-home"></i>
                <span class="title">Dashboard</span>
                <span class="arrow"></span>
            </a>
        </li>

        <li class="nav-item {!! Request::is('dosen/data_master*') ? 'active open' : '' !!}">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-folder"></i>
                <span class="title">Data Diri</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item {!! Request::is('dosen/data_master/show_form') ? 'active open' : '' !!}">
                    <a href="{!! URL::to('dosen/data_master/show_form') !!}" class="nav-link ">
                        {{--<i class="icon-user"></i>--}}
                        <span class="title">Add</span>
                    </a>
                </li>
                <li class="nav-item {!! Request::is('kegiatan/list') ? 'active open' : '' !!} ">
                    <a href="{!! URL::to('dosen/data_master/') !!}" class="nav-link ">
                        {{--<i class="icon-user"></i>--}}
                        <span class="title">View Data Dosen</span>
                    </a>
                </li>
            </ul>
        </li>

    </ul>
    <!-- END SIDEBAR MENU -->
</div>