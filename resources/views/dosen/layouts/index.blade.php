<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Dashboard | SIBA</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link href={!! URL::to('local/resources/assets/assets/global/plugins/font-awesome/css/font-awesome.min.css') !!} rel="stylesheet" type="text/css" />
    <link href={!! URL::to('local/resources/assets/assets/global/plugins/simple-line-icons/simple-line-icons.min.css')!!} rel="stylesheet" type="text/css" />
    <link href={!! URL::to('local/resources/assets/assets/global/plugins/bootstrap/css/bootstrap.min.css')!!} rel="stylesheet" type="text/css" />
    <link href={!! URL::to('local/resources/assets/assets/global/plugins/uniform/css/uniform.default.css')!!} rel="stylesheet" type="text/css" />
    <link href={!! URL::to('local/resources/assets/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css')!!} rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    vi
    <!-- END PAGE LEVEL PLUGINS -->
    @yield('css')
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href={!! URL::to('local/resources/assets/assets/global/css/components-rounded.min.css')!!} rel="stylesheet" id="style_components" type="text/css" />
    <link href={!! URL::to('local/resources/assets/assets/global/css/plugins.min.css') !!} rel="stylesheet" type="text/css" />
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href={!! URL::to('local/resources/assets/assets/layouts/layout4/css/layout.min.css')!!} rel="stylesheet" type="text/css" />
    <link href={!! URL::to('local/resources/assets/assets/layouts/layout4/css/themes/light.min.css')!!} rel="stylesheet" type="text/css" id="style_color" />
    <link href={!! URL::to('local/resources/assets/assets/layouts/layout4/css/custom.min.css')!!} rel="stylesheet" type="text/css" />
    <!-- END THEME LAYOUT STYLES -->
    <link rel="shortcut icon" href="favicon.ico" /> </head>
<!-- END HEAD -->

<body  class="page-container-bg-solid page-sidebar-closed-hide-logo page-header-fixed page-footer-fixed page-sidebar-closed">
<!-- BEGIN HEADER -->
    @include('dosen.layouts.header')
<!-- END HEADER -->
<!-- BEGIN HEADER & CONTENT DIVIDER -->
<div class="clearfix"> </div>
<!-- END HEADER & CONTENT DIVIDER -->
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN SIDEBAR -->
    <div class="page-sidebar-wrapper">
        <!-- BEGIN SIDEBAR -->
        {{--@include('dosen.layouts.menu')--}}
        <!-- END SIDEBAR -->
    </div>
    <!-- END SIDEBAR -->
    @yield('content')

</div>
<!-- END CONTAINER -->
    @include('dosen.layouts.footer')
<!--[if lt IE 9]>
<!--<script src={!! URL::to('local/resources/assets/assets/global/plugins/respond.min.js') !!}></script>-->
<!--<script src={!! URL::to('local/resources/assets/assets/global/plugins/excanvas.min.js') !!}></script>-->
<![endif]-->
<!-- BEGIN CORE PLUGINS -->
<script src={!! URL::to('local/resources/assets/assets/global/plugins/jquery.min.js')!!} type="text/javascript"></script>
<script src={!! URL::to('local/resources/assets/assets/global/plugins/bootstrap/js/bootstrap.min.js')!!} type="text/javascript"></script>
<script src={!! URL::to('local/resources/assets/assets/global/plugins/js.cookie.min.js') !!} type="text/javascript"></script>
<script src={!! URL::to('local/resources/assets/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js')!!} type="text/javascript"></script>
<script src={!! URL::to('local/resources/assets/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')!!} type="text/javascript"></script>
<script src={!! URL::to('local/resources/assets/assets/global/plugins/jquery.blockui.min.js')!!} type="text/javascript"></script>
<script src={!! URL::to('local/resources/assets/assets/global/plugins/uniform/jquery.uniform.min.js')!!} type="text/javascript"></script>
<script src={!! URL::to('local/resources/assets/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js')!!} type="text/javascript"></script>

@yield('js')

<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src={!! URL::to('local/resources/assets/assets/global/scripts/app.min.js')!!} type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->

<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src={!! URL::to('local/resources/assets/assets/layouts/layout4/scripts/layout.min.js')!!} type="text/javascript"></script>

</body>

</html>
