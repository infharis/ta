@extends('dosen.layouts.index')
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">@include('notif')

            <!-- END PAGE BREADCRUMB -->
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light portlet-fit bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class=" icon-layers font-green"></i>
                                <span class="caption-subject font-green bold uppercase">Kelompok Data Dosen</span>
                            </div>
                            {{--<div class="pull-right">--}}
                                {{--<a href="{!! URL::to('dosen/data_master/show_form') !!}" class="btn btn-primary">Form Dosen</a>--}}
                            {{--</div>--}}
                            <div class="pull-right">
                                <a href="{!! URL::to('dosen/biodata_diri') !!}" class="btn btn-primary">Biodata Diri</a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="mt-element-step">
                                <div class="row step-default">
                                    <div class="col-md-6 bg-grey mt-step-col">
                                        <a href="{!! URL::to('dosen/view/aktivitas_dosen_tetap') !!}" style="text-decoration:none">
                                            <div class="mt-step-title  font-grey-cascade">Aktivitas Dosen Tetap</div>
                                            <div class="mt-step-content font-grey-cascade">Aktivitas dosen yang bidang bidang keahliannya sesuai dengan PS dinyatakan dalam sks rata-rata per semester </div>
                                        </a>
                                    </div>
                                    <div class="col-md-6 bg-grey mt-step-col active">
                                        {{--<div class="mt-step-number bg-white font-grey"></div>--}}
                                        <a href="{!! URL::to('dosen/view/data_aktivitas_dosen_tetap') !!}" style="text-decoration:none">
                                            <div class="mt-step-title font-grey-cascade"> Data Aktivitas Dosen</div>
                                            <div class="mt-step-content font-grey-cascade">Data aktivitas mengajar dosen tetap yang bidang keahliannya sesuai dengan PS dan dosen tidak tetap diluar PS</div>
                                        </a>
                                    </div>
                                    <div class="col-md-6 bg-grey mt-step-col active">
                                        {{--<div class="mt-step-number bg-white font-grey"></div>--}}
                                        <a href="{!! URL::to('dosen/view/kegiatan_dosen_tetap') !!}" style="text-decoration:none">
                                            <div class="mt-step-title font-grey-cascade"> Kegiatan Dosen Tetap</div>
                                            <div class="mt-step-content font-grey-cascade">Kegiatan Dosen Tetap Dalam Seminar Ilmiah/Lokakarya/Penataran/Workshop</div>
                                        </a>
                                    </div>
                                    <div class="col-md-6 bg-grey mt-step-col">
                                        {{--<div class="mt-step-number bg-white font-grey"></div>--}}
                                        <a href="{!! URL::to('dosen/view/prestasi') !!}" style="text-decoration:none">
                                            <div class="mt-step-title font-grey-cascade">Prestasi</div>
                                            <div class="mt-step-content font-grey-cascade">
                                                Pencapaian Prestasi/Reputasi Dosen
                                                Pencapaian Prestasi/Reputasi Dosen
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-6 bg-grey mt-step-col ">
                                        {{--<div class="mt-step-number bg-white font-grey"></div>--}}
                                        <a href="{!! URL::to('dosen/view/organisasi') !!}" style="text-decoration:none">
                                            <div class="mt-step-title font-grey-cascade">Organisasi</div>
                                            <div class="mt-step-content font-grey-cascade">Keikutsertaan Dosen Tetap Dalam Organisasi Keilmuan</div>
                                        </a>
                                    </div>
                                    <div class="col-md-6 bg-grey mt-step-col active">
                                        {{--<div class="mt-step-number bg-white font-grey"></div>--}}
                                        <a href="{!! URL::to('dosen/view/pembimbing_akademik') !!}" style="text-decoration:none">
                                            <div class="mt-step-title font-grey-cascade">Pembimbing Akademik</div>
                                            <div class="mt-step-content font-grey-cascade">Nama Dosen Pembimbing Akademik dan Jumlah Mahasiswa</div>
                                        </a>
                                    </div>
                                    <div class="col-md-6 bg-grey mt-step-col active">
                                        {{--<div class="mt-step-number bg-white font-grey"></div>--}}
                                        <a href="{!! URL::to('dosen/view/pembimbing_ta') !!}" style="text-decoration:none">
                                            <div class="mt-step-title font-grey-cascade"> Pembimbing Tugas Akhir</div>
                                            <div class="mt-step-content font-grey-cascade">Pelaksanaan Pembimbingan Tugas Akhir Atau Skripsi</div>
                                        </a>
                                    </div>
                                    <div class="col-md-6 bg-grey mt-step-col">
                                        {{--<div class="mt-step-number bg-white font-grey"></div>--}}
                                        <a href="{!! URL::to('dosen/view/dana_penelitian') !!}" style="text-decoration:none">
                                            <div class="mt-step-title font-grey-cascade"> Data Kegiatan Penelitian</div>
                                            <div class="mt-step-content font-grey-cascade">Dana untuk kegiatan penelitian</div>
                                        </a>
                                    </div>
                                    <div class="col-md-6 bg-grey mt-step-col">
                                        {{--<div class="mt-step-number bg-white font-grey"></div>--}}
                                        <a href="{!! URL::to('dosen/view/dana_p2m') !!}" style="text-decoration:none">
                                            <div class="mt-step-title font-grey-cascade">Data Kegiatan P2M</div>
                                            <div class="mt-step-content font-grey-cascade">Dana untuk kegiatan Pelayanan Dan Pengabdian Masyarakat</div>
                                        </a>
                                    </div>
                                    <div class="col-md-6 bg-grey mt-step-col active">
                                        {{--<div class="mt-step-number bg-white font-grey"></div>--}}
                                        <a href="{!! URL::to('dosen/view/artikel_dosen') !!}" style="text-decoration:none">
                                            <div class="mt-step-title font-grey-cascade">Artikel/Buku Dosen</div>
                                            <div class="mt-step-content font-grey-cascade">Judul Artikel Ilmiah/Karya Ilmiah/Karya Seni/Buku</div>
                                        </a>
                                    </div>
                                    <div class="col-md-6 bg-grey mt-step-col active">
                                        {{--<div class="mt-step-number bg-white font-grey"></div>--}}
                                        <a href="{!! URL::to('dosen/view/kemampuan') !!}" style="text-decoration:none">
                                            <div class="mt-step-title font-grey-cascade"> Peningkatan Kemampuan Dosen</div>
                                            <div class="mt-step-content font-grey-cascade">
                                                Peningkatan Kemampuan Dosen Tetap Melalui Program Tugas Belajar Dalam Bidang Yang Sesuai Dengan Bidang PS
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-6 bg-grey mt-step-col">
                                        {{--<div class="mt-step-number bg-white font-grey"></div>--}}
                                        <a href="{!! URL::to('dosen/view/karya_paten') !!}" style="text-decoration:none">
                                            <div class="mt-step-title font-grey-cascade"> Karya Paten</div>
                                            <div class="mt-step-content font-grey-cascade">
                                                Karya dosen dan atau mahasiswa (Paten)
                                                <br>&nbsp;
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-6 bg-grey mt-step-col ">
                                        {{--<div class="mt-step-number bg-white font-grey"></div>--}}
                                        <a href="{!! URL::to('dosen/view/pendidikan') !!}" style="text-decoration:none">
                                            <div class="mt-step-title font-grey-cascade">Pendidikan Dosen</div>
                                            <div class="mt-step-content font-grey-cascade">
                                                Data Pendidikan Dosen
                                            </div>
                                        </a>
                                    </div>

                                </div>
                                <br/>
                                <br/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
@stop


