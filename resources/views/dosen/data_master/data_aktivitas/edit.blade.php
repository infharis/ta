@extends('dosen.layouts.index')
@section('css')
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/select2/css/select2.min.css') !!}" rel="stylesheet" type="text/css" />
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/select2/css/select2-bootstrap.min.css')!!}" rel="stylesheet" type="text/css" />
@stop
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">@include('notif')
        <!-- END PAGE BREADCRUMB -->
            <div class="row ">
                <div class="col-md-12">
                    <!-- BEGIN SAMPLE FORM PORTLET-->
                    <div class="portlet box purple ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-gift"></i> Form Edit Data Aktivitas Dosen </div>
                            <div class="pull-right" style="padding-top: 0.5%;">
                                <a href="{!! URL::to('dosen/view/data_aktivitas_dosen_tetap') !!}" class="btn btn-sm yellow dropdown-toggle">Back</a>
                            </div>
                            <div class="tools">
                            </div>
                        </div>
                        <div class="portlet-body">
                            <form role="form" action="{!! URL::to('dosen/update/data_aktivitas_dosen_tetap/'.$result->id) !!}" method="post">
                                {!! csrf_field() !!}
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="input-group">
                                            <label for="">Pilih Mk</label><span class="required" aria-required="true"> * </span>
                                            <select id="multiple" class="form-control select2-multiple" name="mk_id">
                                                @foreach($kur as $value)
                                                    @if($value->id==$result->mk_id)
                                                        <option selected value="{!! $value->id !!}">{!! $value->nama_mk !!}</option>
                                                    @else
                                                        <option value="{!! $value->id !!}">{!! $value->nama_mk !!}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                        <!-- /input-group -->
                                    </div>
                                </div>
                                <br>
                                <!-- /.col-md-6 -->
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="">Jumlah Kelas</label><span class="required" aria-required="true"> * </span>
                                        <input type="number" class="form-control" required name="jumlah_kelas" value="{!! $result->jumlah_kelas !!}">
                                    </div>
                                    <div class="col-md-4">
                                        <label for="">Jumlah Pertemuan Direncanakan</label><span class="required" aria-required="true"> * </span>
                                        <input type="number" class="form-control" required name="jumlah_pertemuan" value="{!! $result->jumlah_pertemuan !!}">
                                    </div>
                                    <div class="col-md-4">
                                        <label for="">Jumlah Pertemuan Dilaksanakan</label><span class="required" aria-required="true"> * </span>
                                        <input type="number" class="form-control" required name="jumlah_dilaksanakan" value="{!! $result->jumlah_dilaksanakan !!}">
                                        <!-- /input-group -->
                                    </div>
                                </div>
                                <!-- /.col-md-6 -->
                                <br>
                                <input type="submit" class="btn btn-primary" value="Update">
                            </form>
                        </div>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
@stop
@section('js')
    <script src="{!! URL::to('local/resources/assets/assets/global/plugins/select2/js/select2.full.min.js') !!}" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="{!! URL::to('local/resources/assets/assets/global/scripts/app.min.js')!!}" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{!! URL::to('local/resources/assets/assets/pages/scripts/components-select2.min.js')!!}" type="text/javascript"></script>

@stop