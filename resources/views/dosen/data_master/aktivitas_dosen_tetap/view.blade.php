@extends('dosen.layouts.index')
@section('css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/datatables.min.css')!!}" rel="stylesheet" type="text/css" />
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')!!}" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->
@stop
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">@include('notif')
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>Aktivitas Dosen Tetap
                        {{--<small>dashboard & statistics</small>--}}
                    </h1>
                </div>
                <div class="pull-right">
                    <a href="{!! URL::to('dosen/data_master') !!}" class="btn btn-sm yellow dropdown-toggle">Back</a>
                    <a href="{!! URL::to('dosen/create/aktivitas_dosen_tetap') !!}" class="btn btn-sm green dropdown-toggle">Tambah</a>
                </div>
            </div>
            <!-- END PAGE HEAD-->
            <!-- BEGIN PAGE BREADCRUMB -->

        <!-- END PAGE BREADCRUMB -->
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption font-dark">
                                <i class="icon-settings font-dark"></i>
                                <span class="caption-subject bold"></span>
                            </div>
                            <div class="tools"> </div>
                        </div>
                        <div class="portlet-body">
                            <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
                                <thead>
                                <tr>
                                    {{--<th rowspan="2" class="all">No</th>--}}
                                    <th colspan="3" class="all text-center">Sks Pengajaran</th>
                                    <th rowspan="2" class="all">Sks Penelitian</th>
                                    <th rowspan="2" class="all">Sks Pengabdian</th>
                                    <th colspan="2" class="all text-center">Sks Manajemen</th>
                                    {{--<th rowspan="2" class="all">Jumlah SKS</th>--}}
                                    <th rowspan="2" class="all">Aksi</th>
                                    {{--<th colspan="2"></th>--}}
                                </tr>
                                <tr>
                                    <th>PS Sendiri</th>
                                    <th>PS Sendiri PT Sendiri</th>
                                    <th>PT lain</th>
                                    <th>PT Sendiri</th>
                                    <th>PT lain</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>{!! @$result->ps_sendiri !!}</td>
                                    <td>{!! @$result->ps_lain !!}</td>
                                    <td>{!! @$result->pt_lain !!}</td>
                                    <td>{!! @$result->sks_penelitian !!}</td>
                                    <td>{!! @$result->sks_p2m !!}</td>
                                    <td>{!! @$result->m_pt_sendiri !!}</td>
                                    <td>{!! @$result->m_pt_lain !!}</td>
                                    <td>
                                        @if(@$result)
                                            <a href="#" id="{!! @$result->id !!}"  class="hapus_data btn btn-danger">Hapus</a>
                                            <a href="{!! URL::to('dosen/edit/aktivitas_dosen_tetap/'.$result->id) !!}"  class="btn btn-primary">Edit</a>
                                        @endif
                                    </td>
                                    {{--<td>2</td>--}}
                                    {{--<td><a href="{!! URL::to('dosen/view/aktivitas_dosen_tetap/update') !!}" class="btn btn-primary">Update</a></td>--}}

                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>

            </div>





            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
@stop
@section('js')
    <script type="text/javascript">
        $(document).ready(function() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

        });

        // script remove usulan
        $(document).on("click",".hapus_data",function(){
            var id=$(this).attr('id');
            if(confirm('Apakah anda yakin ingin menghapus data tersebut?'))
            {
                $.ajax({
                    type:'POST',
                    url:"{!! URL::to('dosen/delete/aktivitas_dosen_tetap') !!}",
                    data:{
                        id:id,
                        _method: 'delete'
                    },
                    success:function(response)
                    {
                        if(response==1) {
                            alert('Data berhasil dihapus!!');
                            location.reload();
                        }else {
                            alert('Gagal Dihapus');
                        }
                    }
                });
            }else{
                return false;
            }
        });

    </script>
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{!! URL::to('local/resources/assets/assets/global/scripts/datatable.js')!!}" type="text/javascript"></script>
    <script src="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/datatables.min.js')!!}" type="text/javascript"></script>
    <script src="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')!!}" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="{!! URL::to('local/resources/assets/assets/global/scripts/app.min.js')!!}" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{!! URL::to('local/resources/assets/assets/pages/scripts/table-datatables-responsive.min.js')!!}" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
@stop