@extends('dosen.layouts.index')
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>Form Aktivitas Dosen Tetap
                        {{--<small>bootstrap form controls and more</small>--}}
                    </h1>
                </div>
                <div class="pull-right">
                    <a href="{!! URL::to('dosen/view/aktivitas_dosen_tetap') !!}" class="btn btn-sm yellow dropdown-toggle">Back</a>
                </div>
            </div>
            <div class="row ">
                <div class="col-md-12">
                    <!-- BEGIN SAMPLE FORM PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-body">
                            <div class="portlet-body">
                                <form action="{!! URL::to('dosen/store/aktivitas_dosen_tetap') !!}" method="post">
                                    {!! csrf_field() !!}
                                <table class="table table-striped table-bordered table-hover dt-responsive">
                                    <thead>
                                    <tr>
                                        <th colspan="3" class="text-center">Sks Pengajaran</th>
                                        <th rowspan="2" class="all">Sks Penelitian</th>
                                        <th rowspan="2" class="all">Sks Pengabdian</th>
                                        <th colspan="2" class="text-center">Sks Manajemen</th>
                                        {{--<th rowspan="2" class="all">Jumlah SKS</th>--}}
                                    </tr>
                                    <tr>
                                        <th>PS Sendiri</th>
                                        <th>PS Lain PT Sendiri</th>
                                        <th>PT Lain</th>
                                        <th>PT Sendiri</th>
                                        <th>PT Lain</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>
                                            <input type="number" class="form-control" name="ps_sendiri">
                                        </td>
                                        <td>
                                            <input type="number" class="form-control" name="ps_lain">
                                        </td>
                                        <td>
                                            <input type="number" class="form-control" name="pt_lain">
                                        </td>
                                        <td>
                                            <input type="number" class="form-control" name="sks_penelitian">
                                        </td>
                                        <td>
                                            <input type="number" class="form-control" name="sks_p2m">
                                        </td>
                                        <td>
                                            <input type="number" class="form-control" name="m_pt_sendiri">
                                        </td>
                                        <td>
                                            <input type="number" class="form-control" name="m_pt_lain">
                                        </td>
                                        {{--<td>--}}
                                            {{--<input type="number" class="form-control" name="">--}}
                                        {{--</td>--}}
                                    </tr>
                                    </tbody>
                                </table>
                                <input type="submit" value="Simpan" class="btn btn-primary">
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
@stop