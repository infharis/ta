@extends('dosen.layouts.index')
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>Form Edit Aktivitas Dosen Tetap
                        {{--<small>bootstrap form controls and more</small>--}}
                    </h1>
                </div>
                <div class="pull-right">
                    <a href="{!! URL::to('dosen/view/aktivitas_dosen_tetap') !!}" class="btn btn-sm yellow dropdown-toggle">Back</a>
                </div>
            </div>
            <div class="row ">
                <div class="col-md-12">
                    <!-- BEGIN SAMPLE FORM PORTLET-->
                    <div class="portlet light bordered">
                        <form action="{!! URL::to('dosen/update/aktivitas_dosen_tetap/'.$result->id) !!}" method="post">
                            {!! csrf_field() !!}
                        <div class="portlet-body">
                            <div class="portlet-body">
                                <table class="table table-striped table-bordered table-hover dt-responsive">
                                    <thead>
                                    <tr>
                                        <th colspan="3" class="text-center">Sks Pengajaran</th>
                                        <th rowspan="2" class="all">Sks Penelitian</th>
                                        <th rowspan="2" class="all">Sks Pengabdian</th>
                                        <th colspan="2" class="text-center">Sks Manajemen</th>
                                    </tr>
                                    <tr>
                                        <th>PS Sendiri</th>
                                        <th>PS Sendiri PT Sendiri</th>
                                        <th>PT lain</th>
                                        <th>PT Sendiri</th>
                                        <th>PT lain</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>
                                            <input type="number" class="form-control" name="ps_sendiri" value="{!! $result->ps_sendiri !!}">
                                        </td>
                                        <td>
                                            <input type="number" class="form-control" name="ps_lain" value="{!! $result->ps_lain !!}">
                                        </td>
                                        <td>
                                            <input type="number" class="form-control" name="pt_lain" value="{!! $result->pt_lain !!}">
                                        </td>
                                        <td>
                                            <input type="number" class="form-control" name="sks_penelitian" value="{!! $result->sks_penelitian !!}">
                                        </td>
                                        <td>
                                            <input type="number" class="form-control" name="sks_p2m" value="{!! $result->sks_p2m !!}">
                                        </td>
                                        <td>
                                            <input type="number" class="form-control" name="m_pt_sendiri" value="{!! $result->m_pt_sendiri !!}">
                                        </td>
                                        <td>
                                            <input type="number" class="form-control" name="m_pt_lain" value="{!! $result->m_pt_lain !!}">
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <input type="submit" value="Update" class="btn btn-primary">
                            </div>
                        </div>
                        </form>
                    </div>

                    <!-- END SAMPLE FORM PORTLET-->
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
@stop