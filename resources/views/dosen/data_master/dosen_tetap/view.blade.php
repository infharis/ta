@extends('dosen.layouts.index')
@section('css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/datatables.min.css')!!}" rel="stylesheet" type="text/css" />
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')!!}" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->
@stop
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>Data Dosen Tetap
                        {{--<small>dashboard & statistics</small>--}}
                    </h1>
                </div>
                <div class="pull-right">
                    <a href="{!! URL::to('dosen/data_master') !!}" class="btn btn-sm yellow dropdown-toggle">Back</a>
                     <a href="{!! URL::to('dosen/create/dosen_tetap') !!}" class="btn btn-sm green dropdown-toggle">Tambah Dosen</a>
                </div>
            </div>
            <!-- END PAGE HEAD-->
            <!-- BEGIN PAGE BREADCRUMB -->
            {{--<ul class="page-breadcrumb breadcrumb">--}}
                {{--<li>--}}
                    {{--<a href="index.html">Home</a>--}}
                    {{--<i class="fa fa-circle"></i>--}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--<span class="active">Dashboard</span>--}}
                {{--</li>--}}
            {{--</ul>--}}

        <!-- END PAGE BREADCRUMB -->
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption font-dark">
                                <i class="icon-settings font-dark"></i>
                                <span class="caption-subject bold"></span>
                            </div>
                            <div class="tools"> </div>
                        </div>
                        <div class="portlet-body">
                            <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
                                <thead>
                                <tr>
                                    <th rowspan="2">No</th>
                                    <th rowspan="2">Nama Dosen</th>
                                    <th rowspan="2">NIDN</th>
                                    <th rowspan="2">Tempat/Tgl Lahir</th>
                                    <th rowspan="2">Jabatan</th>
                                    <th rowspan="2">Gelar</th>
                                    <th colspan="3">Pendidikan/Asal Universitas</th>
                                    <th colspan="3">Bidang Keahlian</th>
                                </tr>
                                <tr>
                                    <th colspan="1">S1</th>
                                    <th colspan="1">S2</th>
                                    <th colspan="1">S3</th>
                                    <th colspan="1">S1</th>
                                    <th colspan="1">S2</th>
                                    <th colspan="1">S3</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>Taufik F. Abidin </td>
                                    <td>197010081994031002</td>
                                    <td>Banda Aceh, 08-10-1970</td>
                                    <td>Lektor Kepala</td>
                                    <td>Dr,M.Tech</td>
                                    <td>S1 : ITS, Indonesia</td>
                                    <td>S2 : RMIT Australia</td>
                                    <td>S3 : NDSU, USA</td>
                                    <td style="display:none">Matematika</td>
                                    <td>Computer Science</td>
                                    <td>Computer Science</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>

            </div>





            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
@stop
@section('js')
    {{--<script src={!! URL::to('local/resources/assets/assets/global/plugins/select2/js/select2.full.min.js') !!} type="text/javascript"></script>--}}
    {{--<script src={!! URL::to('local/resources/assets/assets/global/plugins/jquery-validation/js/jquery.validate.min.js')!!} type="text/javascript"></script>--}}
    {{--<script src={!! URL::to('local/resources/assets/assets/global/plugins/jquery-validation/js/additional-methods.min.js')!!} type="text/javascript"></script>--}}
    {{--<script src={!! URL::to('local/resources/assets/assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js')!!} type="text/javascript"></script>--}}
    {{--<!-- BEGIN PAGE LEVEL SCRIPTS -->--}}
    {{--<script src={!! URL::to('local/resources/assets/assets/pages/scripts/form-wizard.min.js')!!} type="text/javascript"></script>--}}
    <!-- END PAGE LEVEL SCRIPTS -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{!! URL::to('local/resources/assets/assets/global/scripts/datatable.js')!!}" type="text/javascript"></script>
    <script src="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/datatables.min.js')!!}" type="text/javascript"></script>
    <script src="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')!!}" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="{!! URL::to('local/resources/assets/assets/global/scripts/app.min.js')!!}" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{!! URL::to('local/resources/assets/assets/pages/scripts/table-datatables-responsive.min.js')!!}" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
@stop