@extends('dosen.layouts.index')
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEAD-->
            {{--<div class="page-head">--}}
                {{--<!-- BEGIN PAGE TITLE -->--}}
                {{--<div class="page-title">--}}
                    {{--<h1>Form Tambah Dosen Tetap--}}
                        {{--<small>bootstrap form controls and more</small>--}}
                    {{--</h1>--}}
                {{--</div>--}}
            {{--</div>--}}
            <!-- END PAGE HEAD-->

            <!-- END PAGE BREADCRUMB -->
            <div class="row ">
                <div class="col-md-12">
                    <!-- BEGIN SAMPLE FORM PORTLET-->
                    <div class="portlet box purple ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-gift"></i> Form Tambah Dosen Tetap </div>
                            <div class="tools">
                                {{--<a href="" class="collapse" data-original-title="" title=""> </a>--}}
                                {{--<a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""> </a>--}}
                                {{--<a href="" class="reload" data-original-title="" title=""> </a>--}}
                                {{--<a href="" class="remove" data-original-title="" title="">Simpan</a>--}}
                            </div>
                        </div>
                        <div class="portlet-body">
                            <form role="form">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <label for="">Nama Dosen</label><span class="required" aria-required="true"> * </span>
                                            <input type="text" class="form-control">
                                        </div>
                                        <!-- /input-group -->
                                    </div>
                                    <!-- /.col-md-6 -->
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <label for="">Email</label><span class="required" aria-required="true"> * </span>
                                            <input type="text" class="form-control" required>
                                        </div>
                                        <!-- /input-group -->
                                    </div>
                                    <!-- /.col-md-6 -->
                                </div>
                                <div class="row">
                                    <!-- /.col-md-6 -->
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <label for="">NIDN</label><span class="required" aria-required="true"> * </span>
                                            <input type="text" class="form-control" required>
                                        </div>
                                        <!-- /input-group -->
                                    </div>
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <label for="">NIP</label>
                                            <input type="text" class="form-control">
                                        </div>
                                        <!-- /input-group -->
                                    </div>
                                    <!-- /.col-md-6 -->
                                </div>
                                <!-- /.row -->
                            </form>
                            <br>
                            <input type="submit" class="btn btn-primary" value="Simpan">

                        </div>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
@stop