@extends('dosen.layouts.index')
@section('content')
    @extends('dosen.layouts.index')
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- END PAGE BREADCRUMB -->
            <div class="row ">
                <div class="col-md-12">
                    <!-- BEGIN SAMPLE FORM PORTLET-->
                    <div class="portlet box purple ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-gift"></i> Form Tambah Organisasi Dosen
                            </div>
                            <div class="pull-right" style="padding-top: 0.5%;">
                                <a href="{!! URL::to('dosen/view/organisasi') !!}" class="btn btn-sm yellow dropdown-toggle">Back</a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <form role="form" action="{!! URL::to('dosen/store/organisasi') !!}" enctype="multipart/form-data" method="post">
                                {!! csrf_field() !!}
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="">Nama Organisasi</label><span class="required" aria-required="true"> * </span>
                                        <input type="text" class="form-control" name="organisasi" required>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="">Start</label><span class="required" aria-required="true"> * </span>
                                        <input type="text" class="form-control" name="start">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="">End</label><span class="required" aria-required="true"> * </span>
                                        <input type="text" class="form-control" name="end">
                                    </div>
                                </div>
                                <!-- /.row -->
                                <br>
                                <div class="row">
                                    <div class="col-md-6">
                                    <label>Tingkat</label>
                                    <div class="radio-list">
                                        <label class="radio-inline">
                                            <input type="radio" name="tingkat" id="optionsRadios4" value="L">Lokal
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="tingkat" id="optionsRadios5" value="N"> Nasional
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="tingkat" id="optionsRadios5" value="I"> Internasional
                                        </label>
                                    </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="">Softcopy</label>
                                        <input type="file" class="form-control" name="file">
                                    </div>
                                </div>
                            <br>
                            <input type="submit" class="btn btn-primary" value="Simpan">
                            </form>
                        </div>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
@stop
@stop