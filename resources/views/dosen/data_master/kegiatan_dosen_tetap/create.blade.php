@extends('dosen.layouts.index')
@section('content')
    @extends('dosen.layouts.index')
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">@include('notif')

            <!-- END PAGE BREADCRUMB -->
            <div class="row ">
                <div class="col-md-12">
                    <!-- BEGIN SAMPLE FORM PORTLET-->
                    <div class="portlet box purple ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-gift"></i> Form Tambah Kegiatan Dosen
                            </div>
                            <div class="pull-right" style="padding-top: 0.5%;">
                                <a href="{!! URL::to('dosen/view/kegiatan_dosen_tetap') !!}" class="btn btn-sm yellow dropdown-toggle">Back</a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <form  enctype="multipart/form-data" role="form" action="{!! URL::to('dosen/store/kegiatan_dosen_tetap') !!}" method="post">
                                {!! csrf_field() !!}
                                <div class="row">
                                    <div class="col-md-12">
                                            <label for="">Jenis Kegiatan</label><span class="required" aria-required="true"> * </span>
                                            <input type="text" class="form-control" name="jenis_kegiatan" required>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="">Tempat</label><span class="required" aria-required="true"> * </span>
                                        <input type="text" class="form-control" name="tempat">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="">Waktu</label><span class="required" aria-required="true"> * </span>
                                        <input type="text" class="form-control" name="waktu" placeholder="2012">
                                    </div>
                                </div>
                                <!-- /.row -->
                                <br>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="">Softcopy</label>
                                        <input type="file" class="form-control" name="file">
                                    </div>
                                    <div class="col-md-6">
                                    <label>Sebagai</label>
                                        <label class="radio-inline">
                                            <input type="radio" name="sebagai" value="Y">Penyaji
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="sebagai" value="N"> Peserta
                                        </label>
                                    </div>
                                </div>
                            <br>
                            <input type="submit" class="btn btn-primary" value="Simpan">
                            </form>
                        </div>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
@stop
@stop