@extends('dosen.layouts.index')
@section('content')
    @extends('dosen.layouts.index')
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- END PAGE BREADCRUMB -->
            <div class="row ">
                <div class="col-md-12">
                    <!-- BEGIN SAMPLE FORM PORTLET-->
                    <div class="portlet box purple ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-gift"></i> Form Tambah Karya dosen dan atau mahasiswa (Paten)
                            </div>
                            <div class="pull-right" style="padding-top: 0.5%;">
                                <a href="{!! URL::to('dosen/view/karya_paten') !!}" class="btn btn-sm yellow dropdown-toggle">Back</a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <form role="form" enctype="multipart/form-data" action="{!! URL::to('dosen/store/karya_paten') !!}" method="post">
                                {!! csrf_field() !!}

                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="">Karya / Paten</label><span class="required" aria-required="true"> * </span>
                                        <input type="text" class="form-control" name="karya" required>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="">Softcopy</label>
                                        <input type="file" class="form-control" name="file">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="">No HKI</label>
                                        <input type="text" class="form-control" name="no_hki">
                                    </div>
                                </div>
                                <!-- /.row -->
                            <br>
                            <input type="submit" class="btn btn-primary" value="Simpan">
                            </form>
                        </div>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
@stop
@stop