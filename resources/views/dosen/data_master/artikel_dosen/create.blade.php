@extends('dosen.layouts.index')
@section('css')
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/select2/css/select2.min.css') !!}" rel="stylesheet" type="text/css" />
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/select2/css/select2-bootstrap.min.css')!!}" rel="stylesheet" type="text/css" />
@stop
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- END PAGE BREADCRUMB -->
            <div class="row ">
                <div class="col-md-12">
                    <!-- BEGIN SAMPLE FORM PORTLET-->
                    <div class="portlet box purple ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-gift"></i> Form Tambah Artikel/Buku
                            </div>
                            <div class="pull-right" style="padding-top: 0.5%;">
                                <a href="{!! URL::to('dosen/view/artikel_dosen') !!}" class="btn btn-sm yellow dropdown-toggle">Back</a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <form role="form" action="{!! URL::to('dosen/store/artikel_dosen') !!}" enctype="multipart/form-data" method="post">
                                {!! csrf_field() !!}
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="">Judul Artikel/Buku</label><span class="required" aria-required="true"> * </span>
                                        <input type="text" class="form-control" name="judul">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="multiple" class="control-label">Anggota</label>
                                            <select id="multiple" name="dosen[]" class="form-control select2-multiple" multiple>
                                                @foreach($dosen as $value)
                                                    <option value="{!! $value->id !!}">{!! $value->nama_dosen !!}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <!-- /.row -->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Dihasilkan/Dipublikasikan</label><span class="required" aria-required="true"> * </span>
                                            <input type="text" class="form-control" name="dihasilkan">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Tahun</label><span class="required" aria-required="true"> * </span>
                                            <input type="text" class="form-control" name="tahun" maxlength="4">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                    <label>Tingkat</label>
                                    <div class="radio-list">
                                        <label class="radio-inline">
                                            <input type="radio" name="tingkat" id="optionsRadiodads4" value="L">Lokal
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="tingkat" id="optionsRadidados5" value="N"> Nasional
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="tingkat" id="optionsRadios6" value="I"> Internasional
                                        </label>
                                    </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label>Jurnal</label>
                                        <div class="radio-list">
                                            <label class="radio-inline">
                                                <input type="radio" name="jurnal" id="optionsRadios7" value="L">Lokal
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="jurnal" id="optionsRadios8" value="N"> Nasional
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="jurnal" id="optionsRadios9" value="I"> Internasional
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-6">
                                    <label>Seminar</label>
                                    <div class="radio-list">
                                        <label class="radio-inline">
                                            <input type="radio" name="seminar" id="optionsRadios10" value="L">Lokal
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="seminar" id="optionsRadios11" value="N"> Nasional
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="seminar" id="optionsRadios12" value="I"> Internasional
                                        </label>
                                    </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label>Buku</label>
                                        <div class="radio-list">
                                            <label class="radio-inline">
                                                <input type="radio" name="buku" id="optionsRadios34" value="Y">Yes
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="buku" id="optionsRadios25" value="N" checked>No
                                            </label>

                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="">Softcopy</label>
                                        <input type="file" name="file" class="form-control">
                                    </div>
                                </div>
                            <br>
                            <input type="submit" class="btn btn-primary" value="Simpan">
                            </form>
                        </div>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
@stop
@section('js')
    <script src="{!! URL::to('local/resources/assets/assets/global/plugins/select2/js/select2.full.min.js') !!}" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="{!! URL::to('local/resources/assets/assets/global/scripts/app.min.js')!!}" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{!! URL::to('local/resources/assets/assets/pages/scripts/components-select2.min.js')!!}" type="text/javascript"></script>
@stop