@extends('dosen.layouts.index')
@section('css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/datatables.min.css')!!}" rel="stylesheet" type="text/css" />
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')!!}" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->
@stop
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>Dashboard
                        <small>dashboard & statistics</small>
                    </h1>
                </div>
                {{--<div class="pull-right">--}}
                {{--<a href="" class="btn btn-sm green dropdown-toggle">Auto Sync</a>--}}
                {{--</div>--}}
            </div>
            <!-- END PAGE HEAD-->
            <!-- BEGIN PAGE BREADCRUMB -->
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <a href="index.html">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span class="active">Dashboard</span>
                </li>
            </ul>
        {{--<div class="alert alert-success">--}}
        {{--<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>--}}
        {{--Last Sync 2 days ago, Klik tombol sync untuk mendapatkan data terbaru <a href="" class="btn btn-sm green dropdown-toggle">Sync</a>--}}
        {{--</div>--}}
        <!-- END PAGE BREADCRUMB -->
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption font-dark">
                                <i class="icon-settings font-dark"></i>
                                <span class="caption-subject bold"></span>
                                Data Dosen Tetap Diluar PS
                            </div>
                            <div class="tools"> </div>
                        </div>
                        <div class="portlet-body">
                            <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
                                <thead>
                                <tr>
                                    <th rowspan="2">No</th>
                                    <th rowspan="2">Nama Dosen</th>
                                    <th rowspan="2">NIDN</th>
                                    <th rowspan="2">Tempat/Tgl Lahir</th>
                                    <th rowspan="2">Jabatan</th>
                                    <th rowspan="2">Gelar</th>
                                    <th colspan="3">Pendidikan/Asal Universitas</th>
                                    <th colspan="3">Bidang Keahlian</th>
                                </tr>
                                <tr>
                                    <th colspan="1">S1</th>
                                    <th colspan="1">S2</th>
                                    <th colspan="1">S3</th>
                                    <th colspan="1">S1</th>
                                    <th colspan="1">S2</th>
                                    <th colspan="1">S3</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>Ilham Maulana</td>
                                    <td>197503061998021000</td>
                                    <td>Banda Aceh, 3/6/1975</td>
                                    <td>Lektor</td>
                                    <td>Dr.rer.nat S.Si</td>
                                    <td>S1: 1997, Universitas Syiah Kuala, Kimia</td>
                                    <td>S2: 2003, Universitat Berlin, Kimia An Organik</td>
                                    <td>S3: 2005, Universitat Leipzig, Kimia Organik</td>
                                    <td>Kimia</td>
                                    <td>Kimia an organik</td>
                                    <td>Kimia an organik</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>

            </div>





            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
@stop
@section('js')
    {{--<script src={!! URL::to('local/resources/assets/assets/global/plugins/select2/js/select2.full.min.js') !!} type="text/javascript"></script>--}}
    {{--<script src={!! URL::to('local/resources/assets/assets/global/plugins/jquery-validation/js/jquery.validate.min.js')!!} type="text/javascript"></script>--}}
    {{--<script src={!! URL::to('local/resources/assets/assets/global/plugins/jquery-validation/js/additional-methods.min.js')!!} type="text/javascript"></script>--}}
    {{--<script src={!! URL::to('local/resources/assets/assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js')!!} type="text/javascript"></script>--}}
    {{--<!-- BEGIN PAGE LEVEL SCRIPTS -->--}}
    {{--<script src={!! URL::to('local/resources/assets/assets/pages/scripts/form-wizard.min.js')!!} type="text/javascript"></script>--}}
    <!-- END PAGE LEVEL SCRIPTS -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{!! URL::to('local/resources/assets/assets/global/scripts/datatable.js')!!}" type="text/javascript"></script>
    <script src="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/datatables.min.js')!!}" type="text/javascript"></script>
    <script src="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')!!}" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="{!! URL::to('local/resources/assets/assets/global/scripts/app.min.js')!!}" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{!! URL::to('local/resources/assets/assets/pages/scripts/table-datatables-responsive.min.js')!!}" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
@stop