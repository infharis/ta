@extends('dosen.layouts.index')
@section('css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/datatables.min.css')!!}" rel="stylesheet" type="text/css" />
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')!!}" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->
@stop
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>Pembimbing Tugas Akhir
                        {{--<small>dashboard & statistics</small>--}}
                    </h1>
                </div>
                <div class="pull-right">
                    <a href="{!! URL::to('dosen/data_master') !!}" class="btn btn-sm yellow dropdown-toggle">Back</a>
                </div>
            </div>
        <!-- END PAGE BREADCRUMB -->
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption font-dark">
                                <i class="icon-settings font-dark"></i>
                                <span class="caption-subject bold"></span>
                            </div>
                            <div class="tools"> </div>
                        </div>
                        <div class="portlet-body">
                            <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
                                <thead>
                                <tr>
                                    {{--<th>No</th>--}}
                                    {{--<th>Nama Dosen</th>--}}
                                    <th>Jumlah Mahasiswa</th>
                                    {{--<th>Aksi</th>--}}
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>{!! $result !!}</td>
                                </tr>
                                    {{--<td>--}}
                                        {{--<button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal">Update</button>--}}
                                        {{--<div class="modal fade" id="myModal" role="dialog">--}}
                                            {{--<div class="modal-dialog">--}}
                                                {{--<!-- Modal content-->--}}
                                                {{--<div class="modal-content">--}}
                                                    {{--<div class="modal-header">--}}
                                                        {{--<button type="button" class="close" data-dismiss="modal">&times;</button>--}}
                                                        {{--<h4 class="modal-title">Update Pembimbing Tugas Akhir</h4>--}}
                                                    {{--</div>--}}
                                                    {{--<div class="modal-body">--}}
                                                        {{--<div class="row">--}}
                                                            {{--<div class="col-md-12">--}}
                                                                {{--<label for="">Jumlah Mahasiswa</label><span class="required" aria-required="true"> * </span>--}}
                                                                {{--<input type="number" class="form-control" value="2">--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                    {{--<div class="modal-footer">--}}
                                                        {{--<input type="submit" value="Simpan" class="btn btn-primary">--}}
                                                        {{--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}

                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</td>--}}
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>

            </div>





            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
@stop
@section('js')
    {{--<script src={!! URL::to('local/resources/assets/assets/global/plugins/select2/js/select2.full.min.js') !!} type="text/javascript"></script>--}}
    {{--<script src={!! URL::to('local/resources/assets/assets/global/plugins/jquery-validation/js/jquery.validate.min.js')!!} type="text/javascript"></script>--}}
    {{--<script src={!! URL::to('local/resources/assets/assets/global/plugins/jquery-validation/js/additional-methods.min.js')!!} type="text/javascript"></script>--}}
    {{--<script src={!! URL::to('local/resources/assets/assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js')!!} type="text/javascript"></script>--}}
    {{--<!-- BEGIN PAGE LEVEL SCRIPTS -->--}}
    {{--<script src={!! URL::to('local/resources/assets/assets/pages/scripts/form-wizard.min.js')!!} type="text/javascript"></script>--}}
    <!-- END PAGE LEVEL SCRIPTS -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{!! URL::to('local/resources/assets/assets/global/scripts/datatable.js')!!}" type="text/javascript"></script>
    <script src="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/datatables.min.js')!!}" type="text/javascript"></script>
    <script src="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')!!}" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="{!! URL::to('local/resources/assets/assets/global/scripts/app.min.js')!!}" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{!! URL::to('local/resources/assets/assets/pages/scripts/table-datatables-responsive.min.js')!!}" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
@stop