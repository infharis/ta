@extends('operator_sarjana.layouts.index')
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>Dashboard
                        <small>dashboard & statistics</small>
                    </h1>
                </div>
                {{--<div class="pull-right">--}}
                {{--<a href="" class="btn btn-sm green dropdown-toggle">Auto Sync</a>--}}
                {{--</div>--}}
            </div>
            <!-- END PAGE HEAD-->
            <!-- BEGIN PAGE BREADCRUMB -->
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <a href="index.html">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span class="active">Dashboard</span>
                </li>
            </ul>
            <!-- END PAGE BREADCRUMB -->
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption font-dark">Form Input Data Pendidikan Dosen
                                <i class="icon-settings font-dark"></i>
                                <span class="caption-subject bold"></span>
                            </div>
                            <div class="tools"></div>
                        </div>
                        <div class="portlet-body">
                            <form class="form-horizontal" role="form">
                                <div class="portlet-body flip-scroll">
                                    <table class="table table-bordered table-striped table-condensed flip-content">
                                        <thead class="flip-content">
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Dosen Tetap</th>
                                            <th>Pendidikan S1,S2,S3 Dan Asal Universitas</th>
                                            <th>Bidang Keahlian untuk Setiap Jenjang Pendidikan</th>
                                        </tr>


                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>Taufik F. Abidin</td>
                                            <td>
                                                <input type="text" class="form-control" value="S1 : ITS, Indonesia">
                                                <input type="text" class="form-control" value="S2 : RMIT Australia">
                                                <input type="text" class="form-control" value="S3 : NDSU, USA">
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" value="Matematika">
                                                <input type="text" class="form-control" value="Computer Science">
                                                <input type="text" class="form-control" value="Computer Science">
                                            </td>
                                        </tr>

                                        </tbody>
                                    </table>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-10">
                                        <button type="submit" class="btn blue">Simpan</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>

            </div>





            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
@stop


