@extends('dosen.layouts.index')
@section('css')
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/select2/css/select2.min.css') !!}" rel="stylesheet" type="text/css" />
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/select2/css/select2-bootstrap.min.css')!!}" rel="stylesheet" type="text/css" />
@stop
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>Form Dosen
                        {{--<small>dashboard & statistics</small>--}}
                    </h1>
                </div>
                <a href="{!! URL::to('dosen/data_master') !!}" class="pull-right btn btn-primary">Back</a>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light bordered" id="form_wizard_1">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class=" icon-layers font-red"></i>
                                <span class="caption-subject font-red bold uppercase"> Form Dosen -
                                            <span class="step-title"> Step 1 of 8 </span>
                                        </span>
                            </div>
                            <div class="actions">
                                <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                    <i class="icon-cloud-upload"></i>
                                </a>
                                <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                    <i class="icon-wrench"></i>
                                </a>
                                <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                    <i class="icon-trash"></i>
                                </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <form class="form-horizontal"  id="submit_form" method="POST">

                                <div class="form-wizard">
                                    <div class="form-body">
                                        <ul class="nav nav-pills nav-justified steps">
                                            <li>
                                                <a href="#tab1" data-toggle="tab" class="step">
                                                    <span class="number"> 1 </span>
                                                    <span class="desc">
                                                                <i class="fa fa-check"></i> Data Diri </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#tab2" data-toggle="tab" class="step">
                                                    <span class="number"> 2 </span>
                                                    <span class="desc">
                                                                <i class="fa fa-check"></i> Pendidikan </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#tab3" data-toggle="tab" class="step active">
                                                    <span class="number"> 3 </span>
                                                    <span class="desc">
                                                                <i class="fa fa-check"></i> Penelitian </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#tab4" data-toggle="tab" class="step active">
                                                    <span class="number"> 4 </span>
                                                    <span class="desc">
                                                                <i class="fa fa-check"></i> Pengabdian </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#tab5" data-toggle="tab" class="step active">
                                                    <span class="number"> 5 </span>
                                                    <span class="desc">
                                                                <i class="fa fa-check"></i> Aktivitas </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#tab7" data-toggle="tab" class="step active">
                                                    <span class="number"> 6 </span>
                                                    <span class="desc">
                                                                <i class="fa fa-check"></i> Kegiatan </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#tab8" data-toggle="tab" class="step active">
                                                    <span class="number"> 7 </span>
                                                    <span class="desc">
                                                                <i class="fa fa-check"></i> Prestasi </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#tab9" data-toggle="tab" class="step active">
                                                    <span class="number"> 8 </span>
                                                    <span class="desc">
                                                                <i class="fa fa-check"></i> Organisasi </span>
                                                </a>
                                            </li>
                                            {{--<li>--}}
                                                {{--<a href="#tab10" data-toggle="tab" class="step active">--}}
                                                    {{--<span class="number"> 10 </span>--}}
                                                    {{--<span class="desc">--}}
                                                                {{--<i class="fa fa-check"></i> Pembimbing </span>--}}
                                                {{--</a>--}}
                                            {{--</li>--}}
                                        </ul>
                                        <div id="bar" class="progress progress-striped" role="progressbar">
                                            <div class="progress-bar progress-bar-success"> </div>
                                        </div>
                                        <div class="tab-content">
                                            <div class="alert alert-danger display-none">
                                                <button class="close" data-dismiss="alert"></button> You have some form errors. Please check below. </div>
                                            <div class="alert alert-success display-none">
                                                <button class="close" data-dismiss="alert"></button> Your form validation is successful! </div>
                                            <div class="tab-pane active" id="tab1">
                                                {{--<h3 class="block">Provide your account details</h3>--}}
                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Nama Dosen
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" required="" id="nama_dosen" name="biodata[]" value="{!! Auth::user()->dosen->nama_dosen !!}"/>
                                                        {{--<span class="help-block"> Provide your username </span>--}}
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Bidang Keahlian
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" name="biodata[]" value="{!! Auth::user()->dosen->bidang !!}"/>
                                                        <span class="help-block"> </span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">NIDN
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" name="biodata[]" required value="{!! Auth::user()->dosen->nidn !!}" id="submit_form_password" />
                                                        {{--<span class="help-block"> Provide your password. </span>--}}
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">NIP
                                                        {{--<span class="required"> * </span>--}}
                                                    </label>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" name="biodata[]" required value="{!! Auth::user()->dosen->nip !!}"/>
                                                        {{--<span class="help-block"> Confirm your password </span>--}}
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Tempat/ Tgl Lahir
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" placeholder="Tempat Lahir Anda" required name="biodata[]" value="{!! Auth::user()->dosen->tempat !!}" />
                                                        {{--<span class="help-block"> Provide your email address </span>--}}
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="date" class="form-control" required placeholder="Tanggal Lahir Anda" name="biodata[]" value="{!! Auth::user()->dosen->tgl_lahir !!}"/>
                                                        {{--<span class="help-block"> Provide your email address </span>--}}
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Jabatan Akademik
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <select id="multiple" name="biodata[]" class="form-control select2-multiple">
                                                            <option value="0">--Pilih Jabatan Anda ---</option>
                                                                @foreach($jb as $value)
                                                                    @if(Auth::user()->dosen->jabatan_id==$value->id)
                                                                        <option selected value="{!! $value->id !!}">{!! $value->nama_jabatan !!}</option>
                                                                    @else
                                                                        <option value="{!! $value->id !!}">{!! $value->nama_jabatan !!}</option>
                                                                    @endif
                                                                @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Pendidikan Terakhir
                                                        <span class="required"> * </span>
                                                    </label>


                                                    <div class="col-md-9">
                                                        <div class="md-radio-inline">
                                                            @foreach($pt as $val)
                                                                <div class="md-radio">
                                                                    <input @if(Auth::user()->dosen->pendidikan_id==$val->id) checked @endif type="radio" id="checkbox1_{!! $val->id !!}" name="biodata[]" value="{!! $val->id !!}" class="md-radiobtn">
                                                                    <label for="checkbox1_{!! $val->id !!}">
                                                                        <span></span>
                                                                        <span class="check"></span>
                                                                        <span class="box"></span> {!! $val->nama_pendidikan !!} </label>
                                                                </div>
                                                            @endforeach

                                                        </div>
                                                        {{--<span class="help-block"> Provide your email address </span>--}}
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="tab-pane" id="tab2">
                                                {{--<h3 class="block">Provide your profile details</h3>--}}
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">S1
                                                        {{--<span class="required"> * </span>--}}
                                                    </label>
                                                    <div class="col-md-8">


                                                            <div class="row">
                                                                <div class="col-md-3">
                                                                    <label>Gelar Akademik</label>
                                                                    <input type="text" class="form-control"  placeholder="S.Si" name="pendidikan[]">
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <label for="">Pendidikan</label>
                                                                    <input type="text" class="form-control"  name="pendidikan[]" placeholder="ITS, Indonesia">
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <label for="">Bidang Keahlian</label>
                                                                    <input type="text" class="form-control" name="pendidikan[]" placeholder="MTK">
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <label for="">Ijazah</label>
                                                                    <input type="file" class="form-control"  name="ijazah[]" placeholder="ijazah">
                                                                </div>
                                                            </div>

                                                        {{--<span class="help-block"> Provide your fullname </span>--}}
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">S2
                                                        {{--<span class="required"> * </span>--}}
                                                    </label>
                                                    <div class="col-md-8">
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <label for="">Gelar Akademik</label>
                                                                <input type="text" class="form-control" name="pendidikan[]">
                                                            </div>
                                                            <div class="col-md-3">
                                                                <label for="">Pendidikan</label>
                                                                <input type="text" class="form-control" name="pendidikan[]">
                                                            </div>
                                                            <div class="col-md-3">
                                                                <label for="">Bidang Keahlian</label>
                                                                <input type="text" class="form-control" name="pendidikan[]">
                                                            </div>
                                                            <div class="col-md-3">
                                                                <label for="">Ijazah</label>
                                                                <input type="file" class="form-control" name="ijazah[]">
                                                            </div>
                                                        </div>

                                                        {{--<span class="help-block"> Provide your fullname </span>--}}
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">S3
                                                        {{--<span class="required"> * </span>--}}
                                                    </label>
                                                    <div class="col-md-8">
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <label for="">Gelar Akademik</label>
                                                                <input type="text" class="form-control"  name="pendidikan[]">
                                                            </div>
                                                            <div class="col-md-3">
                                                                <label for="">Pendidikan</label>
                                                                <input type="text" class="form-control" name="pendidikan[]">
                                                            </div>
                                                            <div class="col-md-3">
                                                                <label for="">Bidang Keahlian</label>
                                                                <input type="text" class="form-control" name="pendidikan[]">
                                                            </div>
                                                            <div class="col-md-3">
                                                                <label for="">Ijazah</label>
                                                                <input type="file" class="form-control" name="ijazah[]">
                                                            </div>
                                                        </div>

                                                        {{--<span class="help-block"> Provide your fullname </span>--}}
                                                    </div>
                                                </div>



                                            </div>
                                            <div class="tab-pane" id="tab3">
                                                {{--<h3 class="block">Provide your billing and credit card details</h3>--}}
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Tahun
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" name="penelitian[]" maxlength="4" />
                                                        {{--<span class="help-block"> </span>--}}
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Judul Penelitian
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" name="penelitian[]" />
                                                        {{--<span class="help-block"> </span>--}}
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Sumber Dan Jenis Dana
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-8">
                                                        <select id="multiple" name="jd_penelitian[]" class="form-control select2-multiple" multiple>
                                                            <option value="0">--Pilih Sumber Dan Jenis Dana --</option>
                                                            @foreach($jd as $value)
                                                                <option value="{!! $value->id !!}">{!! $value->nama_jenis_dana !!}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Jumlah Dana
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-8">
                                                        <input type="text"  class="form-control" name="penelitian[]" />
                                                        {{--<span class="help-block"> e.g 11/2020 </span>--}}
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Anggota Penelitian
                                                        <span class="required"> * </span>
                                                    </label>
                                                        <div class="col-md-8">
                                                            <select id="multiple" name="anggota_p[]" class="form-control select2-multiple" multiple>
                                                                <option value="1">--Pilih Dosen--</option>
                                                                @foreach($ds as $dosen)
                                                                    <option value="{!! $dosen->id !!}">{!! $dosen->nama_dosen !!}</option>
                                                                @endforeach

                                                            </select>
                                                        </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Softcopy Penelitian
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-8">
                                                        <input type="file"  class="form-control" name="softcopy_penelitian" />
                                                        {{--<span class="help-block"> e.g 11/2020 </span>--}}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="tab4">
                                                {{--<h3 class="block">Provide your billing and credit card details</h3>--}}
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Tahun
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" name="p2m[]" maxlength="4" />
                                                        <span class="help-block"> </span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Judul Pengabdian kepada Masyarakat
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" name="p2m[]" />
                                                        <span class="help-block"> </span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Sumber Dana Jenis Dana
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-8">
                                                        <select id="multiple" name="jd_p2m[]" class="form-control select2-multiple" multiple>
                                                            <option value="0">--Pilih Sumber Dan Jenis Dana --</option>
                                                            @foreach($jd as $value)
                                                                <option value="{!! $value->id !!}">{!! $value->nama_jenis_dana !!}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Jumlah Dana
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-8">
                                                        <input type="text"  class="form-control" name="p2m[]" />
                                                        {{--<span class="help-block"> e.g 11/2020 </span>--}}
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Anggota Pengabdian
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-8">
                                                        <select id="multiple" name="anggota_p2m[]" class="form-control select2-multiple" multiple>
                                                            <option value="1">--Pilih Dosen--</option>
                                                            @foreach($ds as $dosen)
                                                                <option value="{!! $dosen->id !!}">{!! $dosen->nama_dosen !!}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Softcopy Pengabdian
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-8">
                                                        <input type="file"  class="form-control" name="softcopy_p2m" />
                                                        {{--<span class="help-block"> e.g 11/2020 </span>--}}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="tab5">
                                                <h4 class="block">Aktivitas Dosen Tetap Yang Bidang Keahliannya Sesuai Dengan PS</h4>
                                                <table class="table table-bordered table-striped table-condensed flip-content">
                                                    <thead class="flip-content">
                                                    <tr>
                                                        {{--<th rowspan="2">No</th>--}}
                                                        <th colspan="3" class="text-center">SKS Pengajaran pada</th>
                                                        <th rowspan="2" class="text-center">SKS Penelitian</th>
                                                        <th rowspan="2" class="text-center">SKS Pengab-dian kepada Masyarakat</th>
                                                        <th colspan="2" class="text-center">sks Manajemen</th>
                                                    </tr>
                                                    <tr>
                                                        <th colspan="1">PS Sendiri</th>
                                                        <th colspan="1">PS Lain PT Sendiri</th>
                                                        <th colspan="1">PT Lain</th>
                                                        <th colspan="1">PT Sendiri</th>
                                                        <th colspan="1">PT Lain</th>
                                                    </tr>

                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td>
                                                            <input type="number" name="sks[]" class="form-control">
                                                        </td>
                                                        <td>
                                                            <input type="number" name="sks[]" class="form-control">
                                                        </td>
                                                        <td>
                                                            <input type="number" name="sks[]" class="form-control">
                                                        </td>
                                                        <td>
                                                            <input type="number" name="sks[]" class="form-control">
                                                        </td>
                                                        <td>
                                                            <input type="number" name="sks[]" class="form-control">
                                                        </td>
                                                        <td>
                                                            <input type="number" name="sks[]" class="form-control">
                                                        </td>
                                                        <td>
                                                            <input type="number" name="sks[]" class="form-control">
                                                        </td>
                                                    </tr>

                                                    </tbody>
                                                </table>
                                            </div>
                                            {{--<div class="tab-pane" id="tab6">--}}
                                                {{--<h4 class="block">Data Aktivitas Mengajar Dosen Tetap Yang Bidang Keahliannya Sesuai Dengan PS</h4>--}}
                                                {{--<!--/row 1-->--}}
                                                {{--<div class="row">--}}
                                                    {{--<div class="col-md-4">--}}
                                                        {{--<div class="form-group">--}}
                                                            {{--<label>Nama MK</label>--}}
                                                            {{--<input type="text" class="form-control" placeholder="ketik Kode Atau Mata Kuliah">--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                    {{--<div class="col-md-2">--}}
                                                        {{--<div class="form-group">--}}
                                                            {{--<label>Jumlah Kelas</label>--}}
                                                            {{--<input type="number" class="form-control"> </div>--}}
                                                    {{--</div>--}}
                                                    {{--<div class="col-md-2">--}}
                                                        {{--<div class="form-group">--}}
                                                            {{--<label>Jumlah direncanakan</label>--}}
                                                            {{--<input type="number" class="form-control"> </div>--}}
                                                    {{--</div>--}}
                                                    {{--<div class="col-md-2">--}}
                                                        {{--<div class="form-group">--}}
                                                            {{--<label>Jumlah dilaksanakan</label>--}}
                                                            {{--<input type="number" class="form-control"> </div>--}}
                                                    {{--</div>--}}
                                                    {{--<!--/span-->--}}
                                                {{--</div>--}}

                                            {{--</div>--}}

                                            <div class="tab-pane" id="tab7">
                                                <h4 class="block">Kegiatan Dosen Tetap Dalam Seminar Ilmiah/Lokakarya/Penataran/Workshop</h4>

                                                <!--/row 1-->
                                                <div class="row">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Jenis Kegiatan
                                                            <span class="required"> * </span>
                                                        </label>
                                                        <div class="col-md-8">
                                                            <input type="text" class="form-control" name="kegiatan[]" />
                                                            {{--<span class="help-block"> Provide your password. </span>--}}
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Tempat
                                                            <span class="required"> * </span>
                                                        </label>
                                                        <div class="col-md-8">
                                                            <input type="text" class="form-control" name="kegiatan[]"   />
                                                            {{--<span class="help-block"> Provide your password. </span>--}}
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Waktu
                                                            <span class="required"> * </span>
                                                        </label>
                                                        <div class="col-md-8">
                                                            <input type="text" class="form-control" name="kegiatan[]"  placeholder="2012" />
                                                            {{--<span class="help-block"> Provide your password. </span>--}}
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Sebagai
                                                            <span class="required"> * </span>
                                                        </label>
                                                            <div class="md-radio-inline">&nbsp;&nbsp;&nbsp;
                                                                <div class="md-radio">
                                                                    <input type="radio" id="checkbox1_21" name="kegiatan[]"  value="Y" class="md-radiobtn">
                                                                    <label for="checkbox1_21">
                                                                        <span></span>
                                                                        <span class="check"></span>
                                                                        <span class="box"></span> Penyaji </label>
                                                                </div>
                                                                <div class="md-radio">
                                                                    <input type="radio" id="checkbox1_22" name="kegiatan[]"  value="N" class="md-radiobtn">
                                                                    <label for="checkbox1_22">
                                                                        <span></span>
                                                                        <span class="check"></span>
                                                                        <span class="box"></span> Peserta </label>
                                                                </div>
                                                            </div>
                                                            {{--<span class="help-block"> Provide your email address </span>--}}
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Softcopy
                                                            <span class="required"> * </span>
                                                        </label>
                                                        <div class="col-md-8">
                                                            <input type="file" class="form-control" name="softcopy_kegiatan"/>
                                                            {{--<span class="help-block"> Provide your password. </span>--}}
                                                        </div>

                                                    </div>
                                                    <!--/span-->
                                                </div>



                                            </div>

                                            <div class="tab-pane" id="tab8">
                                                <h4 class="block">Pencapaian Prestasi/Reputasi Dosen</h4>

                                                <!--/row 1-->
                                                <div class="row">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Prestasi
                                                            <span class="required"> * </span>
                                                        </label>
                                                        <div class="col-md-8">
                                                            <input type="text" class="form-control" name="prestasi[]"/>
                                                            {{--<span class="help-block"> Provide your password. </span>--}}
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Waktu
                                                            <span class="required"> * </span>
                                                        </label>
                                                        <div class="col-md-8">
                                                            <input type="text" class="form-control" name="prestasi[]"/>
                                                            {{--<span class="help-block"> Provide your password. </span>--}}
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Tingkat
                                                            <span class="required"> * </span>
                                                        </label>
                                                            <div class="md-radio-inline">&nbsp;&nbsp;&nbsp;
                                                                <div class="md-radio">
                                                                    <input type="radio" id="checkbox1_23" name="prestasi[]" value="L" class="md-radiobtn">
                                                                    <label for="checkbox1_23">
                                                                        <span></span>
                                                                        <span class="check"></span>
                                                                        <span class="box"></span> Lokal</label>
                                                                </div>
                                                                <div class="md-radio">
                                                                    <input type="radio" id="checkbox1_24" name="prestasi[]" value="N" class="md-radiobtn">
                                                                    <label for="checkbox1_24">
                                                                        <span></span>
                                                                        <span class="check"></span>
                                                                        <span class="box"></span> Nasional </label>
                                                                </div>
                                                                <div class="md-radio">
                                                                    <input type="radio" id="checkbox1_25" name="prestasi[]" value="I" class="md-radiobtn" >
                                                                    <label for="checkbox1_25">
                                                                        <span></span>
                                                                        <span class="check"></span>
                                                                        <span class="box"></span> Internasional </label>
                                                                </div>
                                                            </div>
                                                            {{--<span class="help-block"> Provide your email address </span>--}}
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Softcopy
                                                            <span class="required"> * </span>
                                                        </label>
                                                        <div class="col-md-8">
                                                            <input type="file" class="form-control" name="softcopy_prestasi"/>
                                                            {{--<span class="help-block"> Provide your password. </span>--}}
                                                        </div>

                                                    </div>
                                                    <!--/span-->
                                                </div>



                                            </div>

                                            <div class="tab-pane" id="tab9">
                                                <h4 class="block">Keikutsertaan Dosen Tetap Dalam Organisasi Keilmuan Atau Organisasi Profesi</h4>

                                                <!--/row 1-->
                                                <div class="row">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Organisasi
                                                            <span class="required"> * </span>
                                                        </label>
                                                        <div class="col-md-8">
                                                            <input type="text" class="form-control" name="organisasi[]"  />
                                                            {{--<span class="help-block"> Provide your password. </span>--}}
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Kurun Waktu Mulai
                                                            <span class="required"> * </span>
                                                        </label>
                                                        <div class="col-md-8">
                                                            <input type="text" class="form-control" name="organisasi[]" />
                                                            {{--<span class="help-block"> Provide your password. </span>--}}
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Kurun Waktu Akhir
                                                            <span class="required"> * </span>
                                                        </label>
                                                        <div class="col-md-8">
                                                            <input type="text" class="form-control" name="organisasi[]" />
                                                            {{--<span class="help-block"> Provide your password. </span>--}}
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Tingkat
                                                            <span class="required"> * </span>
                                                        </label>
                                                            <div class="md-radio-inline">&nbsp;&nbsp;&nbsp;
                                                                <div class="md-radio">
                                                                    <input type="radio" id="checkbox1_26" name="organisasi[]" value="L" class="md-radiobtn">
                                                                    <label for="checkbox1_26">
                                                                        <span></span>
                                                                        <span class="check"></span>
                                                                        <span class="box"></span> Lokal</label>
                                                                </div>
                                                                <div class="md-radio">
                                                                    <input type="radio" id="checkbox1_27" name="organisasi[]" value="N" class="md-radiobtn">
                                                                    <label for="checkbox1_27">
                                                                        <span></span>
                                                                        <span class="check"></span>
                                                                        <span class="box"></span> Nasional </label>
                                                                </div>
                                                                <div class="md-radio">
                                                                    <input type="radio" id="checkbox1_28" name="organisasi[]" value="I" class="md-radiobtn" >
                                                                    <label for="checkbox1_28">
                                                                        <span></span>
                                                                        <span class="check"></span>
                                                                        <span class="box"></span> Internasional </label>
                                                                </div>
                                                            </div>
                                                            {{--<span class="help-block"> Provide your email address </span>--}}
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Softcopy
                                                            <span class="required"> * </span>
                                                        </label>
                                                        <div class="col-md-8">
                                                            <input type="file" class="form-control" name="softcopy_organisasi"/>
                                                            {{--<span class="help-block"> Provide your password. </span>--}}
                                                        </div>

                                                    </div>
                                                    <!--/span-->
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <a href="javascript:;" class="btn default button-previous">
                                                    <i class="fa fa-angle-left"></i> Back </a>
                                                <a href="javascript:;" class="btn btn-outline green button-next"> Continue
                                                    <i class="fa fa-angle-right"></i>
                                                </a>

                                                <a href="javascript:;" class="btn green button-submit simpan"> Submit
                                                    <i class="fa fa-check"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>





            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
@stop
@section('js')
    <script src="{!! URL::to('local/resources/assets/assets/global/plugins/select2/js/select2.full.min.js') !!}" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="{!! URL::to('local/resources/assets/assets/global/scripts/app.min.js')!!}" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{!! URL::to('local/resources/assets/assets/pages/scripts/components-select2.min.js')!!}" type="text/javascript"></script>
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{!! URL::to('local/resources/assets/assets/global/plugins/select2/js/select2.full.min.js')!!}" type="text/javascript"></script>
    <script src="{!! URL::to('local/resources/assets/assets/global/plugins/jquery-validation/js/jquery.validate.min.js')!!}" type="text/javascript"></script>
    <script src="{!! URL::to('local/resources/assets/assets/global/plugins/jquery-validation/js/additional-methods.min.js')!!}" type="text/javascript"></script>
    <script src="{!! URL::to('local/resources/assets/assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js')!!}" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{!! URL::to('local/resources/assets/assets/pages/scripts/form-wizard.min.js')!!}" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
@stop