@extends('dosen.layouts.index')
@section('css')
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/select2/css/select2.min.css') !!}" rel="stylesheet" type="text/css" />
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/select2/css/select2-bootstrap.min.css')!!}" rel="stylesheet" type="text/css" />
@stop
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- END PAGE BREADCRUMB -->
            <div class="row ">
                <div class="col-md-12">
                    <!-- BEGIN SAMPLE FORM PORTLET-->
                    <div class="portlet box purple ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-gift"></i> Form Edit Penelitan Dosen
                            </div>
                            <div class="pull-right" style="padding-top: 0.5%;">
                                <a href="{!! URL::to('dosen/view/dana_penelitian') !!}" class="btn btn-sm yellow dropdown-toggle">Back</a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <form role="form" action="{!! URL::to('dosen/update/dana_penelitian/'.$result->id) !!}" enctype="multipart/form-data" method="post">
                                {!! csrf_field() !!}
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="">Judul Penelitian</label><span class="required" aria-required="true"> * </span>
                                        <input type="text" class="form-control" name="jp" required value="{!! $result->judul_penelitian !!}">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="multiple" class="control-label">Anggota Penelitian Dosen</label>
                                            <select id="multiple" name="dosen[]" class="form-control select2-multiple" multiple>
                                                @foreach($ds as $value)
                                                    @if(in_array($value->id,$ds_prev))
                                                        <option selected value="{!! $value->id !!}">{!! $value->nama_dosen !!}</option>
                                                    @else
                                                        <option value="{!! $value->id !!}">{!! $value->nama_dosen !!}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="multiple" class="control-label">Anggota Penelitian Mahasiswa</label>
                                            <select id="multiple" name="mhs[]" class="form-control select2-multiple"  multiple>
                                                @foreach($mhs as $mhs_val)
                                                    @if(in_array($mhs_val->id,$mhs_prev))
                                                        <option  selected value="{!! $mhs_val->id !!}">{!! $mhs_val->nim.' '.$mhs_val->nama !!}</option>
                                                    @else
                                                        <option value="{!! $mhs_val->id !!}">{!! $mhs_val->nim.' '.$mhs_val->nama !!}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="multiple" class="control-label">Sumber Dan Jenis Dana</label>
                                            <select id="multiple" name="jd[]" class="form-control select2-multiple" required multiple>
                                                @foreach($jd as $val)
                                                    @if(in_array($val->id,$jd_prev))
                                                        <option selected value="{!! $val->id !!}">{!! $val->nama_jenis_dana !!}</option>
                                                    @else
                                                        <option value="{!! $val->id !!}">{!! $val->nama_jenis_dana !!}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.row -->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Jumlah Dana</label><span class="required" aria-required="true"> * </span>
                                            <input type="text" class="form-control" name="jumlah_dana" required value="{!! $result->jumlah_dana !!}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Tahun Penelitian</label><span class="required" aria-required="true"> * </span>
                                            <input type="text" class="form-control" name="tahun" maxlength="4" required value="{!! $result->tahun_penelitian !!}">
                                        </div>
                                    </div>
                                </div>
                                {{--<div class="row">--}}
                                    {{--<div class="col-md-12">--}}
                                        {{--<label for="">Softcopy</label>--}}
                                        {{--<input type="file" name="file" class="form-control">--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                <br>
                                <input type="submit" class="btn btn-primary" value="Update">
                            </form>
                        </div>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
@stop
@section('js')
    <script src="{!! URL::to('local/resources/assets/assets/global/plugins/select2/js/select2.full.min.js') !!}" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="{!! URL::to('local/resources/assets/assets/global/scripts/app.min.js')!!}" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{!! URL::to('local/resources/assets/assets/pages/scripts/components-select2.min.js')!!}" type="text/javascript"></script>
@stop