@extends('dosen.layouts.index')
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- END PAGE BREADCRUMB -->
            <div class="row ">
                <div class="col-md-12">
                    <!-- BEGIN SAMPLE FORM PORTLET-->
                    <div class="portlet box purple ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-gift"></i> Form Edit Prestasi Dosen
                            </div>
                            <div class="pull-right" style="padding-top: 0.5%;">
                                <a href="{!! URL::to('dosen/view/prestasi') !!}" class="btn btn-sm yellow dropdown-toggle">Back</a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <form role="form" action="{!! URL::to('dosen/update/prestasi/'.$result->id) !!}" method="post" enctype="multipart/form-data">
                                {!! csrf_field() !!}
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="">Prestasi Yang Dicapai</label><span class="required" aria-required="true"> * </span>
                                        <input type="text" class="form-control" name="prestasi" value="{!! $result->judul_prestasi !!}">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="">Waktu</label><span class="required" aria-required="true"> * </span>
                                        <input type="text" class="form-control" name="waktu" placeholder="2012" value="{!! $result->waktu   !!}">
                                    </div>
                                </div>
                                <!-- /.row -->
                                <br>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Tingkat</label>
                                        <label class="radio-inline">
                                            <input type="radio" name="tingkat" {!! $result->tingkat=='L' ? 'checked' : ' '!!} value="L" checked>Lokal
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="tingkat" {!! $result->tingkat=='N' ? 'checked' : ' '!!} value="N"> Nasional
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="tingkat" {!! $result->tingkat=='I' ? 'checked' : ' '!!} value="I"> Internasional
                                        </label>
                                    </div>
                                </div>
                                <br>
                                <input type="submit" class="btn btn-primary" value="Update">
                            </form>
                        </div>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
@stop
