@extends('dosen.layouts.index')
@section('content')
    @extends('dosen.layouts.index')
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- END PAGE BREADCRUMB -->
            <div class="row ">
                <div class="col-md-12">
                    <!-- BEGIN SAMPLE FORM PORTLET-->
                    <div class="portlet box purple ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-gift"></i> Form Tambah Peningkatan Kemampuan Dosen Tetap
                            </div>
                            <div class="pull-right" style="padding-top: 0.5%;">
                                <a href="{!! URL::to('dosen/view/kemampuan') !!}" class="btn btn-sm yellow dropdown-toggle">Back</a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <form role="form" action="{!! URL::to('dosen/store/kemampuan') !!}" method="post">
                                {!! csrf_field() !!}

                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="">Jenjang Pendidikan</label><span class="required" aria-required="true"> * </span>
                                        <input type="text" class="form-control" name="jenjang">
                                    </div>
                                    <div class="col-md-4">
                                        <label for="">Bidang Studi</label><span class="required" aria-required="true"> * </span>
                                        <input type="text" class="form-control" name="bidang">
                                    </div>
                                    <div class="col-md-4">
                                        <label for="">Perguruan Tinggi</label><span class="required" aria-required="true"> * </span>
                                        <input type="text" class="form-control" name="perguruan">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="">Negara</label><span class="required" aria-required="true"> * </span>
                                        <input type="text" class="form-control" name="negara">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="">Tahun Mulai Studi</label><span class="required" aria-required="true"> * </span>
                                        <input type="text" class="form-control" name="tahun" maxlength="4">
                                    </div>
                                </div>
                                <!-- /.row -->
                            <br>
                            <input type="submit" class="btn btn-primary" value="Simpan">
                            </form>
                        </div>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
@stop
@stop