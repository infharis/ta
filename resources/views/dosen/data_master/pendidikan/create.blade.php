@extends('dosen.layouts.index')
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- END PAGE BREADCRUMB -->
            <div class="row ">
                <div class="col-md-12">
                    <!-- BEGIN SAMPLE FORM PORTLET-->
                    <div class="portlet box purple ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-gift"></i> Form Pendidikan Dosen
                            </div>
                            <div class="pull-right" style="padding-top: 0.5%;">
                                <a href="{!! URL::to('dosen/view/pendidikan') !!}" class="btn btn-sm yellow dropdown-toggle">Back</a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <form role="form" action="{!! URL::to('dosen/store/pendidikan') !!}" method="post" enctype="multipart/form-data">
                                {!! csrf_field() !!}
                                <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="multiple" class="control-label">-- Pilih Jenjang --</label>
                                                <select id="multiple" name="jenjang" class="form-control select2-multiple">
                                                    @foreach($jenjang as $val)
                                                        <option value="{!! $val->id !!}">{!! $val->nama_jenjang !!}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    <div class="col-md-6">
                                        <label for="">Gelar</label>
                                        <input type="text" class="form-control" name="gelar" placeholder="S.Si">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="">Pendidikan</label>
                                        <input type="text" class="form-control" name="pendidikan" placeholder="ITS,Indonesia">
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="">Bidang Keahlian</label>
                                        <input type="text" class="form-control" name="bidang" placeholder="MTK">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="">SoftCopy</label>
                                        <input type="file" class="form-control" name="file">
                                    </div>
                                </div>
                                <br>

                                <br>
                                <input type="submit" class="btn btn-primary" value="Simpan">
                            </form>
                        </div>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
@stop
