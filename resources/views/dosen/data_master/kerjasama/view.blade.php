@extends('dosen.layouts.index')
@section('css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/datatables.min.css')!!}" rel="stylesheet" type="text/css" />
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')!!}" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->
@stop
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>Dashboard
                        <small>dashboard & statistics</small>
                    </h1>
                </div>
                {{--<div class="pull-right">--}}
                {{--<a href="" class="btn btn-sm green dropdown-toggle">Auto Sync</a>--}}
                {{--</div>--}}
            </div>
            <!-- END PAGE HEAD-->
            <!-- BEGIN PAGE BREADCRUMB -->
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <a href="index.html">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span class="active">Dashboard</span>
                </li>
            </ul>
        {{--<div class="alert alert-success">--}}
        {{--<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>--}}
        {{--Last Sync 2 days ago, Klik tombol sync untuk mendapatkan data terbaru <a href="" class="btn btn-sm green dropdown-toggle">Sync</a>--}}
        {{--</div>--}}
        <!-- END PAGE BREADCRUMB -->
            <a href="{!! URL::to('') !!}" class="btn btn-primary pull-right">Tambah</a>
            <br><br>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption font-dark">
                                <i class="icon-settings font-dark"></i>
                                <span class="caption-subject bold"></span>
                                Kerjasama Luar & Dalam Negeri
                            </div>
                            <div class="tools"> </div>
                        </div>
                        <div class="portlet-body">
                            <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
                                <thead>
                                <tr>
                                    <th rowspan="2">No</th>
                                    <th rowspan="2">Nama Instansi</th>
                                    <th rowspan="2">Jenis Kegiatan</th>
                                    <th colspan="2">Kurun Waktu</th>
                                    <th rowspan="2">Manfaat</th>
                                    <th rowspan="2">Kerjasama</th>
                                </tr>
                                <tr>
                                    <th colspan="1">Mulai</th>
                                    <th colspan="1">Berakhir</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>SMU Fatih Bilingual School, Banda Aceh</td>
                                    <td>Pembinaan Olimpiade Komputer SMU Fatih</td>
                                    <td>2010</td>
                                    <td>2010</td>
                                    <td>Meningkatkan kemampuan dosen dalam pengajaran materi untuk olimpiade</td>
                                    <td>Dalam Negeri</td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>Vellore Institure of Technology, India</td>
                                    <td>Kerjasama di bidang pendidikan, penelitian dan studi lanjut/td>
                                    <td>2011</td>
                                    <td>2015</td>
                                    <td>Memberikan kesempatan kepada dosen untuk melakukan penelitian bersama</td>
                                    <td>Luar Negeri</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>

            </div>





            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
@stop
@section('js')
    <!-- END PAGE LEVEL SCRIPTS -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{!! URL::to('local/resources/assets/assets/global/scripts/datatable.js')!!}" type="text/javascript"></script>
    <script src="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/datatables.min.js')!!}" type="text/javascript"></script>
    <script src="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')!!}" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="{!! URL::to('local/resources/assets/assets/global/scripts/app.min.js')!!}" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{!! URL::to('local/resources/assets/assets/pages/scripts/table-datatables-responsive.min.js')!!}" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
@stop