@extends('admin.layouts.index')
@section('css')
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/select2/css/select2.min.css') !!}" rel="stylesheet" type="text/css" />
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/select2/css/select2-bootstrap.min.css')!!}" rel="stylesheet" type="text/css" />
@stop
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">@include('notif')
            <!-- END PAGE BREADCRUMB -->
            <div class="row ">
                <div class="col-md-12">
                    <!-- BEGIN SAMPLE FORM PORTLET-->
                    <div class="portlet box purple ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-gift"></i> Form Tambah Substansi praktikum/praktek
                            </div>
                            <div class="pull-right" style="padding-top: 0.5%;">
                                <a href="{!! URL::to('kurikulum/praktikum/view') !!}" class="btn btn-sm yellow dropdown-toggle">Back</a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <form role="form" action="{!! URL::to('praktikum/store') !!}" method="post">
                                {!! csrf_field() !!}
                                <!-- /.row -->
                                <div class="row">
                                    <label class="control-label col-md-3">Pilih MK
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-12">
                                        <select id="multiple" name="mk_id" class="form-control select2-multiple">
                                            @foreach($result as $value)
                                                <option value="{!! $value->id !!}">{!! $value->nama_mk !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                    <br>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="">Judul Modul</label>
                                        <input type="text" class="form-control" name="judul">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="">Jam Pelaksanaan </label>
                                        <input type="text" class="form-control" name="jam">
                                    </div>
                                </div>
                                    <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="">Tempat/Lokasi Praktikum/Praktek</label>
                                        <input type="text" class="form-control" name="tempat">
                                    </div>
                                </div>
                                <br>
                            <input type="submit" class="btn btn-primary" value="Simpan">
                            </form>
                        </div>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->

@stop
@section('js')
    <script src="{!! URL::to('local/resources/assets/assets/global/plugins/select2/js/select2.full.min.js') !!}" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="{!! URL::to('local/resources/assets/assets/global/scripts/app.min.js')!!}" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{!! URL::to('local/resources/assets/assets/pages/scripts/components-select2.min.js')!!}" type="text/javascript"></script>

@stop
