@extends('admin.layouts.index')
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">@include('notif')
        <!-- END PAGE BREADCRUMB -->
            <div class="row ">
                <div class="col-md-12">
                    <!-- BEGIN SAMPLE FORM PORTLET-->
                    <div class="portlet box purple ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-gift"></i> Form Tambah Aksesibilitas  </div>
                            <div class="pull-right" style="padding-top: 0.5%;">
                                <a href="{!! URL::to('aksesibilitas/view') !!}" class="btn btn-sm yellow dropdown-toggle">Back</a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <form role="form" action="{!! URL::to('aksesibilitas/store') !!}" method="post">
                                {!! csrf_field() !!}
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="">Jenis Data</label><span class="required" aria-required="true"> * </span>
                                        <input type="text" class="form-control" name="jenis_data">
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="md-radio-inline">Manual
                                            <div class="md-radio">
                                                <input type="radio" id="checkbox1_833" name="manual" value="Y" class="md-radiobtn">
                                                <label for="checkbox1_833">
                                                    <span></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span>Yes</label>
                                            </div>
                                            <div class="md-radio">
                                                <input type="radio" id="checkbox1_933" name="manual" value="N" class="md-radiobtn">
                                                <label for="checkbox1_933">
                                                    <span></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span> No </label>
                                            </div>
                                        </div>
                                    </div>

                                        <div class="col-md-6">
                                            <div class="md-radio-inline">Komputer Tanpa Jaringan
                                                <div class="md-radio">
                                                    <input type="radio" id="checkbox1_822" name="komputer" value="Y" class="md-radiobtn">
                                                    <label for="checkbox1_822">
                                                        <span></span>
                                                        <span class="check"></span>
                                                        <span class="box"></span>Yes</label>
                                                </div>
                                                <div class="md-radio">
                                                    <input type="radio" id="checkbox1_922" name="komputer" value="N" class="md-radiobtn">
                                                    <label for="checkbox1_922">
                                                        <span></span>
                                                        <span class="check"></span>
                                                        <span class="box"></span> No </label>
                                                </div>
                                            </div>
                                        </div>

                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="md-radio-inline">Komputer Jaringan Lokal (LAN)
                                            <div class="md-radio">
                                                <input type="radio" id="checkbox1_81" name="lan" value="Y" class="md-radiobtn">
                                                <label for="checkbox1_81">
                                                    <span></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span>Yes</label>
                                            </div>
                                            <div class="md-radio">
                                                <input type="radio" id="checkbox1_91" name="lan" value="N" class="md-radiobtn">
                                                <label for="checkbox1_91">
                                                    <span></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span> No </label>
                                            </div>
                                        </div>
                                    </div>

                                        <div class="col-md-6">
                                            <div class="md-radio-inline">Komputer Jaringan Luas (WAN)
                                                <div class="md-radio">
                                                    <input type="radio" id="checkbox1_8" name="wan" value="Y" class="md-radiobtn">
                                                    <label for="checkbox1_8">
                                                        <span></span>
                                                        <span class="check"></span>
                                                        <span class="box"></span>Yes</label>
                                                </div>
                                                <div class="md-radio">
                                                    <input type="radio" id="checkbox1_9" name="wan" value="N" class="md-radiobtn">
                                                    <label for="checkbox1_9">
                                                        <span></span>
                                                        <span class="check"></span>
                                                        <span class="box"></span> No </label>
                                                </div>
                                            </div>
                                        </div>


                                </div>


                                <br><br>
                                <input type="submit" class="btn btn-primary" value="Simpan">
                            </form>
                        </div>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->

@stop