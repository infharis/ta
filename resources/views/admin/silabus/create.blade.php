@extends('admin.layouts.index')
@section('css')
    <link href="{!! URL::to('local/resources/assets/selectize/css/selectize.bootstrap3.css') !!}" rel="stylesheet">
@stop
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- END PAGE BREADCRUMB -->
            <div class="row ">
                <div class="col-md-12">
                    <!-- BEGIN SAMPLE FORM PORTLET-->
                    <div class="portlet box purple ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-gift"></i> Form Tambah Peninjauan Silabus/SAP dan Buku Ajar
                            </div>
                            <div class="pull-right" style="padding-top: 0.5%;">
                                <a href="{!! URL::to('kurikulum/silabus/view') !!}" class="btn btn-sm yellow dropdown-toggle">Back</a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <form role="form" action="{!! URL::to('kurikulum/silabus/store') !!}" method="post">
                                {!! csrf_field() !!}
                                <!-- /.row -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="">MK</label><span class="required" aria-required="true"> * </span>
                                        <select name="mk" required data-md-selectize>
                                            <option value="">-- Pilih MK --</option>
                                            @foreach($mk as $value)
                                                <option value="{!! $value->id !!}">{!! $value->kode_mk.' '.$value->nama_mk !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="">Status MK</label><span class="required" aria-required="true"> * </span>
                                        <select name="status" required data-md-selectize>
                                            <option value="">-- Pilih Status MK --</option>
                                            <option value="1">Baru</option>
                                            <option value="2">Lama</option>
                                            <option value="3">Hapus</option>
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="">Berlaku mulai Th.</label>
                                        <input type="text" maxlength="4"  name="berlaku" class="form-control">
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                    <label class="control-label col-md-2">Perubahan Pada</label>
                                    <div class="md-radio-inline">&nbsp;&nbsp;&nbsp;
                                        <div class="md-radio">
                                            <input type="radio" id="checkbox1_21" name="silabus" value="Y" class="md-radiobtn">
                                            <label for="checkbox1_21">
                                                <span></span>
                                                <span class="check"></span>
                                                <span class="box"></span> Silabus/SAP </label>
                                        </div>
                                        <div class="md-radio">
                                            <input type="radio" id="checkbox1_22" name="buku_ajar" value="Y" class="md-radiobtn" checked="">
                                            <label for="checkbox1_22">
                                                <span></span>
                                                <span class="check"></span>
                                                <span class="box"></span> Buku Ajar </label>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                <div class="row">
                                    </label>
                                    <div class="col-md-6">
                                        <label class="control-label">Alasan Peninjauan</label>
                                        <input type="text" class="form-control" name="alasan">
                                    </div>
                                    <div class="col-md-6">
                                        <label class="control-label">Atas usulan/ masukan  dari</label>
                                        <input type="text" class="form-control" name="usulan">
                                    </div>
                                </div>
                                <br>
                            <input type="submit" class="btn btn-primary" value="Simpan">
                            </form>
                        </div>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->

@stop
@section('js')
    <script src="{!! URL::to('local/resources/assets/selectize/common.min.js')!!}"></script>
    <script src="{!! URL::to('local/resources/assets/selectize/uikit_custom.min.js')!!}"></script>
    <script src="{!! URL::to('local/resources/assets/selectize/altair_admin_common.min.js')!!}"></script>
@stop
