@extends('admin.layouts.index')
@section('css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/datatables.min.css') !!}" rel="stylesheet" type="text/css" />
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')!!}" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->
@stop
@section('content')
    {{-- begin content --}}
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>
                        Peninjauan silabus/SAP dan buku ajar
                    </h1>
                </div>
            <div class="pull-right">
                <a href="{!! URL::to('kurikulum') !!}" class="btn btn-sm yellow dropdown-toggle">Back</a>
                <a href="{!! URL::to('kurikulum/silabus/create') !!}" class="btn btn-primary">Tambah</a>
            </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption font-dark">
                                <i class="icon-settings font-dark"></i>
                                <span class="caption-subject bold uppercase"></span>

                            </div>
                            {{--<div class="tools"> </div>--}}
                        </div>
                        <div class="portlet-body">
                            <div class="portlet-body flip-scroll">
                                <table class="table table-bordered table-striped table-condensed flip-content" id="sample_2">
                                    <thead class="flip-content">
                                    <tr>
                                        <th rowspan="2">No</th>
                                        <th rowspan="2">Kode MK</th>
                                        <th rowspan="2">Nama MK</th>
                                        <th rowspan="2">MK Baru/Lama/Hapus</th>
                                        <th colspan="2" class="text-center">Perubahan Pada</th>
                                        <th rowspan="2">Alasan Peninjauan</th>
                                        <th rowspan="2">Atas usulan/ masukan  dari</th>
                                        <th rowspan="2">Berlaku mulai Sem/Th</th>
                                        <th rowspan="2">Aksi</th>
                                    </tr>
                                    <tr>
                                        <th colspan="1">Silabus/SAP</th>
                                        <th colspan="1">Buku Ajar</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=1;?>
                                    @foreach($result as $value)
                                        <?php
                                            $single=$value->Mk($value->mk_id);
                                        ?>
                                        <tr>
                                            <td>{!! $i++ !!}</td>
                                            <td>{!! $single->kode_mk !!}</td>
                                            <td>{!! $single->nama_mk !!}</td>
                                            <td>
                                                @if($value->status_mk==1)
                                                    Baru
                                                @elseif($value->status_mk==2)
                                                    Lama
                                                @elseif($value->status_mk==3)
                                                    Hapus
                                                 @endif
                                            </td>
                                            <td>{!! $value->silabus !!}</td>
                                            <td>{!! $value->buku_ajar !!}</td>
                                            <td>{!! $value->alasan !!}</td>
                                            <td>{!! $value->usulan !!}</td>
                                            <td>{!! $value->berlaku !!}</td>
                                            <td>
                                                <a href="{!! URL::to('kurikulum/silabus/edit/'.$value->id) !!}" class="btn btn-primary">Edit</a>
                                                <a href="#" id="delete" value="{!! $value->id !!}" class="btn btn-danger">Delete</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    {{-- end content --}}

@stop
@section('js')
    <script type="text/javascript">
        $(document).ready(function(){

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $(document).on("click","#delete",function(){
                if (confirm('Are You Sure Delete This Data?')) {
                    var val = $(this).attr('value');
                    $.ajax({
                        type: 'DELETE',
                        url: '{!! URL::to('kurikulum/silabus/destroy') !!}',
                        data: {
                            id: val
                        },
                        success: function (response) {
                            if (response == 1) {
                                alert('Data Berhasil Dihapus');
                                location.reload();
                            } else {
                                alert('Gagal Menghapus Data');
                            }
                        }
                    });
                } else {
                    return false;
                }
            });
        });
    </script>
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{!! URL::to('local/resources/assets/assets/global/scripts/datatable.js') !!}" type="text/javascript"></script>
    <script src="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/datatables.min.js')!!}" type="text/javascript"></script>
    <script src="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')!!}" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src={!! URL::to('local/resources/assets/assets/pages/scripts/table-datatables-colreorder.min.js')!!} type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
@stop