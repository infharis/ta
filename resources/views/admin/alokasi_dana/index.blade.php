@extends('admin.layouts.index')
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">@include('notif')
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light portlet-fit bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class=" icon-layers font-green"></i>
                                <span class="caption-subject font-green bold uppercase">Perolehan Dan Alokasi Dana</span>
                            </div>
                            {{--<div class="pull-right" style="padding-top: 0.5%;">--}}
                                {{--<a href="{!! URL::to('data/dosen/create') !!}" class="btn btn-sm yellow dropdown-toggle">Tambah</a>--}}
                            {{--</div>--}}
                        </div>
                        <div class="portlet-body">
                            <div class="mt-element-step">
                                <div class="row step-default">
                                    <div class="col-md-6 bg-grey mt-step-col">
                                        {{--<div class="mt-step-number bg-white font-grey"></div>--}}
                                        <a href="{!! URL::to('realisasi_dana/view') !!}" style="text-decoration:none">
                                            <div class="mt-step-title font-grey-cascade">Perolehan Dana</div>
                                            <div class="mt-step-content font-grey-cascade">Realisasi perolehan dan alokasi dana</div>
                                        </a>
                                    </div>
                                    <div class="col-md-6 bg-grey mt-step-col active">
                                        <a href="{!! URL::to('pengunaan_dana/view') !!}" style="text-decoration:none">
                                            <div class="mt-step-title font-grey-cascade">Penggunaan Dana</div>
                                            <div class="mt-step-content font-grey-cascade">Proporsi penggunaan dana </div>
                                        </a>
                                    </div>
                                    <div class="col-md-6 bg-grey mt-step-col active">
                                        {{--<div class="mt-step-number bg-white font-grey"></div>--}}
                                        <a href="{!! URL::to('pembiayaan/penelitian') !!}" style="text-decoration:none">
                                            <div class="mt-step-title font-grey-cascade">Data Penelitian</div>
                                            <div class="mt-step-content font-grey-cascade">Dana untuk kegiatan penelitian </div>
                                        </a>
                                    </div>
                                    <div class="col-md-6 bg-grey mt-step-col">
                                        {{--<div class="mt-step-number bg-white font-grey"></div>--}}
                                        <a href="{!! URL::to('pembiayaan/p2m') !!}" style="text-decoration:none">
                                            <div class="mt-step-title font-grey-cascade">Data P2M</div>
                                            <div class="mt-step-content font-grey-cascade">Dana untuk kegiatan P2M </div>
                                        </a>
                                    </div>

                                </div>
                                <br/>
                                <br/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
@stop


