<div class="page-sidebar navbar-collapse collapse">
    <ul class="page-sidebar-menu   " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
        <li class="nav-item {!! Request::is('admin*') ? 'active open' : '' !!}">
            <a href="{!! URL::to('admin') !!}" class="nav-link nav-toggle">
                <i class="icon-home"></i>
                <span class="title">Dashboard</span>
                {{--<span class="arrow"></span>--}}
            </a>
        </li>
        <li class="nav-item {!! Request::is('umpan_balik*','mhs*') ? 'active open' : '' !!} {!! Request::is('data/mhs*') ? 'active open' : '' !!} ">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="icon-docs"></i>
                <span class="title">Mhs Dan Lulusan</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item {!! Request::is('mhs/edit*','mhs/view*') ? 'active open' : '' !!} {!! Request::is('data/mhs*') ? 'active open' : '' !!} ">
                    <a href="{!! URL::to('mhs/view') !!}" class="nav-link ">
                        {{--<i class="icon-user"></i>--}}
                        <span class="title">Data Mahasiswa</span>
                    </a>
                </li>
                <li class="nav-item {!! Request::is('mhs/layanan*') ? 'active open' : '' !!} ">
                    <a href="{!! URL::to('mhs/layanan') !!}" class="nav-link ">
                        {{--<i class="icon-user"></i>--}}
                        <span class="title">Layanan Mhs</span>
                    </a>
                </li>
                <li class="nav-item {!! Request::is('mhs/evaluasi*') ? 'active open' : '' !!} ">
                    <a href="{!! URL::to('mhs/evaluasi') !!}" class="nav-link ">
                        {{--<i class="icon-user"></i>--}}
                        <span class="title">Evaluasi Lulusan</span>
                    </a>
                </li>
                <li class="nav-item {!! Request::is('umpan_balik/*') ? 'active open' : '' !!} ">
                    <a href="{!! URL::to('umpan_balik/view') !!}" class="nav-link ">
                        {{--<i class="icon-user"></i>--}}
                        <span class="title">Umpan Balik</span>
                    </a>
                </li>
            </ul>
        </li>

        <li class="nav-item {!! Request::is('sdm*') ? 'active open' : '' !!} {!! Request::is('data/dosen/create') ? 'active open' : '' !!}  ">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="icon-docs"></i>
                <span class="title">Sumber Daya Manusia</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                {{-- daftar dosen tetap --}}
                <li class="nav-item {!! Request::is('sdm/data/aktivitas_dosen_diluar_ps','sdm/data/aktivitas_dosen_tetap','sdm/aktivitas*','sdm/data/dosen_tetap*','sdm/data/dosen_diluar_ps*')  ? 'active open' : '' !!}">
                    <a href="{!! URL::to('sdm/data/dosen_tetap') !!}" class="nav-link ">
                        {{--<i class="icon-user"></i>--}}
                        <span class="title">Data Dosen Tetap</span>
                    </a>
                </li>
                <li class="nav-item {!! Request::is('sdm/dosen_tidak_tetap*','sdm/data_dosen_tidak_tetap') ? 'active open' : '' !!} ">
                    <a href="{!! URL::to('sdm/data_dosen_tidak_tetap') !!}" class="nav-link ">
                        {{--<i class="icon-user"></i>--}}
                        <span class="title">Data Dosen Tidak Tetap</span>
                    </a>
                </li>
                <li class="nav-item {!! Request::is('sdm/dosen_tetap/organisasi','sdm/dosen/prestasi','sdm/dosen_tetap/kegiatan','sdm/dosen_tetap/tugas_belajar','sdm/pakar*','sdm/upaya_peningkatan') ? 'active open' : '' !!} ">
                    <a href="{!! URL::to('sdm/upaya_peningkatan') !!}" class="nav-link ">
                        {{--<i class="icon-user"></i>--}}
                        <span class="title">Upaya Peningkatan SDM</span>
                    </a>
                </li>
                <li class="nav-item {!! Request::is('sdm/kependidikan/*') ? 'active open' : '' !!} ">
                    <a href="{!! URL::to('sdm/kependidikan/view') !!}" class="nav-link ">
                        {{--<i class="icon-user"></i>--}}
                        <span class="title">Tenaga Kependidikan</span>
                    </a>
                </li>
            </ul>
        </li>

        <li class="nav-item {!! Request::is('suasana_akademik*','kurikulum*','bimbingan*') ? 'active open' : '' !!} ">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="icon-docs"></i>
                <span class="title">Kurikulum & Akademik</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item {!! Request::is('kurikulum*') ? 'active open' : '' !!} ">
                    <a href="{!! URL::to('kurikulum') !!}" class="nav-link ">
                        {{--<i class="icon-user"></i>--}}
                        <span class="title">Kurikulum</span>
                    </a>
                </li>
                <li class="nav-item {!! Request::is('bimbingan*') ? 'active open' : '' !!} ">
                    <a href="{!! URL::to('bimbingan') !!}" class="nav-link ">
                        {{--<i class="icon-user"></i>--}}
                        <span class="title">Bimbingan</span>
                    </a>
                </li>
                <li class="nav-item {!! Request::is('suasana_akademik') ? 'active open' : '' !!} ">
                    <a href="{!! URL::to('suasana_akademik') !!}" class="nav-link ">
                        {{--<i class="icon-user"></i>--}}
                        <span class="title">Peningkatan Suasana Akademik</span>
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-item {!! Request::is('aksesibilitas*','peralatan*','pustaka*','data/prasarana*','ruang_kerja*','pembiayaan*','realisasi_dana*','pengunaan_dana*') ? 'active open' : '' !!} ">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="icon-docs"></i>
                <span class="title">Pembiayaan & Sarana </span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item {!! Request::is('pembiayaan/penelitian','pengunaan_dana*','realisasi_dana*','pembiayaan/alokasi_dana') ? 'active open' : '' !!} ">
                    <a href="{!! URL::to('pembiayaan/alokasi_dana') !!}" class="nav-link ">
                        {{--<i class="icon-user"></i>--}}
                        <span class="title">Perolehan Alokasi Dana</span>
                    </a>
                </li>
                <li class="nav-item {!! Request::is('data/prasarana*','ruang_kerja*','pembiayaan/prasarana') ? 'active open' : '' !!} ">
                    <a href="{!! URL::to('pembiayaan/prasarana') !!}" class="nav-link ">
                        {{--<i class="icon-user"></i>--}}
                        <span class="title">Prasarana</span>
                    </a>
                </li>
                <li class="nav-item {!! Request::is('aksesibilitas*','peralatan*','pustaka*','pembiayaan/sarana') ? 'active open' : '' !!} ">
                    <a href="{!! URL::to('pembiayaan/sarana') !!}" class="nav-link ">
                        {{--<i class="icon-user"></i>--}}
                        <span class="title">Sarana Kegiatan Akademik</span>
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-item {!! Request::is('kerjasama*','p2m*','penelitian*') ? 'active open' : '' !!} ">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="icon-docs"></i>
                <span class="title">Penelitian & P2M </span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item {!! Request::is('penelitian*') ? 'active open' : '' !!} ">
                    <a href="{!! URL::to('penelitian/dosen_tetap') !!}" class="nav-link ">
                        {{--<i class="icon-user"></i>--}}
                        <span class="title">Penelitian Dosen</span>
                    </a>
                </li>
                <li class="nav-item {!! Request::is('p2m*') ? 'active open' : '' !!} ">
                    <a href="{!! URL::to('p2m/kegiatan') !!}" class="nav-link ">
                        {{--<i class="icon-user"></i>--}}
                        <span class="title">Kegiatan P2M</span>
                    </a>
                </li>
                <li class="nav-item {!! Request::is('kerjasama*') ? 'active open' : '' !!} ">
                    <a href="{!! URL::to('kerjasama/view') !!}" class="nav-link ">
                        {{--<i class="icon-user"></i>--}}
                        <span class="title">Kegiatan Kerjasama</span>
                    </a>
                </li>
            </ul>
        </li>

    </ul>
    <!-- END SIDEBAR MENU -->
</div>