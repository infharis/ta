<div class="page-header navbar navbar-fixed-top">
    <!-- BEGIN HEADER INNER -->
    <div class="page-header-inner ">
        <!-- BEGIN LOGO -->
        <div class="page-logo">
            <a href="#">
                <img src="https://i0.wp.com/ppl.fkip.unsyiah.ac.id/wp-content/uploads/2016/08/Logo-Unsyiah-Kuning-HD-1012x972-Transparan-1.png" width="64" height="64" alt="logo" class="logo-default" />
                </a>
            <div class="menu-toggler sidebar-toggler">
                <!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->

            </div>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
        <!-- END RESPONSIVE MENU TOGGLER -->

        <!-- BEGIN PAGE TOP -->
        <div class="page-top">
            <!-- BEGIN TOP NAVIGATION MENU -->
            <div class="top-menu">
                <ul class="nav navbar-nav pull-right">

                    <!-- END TODO DROPDOWN -->
                    <!-- BEGIN USER LOGIN DROPDOWN -->
                    <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                    <li class="dropdown dropdown-user dropdown-dark">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            {{--<span class="username username-hide-on-mobile"> Admin </span>--}}
                            <span class="username username-hide-on-mobile"> {!! Auth::user()->name !!} </span>
                            <!-- DOC: Do not remove below empty space(&nbsp;) as its purposely used -->
                            <img alt="" class="img-circle" src="{!! URL::to('local/storage/app/home/operator.png') !!}" /> </a>
                        <ul class="dropdown-menu dropdown-menu-default">
                            <li>
                                <a href="{!! URL::to('login/as') !!}">
                                    <i class="icon-user"></i> Login As </a>
                            </li>
                            <li>
                                <a href="{!! URL::to('change_password_admin') !!}">
                                    <i class="icon-user"></i> Ubah Password </a>
                            </li>
                            <li>
                                <a href="{!! URL::to('logout') !!}">
                                    <i class="icon-key"></i> Keluar </a>
                            </li>
                        </ul>
                    </li>
                    <!-- END USER LOGIN DROPDOWN -->
                </ul>
            </div>
            <!-- END TOP NAVIGATION MENU -->
        </div>
        <!-- END PAGE TOP -->
    </div>
    <!-- END HEADER INNER -->
</div>