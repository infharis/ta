@extends('admin.layouts.index')
@section('css')
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/select2/css/select2.min.css') !!}" rel="stylesheet" type="text/css" />
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/select2/css/select2-bootstrap.min.css')!!}" rel="stylesheet" type="text/css" />
@stop
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">@include('notif')
        <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>
                        Form Tambah Penggunaan Dana
                    </h1>
                </div>
                <div class="pull-right" style="padding-top: 0.5%;">
                    <a href="{!! URL::to('pengunaan_dana/view') !!}" class="btn btn-sm yellow dropdown-toggle">Back</a>
                </div>
            </div>

            <!-- END PAGE BREADCRUMB -->
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="tools"></div>
                        </div>
                        <div class="portlet-body">
                            <form class="form-horizontal" role="form" action="{!! URL::to('pengunaan_dana/store') !!}" method="post">
                                {!! csrf_field() !!}
                                <div  class="form-group">
                                    <label for="inputEmail1" class="col-md-2 control-label">Jenis Dana</label>
                                    <div class="col-md-10">
                                        <select name="jd[]" required class="form-control select2-multiple" multiple>
                                            <option value="0">--Pilih Jenis Dana--</option>
                                            @foreach($result as $value)
                                                <option value="{!! $value->id !!}">{!! $value->nama_jenis_dana !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div  class="form-group">
                                    <label for="inputEmail1" class="col-md-2 control-label">Nama Jenis Penggunaan</label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" name="jp" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-offset-2 col-md-10">
                                        <button type="submit" class="btn blue">Simpan</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
@stop
@section('js')
    <script src="{!! URL::to('local/resources/assets/assets/global/plugins/select2/js/select2.full.min.js') !!}" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="{!! URL::to('local/resources/assets/assets/global/scripts/app.min.js')!!}" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{!! URL::to('local/resources/assets/assets/pages/scripts/components-select2.min.js')!!}" type="text/javascript"></script>
    <!-- BEGIN PAGE LEVEL PLUGINS -->
@stop