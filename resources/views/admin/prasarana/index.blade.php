@extends('admin.layouts.index')
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">@include('notif')
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light portlet-fit bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class=" icon-layers font-green"></i>
                                <span class="caption-subject font-green bold uppercase">Data Prasarana & Ruang Kerja</span>
                            </div>
                            {{--<div class="pull-right" style="padding-top: 0.5%;">--}}
                                {{--<a href="{!! URL::to('data/dosen/create') !!}" class="btn btn-sm yellow dropdown-toggle">Tambah</a>--}}
                            {{--</div>--}}
                        </div>
                        <div class="portlet-body">
                            <div class="mt-element-step">
                                <div class="row step-default">
                                    <div class="col-md-12 bg-grey mt-step-col">
                                        {{--<div class="mt-step-number bg-white font-grey"></div>--}}
                                        <a href="{!! URL::to('ruang_kerja/view') !!}" style="text-decoration:none">
                                            <div class="mt-step-title font-grey-cascade">Ruang Kerja</div>
                                            <div class="mt-step-content font-grey-cascade">Data ruang kerja dosen </div>
                                        </a>
                                    </div>
                                    <div class="col-md-12 bg-grey mt-step-col active">
                                        <a href="{!! URL::to('data/prasarana') !!}" style="text-decoration:none">
                                            <div class="mt-step-title font-grey-cascade">Data Prasarana</div>
                                            <div class="mt-step-content font-grey-cascade">Data prasarana yang dipergunakan PS dalam proses belajar mengajar  </div>
                                        </a>
                                    </div>
                                    <div class="col-md-12 bg-grey mt-step-col">
                                        {{--<div class="mt-step-number bg-white font-grey"></div>--}}
                                        <a href="{!! URL::to('data/prasarana_lain') !!}" style="text-decoration:none">
                                            <div class="mt-step-title font-grey-cascade">Prasarana Lain</div>
                                            <div class="mt-step-content font-grey-cascade">Data prasarana lain yang menunjang </div>
                                        </a>
                                    </div>

                                </div>
                                <br/>
                                <br/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
@stop


