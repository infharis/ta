@extends('admin.layouts.index')
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- END PAGE BREADCRUMB -->
            <div class="row ">
                <div class="col-md-12">
                    <!-- BEGIN SAMPLE FORM PORTLET-->
                    <div class="portlet box purple ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-gift"></i> Form Edit Data Prasarana
                            </div>
                            <div class="pull-right" style="padding-top: 0.5%;">
                                <a href="{!! URL::to('data/prasarana/') !!}" class="btn btn-sm yellow dropdown-toggle">Back</a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <form role="form" action="{!! URL::to('data/prasarana/update/'.$result->id) !!}" method="post">
                            {!! csrf_field() !!}
                            <!-- /.row -->
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="">Jenis Prasarana</label>
                                        <input type="text" class="form-control" name="jenis_prasarana" value="{!! $result->jenis_prasarana !!}">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="">Jumlah Unit</label>
                                        <input type="number" class="form-control" name="jumlah_unit" value="{!! $result->jumlah_unit !!}">
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="">Total Luas</label>
                                        <input type="text" class="form-control" name="total_luas" value="{!! $result->total_luas !!}">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="">Utilisasi (Jam/Minggu)</label>
                                        <input type="text" class="form-control" name="utilisasi" value="{!! $result->utilisasi !!}">
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="md-radio-inline">Kepemilikan
                                            &nbsp;
                                            <div class="md-radio">
                                                <input type="radio" id="checkbox1_8" {!! $result->kepemilikan=='SD' ? 'checked' : ' ' !!} name="kepemilikan" value="SD" class="md-radiobtn">
                                                <label for="checkbox1_8">
                                                    <span></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span> Sendiri </label>
                                            </div>
                                            <div class="md-radio">
                                                <input type="radio" id="checkbox1_9"  {!! $result->kepemilikan=='SW' ? 'checked' : ' ' !!} name="kepemilikan"  value="SW"  class="md-radiobtn">
                                                <label for="checkbox1_9">
                                                    <span></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span> Sewa </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="md-radio-inline">Kondisi
                                            &nbsp;
                                            <div class="md-radio">
                                                <input type="radio" id="checkbox1_10"  {!! $result->kondisi=='Y' ? 'checked' : ' ' !!} name="kondisi" value="Y"  class="md-radiobtn">
                                                <label for="checkbox1_10">
                                                    <span></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span> Terawat </label>
                                            </div>
                                            <div class="md-radio">
                                                <input type="radio" id="checkbox1_11" name="kondisi" {!! $result->kondisi=='N' ? 'checked' : ' ' !!}  value="N"  class="md-radiobtn">
                                                <label for="checkbox1_11">
                                                    <span></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span> Tidak Terawat </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <input type="submit" class="btn btn-primary" value="Update">
                            </form>
                        </div>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->

@stop
