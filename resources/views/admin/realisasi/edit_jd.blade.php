@extends('admin.layouts.index')
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">@include('notif')
        <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>
                        Form Edit Jenis Dana
                    </h1>
                </div>
                <div class="pull-right">
                    <a href="{!! URL::to('realisasi_dana/view') !!}" class="btn btn-sm green dropdown-toggle">Back</a>
                </div>
            </div>

            <!-- END PAGE BREADCRUMB -->
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption font-dark">
                                <i class="icon-settings font-dark"></i>
                                <span class="caption-subject bold"></span>
                            </div>
                            <div class="tools"></div>
                        </div>
                        <div class="portlet-body">
                            <form class="form-horizontal" role="form" action="{!! URL::to('realisasi_dana/update_jd/'.$result->id) !!}" method="post">
                                {!! csrf_field() !!}
                                <div  class="form-group">
                                    <label for="inputEmail1" class="col-md-2 control-label">Sumber Dana</label>
                                    <div class="col-md-10">
                                        <select name="sd" id="" class="form-control" required>
                                            <option value="">Pilih Sumber Dana</option>
                                            @foreach($sd as $value)
                                                @if($value->id==$result->sumber_dana_id)
                                                    <option selected value="{!! $value->id !!}">{!! $value->nama_sumber_dana !!}</option>
                                                @else
                                                    <option value="{!! $value->id !!}">{!! $value->nama_sumber_dana !!}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div  class="form-group">
                                    <label for="inputEmail1" class="col-md-2 control-label">Jenis Dana</label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" name="jd" required value="{!! $result->nama_jenis_dana !!}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-offset-2 col-md-10">
                                        <button type="submit" class="btn blue">Update</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>

            </div>





            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
@stop