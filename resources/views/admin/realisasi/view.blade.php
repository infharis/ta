@extends('admin.layouts.index')
@section('css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/datatables.min.css') !!}" rel="stylesheet" type="text/css" />
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')!!}" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->
@stop
@section('content')
    {{-- begin content --}}
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">@include('notif')
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>
                        Realisasi perolehan dan alokasi dana
                    </h1>
                </div>
                <div class="pull-right">
                    <a href="{!! URL::to('pembiayaan/alokasi_dana') !!}" class="btn btn-warning">Back</a>
                    <a href="{!! URL::to('realisasi_dana/create_sd') !!}" class="btn btn-primary">Tambah Sumber Dana</a>
                    <a href="{!! URL::to('realisasi_dana/create_jd') !!}" class="btn btn-danger">Tambah Jenis Dana</a>
                </div>
                <!-- END PAGE TITLE -->
                <!-- BEGIN PAGE TOOLBAR -->

                <!-- END PAGE TOOLBAR -->
            </div>
            <!-- END PAGE HEAD-->
            <!-- BEGIN PAGE BREADCRUMB -->

            <!-- END PAGE BREADCRUMB -->
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption font-dark">
                                <i class="icon-settings font-dark"></i>
                                <span class="caption-subject bold uppercase"></span>

                            </div>
                            {{--<div class="tools"> </div>--}}
                        </div>
                        <div class="portlet-body">
                            <div class="portlet-body flip-scroll">
                                <table class="table table-bordered table-striped table-condensed flip-content" id="sample_2">
                                    <thead class="flip-content">
                                    <tr>
                                        <th rowspan="2" style="width:10px">No</th>
                                        <th rowspan="2">Sumber Dana</th>
                                        <th rowspan="2" style="width:30%" class="text-center">Jenis Dana</th>
                                        <th colspan="3" class="text-center">Jumlah dana</th>
                                        <th colspan="2" class="text-center">Aksi</th>
                                    </tr>
                                    <tr>
                                        <th colspan="1">TS-2 <b>{!! $ts2 !!}</b></th>
                                        <th colspan="1">TS-1 <b>{!! $ts1 !!}</b></th>
                                        <th colspan="1">TS   <b>{!! $ts !!}</b></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=1;?>
                                    @foreach($result as $value)
                                        <?php $re=$value->Common('jenis_dana',array('sumber_dana_id'=>$value->id));
                                         $count=1;
                                         ?>
                                        @foreach($re->get() as $va)
                                            @if($count==1)
                                            <tr>
                                                <td rowspan="{!! $re->count() !!}">{!! $i++ !!}</td>
                                                <td rowspan="{!! $re->count() !!}">{!! $value->nama_sumber_dana !!}</td>
                                                <td>{!! $va->nama_jenis_dana !!}</td>
                                                <td>
                                                    {!! $value->SumJD($va->id,$ts2) !!}
                                                </td>
                                                <td>
                                                    {!! $value->SumJD($va->id,$ts1) !!}
                                                </td>
                                                <td>
                                                    {!! $value->SumJD($va->id,$ts) !!}
                                                </td>
                                                <td>
                                                    <a href="{!! URL::to('realisasi_dana/edit_sd/'.$value->id) !!}" class="btn btn-warning">Edit</a>
                                                </td>
                                                <td>
                                                    <a href="{!! URL::to('realisasi_dana/edit_jd/'.$va->id) !!}" class="btn btn-primary">Edit</a>
                                                </td>
                                            </tr>
                                                <?php $count++; ?>
                                            @else
                                                <tr>
                                                    <td>{!! $va->nama_jenis_dana !!}</td>
                                                    <td>
                                                        {!! $value->SumJD($va->id,$ts2) !!}
                                                    </td>
                                                    <td>
                                                        {!! $value->SumJD($va->id,$ts1) !!}
                                                    </td>
                                                    <td>
                                                        {!! $value->SumJD($va->id,$ts) !!}
                                                    </td>
                                                    <td>

                                                    </td>
                                                    <td>
                                                        <a href="{!! URL::to('realisasi_dana/edit_jd/'.$va->id) !!}" class="btn btn-primary">Edit</a>
                                                    </td>

                                                </tr>
                                            @endif
                                        @endforeach
                                    @endforeach

                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    {{-- end content --}}

@stop
@section('js')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{!! URL::to('local/resources/assets/assets/global/scripts/datatable.js') !!}" type="text/javascript"></script>
    <script src="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/datatables.min.js')!!}" type="text/javascript"></script>
    <script src="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')!!}" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src={!! URL::to('local/resources/assets/assets/pages/scripts/table-datatables-colreorder.min.js')!!} type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
@stop