@extends('admin.layouts.index')
@section('css')
    <link href="{!! URL::to('local/resources/assets/selectize/css/selectize.bootstrap3.css') !!}" rel="stylesheet">
@stop
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- END PAGE BREADCRUMB -->
            <div class="row ">
                <div class="col-md-12">
                    <!-- BEGIN SAMPLE FORM PORTLET-->
                    <div class="portlet box purple ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-gift"></i> Form Edit Peralatan utama laboratorium
                            </div>
                            <div class="pull-right" style="padding-top: 0.5%;">
                                <a href="{!! URL::to('peralatan/lab') !!}" class="btn btn-sm yellow dropdown-toggle">Back</a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <form role="form" action="{!! URL::to('peralatan/update_peralatan/'.$result->id) !!}" method="post">
                            {!! csrf_field() !!}
                            <!-- /.row -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="">Daftar Lab</label><span class="required" aria-required="true"> * </span>
                                        <select name="data_lab_id" required data-md-selectize>
                                            <option value="">-- Pilih Lab --</option>
                                            @foreach($data_lab as $value)
                                                @if($value->id==$result->data_lab_id)
                                                    <option selected value="{!! $value->id !!}">{!! $value->nama_lab !!}</option>
                                                @else
                                                    <option value="{!! $value->id !!}">{!! $value->nama_lab !!}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="">Jenis Peralatan</label>
                                        <input type="text" class="form-control" name="jenis_peralatan" value="{!! $result->jenis_peralatan !!}">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="">Jumlah Unit</label>
                                        <input type="number" class="form-control" name="jumlah_unit" value="{!! $result->jumlah_unit !!}">
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="">Rata-rata Waktu Penggunaan (jam/minggu)</label>
                                        <input type="text" class="form-control" name="waktu" value="{!! $result->waktu !!}">
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="md-radio-inline">Kepemilikan
                                            &nbsp;
                                            <div class="md-radio">
                                                <input type="radio" id="checkbox1_8" {!! $result->kepemilikan=='SD' ? 'checked' : ' '  !!} name="kepemilikan" value="SD" class="md-radiobtn">
                                                <label for="checkbox1_8">
                                                    <span></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span> Sendiri </label>
                                            </div>
                                            <div class="md-radio">
                                                <input type="radio" id="checkbox1_9" {!! $result->kepemilikan=='SW' ? 'checked' : ' '  !!} name="kepemilikan"  value="SW"  class="md-radiobtn">
                                                <label for="checkbox1_9">
                                                    <span></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span> Sewa </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="md-radio-inline">Kondisi
                                            &nbsp;
                                            <div class="md-radio">
                                                <input type="radio" id="checkbox1_10" {!! $result->kondisi=='Y' ? 'checked' : ' '  !!} name="kondisi" value="Y"  class="md-radiobtn">
                                                <label for="checkbox1_10">
                                                    <span></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span> Terawat </label>
                                            </div>
                                            <div class="md-radio">
                                                <input type="radio" id="checkbox1_11"  {!! $result->kondisi=='N' ? 'checked' : ' '  !!} name="kondisi" value="N"  class="md-radiobtn">
                                                <label for="checkbox1_11">
                                                    <span></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span> Tidak Terawat </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <input type="submit" class="btn btn-primary" value="Update">
                            </form>
                        </div>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->

@stop
@section('js')
    <script src="{!! URL::to('local/resources/assets/selectize/common.min.js')!!}"></script>
    <script src="{!! URL::to('local/resources/assets/selectize/uikit_custom.min.js')!!}"></script>
    <script src="{!! URL::to('local/resources/assets/selectize/altair_admin_common.min.js')!!}"></script>
@stop