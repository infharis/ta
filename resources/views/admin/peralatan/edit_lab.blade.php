@extends('admin.layouts.index')
@section('css')
    <link href="{!! URL::to('local/resources/assets/selectize/css/selectize.bootstrap3.css') !!}" rel="stylesheet">
@stop
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">@include('notif')
        <!-- END PAGE BREADCRUMB -->
            <div class="row ">
                <div class="col-md-12">
                    <!-- BEGIN SAMPLE FORM PORTLET-->
                    <div class="portlet box purple ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-gift"></i> Form Edit Nama Lab
                            </div>
                            <div class="pull-right" style="padding-top: 0.5%;">
                                <a href="{!! URL::to('peralatan/lab') !!}" class="btn btn-sm yellow dropdown-toggle">Back</a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <form role="form" action="{!! URL::to('peralatan/update_lab/'.$result->id) !!}" method="post">
                            {!! csrf_field() !!}
                            <!-- /.row -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="">Nama Laboratorium</label>
                                        <input type="text" name="nama_lab" class="form-control" value="{!! $result->nama_lab !!}">
                                    </div>
                                </div>
                                <br>
                                <input type="submit" class="btn btn-primary" value="Update">
                            </form>
                        </div>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->

@stop
@section('js')
    <script src="{!! URL::to('local/resources/assets/selectize/common.min.js')!!}"></script>
    <script src="{!! URL::to('local/resources/assets/selectize/uikit_custom.min.js')!!}"></script>
    <script src="{!! URL::to('local/resources/assets/selectize/altair_admin_common.min.js')!!}"></script>
@stop
