@extends('admin.layouts.index')
@section('css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/datatables.min.css') !!}" rel="stylesheet" type="text/css" />
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')!!}" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->
@stop
@section('content')
    {{-- begin content --}}
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">@include('notif')
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>
                        Peralatan Lab
                    </h1>
                </div>
                <div class="pull-right">
                    <a href="{!! URL::to('pembiayaan/sarana') !!}" class="btn btn-warning">Back</a>
                    <a href="{!! URL::to('peralatan/create_lab') !!}" class="btn btn-danger">Tambah Data Lab</a>
                    <a href="{!! URL::to('peralatan/create_peralatan') !!}" class="btn btn-primary">Tambah Peralatan Lab</a>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption font-dark">
                                <i class="icon-settings font-dark"></i>
                                <span class="caption-subject bold uppercase"></span>
                            </div>
                            {{--<div class="tools"> </div>--}}
                        </div>
                        <div class="portlet-body">
                            <div class="portlet-body flip-scroll">
                                <table class="table table-bordered table-striped table-condensed flip-content" id="sample_2">
                                    <thead class="flip-content">
                                    <tr>
                                        <th rowspan="2">No</th>
                                        <th rowspan="2">Nama Lab</th>
                                        <th rowspan="2">Jenis Peralatan Utama</th>
                                        <th rowspan="2">Jumlah Unit</th>
                                        <th colspan="2">Kepemilikan</th>
                                        <th colspan="2">Kondisi</th>
                                        <th rowspan="2">Rata-rata Waktu Penggunaan (jam/minggu)</th>
                                        <th colspan="2" rowspan="2" class="text-center">Aksi</th>
                                    </tr>
                                    <tr>
                                        <th colspan="1">SD</th>
                                        <th colspan="1">SW</th>
                                        <th colspan="1">Terawat</th>
                                        <th colspan="1">Tidak Terawat</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=1;?>
                                    @foreach($result as $value)
                                        <?php $count=1;?>
                                            @foreach(App\Models\Admin\Peralatan::Peralatan($value->id) as $val)
                                                @if($count==1)
                                                    <tr>
                                                <td rowspan="{!! $val->count() !!}">{!! $i++ !!}</td>
                                                <td rowspan="{!! $val->count() !!}">{!! $value->nama_lab !!}</td>
                                                <td>{!! $val->jenis_peralatan !!}</td>
                                                <td>{!! $val->jumlah_unit !!}</td>
                                                <td>
                                                    @if($val->kepemilikan=="SD")
                                                        <div class="input-icon right">
                                                            <i class="fa fa-check tooltips" data-original-title="You look OK!" data-container="body"></i>
                                                        </div>
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($val->kepemilikan=="SW")
                                                        <div class="input-icon right">
                                                            <i class="fa fa-check tooltips" data-original-title="You look OK!" data-container="body"></i>
                                                        </div>
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($val->kondisi=="Y")
                                                        <div class="input-icon right">
                                                            <i class="fa fa-check tooltips" data-original-title="You look OK!" data-container="body"></i>
                                                        </div>
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($val->kondisi=="N")
                                                        <div class="input-icon right">
                                                            <i class="fa fa-check tooltips" data-original-title="You look OK!" data-container="body"></i>
                                                        </div>
                                                    @endif
                                                </td>
                                                <td>
                                                    {!! $val->waktu !!}
                                                </td>
                                                        <td rowspan="{!! $val->count() !!}" style="padding-top:10%">
                                                            <a href="{!! URL::to('peralatan/edit_lab/'.$value->id) !!}" class="btn btn-warning">Edit</a>
                                                        </td>
                                                        <td>
                                                            <a href="{!! URL::to('peralatan/edit_peralatan/'.$val->id) !!}" class="btn btn-primary">Edit</a>
                                                        </td>
                                                    </tr>
                                                    <?php $count++; ?>
                                                @else
                                                    <tr>
                                                        <td>{!! $val->jenis_peralatan !!}</td>
                                                        <td>{!! $val->jumlah_unit !!}</td>
                                                        <td>
                                                            @if($val->kepemilikan=="SD")
                                                                <div class="input-icon right">
                                                                    <i class="fa fa-check tooltips" data-original-title="You look OK!" data-container="body"></i>
                                                                </div>
                                                            @endif
                                                        </td>
                                                        <td>
                                                            @if($val->kepemilikan=="SW")
                                                                <div class="input-icon right">
                                                                    <i class="fa fa-check tooltips" data-original-title="You look OK!" data-container="body"></i>
                                                                </div>
                                                            @endif
                                                        </td>
                                                        <td>
                                                            @if($val->kondisi=="Y")
                                                                <div class="input-icon right">
                                                                    <i class="fa fa-check tooltips" data-original-title="You look OK!" data-container="body"></i>
                                                                </div>
                                                            @endif
                                                        </td>
                                                        <td>
                                                            @if($val->kondisi=="N")
                                                                <div class="input-icon right">
                                                                    <i class="fa fa-check tooltips" data-original-title="You look OK!" data-container="body"></i>
                                                                </div>
                                                            @endif
                                                        </td>
                                                        <td>
                                                            {!! $val->waktu !!}
                                                        </td>

                                                        <td>
                                                            <a href="{!! URL::to('peralatan/edit_peralatan/'.$val->id) !!}" class="btn btn-primary">Edit</a>
                                                        </td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    {{-- end content --}}

@stop
@section('js')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{!! URL::to('local/resources/assets/assets/global/scripts/datatable.js') !!}" type="text/javascript"></script>
    <script src="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/datatables.min.js')!!}" type="text/javascript"></script>
    <script src="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')!!}" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src={!! URL::to('local/resources/assets/assets/pages/scripts/table-datatables-colreorder.min.js')!!} type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
@stop