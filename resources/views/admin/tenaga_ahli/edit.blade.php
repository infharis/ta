@extends('admin.layouts.index')
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>
                        Form Edit Kegiatan Tenaga Ahli/Pakar
                    </h1>
                </div>
                <div class="pull-right" style="padding-top: 0.5%;">
                    <a href="{!! URL::to('sdm/pakar/view') !!}" class="btn btn-sm yellow dropdown-toggle">Back</a>
                </div>
            </div>

            <!-- END PAGE BREADCRUMB -->
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption font-dark">
                                <i class="icon-settings font-dark"></i>
                                <span class="caption-subject bold"></span>
                            </div>
                            <div class="tools"></div>
                        </div>
                        <div class="portlet-body">
                            <form class="form-horizontal" method="post" role="form" action="{!! URL::to('sdm/pakar/update/'.$result->id) !!}">
                                {!! csrf_field() !!}
                                <div  class="form-group">
                                    <label for="inputEmail1" class="col-md-2 control-label">Tenaga Ahli/Pakar</label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" name="pakar" value="{!! $result->pakar !!}">
                                    </div>
                                </div>
                                <div  class="form-group">
                                    <label for="inputEmail1" class="col-md-2 control-label">Judul Kegiatan</label>
                                    <div class="col-md-10">
                                        <textarea name="judul" class="form-control">
                                           {!! $result->judul !!}
                                        </textarea>
                                    </div>
                                </div>
                                <div  class="form-group">
                                    <label for="inputEmail1" class="col-md-2 control-label">Waktu Pelaksanaan</label>
                                    <div class="col-md-10">
                                        <input type="text" maxlength="4" class="form-control" name="waktu" value="{!! $result->waktu !!}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-offset-2 col-md-10">
                                        <button type="submit" class="btn blue">Update</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>

            </div>





            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
@stop