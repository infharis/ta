@extends('admin.layouts.index')
@section('css')
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/select2/css/select2.min.css') !!}" rel="stylesheet" type="text/css" />
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/select2/css/select2-bootstrap.min.css')!!}" rel="stylesheet" type="text/css" />
@stop
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">@include('notif')
        <!-- END PAGE BREADCRUMB -->
            <div class="row ">
                <div class="col-md-12">
                    <!-- BEGIN SAMPLE FORM PORTLET-->
                    <div class="portlet box purple ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-gift"></i> Form Edit Struktur kurikulum berdasarkan urutan mata kuliah (MK)
                            </div>
                            <div class="pull-right" style="padding-top: 0.5%;">
                                <a href="{!! URL::to('kurikulum/view') !!}" class="btn btn-sm yellow dropdown-toggle">Back</a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <form role="form" action="{!! URL::to('kurikulum/update/'.$result->id) !!}" method="post">
                            {!! csrf_field() !!}
                            <!-- /.row -->
                                <div class="row">
                                    <label class="control-label col-md-3">Semester
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-12">
                                        <select id="multiple" name="semester_id" class="form-control select2-multiple">
                                            @foreach($sm as $value)
                                                @if($value->id==$result->semester_id)
                                                    <option value="{!! $value->id !!}" selected>Semester {!! $value->semester !!}</option>
                                                @else
                                                    <option value="{!! $value->id !!}">Semester {!! $value->semester !!}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="">Kode MK</label>
                                        <input type="text" class="form-control" name="kode_mk" value="{!! $result->kode_mk !!}">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="">Nama MK</label>
                                        <input type="text" class="form-control" name="nama_mk" value="{!! $result->nama_mk !!}">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="">Bobot sks</label>
                                        <input type="number" class="form-control" name="bobot_sks" value="{!! $result->bobot_sks !!}">
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <label class="control-label col-md-3">Sks MK dalam Kurikulum
                                        {{--<span class="required"> * </span>--}}
                                    </label>
                                    <div class="md-radio-inline">&nbsp;&nbsp;&nbsp;
                                        <div class="md-radio">
                                            <input type="radio" id="checkbox1_21" {!! $result->inti=='Y' ? 'checked' : ' ' !!} name="inti" value="Y" class="md-radiobtn">
                                            <label for="checkbox1_21">
                                                <span></span>
                                                <span class="check"></span>
                                                <span class="box"></span> Inti </label>
                                        </div>
                                        <div class="md-radio">
                                            <input type="radio" id="checkbox1_22" {!! $result->inti=='N' ? 'checked' : ' ' !!} name="inti" value="N" class="md-radiobtn">
                                            <label for="checkbox1_22">
                                                <span></span>
                                                <span class="check"></span>
                                                <span class="box"></span> Institusional </label>
                                        </div>
                                    </div>
                                    {{--<span class="help-block"> Provide your email address </span>--}}
                                </div>
                                <div class="row">
                                    <label class="control-label col-md-3">Bobot
                                        {{--<span class="required"> * </span>--}}
                                    </label>
                                    <div class="md-radio-inline">&nbsp;&nbsp;&nbsp;
                                        <div class="md-radio">
                                            <input type="radio" id="checkbox1_23" {!! $result->bobot_tugas=='Y' ? 'checked' : ' ' !!} name="bobot_tugas" value="Y" class="md-radiobtn">
                                            <label for="checkbox1_23">
                                                <span></span>
                                                <span class="check"></span>
                                                <span class="box"></span> Yes </label>
                                        </div>
                                        <div class="md-radio">
                                            <input type="radio" id="checkbox1_24" {!! $result->bobot_tugas=='N' ? 'checked' : ' ' !!} name="bobot_tugas" value="N" class="md-radiobtn">
                                            <label for="checkbox1_24">
                                                <span></span>
                                                <span class="check"></span>
                                                <span class="box"></span> No </label>
                                        </div>
                                    </div>
                                    {{--<span class="help-block"> Provide your email address </span>--}}
                                </div>
                                <div class="row">
                                    <label class="control-label col-md-3">Kelengkapan
                                        {{--<span class="required"> * </span>--}}
                                    </label>
                                    <div class="checkbox-list">
                                        <label class="checkbox-inline">
                                            <input type="checkbox" id="inlineCheckbox1" {!! $result->deskripsi=='Y' ? 'checked' : ' ' !!} name="deskripsi" value="Y"> Deskripsi </label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" id="inlineCheckbox2" {!! $result->silabus=='Y' ? 'checked' : ' ' !!} name="silabus" value="Y"> Silabus </label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" id="inlineCheckbox3" {!! $result->sap=='Y' ? 'checked' : ' ' !!} name="sap" value="Y"> SAP </label>
                                    </div>
                                    {{--<span class="help-block"> Provide your email address </span>--}}
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="">Unit/ Jur/ Fak Penyelenggara</label>
                                        <select id="multiple" name="unit" class="form-control select2-multiple">
                                            <option value="Unit MKU" {!! $result->unit=='Unit MKU' ? 'selected' : ' ' !!}>Unit MKU</option>
                                            <option value="JUR" {!! $result->unit=='JUR' ? 'selected' : ' ' !!}>JUR</option>
                                            <option value="FMIPA" {!! $result->unit=='FMIPA' ? 'selected' : ' ' !!}>FMIPA</option>
                                        </select>
                                    </div>
                                </div>
                                <br>
                                <br><br>
                                <input type="submit" class="btn btn-primary" value="Update">
                            </form>
                        </div>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
@stop
@section('js')
    <script src="{!! URL::to('local/resources/assets/assets/global/plugins/select2/js/select2.full.min.js') !!}" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="{!! URL::to('local/resources/assets/assets/global/scripts/app.min.js')!!}" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{!! URL::to('local/resources/assets/assets/pages/scripts/components-select2.min.js')!!}" type="text/javascript"></script>

@stop
