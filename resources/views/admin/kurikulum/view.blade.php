@extends('admin.layouts.index')
@section('css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/datatables.min.css') !!}" rel="stylesheet" type="text/css" />
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')!!}" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->
@stop
@section('content')
    {{-- begin content --}}
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">@include('notif')
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>
                        Struktur kurikulum berdasarkan urutan mata kuliah (MK)
                    </h1>
                </div>
                <div class="pull-right">
                    <a href="{!! URL::to('kurikulum') !!}" class="btn btn-sm yellow dropdown-toggle">Back</a>
                    <a href="{!! URL::to('kurikulum/create') !!}" class="btn btn-sm green dropdown-toggle">Tambah</a>
                </div>
                <!-- END PAGE TITLE -->
                <!-- BEGIN PAGE TOOLBAR -->

                <!-- END PAGE TOOLBAR -->
            </div>
            <!-- END PAGE HEAD-->
            <!-- BEGIN PAGE BREADCRUMB -->
            <!-- END PAGE BREADCRUMB -->
            <div class="row">
                <div class="col-md-2 pull-right">
                    <form action="{!! URL::to('kurikulum/sinkron/') !!}" method="post" id="sinkron_data">
                        {!! csrf_field() !!}
                        <div class="color-demo tooltips sinkron_mhs" data-original-title="" data-toggle="modal" data-target="#demo_modal_white">
                            <div class="color-view bg-blue-steel bg-font-blue-steel bold uppercase">
                                <i class="fa fa-user-plus"></i>&nbsp; Kurikulum</div>
                            <div class="color-info bg-white c-font-14 sbold sinkron_dosen">Sinkronkan Data Kurikulum</div>
                        </div>
                    </form>
                </div>
                <div class="col-md-10">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption font-dark">
                                <i class="icon-settings font-dark"></i>
                                <span class="caption-subject bold uppercase"></span>
                            </div>
                            {{--<div class="tools"> </div>--}}
                        </div>

                        <div class="portlet-body">
                            <div class="portlet-body flip-scroll">
                                <table class="table table-bordered table-striped table-condensed flip-content" id="sample_2">
                                    <thead class="flip-content">
                                    <tr>
                                        <th>No</th>
                                        <th>Kode MK</th>
                                        <th>Nama Mata Kuliah</th>
                                        <th>Bobot sks</th>
                                        <th >Tahun</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=1;?>
                                    @foreach($result as $value)
                                        <tr>
                                            <td>{!! $i++ !!}</td>
                                            <td>{!! $value->kode_mk !!}</td>
                                            <td>{!! $value->nama_mk !!}</td>
                                            <td>{!! $value->bobot_sks !!}</td>
                                            <td>{!! $value->tahun !!}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    {{-- end content --}}

@stop
@section('js')
    <script type="text/javascript">
        $('.sinkron_dosen').click(function() {
            $("#sinkron_data").submit();
        });
    </script>
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{!! URL::to('local/resources/assets/assets/global/scripts/datatable.js') !!}" type="text/javascript"></script>
    <script src="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/datatables.min.js')!!}" type="text/javascript"></script>
    <script src="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')!!}" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src={!! URL::to('local/resources/assets/assets/pages/scripts/table-datatables-colreorder.min.js')!!} type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
@stop