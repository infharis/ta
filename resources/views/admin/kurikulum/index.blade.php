@extends('admin.layouts.index')
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">@include('notif')
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light portlet-fit bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class=" icon-layers font-green"></i>
                                <span class="caption-subject font-green bold uppercase">Kurikulum & Akademik</span>
                            </div>
                            <div class="pull-right" style="padding-top: 0.5%;">
                                <a href="{!! URL::to('data/dosen/create') !!}" class="btn btn-sm yellow dropdown-toggle">Tambah</a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="mt-element-step">
                                <div class="row step-default">
                                    <div class="col-md-6 bg-grey mt-step-col">
                                        {{--<div class="mt-step-number bg-white font-grey"></div>--}}
                                        <a href="{!! URL::to('kurikulum/jumlah_sks') !!}" style="text-decoration:none">
                                            <div class="mt-step-title font-grey-cascade">Jumlah Sks</div>
                                            <div class="mt-step-content font-grey-cascade">Jumlah sks minimum pada kurikulum untuk kelulusan</div>
                                        </a>
                                    </div>
                                    <div class="col-md-6 bg-grey mt-step-col active">
                                        <a href="{!! URL::to('kurikulum/view') !!}" style="text-decoration:none">
                                            <div class="mt-step-title font-grey-cascade">Struktur Kurikulum</div>
                                            <div class="mt-step-content font-grey-cascade">Struktur kurikulum berdasarkan urutan mata kuliah per semester</div>
                                        </a>
                                    </div>
                                    <div class="col-md-6 bg-grey mt-step-col active">
                                        {{--<div class="mt-step-number bg-white font-grey"></div>--}}
                                        <a href="{!! URL::to('kurikulum/silabus/view') !!}" style="text-decoration:none">
                                            <div class="mt-step-title font-grey-cascade">Peninjauan</div>
                                            <div class="mt-step-content font-grey-cascade">Peninjauan Kurikulum </div>
                                        </a>
                                    </div>
                                    <div class="col-md-6 bg-grey mt-step-col">
                                        {{--<div class="mt-step-number bg-white font-grey"></div>--}}
                                        <a href="{!! URL::to('kurikulum/mk_pilihan/view') !!}" style="text-decoration:none">
                                            <div class="mt-step-title font-grey-cascade">MK Pilihan</div>
                                            <div class="mt-step-content font-grey-cascade">Mata kuliah pilihan yang dilaksanakan pada tahun akademik  </div>
                                        </a>
                                    </div>
                                    <div class="col-md-12 bg-grey mt-step-col" style="background-color: #2d558d !important;">
                                        {{--<div class="mt-step-number bg-white font-grey"></div>--}}
                                        <a href="{!! URL::to('kurikulum/praktikum/view') !!}" style="text-decoration:none">
                                            <div class="mt-step-title font-grey-cascade">Praktikum</div>
                                            <div class="mt-step-content font-grey-cascade">Substansi praktikum yang merupakan bagian dari mata kuliah tertentu </div>
                                        </a>
                                    </div>

                                </div>
                                <br/>
                                <br/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
@stop


