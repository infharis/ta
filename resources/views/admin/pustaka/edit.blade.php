@extends('admin.layouts.index')
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">@include('notif')
        <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>
                        Form Edit Pustaka
                    </h1>
                </div>
                <div class="pull-right" style="padding-top: 0.5%;">
                    <a href="{!! URL::to('pustaka/view') !!}" class="btn btn-sm yellow dropdown-toggle">Back</a>
                </div>
            </div>

            <!-- END PAGE BREADCRUMB -->
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption font-dark">
                                <i class="icon-settings font-dark"></i>
                                <span class="caption-subject bold"></span>
                            </div>
                            <div class="tools"></div>
                        </div>
                        <div class="portlet-body">
                            <form class="form-horizontal" method="post" role="form" action="{!! URL::to('pustaka/update/'.$result->id) !!}">
                                {!! csrf_field() !!}
                                <div  class="form-group">
                                    <label for="inputEmail1" class="col-md-2 control-label">Jenis Pustaka</label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" name="jenis_pustaka" value="{!! $result->jenis_pustaka !!}">
                                    </div>
                                </div>
                                <div  class="form-group">
                                    <label for="inputEmail1" class="col-md-2 control-label">Jumlah Judul</label>
                                    <div class="col-md-10">
                                        <input type="number" class="form-control" name="jumlah_judul" value="{!! $result->jumlah_judul !!}">
                                    </div>
                                </div>
                                <div  class="form-group">
                                    <label for="inputEmail1" class="col-md-2 control-label">Jumlah Copy</label>
                                    <div class="col-md-10">
                                        <input type="number" class="form-control" name="jumlah_copy" value="{!! $result->jumlah_copy !!}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-offset-2 col-md-10">
                                        <button type="submit" class="btn blue">Update</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>

            </div>





            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
@stop