@extends('admin.layouts.index')
@section('css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/datatables.min.css') !!}" rel="stylesheet" type="text/css" />
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')!!}" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->
@stop
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">@include('notif')
            <div class="row">
                <div class="col-md-12 col-sm-6">
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-share font-blue"></i>
                                <span class="caption-subject font-blue bold uppercase">Daftar Report Borang Akreditasi @lang("prodi.nama_prodi")@lang("prodi.kampus")</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="portlet-body flip-scroll">
                                <table class="table table-bordered table-striped table-condensed flip-content" id="sample_1">
                                    <thead class="flip-content">
                                    <tr>
                                        <th  style="width:10px">No</th>
                                        <th>Report</th>
                                        <th>Ket</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>Daftar Dosen @lang("prodi.nama_jurusan")</td>
                                        <td>
                                            <a href="{!! URL::to('d/daftar_dosen') !!}" class="btn btn-primary">Download</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Data Penelitian @lang("prodi.nama_jurusan")</td>
                                        <td>
                                            <a href="{!! URL::to('d/data_penelitian') !!}" class="btn btn-primary">Download</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>Data Pelayanan Dan Pengabdian Masyarakat</td>
                                        <td>
                                            <a href="{!! URL::to('d/data_p2m') !!}" class="btn btn-primary">Download</a>
                                        </td>
                                    </tr>
                                        <?php $i=1;?>
                                        {{--@for($row=0,$no=0;$row<count($result);$row++,$no++)--}}
                                            {{--@for($col=1;$col<=1;$col++)--}}
                                                {{--<tr>--}}
                                                    {{--<td>{!! $i++ !!}</td>--}}
                                                    {{--<td>{!! $result[$no][0] !!}</td>--}}
                                                    {{--<td>{!! $result[$row][$col] !!}</td>--}}
                                                    {{--<td>--}}
                                                        {{--<a href="{!! URL::to($result[$row][++$col]) !!}" class="btn btn-primary">Download</a>--}}
                                                    {{--</td>--}}
                                                {{--</tr>--}}
                                            {{--@endfor--}}
                                        {{--@endfor--}}
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
            <div class="clearfix"></div>
            <!-- END DASHBOARD STATS 1-->

            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>

    {{--<div class="row">--}}
        {{--<div class="col-md-12">--}}
            {{--<!-- BEGIN PORTLET-->--}}
            {{--<div class="portlet light bordered form-fit">--}}
                {{--<div class="portlet-title">--}}
                    {{--<div class="caption font-green-sharp">--}}
                        {{--<i class="icon-speech font-green-sharp"></i>--}}
                        {{--<span class="caption-subject bold uppercase">Peta Situs</span>--}}
                        {{--<span class="caption-helper"></span>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="portlet-body form">--}}
                    {{--<div class="form-horizontal form-bordered">--}}
                        {{--<div class="form-group">--}}
                            {{--<label class="col-md-4 control-label" style="text-align:center;"><strong>STANDAR 3. KEMAHASISWAAN DAN LULUSAN</strong></label>--}}
                            {{--<div class="col-md-6">--}}
                                    {{--<div class="row">--}}
                                        {{--<div class="radio-list">--}}
                                            {{--<label class="col-md-6"><h4>Profil Mahasiswa dan Lulusan</h4>--}}
                                                {{--<ul>--}}
                                                    {{--<li><a href="">Data seluruh mahasiswa reguler dan lulusannya dalam lima tahun terakhir </a></li>--}}
                                                    {{--<li><a href="">Data mahasiswa non-reguler dalam lima tahun terakhir  </a></li>--}}
                                                    {{--<li><a href="">Pencapaian prestasi/reputasi mahasiswa</a></li>--}}
                                                    {{--<li><a href="">Data jumlah mahasiswa reguler tujuh tahun terakhir </a></li>--}}
                                                {{--</ul>--}}
                                            {{--</label>--}}
                                            {{--<label class="col-md-6"><h4>Layanan kepada Mahasiswa </h4>--}}
                                                {{--<ul>--}}
                                                    {{--<li><a href="">Jenis pelayanan kepada mahasiswa pada PS </a></li>--}}
                                                {{--</ul>--}}
                                            {{--</label>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="row">--}}
                                        {{--<div class="radio-list">--}}
                                            {{--<label class="col-md-6"><h4>Evaluasi Lulusan</h4>--}}
                                                {{--<ul>--}}
                                                    {{--<li><a href="">Evaluasi Kinerja lulusan oleh Pihak Pengguna Lulusan</a></li>--}}
                                                    {{--<li><a href="">Rata-rata waktu tunggu lulusan untuk memperoleh pekerjaan yang pertama </a></li>--}}
                                                {{--</ul>--}}
                                            {{--</label>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="form-group last">--}}
                            {{--<label class="col-md-4 control-label" style="text-align:center;"><strong>STANDAR 4. SUMBER DAYA MANUSIA</strong></label>--}}
                            {{--<div class="col-md-6">--}}
                                {{--<div class="row">--}}
                                    {{--<div class="radio-list">--}}
                                        {{--<label class="col-md-6"><h4>Profil Mahasiswa dan Lulusan</h4>--}}
                                            {{--<ul>--}}
                                                {{--<li><a href="">Data seluruh mahasiswa reguler dan lulusannya dalam lima tahun terakhir </a></li>--}}
                                                {{--<li><a href="">Data mahasiswa non-reguler dalam lima tahun terakhir  </a></li>--}}
                                                {{--<li><a href="">Pencapaian prestasi/reputasi mahasiswa</a></li>--}}
                                                {{--<li><a href="">Data jumlah mahasiswa reguler tujuh tahun terakhir </a></li>--}}
                                            {{--</ul>--}}
                                        {{--</label>--}}
                                        {{--<label class="col-md-6"><h4>Layanan kepada Mahasiswa </h4>--}}
                                            {{--<ul>--}}
                                                {{--<li><a href="">Jenis pelayanan kepada mahasiswa pada PS </a></li>--}}
                                            {{--</ul>--}}
                                        {{--</label>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="row">--}}
                                    {{--<div class="radio-list">--}}
                                        {{--<label class="col-md-6"><h4>Evaluasi Lulusan</h4>--}}
                                            {{--<ul>--}}
                                                {{--<li><a href="">Evaluasi Kinerja lulusan oleh Pihak Pengguna Lulusan</a></li>--}}
                                                {{--<li><a href="">Rata-rata waktu tunggu lulusan untuk memperoleh pekerjaan yang pertama </a></li>--}}
                                            {{--</ul>--}}
                                        {{--</label>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="form-actions">--}}
                            {{--<div class="row">--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</form>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<!-- END PORTLET-->--}}
        {{--</div>--}}
    {{--</div>--}}


{{--@stop--}}
{{--@section('js')--}}
    {{--<!-- BEGIN PAGE LEVEL PLUGINS -->--}}
    {{--<script src="{!! URL::to('local/resources/assets/assets/global/scripts/datatable.js') !!}" type="text/javascript"></script>--}}
    {{--<script src="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/datatables.min.js')!!}" type="text/javascript"></script>--}}
    {{--<script src="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')!!}" type="text/javascript"></script>--}}
    {{--<!-- END PAGE LEVEL PLUGINS -->--}}
    {{--<!-- BEGIN PAGE LEVEL SCRIPTS -->--}}
    {{--<script src={!! URL::to('local/resources/assets/assets/pages/scripts/table-datatables-colreorder.min.js')!!} type="text/javascript"></script>--}}
    {{--<!-- END PAGE LEVEL SCRIPTS -->--}}
@stop