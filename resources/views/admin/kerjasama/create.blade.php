@extends('admin.layouts.index')
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">@include('notif')
            <!-- END PAGE BREADCRUMB -->
            <div class="row ">
                <div class="col-md-12">
                    <!-- BEGIN SAMPLE FORM PORTLET-->
                    <div class="portlet box purple ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-gift"></i> Form Tambah Kerjasama </div>
                            <div class="pull-right" style="padding-top: 0.5%;">
                                <a href="{!! URL::to('kerjasama/view') !!}" class="btn btn-sm yellow dropdown-toggle">Back</a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <form role="form" action="{!! URL::to('kerjasama/store') !!}" method="post" enctype="multipart/form-data">
                                {!! csrf_field() !!}
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="">Nama Instansi</label><span class="required" aria-required="true"> * </span>
                                        <input type="text" class="form-control" name="nama_instansi">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="">Jenis Kegiatan</label><span class="required" aria-required="true"> * </span>
                                        <input type="text" class="form-control" name="jenis_kegiatan">
                                    </div>
                                </div>
                                <!-- /.row -->
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="">Kurun Waktu : Start</label><span class="required" aria-required="true"> * </span>
                                        <input type="text" class="form-control" maxlength="4" name="start">
                                    </div>
                                    <div class="col-md-6">
                                        <label for=""> Kurun Waktu : End</label><span class="required" aria-required="true"> * </span>
                                        <input type="text" class="form-control" maxlength="4" name="end">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="">Manfaat Kerjasama</label>
                                        <textarea name="manfaat" class="form-control" id="" cols="30" rows="5"></textarea>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="md-radio-inline">Kerjasama
                                            <div class="md-radio">
                                                <input type="radio" id="checkbox1_8" name="kerjasama" value="L" class="md-radiobtn">
                                                <label for="checkbox1_8">
                                                    <span></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span>Luar Negri</label>
                                            </div>
                                            <div class="md-radio">
                                                <input type="radio" id="checkbox1_9" name="kerjasama" value="D" class="md-radiobtn">
                                                <label for="checkbox1_9">
                                                    <span></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span> Dalam Negri </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="">SoftCopy</label>
                                        <input type="file" class="form-control" name="softcopy">
                                    </div>
                                </div>

                                <br>
                            <br><br>
                            <input type="submit" class="btn btn-primary" value="Simpan">
                            </form>
                        </div>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->

@stop