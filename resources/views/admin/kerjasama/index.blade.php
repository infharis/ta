@extends('admin.layouts.index')
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">@include('notif')
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light portlet-fit bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class=" icon-layers font-green"></i>
                                <span class="caption-subject font-green bold uppercase">KEGIATAN KERJASAMA DENGAN INSTANSI LAIN </span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="mt-element-step">
                                <div class="row step-default">
                                    <div class="col-md-6 bg-grey mt-step-col">
                                        {{--<div class="mt-step-number bg-white font-grey"></div>--}}
                                        <a href="{!! URL::to('kerjasama/view') !!}" style="text-decoration:none">
                                            <div class="mt-step-title font-grey-cascade">Dalam Negri</div>
                                            <div class="mt-step-content font-grey-cascade">Instansi dalam negeri yang menjalin kerjasama yang terkait dengan PS </div>
                                        </a>
                                    </div>
                                    <div class="col-md-6 bg-grey mt-step-col active">
                                        <a href="{!! URL::to('kerjasama/view') !!}" style="text-decoration:none">
                                            <div class="mt-step-title font-grey-cascade">Luar Negri</div>
                                            <div class="mt-step-content font-grey-cascade">Instansi luar negeri yang menjalin kerjasama* yang terkait dengan PS </div>
                                        </a>
                                    </div>
                                </div>
                                <br/>
                                <br/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
@stop


