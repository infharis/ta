@extends('admin.layouts.index')
@section('css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/datatables.min.css')!!}" rel="stylesheet" type="text/css" />
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')!!}" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->

@stop
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">@include('notif')
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>
                        Prestasi Mahasiswa @lang("prodi.nama_jurusan")
                    </h1>
                </div>
                <div class="pull-right">
                    <a href="{!! URL::to('mhs/view/data') !!}" class="btn btn-sm yellow dropdown-toggle">Back</a>
                    <a href="{!! URL::to('data/mhs/prestasi_mhs/create') !!}" class="btn btn-sm green dropdown-toggle">Tambah Prestasi</a>
                </div>
            </div>

            <!-- END PAGE BREADCRUMB -->
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-warning alert-block">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        Untuk Mengisi Data <b>Prestasi Mahasiswa</b> Anda Harus Mendaftarkan Mhs Terlebih Dahulu
                    </div>
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption font-dark">
                                <i class="icon-settings font-dark"></i>
                                <span class="caption-subject bold"></span>
                            </div>
                            {{--<div class="pull-right">--}}
                                {{--<a href="{!! URL::to('mhs/view/data/format_biodata_mhs') !!}" class="btn blue btn-outline sbold">Download Format File Data MHS</a>--}}
                                {{--<a class="btn red btn-outline sbold" data-toggle="modal" href="#basic"> Import Data MHS </a>--}}
                                {{--<div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true">--}}
                                    {{--<div class="modal-dialog">--}}
                                        {{--<form action="{!! URL::to('data/mhs/biodata_mhs/import_data_mhs') !!}" method="post" enctype="multipart/form-data">--}}
                                            {{--{!! csrf_field() !!}--}}
                                            {{--<div class="modal-content">--}}
                                                {{--<div class="modal-header">--}}
                                                    {{--<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>--}}
                                                    {{--<h4 class="modal-title">Form Import Data MHS</h4>--}}
                                                {{--</div>--}}
                                                {{--<div class="modal-body">--}}
                                                    {{--<input type="file" name="data_mhs" class="form-control" required>--}}
                                                {{--</div>--}}
                                                {{--<div class="modal-footer">--}}
                                                    {{--<button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>--}}
                                                    {{--<button type="submit" class="btn green">Simpan</button>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</form>--}}
                                        {{--<!-- /.modal-content -->--}}
                                    {{--</div>--}}
                                    {{--<!-- /.modal-dialog -->--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        </div>
                        <div class="portlet-body">
                            <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Nama Kegiatan</th>
                                    <th>Prestasi</th>
                                    <th>Tingkat</th>
                                    <th>Tahun</th>
                                    <th>Aksi</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i=1;?>
                                @foreach($result as $value)
                                    <tr>
                                        <td>{!! $i++ !!}</td>
                                        <td>{!! $value->Mhs($value->mhs_id) !!}</td>
                                        <td>{!! $value->nama_kegiatan !!}</td>
                                        <td>{!! $value->prestasi !!}</td>
                                        <td>
                                            @if($value->tingkat=='L')
                                                Lokal
                                            @elseif($value->tingkat=='R')
                                                Regional
                                            @elseif($value->tingkat=='N')
                                                Nasional
                                            @elseif($value->tingkat=='I')
                                                Internasional
                                             @endif
                                        </td>
                                        <td>{!! $value->tahun !!}</td>
                                        <td>
                                            <a href="{!! URL::to('data/mhs/prestasi_mhs/edit/'.$value->id) !!}" class="btn btn-primary">Edit</a>
                                            <a href="#" id="delete" value="{!! $value->id !!}" class="btn btn-danger">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
                <div class="col-md-12">
                    {{--<div id="columnchart_values"></div>--}}
                    <div id="container"></div>
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
@stop
@section('js')
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    {{--<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>--}}
    <script type="text/javascript">

        Highcharts.chart('container', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Prestasi Mahasiswa'
            },
            xAxis: {
                categories: [
                    @foreach($tp as $t)
                        {!! $t->tahun !!},
                    @endforeach
                ]
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Total Prestasi Mahasiswa/i'
                },
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    }
                }
            },
            legend: {
                align: 'right',
                x: -30,
                verticalAlign: 'top',
                y: 25,
                floating: true,
                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                borderColor: '#CCC',
                borderWidth: 1,
                shadow: false
            },
            tooltip: {
                headerFormat: '<b>{point.x}</b><br/>',
                pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
            },
            plotOptions: {
                column: {
                    stacking: 'normal',
                    dataLabels: {
                        enabled: true,
                        color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                    }
                }
            },
            series: [{
                name: 'Lokal',
                data: [
                    @foreach($tp as $val_l)
                        <?php $local=$val_l->CountP(array('tahun'=>$val_l->tahun,'tingkat'=>'L'));?>
                        @if($local!=' ')
                            {!! $local  !!},
                        @else
                            0,
                        @endif
                    @endforeach
                ]
            },{
                name: 'Regional',
                data: [
                    @foreach($tp as $val_l)
                    <?php $regional=$val_l->CountP(array('tahun'=>$val_l->tahun,'tingkat'=>'R'));?>
                        @if($regional!=' ')
                            {!! $regional  !!},
                        @else
                            0,
                        @endif
                    @endforeach
                ]
            },{
                name: 'Nasional',
                data: [
                    @foreach($tp as $val_l)
                    <?php $nasional=$val_l->CountP(array('tahun'=>$val_l->tahun,'tingkat'=>'N'));?>
                    @if($nasional!=' ')
                        {!! $nasional  !!},
                    @else
                        0,
                    @endif
                    @endforeach
                ]
            },{
                name: 'Internasional',
                data: [
                    @foreach($tp as $val_l)
                        <?php $intern=$val_l->CountP(array('tahun'=>$val_l->tahun,'tingkat'=>'I'));?>
                            @if($intern!=' ')
                                {!! $intern  !!},
                            @else
                                0,
                            @endif
                    @endforeach
                ]
            }
            ]
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function(){

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $(document).on("click","#delete",function(){
                    if (confirm('Are You Sure Delete This Data?')) {
                        var val = $(this).attr('value');
                        $.ajax({
                            type: 'DELETE',
                            url: '{!! URL::to('data/mhs/prestasi_mhs/destroy') !!}',
                            data: {
                                id: val
                            },
                            success: function (response) {
                                if (response == 1) {
                                    alert('Data Berhasil Dihapus');
                                    location.reload();
                                } else {
                                    alert('Gagal Menghapus Data');
                                }
                            }
                        });
                    } else {
                        return false;
                    }
            });
        });
    </script>

    <!-- END PAGE LEVEL SCRIPTS -->
    <script src="{!! URL::to('local/resources/assets/assets/global/scripts/datatable.js')!!}" type="text/javascript"></script>
    <script src="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/datatables.min.js')!!}" type="text/javascript"></script>
    <script src="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')!!}" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->

    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="{!! URL::to('local/resources/assets/assets/global/scripts/app.min.js')!!}" type="text/javascript"></script>

    <!-- END THEME GLOBAL SCRIPTS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{!! URL::to('local/resources/assets/assets/pages/scripts/table-datatables-responsive.min.js')!!}" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
@stop