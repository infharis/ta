@extends('admin.layouts.index')
@section('css')
    <link href="{!! URL::to('local/resources/assets/selectize/css/selectize.bootstrap3.css') !!}" rel="stylesheet">
@stop
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">@include('notif')
            <!-- END PAGE BREADCRUMB -->
            <div class="row ">
                <div class="col-md-12">
                    <!-- BEGIN SAMPLE FORM PORTLET-->
                    <div class="portlet box purple ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-gift"></i> Form Tambah Prestasi Mahasiswa/i</div>
                            <div class="pull-right" style="padding-top: 0.5%;">
                                <a href="{!! URL::to('data/mhs/prestasi_mhs') !!}" class="btn btn-sm yellow dropdown-toggle">Back</a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <form role="form" enctype="multipart/form-data" method="post" action="{!! URL::to('data/mhs/prestasi_mhs/store') !!}">
                                {!! csrf_field() !!}
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="">Nama</label><span class="required" aria-required="true"> * </span>
                                        <select name="mhs_id" required data-md-selectize>
                                            <option value="">-- Pilih Mahasiswa --</option>
                                            @foreach($mhs as $value)
                                                <option value="{!! $value->id !!}">{!! $value->nim.' '.$value->nama !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <!-- /.row -->
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="">Nama Kegiatan</label><span class="required" aria-required="true"> * </span>
                                        <input type="text" class="form-control" name="kegiatan" required value="{!! old('kegiatan') !!}">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="">Softcopy</label>
                                        <input type="file" class="form-control" name="file">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="">Prestasi</label><span class="required" aria-required="true"> * </span>
                                        <input type="text" class="form-control" name="prestasi" value="{!! old('prestasi') !!}">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="">Tahun</label><span class="required" aria-required="true"> * </span>
                                        <input type="text" class="form-control" name="tahun" maxlength="4" value="{!! old('tahun') !!}">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="md-radio-inline">Tingkat
                                            <div class="md-radio">
                                                <input type="radio" id="checkbox13_8" name="tingkat" value="L" class="md-radiobtn">
                                                <label for="checkbox13_8">
                                                    <span></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span>Lokal</label>
                                            </div>
                                            <div class="md-radio">
                                                <input type="radio" id="checkbox14_9" name="tingkat" value="R" class="md-radiobtn">
                                                <label for="checkbox14_9">
                                                    <span></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span> Regional </label>
                                            </div> <div class="md-radio">
                                                <input type="radio" id="checkbox15_9" name="tingkat" value="N" class="md-radiobtn">
                                                <label for="checkbox15_9">
                                                    <span></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span> Nasional </label>
                                            </div> <div class="md-radio">
                                                <input type="radio" id="checkbox16_9" name="tingkat" value="I" class="md-radiobtn">
                                                <label for="checkbox16_9">
                                                    <span></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span> Internasional </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <br>
                            <input type="submit" class="btn btn-primary" value="Simpan">
                            </form>
                        </div>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->

@stop
@section('js')
    <script src="{!! URL::to('local/resources/assets/selectize/common.min.js')!!}"></script>
    <script src="{!! URL::to('local/resources/assets/selectize/uikit_custom.min.js')!!}"></script>
    <script src="{!! URL::to('local/resources/assets/selectize/altair_admin_common.min.js')!!}"></script>
@stop