@extends('admin.layouts.index')
@section('css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/datatables.min.css')!!}" rel="stylesheet" type="text/css" />
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')!!}" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->
@stop
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>
                        Data Alumni @lang("prodi.nama_jurusan")
                    </h1>
                </div>
                <div class="pull-right">
                    <a href="{!! URL::to('mhs/view/data') !!}" class="btn btn-sm yellow dropdown-toggle">Back</a>
                    {{--<a href="{!! URL::to('data/mhs/akademik/create') !!}" class="btn btn-sm green dropdown-toggle">Tambah IPK/Semester</a>--}}
                </div>
            </div>

            <!-- END PAGE BREADCRUMB -->
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12">
                    {{--<div class="alert alert-warning alert-block">--}}
                        {{--<button type="button" class="close" data-dismiss="alert">&times;</button>--}}
                        {{--Untuk Mengisi Data <b>Akademik</b> Anda Harus Mendaftarkan Mhs Terlebih Dahulu--}}
                    {{--</div>--}}
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption font-dark">
                                <i class="icon-settings font-dark"></i>
                                <span class="caption-subject bold"></span>
                            </div>
                            <div class="tools"> </div>
                        </div>
                        <div class="portlet-body">
                            <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>NPM</th>
                                    <th>Nama</th>
                                    <th>Nilai TOEFL</th>
                                    <th>Email</th>
                                    <th class="none">Lama Menyelesaikan Skripsi : </th>
                                    <th class="none">Lama menyelesaikan studi :</th>
                                    <th class="none">Tanggal menyelesaikan studi :</th>
                                    <th class="none">Lama waktu tunggu mendapatkan pekerjaan pertama (bulan) :</th>
                                    <th class="none">Gaji Pertama :</th>
                                    <th class="none">Pekerjaan :</th>
                                    <th class="none">Nama Instansi :</th>
                                    <th>Aksi</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i=1;?>
                                @foreach($result as $value)
                                    <?php
                                            // kalkulasi skripsi
                                        $start=new DateTime($value->proposal);
                                        $end=new DateTime($value->sidang);
                                        $interval = $start->diff($end);
                                        $tgl_sidang=date_create($value->sidang);

                                        // kalkulasi massa studi
                                        $date=strtotime($value->tahun_masuk.'-01-01');
                                        $tahun_masuk=strtotime('+1 year',$date);
                                        $tahun_masuk=new DateTime(date('Y-m-d',$tahun_masuk));
                                        $mssa_studi=$tahun_masuk->diff($end);
                                    ?>
                                    <tr>
                                        <td>{!! $i++ !!}</td>
                                        <td>{!! $value->nim !!}</td>
                                        <td>{!! ucwords($value->nama) !!}</td>
                                        <td>{!! $value->toefl !!}</td>
                                        <td>{!! $value->email !!}</td>
                                        <td>{!! $interval->format('%y year %m months and %d days') !!}</td>
                                        <td>{!! $mssa_studi->format('%y year %m months and %d days') !!}</td>
                                        <td>{!! date_format($tgl_sidang,'l jS F Y') !!}</td>
                                        <td>{!! $value->WaktuPekerjaan($value->dapat_pekerjaan) !!}</td>
                                        <td>{!! $value->gaji !!}</td>
                                        <td>{!! $value->pekerjaan !!}</td>
                                        <td>{!! $value->instansi !!}</td>
                                        <td>
                                            <a href="{!! URL::to('data/mhs/data_alumni/'.$value->nim.'/edit') !!}" class="btn btn-primary">Edit</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>

            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
        {{-- info grafis --}}
        <div class="col-md-12">
            <div id="container"></div>
        </div>
    </div>
@stop
@section('js')
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    {{--<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>--}}
    <script type="text/javascript">

        Highcharts.chart('container', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Kategori Waktu Mendapatkan Pekerjaan'
            },
            xAxis: {
                categories: [
                    @foreach($daftar_tahun as $val)
                             "{!! 'Angkatan '.$val->tahun_masuk !!}",
                    @endforeach
                ]
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Total Alumni Mendapatkan Pekerjaan'
                },
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    }
                }
            },
            legend: {
                align: 'right',
                x: -30,
                verticalAlign: 'top',
                y: 25,
                floating: true,
                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                borderColor: '#CCC',
                borderWidth: 1,
                shadow: false
            },
            tooltip: {
                headerFormat: '<b>{point.x}</b><br/>',
                pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
            },
            plotOptions: {
                column: {
                    stacking: 'normal',
                    dataLabels: {
                        enabled: true,
                        color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                    }
                }
            },
            series: [
                    {
                        name: "Belum Bekerja",
                        data: [
                            @foreach($daftar_tahun as $tm)
                                <?php $count=App\Models\Admin\Biodata::CountKerja(array('tahun_masuk'=>$tm->tahun_masuk,'dapat_pekerjaan'=>1));?>
                                    @if($count!=' ')
                                        {!! $count !!},
                                    @else
                                        0,
                                    @endif
                            @endforeach
                        ]
                    },
                {
                    name: "Kurang dari 1 Bulan",
                    data: [
                        @foreach($daftar_tahun as $tm)
                            <?php $count=App\Models\Admin\Biodata::CountKerja(array('tahun_masuk'=>$tm->tahun_masuk,'dapat_pekerjaan'=>2));?>
                                @if($count!=' ')
                                    {!! $count !!},
                                @else
                                    0,
                                @endif
                        @endforeach
                    ]
                },{
                    name: "2 Bulan",
                    data: [
                        @foreach($daftar_tahun as $tm)
                            <?php $count=App\Models\Admin\Biodata::CountKerja(array('tahun_masuk'=>$tm->tahun_masuk,'dapat_pekerjaan'=>3));?>
                            @if($count!=' ')
                                {!! $count !!},
                            @else
                                0,
                            @endif
                        @endforeach
                    ]
                },{
                    name: "3 Bulan",
                    data: [
                        @foreach($daftar_tahun as $tm)
                            <?php $count=App\Models\Admin\Biodata::CountKerja(array('tahun_masuk'=>$tm->tahun_masuk,'dapat_pekerjaan'=>4));?>
                            @if($count!=' ')
                            {!! $count !!},
                            @else
                                0,
                            @endif
                        @endforeach
                    ]
                },{
                    name: "4 Bulan",
                    data: [
                        @foreach($daftar_tahun as $tm)
                            <?php $count=App\Models\Admin\Biodata::CountKerja(array('tahun_masuk'=>$tm->tahun_masuk,'dapat_pekerjaan'=>5));?>
                            @if($count!=' ')
                                {!! $count !!},
                            @else
                                0,
                            @endif
                        @endforeach
                    ]
                },{
                    name: "5 Bulan",
                    data: [
                        @foreach($daftar_tahun as $tm)
                        <?php $count=App\Models\Admin\Biodata::CountKerja(array('tahun_masuk'=>$tm->tahun_masuk,'dapat_pekerjaan'=>6));?>
                        @if($count!=' ')
                        {!! $count !!},
                        @else
                            0,
                        @endif
                        @endforeach
                    ]
                },{
                    name: "6 Bulan",
                    data: [
                        @foreach($daftar_tahun as $tm)
                            <?php $count=App\Models\Admin\Biodata::CountKerja(array('tahun_masuk'=>$tm->tahun_masuk,'dapat_pekerjaan'=>7));?>
                            @if($count!=' ')
                            {!! $count !!},
                            @else
                                0,
                            @endif
                        @endforeach
                    ]
                },{
                    name: "7 s.d 12 bulan",
                    data: [
                        @foreach($daftar_tahun as $tm)
                        <?php $count=App\Models\Admin\Biodata::CountKerja(array('tahun_masuk'=>$tm->tahun_masuk,'dapat_pekerjaan'=>8));?>
                        @if($count!=' ')
                        {!! $count !!},
                        @else
                            0,
                        @endif
                        @endforeach
                    ]
                }
            ]
        });
    </script>
    <script src="{!! URL::to('local/resources/assets/assets/global/scripts/datatable.js')!!}" type="text/javascript"></script>
    <script src="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/datatables.min.js')!!}" type="text/javascript"></script>
    <script src="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')!!}" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="{!! URL::to('local/resources/assets/assets/global/scripts/app.min.js')!!}" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{!! URL::to('local/resources/assets/assets/pages/scripts/table-datatables-responsive.min.js')!!}" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
@stop