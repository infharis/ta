@extends('admin.layouts.index')
@section('css')
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/select2/css/select2.min.css') !!}" rel="stylesheet" type="text/css" />
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/select2/css/select2-bootstrap.min.css')!!}" rel="stylesheet" type="text/css" />
@stop
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- END PAGE BREADCRUMB -->
            <div class="row ">
                <div class="col-md-12">
                    <!-- BEGIN SAMPLE FORM PORTLET-->
                    <div class="portlet box purple ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-gift"></i> Form Edit Data Alumni
                            </div>
                            <div class="pull-right" style="padding-top: 0.5%;">
                                <a href="{!! URL::to('data/mhs/data_alumni') !!}" class="btn btn-sm yellow dropdown-toggle">Back</a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <form role="form" action="{!! URL::to('data/mhs/data_alumni/'.$result->id.'/update') !!}" method="post">
                                {!! csrf_field() !!}
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="">Nama Mahasiswa</label>
                                        <input readonly type="text" class="form-control" value="{!! $result->Common('biodata_mhs',array('id'=>$result->id))->nama !!}">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="radio-list">
                                        <label class="col-md-6">Email
                                            <input type="email" class="form-control" name="email" value="{!! $result->email !!}">
                                        </label>
                                        <label class="col-md-6">TOEFL
                                            <input type="text" class="form-control" name="toefl" value="{!! $result->toefl !!}">
                                        </label>
                                    </div>
                                </div>
                                <!-- /.row -->
                                <div class="row">
                                    <label style="margin-left:43%;margin-top:2%"><b>Uraian Pekerjaan</b></label>
                                    <div class="radio-list">
                                        <label class="col-md-6">Lama waktu tunggu mendapatkan pekerjaan pertama (bulan)
                                            <select id="multiple" name="waktu" class="form-control select2-multiple">
                                                @foreach($pekerjaan as $value)
                                                    @if($value->id==$result->dapat_pekerjaan)
                                                        <option value="{!! $value->id !!}" selected>{!! $value->waktu !!}</option>
                                                    @else
                                                        <option value="{!! $value->id !!}">{!! $value->waktu !!}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </label>
                                        <label class="col-md-6">Gaji
                                            <input type="text" class="form-control" name="gaji" value="{!! $result->gaji !!}">
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="radio-list">
                                        <label class="col-md-6">Pekerjaan
                                            <input type="text" class="form-control" name="pekerjaan" value="{!! $result->pekerjaan !!}">
                                        </label>
                                        <label class="col-md-6">Nama Instansi
                                            <input type="text" class="form-control" name="instansi" value="{!! $result->instansi !!}">
                                        </label>
                                    </div>
                                </div>


                                <br>
                                <input type="submit" class="btn btn-primary" value="Update">
                            </form>
                        </div>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->

@stop
@section('js')
    <script src="{!! URL::to('local/resources/assets/assets/global/plugins/select2/js/select2.full.min.js') !!}" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="{!! URL::to('local/resources/assets/assets/global/scripts/app.min.js')!!}" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{!! URL::to('local/resources/assets/assets/pages/scripts/components-select2.min.js')!!}" type="text/javascript"></script>

@stop