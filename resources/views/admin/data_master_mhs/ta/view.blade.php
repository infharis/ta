@extends('admin.layouts.index')
@section('css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/datatables.min.css')!!}" rel="stylesheet" type="text/css" />
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')!!}" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->
@stop
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">@include('notif')
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>
                        Bimbingan Mahasiswa Tugas Akhir
                    </h1>
                </div>
                <div class="pull-right">
                    <a href="{!! URL::to('mhs/view/data') !!}" class="btn btn-sm yellow dropdown-toggle">Back</a>
                    {{--<a href="{!! URL::to('data/mhs/ta/create') !!}" class="btn btn-sm green dropdown-toggle">Tambah</a>--}}
                </div>
            </div>

            <!-- END PAGE BREADCRUMB -->
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-warning alert-block">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        Untuk Mengisi Data <b>Tugas Akhir</b> Anda Harus Mendaftarkan Mhs Terlebih Dahulu
                    </div>
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption font-dark">
                                <i class="icon-settings font-dark"></i>
                                <span class="caption-subject bold"></span>
                            </div>
                            <div class="tools"> </div>
                        </div>
                        <div class="portlet-body">
                            <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
                                <thead>
                                <tr>
                                    <th rowspan="2">No</th>
                                    <th rowspan="2">Nama</th>
                                    <th colspan="3" class="text-center">Seminar</th>
                                    <th colspan="2" class="text-center">Dosen Pembimbing</th>
                                    <th rowspan="2">Officially S.kom</th>
                                    <th rowspan="2">Aksi</th>
                                </tr>
                                <tr>
                                    <th>Proposal</th>
                                    <th>Hasil</th>
                                    <th>Sidang</th>
                                    <th>I</th>
                                    <th>II</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i=1;
                                    $ta_mhs=0;
                                    $bulan=0;
                                    $count=0;
                                ?>
                                @foreach($result as $value)
                                    <?php
                                            $count=$value->count();
                                            $start=new DateTime($value->proposal);
                                            $end=new DateTime($value->sidang);
                                            $interval = $start->diff($end);
                                            $done_ta=$interval->format('%m');
                                            if($done_ta<=6)
                                            {
                                                $ta_mhs++;
                                                $bulan+=$done_ta;
                                            }
                                    ?>
                                    <tr>
                                        <td>{!! $i++ !!}</td>
                                        <td>{!! $value->nama !!}</td>
                                        <td>{!! $value->proposal !!}</td>
                                        <td>{!! $value->hasil !!}</td>
                                        <td>{!! $value->sidang !!}</td>
                                        <td>{!! @$value->Common('dosen',array('id'=>$value->doping1))->nama_dosen  !!}</td>
                                        <td>{!! @$value->Common('dosen',array('id'=>$value->doping2))->nama_dosen !!}</td>
                                        <td>
                                            {{--{!! $interval->format('%y years %m months and %d days') !!}--}}
                                            {!! $interval->format('%m months and %d days') !!}
                                        </td>
                                        <td>
                                            <a href="{!! URL::to('data/mhs/ta/edit/'.$value->id) !!}" class="btn btn-primary">Edit</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tr>
                                    <td colspan="9"><b>Jumlah Mhs Waktu Penyelesaian TA <=6 Bulan : </b>{!! $ta_mhs !!}</td>
                                </tr>
                                <tr>
                                    <td colspan="9"><b>Jumlah Mhs Waktu Penyelesaian TA > 6 Bulan : </b>{!! $count-$ta_mhs !!}</td>
                                </tr>
                                <tr>
                                    <td colspan="9"><b>Rata-Rata Waktu Penyelesaian TA <=6 Bulan : </b>
                                        {!! $bulan ? $bulan/$ta_mhs : ' ' !!}
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>

            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
@stop
@section('js')

    <script src="{!! URL::to('local/resources/assets/assets/global/scripts/datatable.js')!!}" type="text/javascript"></script>
    <script src="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/datatables.min.js')!!}" type="text/javascript"></script>
    <script src="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')!!}" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="{!! URL::to('local/resources/assets/assets/global/scripts/app.min.js')!!}" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{!! URL::to('local/resources/assets/assets/pages/scripts/table-datatables-responsive.min.js')!!}" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
@stop