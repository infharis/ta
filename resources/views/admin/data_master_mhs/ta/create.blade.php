@extends('admin.layouts.index')
@section('css')
    <link href="{!! URL::to('local/resources/assets/selectize/css/selectize.bootstrap3.css') !!}" rel="stylesheet">
@stop
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- END PAGE BREADCRUMB -->
            <div class="row ">
                <div class="col-md-12">
                    <!-- BEGIN SAMPLE FORM PORTLET-->
                    <div class="portlet box purple ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-gift"></i> Form Tambah Bimbingan Tugas Akhir
                            </div>
                            <div class="pull-right" style="padding-top: 0.5%;">
                                <a href="{!! URL::to('data/mhs/ta') !!}" class="btn btn-sm yellow dropdown-toggle">Back</a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <form role="form" action="{!! URL::to('data/mhs/ta/store') !!}" method="post">
                                {!! csrf_field() !!}
                                <div class="row">
                                    <div class="col-md-12">
                                        <select name="nim" required data-md-selectize>
                                            <option value="">-- Pilih Mahasiswa --</option>
                                            @foreach($mhs as $value)
                                                <option value="{!! $value->nim !!}">{!! $value->nim.' '.$value->nama !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <!-- /.row -->
                                <div class="row">
                                    <label style="margin-left:43%;margin-top:2%"><b>Dosen Pembimbing</b></label>
                                    <div class="radio-list">
                                        <label class="col-md-6">I
                                            <input type="text" class="form-control" name="doping1"></label>
                                        <label class="col-md-6">II
                                            <input type="text" class="form-control" name="doping2"> </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <label style="margin-left:43%;margin-top:2%"><b>Seminar</b></label>
                                    <div class="radio-list">
                                        <label class="col-md-4">Proposal
                                            <input type="date" class="form-control" name="proposal"></label>
                                        <label class="col-md-4">Hasil
                                            <input type="date" class="form-control" name="hasil"> </label>
                                        <label class="col-md-4">Sidang
                                            <input type="date" class="form-control" name="sidang"> </label>
                                    </div>
                                </div>
                            <br>
                            <input type="submit" class="btn btn-primary" value="Simpan">
                            </form>
                        </div>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->

@stop
@section('js')
    <script src="{!! URL::to('local/resources/assets/selectize/common.min.js')!!}"></script>
    <script src="{!! URL::to('local/resources/assets/selectize/uikit_custom.min.js')!!}"></script>
    <script src="{!! URL::to('local/resources/assets/selectize/altair_admin_common.min.js')!!}"></script>
@stop