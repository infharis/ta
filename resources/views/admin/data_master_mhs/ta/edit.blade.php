@extends('admin.layouts.index')
@section('css')
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/select2/css/select2.min.css') !!}" rel="stylesheet" type="text/css" />
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/select2/css/select2-bootstrap.min.css')!!}" rel="stylesheet" type="text/css" />
@stop
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- END PAGE BREADCRUMB -->
            <div class="row ">
                <div class="col-md-12">
                    <!-- BEGIN SAMPLE FORM PORTLET-->
                    <div class="portlet box purple ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-gift"></i> Form Edit Bimbingan/Jadwal Tugas Akhir
                            </div>
                            <div class="pull-right" style="padding-top: 0.5%;">
                                <a href="{!! URL::to('data/mhs/ta') !!}" class="btn btn-sm yellow dropdown-toggle">Back</a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <form role="form" action="{!! URL::to('data/mhs/ta/update/'.$result->id) !!}" method="post">
                                {!! csrf_field() !!}
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="">Nama Mahasiswa</label><span class="required" aria-required="true"> * </span>
                                        <input readonly type="text" class="form-control" value="{!! $result->Common('biodata_mhs',array('id'=>$result->id))->nama !!}">
                                    </div>
                                </div>
                                <!-- /.row -->
                                <div class="row">
                                    <label style="margin-left:43%;margin-top:2%"><b>Dosen Pembimbing</b></label>
                                    <div class="radio-list">
                                        <label class="col-md-6">I
                                            <select id="multiple" name="doping1" class="form-control select2-multiple">
                                                <option value=""></option>
                                                @foreach($ds as $value)
                                                    @if($value->id==$result->doping1)
                                                        <option selected value="{!! $value->id !!}">{!! $value->nama_dosen !!}</option>
                                                    @else
                                                        <option value="{!! $value->id !!}">{!! $value->nama_dosen !!}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </label>
                                        <label class="col-md-6">II
                                            <select id="multiple" name="doping2" class="form-control select2-multiple">
                                                <option value=""></option>
                                                @foreach($ds as $value)
                                                    @if($value->id==$result->doping2)
                                                        <option selected value="{!! $value->id !!}">{!! $value->nama_dosen !!}</option>
                                                    @else
                                                        <option value="{!! $value->id !!}">{!! $value->nama_dosen !!}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <label style="margin-left:43%;margin-top:2%"><b>Dosen Penguji</b></label>
                                    <div class="radio-list">
                                        <label class="col-md-4">I
                                            <select id="multiple" name="penguji1" class="form-control select2-multiple">
                                                <option value=""></option>
                                                @foreach($ds as $value)
                                                    @if($value->id==$result->penguji1)
                                                        <option selected value="{!! $value->id !!}">{!! $value->nama_dosen !!}</option>
                                                    @else
                                                        <option value="{!! $value->id !!}">{!! $value->nama_dosen !!}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </label>
                                        <label class="col-md-4">II
                                            <select id="multiple" name="penguji2" class="form-control select2-multiple">
                                                <option value=""></option>
                                                @foreach($ds as $value)
                                                    @if($value->id==$result->penguji2)
                                                        <option selected value="{!! $value->id !!}">{!! $value->nama_dosen !!}</option>
                                                    @else
                                                        <option value="{!! $value->id !!}">{!! $value->nama_dosen !!}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </label>
                                        <label class="col-md-4">III
                                            <select id="multiple" name="penguji3" class="form-control select2-multiple">
                                                <option value=""></option>
                                                @foreach($ds as $value)
                                                    @if($value->id==$result->penguji3)
                                                        <option selected value="{!! $value->id !!}">{!! $value->nama_dosen !!}</option>
                                                    @else
                                                        <option value="{!! $value->id !!}">{!! $value->nama_dosen !!}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <label style="margin-left:43%;margin-top:2%"><b>Seminar</b></label>
                                    <div class="radio-list">
                                        <label class="col-md-4">Proposal
                                            <input type="date" class="form-control" name="proposal" value="{!! $result->proposal !!}"></label>
                                        <label class="col-md-4">Hasil
                                            <input type="date" class="form-control" name="hasil" value="{!! $result->hasil !!}"> </label>
                                        <label class="col-md-4">Sidang
                                            <input type="date" class="form-control" name="sidang" value="{!! $result->sidang !!}"> </label>
                                    </div>
                                </div>
                                <br>
                                <input type="submit" class="btn btn-primary" value="Update">
                            </form>
                        </div>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->

@stop
@section('js')
    <script src="{!! URL::to('local/resources/assets/assets/global/plugins/select2/js/select2.full.min.js') !!}" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="{!! URL::to('local/resources/assets/assets/global/scripts/app.min.js')!!}" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{!! URL::to('local/resources/assets/assets/pages/scripts/components-select2.min.js')!!}" type="text/javascript"></script>

@stop