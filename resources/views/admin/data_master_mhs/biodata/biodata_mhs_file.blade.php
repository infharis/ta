<?php
$sheet1='data mahasiswa';
$name_file='Format Import Data Mahasiswa';

Excel::create($sheet1,function($excel)use($sheet1,$name_file){

    $excel->sheet($sheet1,function($sheet)use($name_file){
        // set heading border
//        $sheet->cells('B3', function($cells) {
//            $cells->setFontColor('#ffffff');
//            $cells->setAlignment('center');
//            $cells->setFontSize(24);
//            $cells->setValue('Mohon Mengikuti Format Berikut, Silahkan diisi data MHS dibawah ini!!!');
//        });
//        $sheet->cells('A1:A4', function($cells) {
//            // $cells->setAlignment('center');
//            $cells->setBackground('#1e422a');
//        });
//        $sheet->cells('B1:B4', function($cells) {
//            // $cells->setAlignment('center');
//            $cells->setBackground('#1e422a');
//        }); $sheet->cells('C1:C4', function($cells) {
//            // $cells->setAlignment('center');
//            $cells->setBackground('#1e422a');
//        });

        $sheet->setBorder('A1:B1', 'thick');

        // heading title
        $sheet->setWidth('A',30);
        $sheet->cell('A1', function($cell) {
            $cell->setFont(array(
                'family'     => 'Calibri',
                'size'       => '16',
                'bold'       =>  true
            ));
            $cell->setValue('NPM');
        });
        $sheet->setWidth('B',80);
        $sheet->cell('B1', function($cell) {
            $cell->setFont(array(
                'family'     => 'Calibri',
                'size'       => '16',
                'bold'       =>  true
            ));
            $cell->setValue('Nama Mahasiswa');
        });
//        $sheet->setWidth('C',30);
//        $sheet->cell('C1', function($cell) {
//            $cell->setFont(array(
//                'family'     => 'Calibri',
//                'size'       => '16',
//                'bold'       =>  true
//            ));
//            $cell->setValue('Tahun Masuk');
//        });
    });
})->export('xls');
