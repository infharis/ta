@extends('admin.layouts.index')
@section('css')
    <link href="{!! URL::to('local/resources/assets/selectize/css/selectize.bootstrap3.css') !!}" rel="stylesheet">
@stop
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">@include('notif')
            <!-- END PAGE BREADCRUMB -->
            <div class="row ">
                <div class="col-md-12">

                    <!-- BEGIN SAMPLE FORM PORTLET-->
                    <div class="portlet box purple ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-gift"></i> Form Edit Mahasiswa/i
                            </div>
                            <div class="pull-right" style="padding-top: 0.5%;">
                                <a href="{!! URL::to('data/mhs/biodata_mhs') !!}" class="btn btn-sm yellow dropdown-toggle">Back</a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <form role="form" action="{!! URL::to('data/mhs/biodata_mhs/update/'.$result->id) !!}" method="post">
                                {!! csrf_field() !!}
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="">Nama</label><span class="required" aria-required="true"> * </span>
                                        <input type="text" class="form-control" name="nama" required value="{!! $result->nama !!}">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="">NIM</label><span class="required" aria-required="true"> * </span>
                                        <input type="text" class="form-control" name="nim" required value="{!! $result->nim !!}">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="">Tahun Masuk</label><span class="required" aria-required="true"> * </span>
                                        <input type="text" value="{!! $result->tahun_masuk !!}" name="tahun_masuk" class="form-control" placeholder="contoh 2012" required>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="">Tahun Keluar</label>
                                        <input type="text" value="{!! $result->tahun_keluar !!}"name="tahun_keluar" class="form-control" placeholder="contoh 2016">
                                    </div>
                                </div>
                                <!-- /.row -->
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="">Asal Sekolah</label><span class="required" aria-required="true"> * </span>
                                        <input type="text" class="form-control" name="asal" required value="{!! $result->asal_sekolah !!}">
                                    </div>
                                    <div class="col-md-4">
                                        <label for="multiple">Provinsi</label><span class="required" aria-required="true"> * </span>
                                        <select name="prov" id="prov" class="prov" required data-md-selectize>
                                            <option value="">--Pilih Provinsi Anda--</option>
                                            @foreach($prov as $value)
                                                @if($value->id==$result->prov_id)
                                                    <option selected value="{!! $value->id !!}">{!! $value->nama_provinsi !!}</option>
                                                @else
                                                    <option value="{!! $value->id !!}">{!! $value->nama_provinsi !!}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="">Kab/kota</label><span class="required" aria-required="true"> * </span>
                                        <select name="kab" class="kab" id="kab" required data-md-selectize>
                                            <option value="">--Pilih Kab/Kota Anda--</option>
                                            @foreach($kab as $val)
                                                @if($val->id==$result->kab_id)
                                                    <option selected value="{!! $val->id !!}">{!! $val->nama_kab !!}</option>
                                                @else
                                                    <option value="{!! $val->id !!}">{!! $val->nama_kab !!}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="md-radio-inline">Beasiswa
                                            <div class="md-radio">
                                                <input type="radio" id="checkbox11_8" @if($result->beasiswa=='Y') checked @endif name="beasiswa" value="Y" class="md-radiobtn">
                                                <label for="checkbox11_8">
                                                    <span></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span> Y</label>
                                            </div>
                                            <div class="md-radio">
                                                <input type="radio" id="checkbox12_9" @if($result->beasiswa=='N') checked @endif  name="beasiswa" value="N" class="md-radiobtn">
                                                <label for="checkbox12_9">
                                                    <span></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span> N </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="">Sumber Beasiswa</label><span class="required" aria-required="true"> * </span>
                                        <input type="text" class="form-control" name="sumber_beasiswa" value="{!! $result->sumber_beasiswa !!}" required>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="md-radio-inline">Status
                                            <div class="md-radio">
                                                <input type="radio" @if($result->status=='R') checked @endif id="checkbox13_8" name="status" value="R" class="md-radiobtn">
                                                <label for="checkbox13_8">
                                                    <span></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span>Reguler</label>
                                            </div>
                                            <div class="md-radio">
                                                <input type="radio" @if($result->status=='T') checked @endif id="checkbox14_9" name="status" value="T" class="md-radiobtn">
                                                <label for="checkbox14_9">
                                                    <span></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span> Transfer </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="multiple" class="control-label">Pembimbing Akademik<span class="required" aria-required="true">*</span></label>
                                        <select  name="dosen" data-md-selectize>
                                            <option value="0">--Pilih Dosen Wali --</option>
                                            @foreach($dosen as $dos)
                                                @if($dos->id==$result->dosen_id)
                                                    <option value="{!! $dos->id !!}" selected>{!! $dos->nama_dosen !!}</option>
                                                @else
                                                    <option value="{!! $dos->id !!}">{!! $dos->nama_dosen !!}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <br><br>
                                <input type="submit" class="btn btn-primary" value="Update">
                            </form>
                        </div>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->

@stop
@section('js')
    <script type="text/javascript">
        $(document).ready(function(){
            // program
            $(document).on("change",".prov",function(){
                var id=$(this).val();
                // kegiatan
                $.ajax({
                    type:'GET',
                    url:"{!! URL::to('data/mhs/biodata_mhs/get_prov') !!}",
                    data:{
                        "id":id
                    },
                    success:function(response)
                    {

                        var new_value_options   = '[';
                        $.each(JSON.parse(response),function(index,value){

                            new_value_options += '{value:' +value.id + ',text:"'+ value.nama_kab + '"},';
                        });
                        new_value_options   += ']';
                        new_value_options = eval('('+new_value_options+')');

                        var selectize1 = $(".kab")[0].selectize;

                        selectize1.clear();
                        selectize1.clearOptions();
                        selectize1.renderCache['option'] = {};
                        selectize1.renderCache['item'] = {};
                        selectize1.addOption(new_value_options);


                    }
                });


            });


        });
    </script>
    <script src="{!! URL::to('local/resources/assets/selectize/common.min.js')!!}"></script>
    <script src="{!! URL::to('local/resources/assets/selectize/uikit_custom.min.js')!!}"></script>
    <script src="{!! URL::to('local/resources/assets/selectize/altair_admin_common.min.js')!!}"></script>
@stop