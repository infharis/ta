@extends('admin.layouts.index')
@section('css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/datatables.min.css')!!}" rel="stylesheet" type="text/css" />
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')!!}" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css')!!}" rel="stylesheet" type="text/css" />
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css')!!}" rel="stylesheet" type="text/css" />
@stop
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">@include('notif')
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>
                        Data Mahasiswa @lang("prodi.nama_jurusan")
                    </h1>
                </div>
                <div class="pull-right">
                    <a href="{!! URL::to('mhs/view/data') !!}" class="btn btn-sm yellow dropdown-toggle">Back</a>
                    <a href="{!! URL::to('data/mhs/biodata_mhs/create') !!}" class="btn btn-sm green dropdown-toggle">Tambah MHS</a>
                </div>
            </div>


        <!-- END PAGE BREADCRUMB -->
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-2 pull-right">

                    <div class="color-demo tooltips" data-target="#stack1" data-toggle="modal" disabled="">
                        <div class="color-view bg-blue-steel bg-font-blue-steel bold uppercase">
                            <i class="fa fa-user-plus"></i>Mahasiswa</div>
                        <div class="color-info bg-white c-font-14 sbold">Sinkronkan Data Mahasiswa</div>
                    </div>

                </div>

                {{--<a class="btn btn-outline dark" data-target="#stack1" data-toggle="modal"> View Demo </a>--}}
                <div id="stack1" class="modal fade" tabindex="-1" data-focus-on="input:first">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Form Request Data Mhs</h4>
                    </div>
                    <form action="{!! URL::to('sinkron_data_mhs') !!}" method="post" id="sinkron_data">
                        {!! csrf_field() !!}
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="">Masukan Tahun Angkatan Mhs (Wajib 4 Digit)</label>
                            <input class="form-control" type="text" id="tahun" name="tahun" data-tabindex="1" maxlength="4" placeholder="2012" required>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="text-center"><strong>Masukan Nim (Angka Terakhir)</strong></div>
                                <div class="col-md-6">Nim Mulai
                                    <div class="form-group form-md-line-input has-success">
                                        <input type="number" class="form-control" id="nim_awal" required name="nim_awal" min="1" maxlength="1">
                                    </div>
                                </div>
                                <div class="col-md-6">Nim Akhir
                                    <div class="form-group form-md-line-input has-warning">
                                        <input type="text" class="form-control" id="nim_akhir" name="nim_akhir" required maxlength="2">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
                        <button type="submit" class="btn green sinkron_mhs">Ok</button>
                    </div>
                    </form>
                </div>
                <div class="col-md-10">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption font-dark">
                                <i class="icon-settings font-dark"></i>
                                <span class="caption-subject bold"></span>
                            </div>
                            <div class="pull-right">
                                <a href="{!! URL::to('mhs/view/data/format_biodata_mhs') !!}" class="btn blue btn-outline sbold">Download Format File Data MHS</a>
                                <a class="btn red btn-outline sbold" data-toggle="modal" href="#basic"> Import Data MHS </a>
                                <div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <form action="{!! URL::to('data/mhs/biodata_mhs/import_data_mhs') !!}" method="post" enctype="multipart/form-data">
                                            {!! csrf_field() !!}
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                    <h4 class="modal-title">Form Import Data MHS</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <input type="file" name="data_mhs" class="form-control" required>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn green">Simpan</button>
                                                </div>
                                            </div>
                                        </form>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                            </div>
                        </div>

                        <div class="portlet-body">
                            <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
                                <div class="alert alert-info">
                                    @if($last_updated)
                                        Last Sinkron {!! @$last_updated->created_at->diffForHumans() !!}
                                    @endif
                                </div>
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nim</th>
                                        <th>Nama</th>
                                        <th>Doping</th>
                                        <th>IPK</th>
                                        <th>Asal Sekolah</th>
                                        <th>Tahun Masuk</th>
                                        <th>Tahun Keluar</th>
                                        <th>Massa Studi</th>
                                        <th>Status</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php $i=1;?>
                                @foreach($result as $value)
                                    <?php
                                        $nama=
                                        $data_mhs=0;
                                        $tahun_keluar=$value->tahun_keluar ? $value->tahun_keluar : 0;
                                        $tahun_masuk=$value->tahun_masuk ? $value->tahun_masuk : 0;
                                        $data_mhs=$value->DataMhs($value->id);
                                        // interval tahun
                                        $done_ms=(int)$tahun_keluar-(int)$tahun_masuk;
                                    ?>
                                    <tr>
                                        <td>{!! $i++ !!}</td>
                                        <td>{!! $value->nim !!}</td>
                                        <td>{!! $value->nama !!}</td>
                                        <td>
                                            {!! @$value->Dosen($value->nim)->nama_dosen !!}
                                        </td>
                                        <td>{!! $value->ipk !!}</td>
                                        <td>{!! $value->asal_sekolah !!}</td>
                                        <td>{!! $value->tahun_masuk or ' ' !!}</td>
                                        <td>{!! $value->tahun_keluar or ' ' !!}</td>
                                        <td>
                                            @if($value->tahun_keluar)
                                                {!! $done_ms > 0 ? $done_ms : ' '  !!} Years
                                            @endif
                                        </td>
                                        <td>
                                            @if($value->status='R')
                                                Reguler
                                            @else
                                                Transfer
                                            @endif
                                        </td>
                                        <td>
                                            <a href="{!! URL::to('data/mhs/biodata_mhs/edit/'.$value->id) !!}" class="btn btn-primary">Edit</a>
                                            <a href="#" id="delete" value="{!! $value->id !!}" data_mhs="{!! $data_mhs !!}" class="btn btn-danger">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>

            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
@stop
@section('js')
    <script type="text/javascript">
        $(document).ready(function(){


            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('.sinkron_mhs').click(function() {
                var tahun=$("#tahun").val();
                var awal=$("#nim_awal").val();
                var akhir=$("#nim_akhir").val();

                if(tahun!=='' && awal!=='' && akhir!=='') {
                    $("#sinkron_data").submit();
                    $(".sinkron_mhs").prop('disabled', true);
                    $(".sinkron_mhs").text('Mohon Tunggu Sebentar, Sedang Menyinkronkan Data....');
                }
            });

            $(document).on("click","#delete",function(){
                var data_mhs=0;
                data_mhs=$(this).attr('data_mhs');
                if(data_mhs=='kosong') {
                    if (confirm('Are You Sure Delete This Data?')) {
                        var val = $(this).attr('value');
                        $.ajax({
                            type: 'DELETE',
                            url: '{!! URL::to('data/mhs/biodata_mhs/destroy') !!}',
                            data: {
                                id: val
                            },
                            success: function (response) {
                                if (response == 1) {
                                    alert('Data Berhasil Dihapus');
                                    location.reload();
                                } else {
                                    alert('Gagal Menghapus Data');
                                }
                            }
                        });
                    } else {
                        return false;
                    }
                }else {
                    alert('Sorry, Untuk Menghapus Biodata Mahasiswa, Anda Harus Menghapus Data Mhs Lainnya (Prestai, IPK,Penelitian, P2m)');
                }
            });
        });
    </script>
    <script src="{!! URL::to('local/resources/assets/assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js')!!}" type="text/javascript"></script>
    <script src="{!! URL::to('local/resources/assets/assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js')!!}" type="text/javascript"></script>
    <script src="{!! URL::to('local/resources/assets/assets/global/scripts/datatable.js')!!}" type="text/javascript"></script>
    <script src="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/datatables.min.js')!!}" type="text/javascript"></script>
    <script src="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')!!}" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="{!! URL::to('local/resources/assets/assets/global/scripts/app.min.js')!!}" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{!! URL::to('local/resources/assets/assets/pages/scripts/table-datatables-responsive.min.js')!!}" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
@stop