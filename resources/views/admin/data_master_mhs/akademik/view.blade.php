@extends('admin.layouts.index')
@section('css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/datatables.min.css')!!}" rel="stylesheet" type="text/css" />
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')!!}" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->
@stop
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>
                        Data IPK Mahasiswa @lang("prodi.nama_jurusan")
                    </h1>
                </div>
                <div class="pull-right">
                    <a href="{!! URL::to('mhs/view/data') !!}" class="btn btn-sm yellow dropdown-toggle">Back</a>
                    <a href="{!! URL::to('data/mhs/akademik/create') !!}" class="btn btn-sm green dropdown-toggle">Tambah IPK/Semester</a>
                </div>
            </div>

            <!-- END PAGE BREADCRUMB -->
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-warning alert-block">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        Untuk Mengisi Data <b>Akademik</b> Anda Harus Mendaftarkan Mhs Terlebih Dahulu
                    </div>
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption font-dark">
                                <i class="icon-settings font-dark"></i>
                                <span class="caption-subject bold"></span>
                            </div>
                            <div class="tools"> </div>
                        </div>
                        <div class="portlet-body">
                            <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Semester</th>
                                    <th>IPK</th>
                                    <th>Aksi</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i=1;?>
                                @foreach($result as $value)
                                    <tr>
                                        <td>{!! $i++ !!}</td>
                                        <td>{!! @$value->Common('biodata_mhs',array('id'=>$value->mhs_id))->nama !!}</td>
                                        <td>Semester {!! $value->Common('semester',array('id'=>$value->semester_id))->semester !!}</td>
                                        <td>{!! $value->ipk !!}</td>
                                        <td>
                                            <a href="{!! URL::to('data/mhs/akademik/edit/'.$value->id) !!}" class="btn btn-primary">Edit</a>
                                            <a href="#" id="delete" value="{!! $value->id !!}" class="btn btn-danger">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>

            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
@stop
@section('js')
    <script type="text/javascript">
        $(document).ready(function(){

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $(document).on("click","#delete",function(){
                if (confirm('Are You Sure Delete This Data?')) {
                    var val = $(this).attr('value');
                    $.ajax({
                        type: 'DELETE',
                        url: '{!! URL::to('data/mhs/akademik/destroy') !!}',
                        data: {
                            id: val
                        },
                        success: function (response) {
                            if (response == 1) {
                                alert('Data Berhasil Dihapus');
                                location.reload();
                            } else {
                                alert('Gagal Menghapus Data');
                            }
                        }
                    });
                } else {
                    return false;
                }
            });
        });
    </script>

    <script src="{!! URL::to('local/resources/assets/assets/global/scripts/datatable.js')!!}" type="text/javascript"></script>
    <script src="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/datatables.min.js')!!}" type="text/javascript"></script>
    <script src="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')!!}" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="{!! URL::to('local/resources/assets/assets/global/scripts/app.min.js')!!}" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{!! URL::to('local/resources/assets/assets/pages/scripts/table-datatables-responsive.min.js')!!}" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
@stop