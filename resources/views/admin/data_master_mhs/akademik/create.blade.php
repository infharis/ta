@extends('admin.layouts.index')
@section('css')
    <link href="{!! URL::to('local/resources/assets/selectize/css/selectize.bootstrap3.css') !!}" rel="stylesheet">
@stop
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- END PAGE BREADCRUMB -->
            <div class="row ">
                <div class="col-md-12">
                    <!-- BEGIN SAMPLE FORM PORTLET-->
                    <div class="portlet box purple ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-gift"></i> Form Tambah IPK Mahasiswa/i
                            </div>
                            <div class="pull-right" style="padding-top: 0.5%;">
                                <a href="{!! URL::to('data/mhs/akademik') !!}" class="btn btn-sm yellow dropdown-toggle">Back</a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <form role="form" action="{!! URL::to('data/mhs/akademik/store') !!}" method="post">
                                {!! csrf_field() !!}
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="">Nama</label><span class="required" aria-required="true"> * </span>
                                        <select name="mhs_id" required data-md-selectize>
                                            <option value="">-- Pilih Mahasiswa --</option>
                                            @foreach($mhs as $value)
                                                <option value="{!! $value->id !!}">{!! $value->nim.' '.$value->nama !!}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                </div>
                                <!-- /.row -->
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="">Semester</label><span class="required" aria-required="true"> * </span>
                                        <select name="semester" required data-md-selectize>
                                            <option value="">-- Pilih Semester --</option>
                                            @foreach($sm as $value1)
                                                <option value="{!! $value1->id !!}">{!! 'Semester '.$value1->semester !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="">IPK</label><span class="required" aria-required="true"> * </span>
                                        <input type="text" class="form-control" name="ipk">
                                    </div>
                                </div>
                                <br>
                            <input type="submit" class="btn btn-primary" value="Simpan">
                            </form>
                        </div>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->

@stop
@section('js')
    <script src="{!! URL::to('local/resources/assets/selectize/common.min.js')!!}"></script>
    <script src="{!! URL::to('local/resources/assets/selectize/uikit_custom.min.js')!!}"></script>
    <script src="{!! URL::to('local/resources/assets/selectize/altair_admin_common.min.js')!!}"></script>
@stop