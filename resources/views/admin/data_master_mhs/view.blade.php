@extends('admin.layouts.index')
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light portlet-fit bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class=" icon-layers font-green"></i>
                                <span class="caption-subject font-green bold uppercase">Kelompok Data MHS</span>
                            </div>
                            <div class="pull-right" style="padding-top: 0.5%;">
                                <a href="{!! URL::to('mhs/view') !!}" class="btn btn-sm yellow dropdown-toggle">Back</a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="mt-element-step">
                                <div class="row step-default">
                                    <div class="col-md-6 bg-grey mt-step-col">
                                        <a href="{!! URL::to('data/mhs/biodata_mhs') !!}" style="text-decoration:none">
                                            <div class="mt-step-title font-grey-cascade">Biodata MHS</div>
                                        </a>
                                    </div>
                                    <div class="col-md-6 bg-grey mt-step-col active">
                                        <a href="{!! URL::to('data/mhs/prestasi_mhs') !!}" style="text-decoration:none">
                                            <div class="mt-step-title font-grey-cascade">Prestasi MHS</div>
                                        </a>
                                    </div>
                                    <div class="col-md-6 bg-grey mt-step-col active">
                                        <a href="{!! URL::to('data/mhs/akademik') !!}" style="text-decoration:none">
                                            <div class="mt-step-title font-grey-cascade">Akademik</div>
                                        </a>
                                    </div>
                                    <div class="col-md-6 bg-grey mt-step-col">
                                        <a href="{!! URL::to('data/mhs/ta') !!}" style="text-decoration:none">
                                            <div class="mt-step-title font-grey-cascade">Tugas Akhir</div>
                                        </a>
                                    </div>
                                    <div class="col-md-12 bg-grey mt-step-col" style="background:#2d538d!important;">
                                        <a href="{!! URL::to('data/mhs/data_alumni') !!}" style="text-decoration:none;">
                                            <div class="mt-step-title font-grey-cascade">Data Alumni</div>
                                        </a>
                                    </div>
                                </div>
                                <br/>
                                <br/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
@stop


