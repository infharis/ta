@extends('admin.layouts.index')
@section('css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/datatables.min.css')!!}" rel="stylesheet" type="text/css" />
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')!!}" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->
@stop
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">@include('notif')
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>Data Dosen Tetap
                        {{--<small>dashboard & statistics</small>--}}
                    </h1>
                </div>
                <div class="pull-right">
                    <a href="{!! URL::to('sdm/data/dosen_tetap') !!}" class="btn btn-sm yellow dropdown-toggle">Back</a>
                </div>
            </div>
            <!-- END PAGE HEAD-->

            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-2 pull-right">
                    <form action="{!! URL::to('sinkron_data_dosen') !!}" method="post" id="sinkron_data">
                        {!! csrf_field() !!}
                        <div class="color-demo tooltips sinkron_mhs" data-original-title="" data-toggle="modal" data-target="#demo_modal_white">
                            <div class="color-view bg-blue-steel bg-font-blue-steel bold uppercase">
                                <i class="fa fa-user-plus"></i>&nbsp; Dosen</div>
                            <div class="color-info bg-white c-font-14 sbold sinkron_dosen">Sinkronkan Data Dosen</div>
                        </div>
                    </form>
                </div>
                <div class="col-md-10">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light bordered">
                        {{--<div class="portlet-title">--}}
                                {{--<div class="caption font-dark">--}}
                                    {{--<i class="icon-settings font-dark"></i>--}}
                                    {{--<span class="caption-subject bold"></span>--}}
                                {{--</div>--}}
                                {{--<div class="pull-right">--}}
                                    {{--<a href="{!! URL::to('dosen/view/data/format_biodata_dosen') !!}" class="btn blue btn-outline sbold">Download Format File Data Dosen</a>--}}
                                    {{--<a class="btn red btn-outline sbold" data-toggle="modal" href="#basic"> Import Data Dosen </a>--}}
                                    {{--<div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true">--}}
                                        {{--<div class="modal-dialog">--}}
                                            {{--<form action="{!! URL::to('data/dosen/biodata_dosen/import_data_dosen') !!}" method="post" enctype="multipart/form-data">--}}
                                                {{--{!! csrf_field() !!}--}}
                                                {{--<div class="modal-content">--}}
                                                    {{--<div class="modal-header">--}}
                                                        {{--<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>--}}
                                                        {{--<h4 class="modal-title">Form Import Data Dosen</h4>--}}
                                                    {{--</div>--}}
                                                    {{--<div class="modal-body">--}}
                                                        {{--<input type="file" name="data_dosen" class="form-control" required>--}}
                                                    {{--</div>--}}
                                                    {{--<div class="modal-footer">--}}
                                                        {{--<button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>--}}
                                                        {{--<button type="submit" class="btn green">Simpan</button>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            {{--</form>--}}
                                        {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        <div class="portlet-body">
                            <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
                                <thead>
                                <tr>
                                    <th class="all">No</th>
                                    <th class="all">Aksi</th>
                                    <th class="all">NIDN</th>
                                    <th class="all">NIP</th>
                                    <th class="all">Nama Dosen</th>
                                    <th class="all">Bidang</th>
                                    <th class="all">Golongan</th>
                                    <th class="all">Pembimbing TA</th>
                                    <th class="all">Penguji TA</th>
                                    <th class="all">Jabatan Fungsional</th>
                                    <th class="all">Sertifikasi</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i=1;
                                    $count=0;$s1=0;$s2=0;$s3=0;
                                ?>
                                @foreach($result as $value)
                                    <?php  $dosen=$value->Common('dosen',array('user_id'=>$value->id));?>
                                    @if(@$dosen->kategori_dosen==2)
                                    {{--@if($value->roles->first()->id==2)--}}
                                        <?php
                                             $output=$value->Dosen($value->id);
                                             $dosen=$value->Common('dosen',array('user_id'=>$value->id));
                                             $result_s1=0;$result_s2=0;$result_s3=0;
                                             $count++;
                                        ?>
                                        @if(@$dosen->pendidikan_id==1)
                                            <?php $s3++; ?>
                                        @endif
                                        @if(@$dosen->pendidikan_id==2)
                                            <?php $s2++; ?>
                                        @endif
                                        @if(@$dosen->pendidikan_id==3)
                                            <?php $s1++; ?>
                                        @endif
                                        <tr>
                                            <td>{!! $i++ !!}</td>
                                            <td>
                                                <a href="{!! URL::to('sdm/data/dosen_tetap/perwalian_mhs/'.$dosen->nip) !!}" target="_blank" class="tooltips" data-container="body" data-placement="top" data-original-title="Detail Perwalian Mhs">
                                                    <i class="fa fa-list col-md-4" ></i>
                                                </a>
                                            </td>
                                            <td>{!! @$dosen->nidn !!}</td>
                                            <td>{!! @$dosen->nip !!}</td>
                                            <td>{!! @$dosen->nama_dosen !!}</td>
                                            <td>{!! @$dosen->bidang !!}</td>
                                            <td>{!! @$dosen->golongan !!}</td>
                                            <td>
                                                {!! $value->CountTa($dosen->id) !!}
                                                {{--<a href="{!! URL::to('sdm/data/dosen_tetap/pembimbing_ta/'.$dosen->id) !!}">{!! $value->CountTa($dosen->id) !!}</a>--}}
                                            </td>
                                            <td>{!! $value->CountPenguji($dosen->id) !!}</td>
                                            <td>
                                                @if($dosen->jabatan_id==1)
                                                    Guru Besar
                                                @elseif($dosen->jabatan_id==2)
                                                    Lektor Kepala
                                                @elseif($dosen->jabatan_id==3)
                                                    Lektor
                                                @elseif($dosen->jabatan_id==4)
                                                    Asisten Ahli
                                                @elseif($dosen->jabatan_id==5)
                                                    Tenaga Pengajar
                                                @endif
                                            </td>
                                            <td>
                                                @if($dosen->sertifikasi=='Y')
                                                    <div class="input-icon right">
                                                        <i class="fa fa-check tooltips" data-original-title="Sertifikasi!" data-container="body"></i>
                                                    </div>
                                                @endif
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                                </tbody>
                                <tr>
                                    <td colspan="11"><b>Jumlah Dosen Tetap : </b>{!! $count !!}</td>
                                </tr>
                                <tr>
                                    <td colspan="11"><b>Jumlah Dosen Tetap  S3: </b>{!! $s3 !!}</td>
                                </tr>
                                <tr>
                                    <td colspan="11"><b>Jumlah Dosen Tetap  S2: </b>{!! $s2 !!}</td>
                                </tr>
                                <tr>
                                    <td colspan="11"><b>Jumlah Dosen Tetap  S1: </b>{!! $s1 !!}</td>
                                </tr>
                                <tr>
                                    <td colspan="11">
                                        {{-- parameter ke-1 kode kategori dosen, ke-2 kode jabatan fungsional, --}}
                                        <b>Jumlah Guru Besar : {!! $value->JabatanFungsional(2,1) !!} </b><br>
                                        <b>Jumlah Lektor Kepala : {!! $value->JabatanFungsional(2,2) !!}</b><br>
                                        <b>Jumlah Lektor : {!! $value->JabatanFungsional(2,3) !!}</b><br>
                                        <b>Jumlah Asisten Ahli : {!! $value->JabatanFungsional(2,4) !!}</b><br>
                                        <b>Jumlah Tenaga Pengajar : {!! $value->JabatanFungsional(2,5) !!}</b><br>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>

            </div>

            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
@stop
@section('js')
    <script type="text/javascript">
        $('.sinkron_dosen').click(function() {
            $("#sinkron_data").submit();
        });
    </script>
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{!! URL::to('local/resources/assets/assets/global/scripts/datatable.js')!!}" type="text/javascript"></script>
    <script src="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/datatables.min.js')!!}" type="text/javascript"></script>
    <script src="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')!!}" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="{!! URL::to('local/resources/assets/assets/global/scripts/app.min.js')!!}" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{!! URL::to('local/resources/assets/assets/pages/scripts/table-datatables-responsive.min.js')!!}" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
@stop