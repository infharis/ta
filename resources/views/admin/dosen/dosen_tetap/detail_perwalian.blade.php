@extends('admin.layouts.index')
@section('css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/datatables.min.css')!!}" rel="stylesheet" type="text/css" />
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')!!}" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->
@stop
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">@include('notif')
        <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>Perwalian Dosen <strong>{!! $dosen->nama_dosen !!}</strong>
                        {{--<small>dashboard & statistics</small>--}}
                    </h1>
                </div>
                <div class="pull-right">
                    <a href="{!! URL::to('sdm/data/dosen_tetap') !!}" class="btn btn-sm yellow dropdown-toggle">Back</a>
                </div>
            </div>
            <!-- END PAGE HEAD-->

            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-body">
                            <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
                                <thead>
                                <tr>
                                    <th class="all">No</th>
                                    <th class="all">NIM</th>
                                    <th class="all">Nama</th>
                                    <th class="all">Tahun</th>
                                    <th class="all">Semester</th>
                                    {{--<th>Aksi</th>--}}
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i=1;
                                ?>
                                @foreach($result as $value)
                                    <?php
                                            $mhs=App\Models\Admin\Biodata::where('nim',$value->nim_mhs)->first();
                                            ?>
                                    <tr>
                                        <td>{!! $i++ !!}</td>
                                        <td>{!! $value->nim_mhs !!}</td>
                                        <td>{!! @$mhs->nama !!}</td>
                                        <td>{!! $value->tahun !!}</td>
                                        <td>{!! $value->semester !!}</td>
                                    </tr>
                                @endforeach

                            </table>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>

            </div>

            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
@stop
@section('js')
    <script type="text/javascript">
        $('.sinkron_dosen').click(function() {
            $("#sinkron_data").submit();
        });
    </script>
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{!! URL::to('local/resources/assets/assets/global/scripts/datatable.js')!!}" type="text/javascript"></script>
    <script src="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/datatables.min.js')!!}" type="text/javascript"></script>
    <script src="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')!!}" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="{!! URL::to('local/resources/assets/assets/global/scripts/app.min.js')!!}" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{!! URL::to('local/resources/assets/assets/pages/scripts/table-datatables-responsive.min.js')!!}" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
@stop