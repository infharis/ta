@extends('admin.layouts.index')
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">@include('notif')
        <!-- END PAGE BREADCRUMB -->
            <div class="row ">
                <div class="col-md-12">
                    <!-- BEGIN SAMPLE FORM PORTLET-->
                    <div class="portlet box purple ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-gift"></i> Form Edit Dosen Informatika </div>
                            <div class="pull-right" style="padding-top: 0.5%;">
                                <a href="{!! URL::to('sdm/data/dosen_tetap') !!}" class="btn btn-sm yellow dropdown-toggle">Back</a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <form role="form" action="{!! URL::to('sdm/data/dosen_tetap/update/'.$result->id) !!}" method="post">
                                {!! csrf_field() !!}
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="">Email</label><span class="required" aria-required="true"> * </span>
                                        <input type="email" class="form-control" name="email" value="{!! $result->email !!}">
                                    </div>
                                </div>
                                <!-- /.row -->
                                <br>
                                <div class="form-group">
                                    <div class="col-md-9">
                                        <div class="md-radio-inline">
                                            <div class="md-radio">
                                                <input type="radio" id="checkbox1_8" {!! $result->roles->first()->id==2 ? 'checked' : ' ' !!} name="status" value="2" required class="md-radiobtn">
                                                <label for="checkbox1_8">
                                                    <span></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span> Dosen Tetap PS </label>
                                            </div>
                                            <div class="md-radio">
                                                <input type="radio" id="checkbox1_9" {!! $result->roles->first()->id==3 ? 'checked' : ' ' !!} name="status" value="3" required class="md-radiobtn">
                                                <label for="checkbox1_9">
                                                    <span></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span> Dosen Tetap Diluar PS </label>
                                            </div>
                                            <div class="md-radio">
                                                <input type="radio" id="checkbox1_10" {!! $result->roles->first()->id==4 ? 'checked' : ' ' !!} name="status" value="4"  required class="md-radiobtn">
                                                <label for="checkbox1_10">
                                                    <span></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span> Dosen Tidak Tetap PS </label>
                                            </div>

                                            <div class="md-radio">
                                                <input type="radio" id="checkbox1_11"  {!! $result->roles->first()->id==5 ? 'checked' : ' ' !!} name="status" value="5"  required class="md-radiobtn">
                                                <label for="checkbox1_11">
                                                    <span></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span> Dosen Lain </label>
                                            </div>
                                        </div>
                                        {{--<span class="help-block"> Provide your email address </span>--}}
                                    </div>
                                </div>
                                <br><br>
                                <input type="submit" class="btn btn-primary" value="Update">
                            </form>
                        </div>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
@stop