<?php
$sheet1='data dosen';
$name_file='Format Import Data Dosen';

Excel::create($sheet1,function($excel)use($sheet1,$name_file){

    $excel->sheet($sheet1,function($sheet)use($name_file){
        $sheet->setBorder('A1:D1', 'thick');

        // heading title
        $sheet->setWidth('A',30);
        $sheet->cell('A1', function($cell) {
            $cell->setFont(array(
                'family'     => 'Calibri',
                'size'       => '16',
                'bold'       =>  true
            ));
            $cell->setValue('NIP');
        });
        $sheet->setWidth('B',10);
        $sheet->cell('B1', function($cell) {
            $cell->setFont(array(
                'family'     => 'Calibri',
                'size'       => '16',
                'bold'       =>  true
            ));
            $cell->setValue('NIDN');
        });
        $sheet->setWidth('C',50);
        $sheet->cell('C1', function($cell) {
            $cell->setFont(array(
                'family'     => 'Calibri',
                'size'       => '16',
                'bold'       =>  true
            ));
            $cell->setValue('Nama Dosen');
        });
        $sheet->setWidth('D',50);
        $sheet->cell('D1', function($cell) {
            $cell->setFont(array(
                'family'     => 'Calibri',
                'size'       => '16',
                'bold'       =>  true
            ));
            $cell->setValue('Bidang Keahlian');
        });

    });
})->export('xls');
