@extends('admin.layouts.index')
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">@include('notif')
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light portlet-fit bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class=" icon-layers font-green"></i>
                                <span class="caption-subject font-green bold uppercase">Upaya Peningkatan Sumber Daya Manusia</span>
                            </div>
                            {{--<div class="pull-right" style="padding-top: 0.5%;">--}}
                            {{--<a href="{!! URL::to('data/dosen/create') !!}" class="btn btn-sm yellow dropdown-toggle">Tambah</a>--}}
                            {{--</div>--}}
                        </div>
                        <div class="portlet-body">
                            <div class="mt-element-step">
                                <div class="row step-default">
                                    <div class="col-md-6 bg-grey mt-step-col">
                                        {{--<div class="mt-step-number bg-white font-grey"></div>--}}
                                        <a href="{!! URL::to('sdm/pakar/view') !!}" style="text-decoration:none">
                                            <div class="mt-step-title font-grey-cascade">Kegiatan</div>
                                            <div class="mt-step-content font-grey-cascade">Kegiatan tenaga ahli/pakar sebagai pembicara  </div>
                                        </a>
                                    </div>
                                    <div class="col-md-6 bg-grey mt-step-col active">
                                        <a href="{!! URL::to('sdm/dosen_tetap/tugas_belajar') !!}" style="text-decoration:none">
                                            <div class="mt-step-title font-grey-cascade">Dosen Tugas Belajar</div>
                                            <div class="mt-step-content font-grey-cascade">Dosen tetap yang sedang tugas belajar/izin belajar </div>
                                        </a>
                                    </div>
                                    <div class="col-md-6 bg-grey mt-step-col active">
                                        {{--<div class="mt-step-number bg-white font-grey"></div>--}}
                                        <a href="{!! URL::to('sdm/dosen_tetap/kegiatan') !!}" style="text-decoration:none">
                                            <div class="mt-step-title font-grey-cascade">Kegiatan Dosen Tetap</div>
                                            <div class="mt-step-content font-grey-cascade">Kegiatan dosen tetap yang bidang keahliannya sesuai dengan PS </div>
                                        </a>
                                    </div>
                                    <div class="col-md-6 bg-grey mt-step-col">
                                        {{--<div class="mt-step-number bg-white font-grey"></div>--}}
                                        <a href="{!! URL::to('sdm/dosen/prestasi') !!}" style="text-decoration:none">
                                            <div class="mt-step-title font-grey-cascade">Prestasi Dosen </div>
                                            <div class="mt-step-content font-grey-cascade">Pencapaian prestasi/reputasi dosen</div>
                                        </a>
                                    </div>
                                    <div class="col-md-12 bg-grey mt-step-col" style="background-color: #2d558d !important;">
                                        {{--<p style="border-top:5px solid black;"></p>--}}
                                        {{--<div class="mt-step-number bg-white font-grey"></div>--}}
                                        <a href="{!! URL::to('sdm/dosen_tetap/organisasi') !!}" style="text-decoration:none">
                                            <div class="mt-step-title font-grey-cascade">Organisasi Keilmuan </div>
                                            <div class="mt-step-content font-grey-cascade">Keikutsertaan dosen tetap dalam organisasi keilmuan atau organisasi profesi </div>
                                        </a>
                                    </div>

                                </div>
                                <br/>
                                <br/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
@stop


