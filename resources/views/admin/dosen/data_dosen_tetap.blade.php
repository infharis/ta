@extends('admin.layouts.index')
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">@include('notif')
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light portlet-fit bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class=" icon-layers font-green"></i>
                                <span class="caption-subject font-green bold uppercase">Data Dosen Tetap</span>
                            </div>
                            <div class="pull-right" style="padding-top: 0.5%;">
                                <a href="{!! URL::to('data/dosen/create') !!}" class="btn btn-sm yellow dropdown-toggle">Tambah Dosen</a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="mt-element-step">
                                <div class="row step-default">
                                    <div class="col-md-6 bg-grey mt-step-col">
                                        {{--<div class="mt-step-number bg-white font-grey"></div>--}}
                                        <a href="{!! URL::to('sdm/data/dosen_tetap/view') !!}" style="text-decoration:none">
                                            <div class="mt-step-title font-grey-cascade">Dosen Tetap</div>
                                            <div class="mt-step-content font-grey-cascade">Data dosen tetap yang bidang keahliannya sesuai bidang PS</div>
                                        </a>
                                    </div>
                                    <div class="col-md-6 bg-grey mt-step-col active">
                                        <a href="{!! URL::to('sdm/data/dosen_diluar_ps/view') !!}" style="text-decoration:none">
                                            <div class="mt-step-title font-grey-cascade">Dosen Diluar Bidang PS</div>
                                            <div class="mt-step-content font-grey-cascade">Data dosen tetap yang bidang keahliannya di luar bidang PS</div>
                                        </a>
                                    </div>
                                    <div class="col-md-6 bg-grey mt-step-col active">
                                        {{--<div class="mt-step-number bg-white font-grey"></div>--}}
                                        <a href="{!! URL::to('sdm/aktivitas/dosen_tetap') !!}" style="text-decoration:none">
                                            <div class="mt-step-title font-grey-cascade">Aktivitas Dosen Tetap</div>
                                            <div class="mt-step-content font-grey-cascade">Aktivitas dosen tetap yang bidang keahliannya sesuai dengan PS</div>
                                        </a>
                                    </div>
                                    <div class="col-md-6 bg-grey mt-step-col">
                                        {{--<div class="mt-step-number bg-white font-grey"></div>--}}
                                        <a href="{!! URL::to('sdm/data/aktivitas_dosen_tetap') !!}" style="text-decoration:none">
                                            <div class="mt-step-title font-grey-cascade">Data Aktivitas Dosen Tetap</div>
                                            <div class="mt-step-content font-grey-cascade">Data Aktivitas Mengajar Dosen Tetap Bidang PS </div>
                                        </a>
                                    </div>
                                    <div class="col-md-12 bg-grey mt-step-col" style="background-color: #2d558d !important;">
                                        {{--<p style="border-top:5px solid black;"></p>--}}
                                        {{--<div class="mt-step-number bg-white font-grey"></div>--}}
                                        <a href="{!! URL::to('sdm/data/aktivitas_dosen_diluar_ps') !!}" style="text-decoration:none">
                                            <div class="mt-step-title font-grey-cascade">Data Aktivitas Dosen Diluar PS</div>
                                            <div class="mt-step-content font-grey-cascade">Data aktivitas mengajar dosen tetap yang bidang keahliannya di luar PS  </div>
                                        </a>
                                    </div>

                                </div>
                                <br/>
                                <br/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
@stop


