@extends('admin.layouts.index')
@section('css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/datatables.min.css')!!}" rel="stylesheet" type="text/css" />
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')!!}" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->
@stop
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">@include('notif')
        <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>Prestasi Dosen
                        {{--<small>dashboard & statistics</small>--}}
                    </h1>
                </div>
                <div class="pull-right">
                    <a href="{!! URL::to('sdm/upaya_peningkatan') !!}" class="btn btn-sm yellow dropdown-toggle">Back</a>
                    {{--<a href="{!! URL::to('dosen/create/prestasi') !!}" class="btn btn-sm green dropdown-toggle">Tambah Prestasi</a>--}}
                </div>
            </div>
            <!-- END PAGE HEAD-->
            <!-- END PAGE BREADCRUMB -->
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption font-dark">
                                <i class="icon-settings font-dark"></i>
                                <span class="caption-subject bold"></span>
                            </div>
                            <div class="tools"> </div>
                        </div>
                        <div class="portlet-body">
                            <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Dosen</th>
                                    <th>Prestasi Yang Dicapai</th>
                                    <th>Waktu</th>
                                    <th>Tingkat</th>
                                    {{--<th>Aksi</th>--}}
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i=1; ?>
                                @foreach($result as $value)
                                    <tr>
                                        <td>{!! $i++ !!}</td>
                                        <td>{!! $value->NamaDosen($value->dosen_id) !!}</td>
                                        <td>{!! $value->judul_prestasi !!}</td>
                                        <td>{!! $value->waktu !!}</td>
                                        <td>
                                            @if($value->tingkat=='L')
                                                Lokal
                                            @elseif($value->tingkat=='N')
                                                Nasional
                                            @else
                                                Internasional
                                            @endif
                                        </td>

                                        {{--<td>--}}
                                            {{--<a href="#" id="{!! $value->id !!}" class="btn btn-danger hapus_data">Hapus</a>--}}
                                        {{--</td>--}}
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>

            </div>





            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
@stop
@section('js')
    <script type="text/javascript">
        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

        });

        // script remove usulan
        $(document).on("click",".hapus_data",function(){
            var id=$(this).attr('id');
            if(confirm('Apakah anda yakin ingin menghapus data tersebut?'))
            {
                $.ajax({
                    type:'POST',
                    url:"{!! URL::to('dosen/delete/prestasi') !!}",
                    data:{
                        id:id,
                        _method: 'delete'
                    },
                    success:function(response)
                    {
                        if(response==1) {
                            alert('Data berhasil dihapus!!');
                            location.reload();
                        }else {
                            alert('Gagal Dihapus');
                        }
                    }
                });
            }else{
                return false;
            }
        });

    </script>
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{!! URL::to('local/resources/assets/assets/global/scripts/datatable.js')!!}" type="text/javascript"></script>
    <script src="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/datatables.min.js')!!}" type="text/javascript"></script>
    <script src="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')!!}" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="{!! URL::to('local/resources/assets/assets/global/scripts/app.min.js')!!}" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{!! URL::to('local/resources/assets/assets/pages/scripts/table-datatables-responsive.min.js')!!}" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
@stop