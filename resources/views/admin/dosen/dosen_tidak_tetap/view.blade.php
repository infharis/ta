@extends('admin.layouts.index')
@section('css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/datatables.min.css')!!}" rel="stylesheet" type="text/css" />
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')!!}" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->
@stop
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>Data Dosen Tidak tetap
                        {{--<small>dashboard & statistics</small>--}}
                    </h1>
                </div>
                <div class="pull-right">
                    <a href="{!! URL::to('sdm/data_dosen_tidak_tetap') !!}" class="btn btn-sm yellow dropdown-toggle">Back</a>
                </div>
            </div>
            <!-- END PAGE HEAD-->

            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption font-dark">
                                <i class="icon-settings font-dark"></i>
                                <span class="caption-subject bold"></span>
                            </div>
                            <div class="tools"> </div>
                        </div>
                        <div class="portlet-body">
                            <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>NIDN</th>
                                    <th>Nama Dosen</th>
                                    <th>Email</th>
                                    <th>Sertifikasi</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i=1;
                                    $count=0;$s1=0;$s2=0;$s3=0;
                                ?>
                                @foreach($result as $value)
                                    <?php  $dosen=$value->Common('dosen',array('user_id'=>$value->id));?>
                                    @if(@$dosen->kategori_dosen==4)
                                    {{--@if($value->roles->first()->id==4)--}}
                                        <?php
                                            $output=$value->Dosen($value->id);
                                            //$dosen=$value->Common('dosen',array('user_id'=>$value->id));
//                                            $s1+=$value->JenjangDos('pendidikan_dosen',array('dosen_id'=>$dosen->id,'jenjang_id'=>1));
//                                            $s2+=$value->JenjangDos('pendidikan_dosen',array('dosen_id'=>$dosen->id,'jenjang_id'=>2));
//                                            $s3+=$value->JenjangDos('pendidikan_dosen',array('dosen_id'=>$dosen->id,'jenjang_id'=>3));
//                                            $count++;
                                        $result_s1=0;$result_s2=0;$result_s3=0;
//                                        if($result_s1=is_int($value->JenjangDos('pendidikan_dosen',array('dosen_id'=>$dosen->id,'jenjang_id'=>1)))):
//                                            $s1+=$result_s1;
//                                        endif;
//                                        if($result_s2=is_int($value->JenjangDos('pendidikan_dosen',array('dosen_id'=>$dosen->id,'jenjang_id'=>2)))):
//                                            $s2+=$result_s2;
//                                        endif;
//                                        if($result_s3=is_int($value->JenjangDos('pendidikan_dosen',array('dosen_id'=>$dosen->id,'jenjang_id'=>2)))):
//                                            $s3+=$result_s3;
//                                        endif;
                                        //                                             $s3+=$value->JenjangDos('pendidikan_dosen',array('dosen_id'=>$dosen->id,'jenjang_id'=>3));
                                        $count++;
                                        ?>
                                    @if(@$dosen->pendidikan_id==1)
                                        <?php $s3++; ?>
                                    @endif
                                    @if(@$dosen->pendidikan_id==2)
                                        <?php $s2++; ?>
                                    @endif
                                    @if(@$dosen->pendidikan_id==3)
                                        <?php $s1++; ?>
                                    @endif
                                        <tr>
                                            <td>{!! $i++ !!}</td>
                                            <td>{!! @$dosen->nidn !!}</td>
                                            <td>{!! @$dosen->nama_dosen !!}</td>
                                            <td>{!! $value->email !!}</td>
                                            <td>
                                                @if($value->sertifikasi=='Y')
                                                    <div class="input-icon right">
                                                        <i class="fa fa-check tooltips" data-original-title="You look OK!" data-container="body"></i>
                                                    </div>
                                                @endif
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                                </tbody>
                                <tr>
                                    <td colspan="4"><b>Jumlah Dosen Tidak Tetap : </b>{!! $count !!}</td>
                                </tr>
                                <tr>
                                    <td colspan="4"><b>Jumlah Dosen Tidak Tetap S3: </b>{!! $s3 !!}</td>
                                </tr>
                                <tr>
                                    <td colspan="4"><b>Jumlah Dosen Tidak Tetap S2: </b>{!! $s2 !!}</td>
                                </tr>
                                <tr>
                                    <td colspan="4"><b>Jumlah Dosen Tidak Tetap S1: </b>{!! $s1 !!}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>

            </div>





            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
@stop
@section('js')

    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{!! URL::to('local/resources/assets/assets/global/scripts/datatable.js')!!}" type="text/javascript"></script>
    <script src="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/datatables.min.js')!!}" type="text/javascript"></script>
    <script src="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')!!}" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="{!! URL::to('local/resources/assets/assets/global/scripts/app.min.js')!!}" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{!! URL::to('local/resources/assets/assets/pages/scripts/table-datatables-responsive.min.js')!!}" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
@stop