@extends('admin.layouts.index')
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">@include('notif')
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light portlet-fit bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class=" icon-layers font-green"></i>
                                <span class="caption-subject font-green bold uppercase">Data Dosen Tidak Tetap</span>
                            </div>
                            <div class="pull-right" style="padding-top: 0.5%;">
                                <a href="{!! URL::to('data/dosen/create') !!}" class="btn btn-sm yellow dropdown-toggle">Tambah Dosen</a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="mt-element-step">
                                <div class="row step-default">
                                    <div class="col-md-6 bg-grey mt-step-col">
                                        {{--<div class="mt-step-number bg-white font-grey"></div>--}}
                                        <a href="{!! URL::to('sdm/dosen_tidak_tetap/view') !!}" style="text-decoration:none">
                                            <div class="mt-step-title font-grey-cascade">Dosen Tidak Tetap</div>
                                            <div class="mt-step-content font-grey-cascade">Data dosen tidak tetap pada PS </div>
                                        </a>
                                    </div>
                                    <div class="col-md-6 bg-grey mt-step-col active">
                                        {{--<div class="mt-step-number bg-white font-grey"></div>--}}
                                        <a href="{!! URL::to('sdm/dosen_tidak_tetap/data_aktivitas') !!}" style="text-decoration:none">
                                            <div class="mt-step-title font-grey-cascade">Aktivitas Dosen Tidak Tetap</div>
                                            <div class="mt-step-content font-grey-cascade">Data Aktivitas Mengajar Dosen Tidak Tetap Bidang PS </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
@stop


