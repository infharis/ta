@extends('admin.layouts.index')
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- END PAGE BREADCRUMB -->
            <div class="row ">
                <div class="col-md-12">
                    <!-- BEGIN SAMPLE FORM PORTLET-->
                    <div class="portlet box purple ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-gift"></i> Form Tambah Mata Kuliah Pilihan
                            </div>
                            <div class="pull-right" style="padding-top: 0.5%;">
                                <a href="{!! URL::to('kurikulum/mk_pilihan/view') !!}" class="btn btn-sm yellow dropdown-toggle">Back</a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <form role="form" action="{!! URL::to('mk_pilihan/store') !!}" method="post">
                                {!! csrf_field() !!}
                                <!-- /.row -->
                                <div class="row">
                                    <label class="control-label col-md-3">Semester
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-12">
                                        <select id="multiple" name="data_mk_pilihan_id" class="form-control">
                                            @foreach($result as $value)
                                                <option value="{!! $value->id !!}">{!! $value->semester !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="">Kode MK</label>
                                        <input type="text" class="form-control" name="kode_mk">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="">Nama MK</label>
                                        <input type="text" class="form-control" name="nama">
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <label class="control-label col-md-3">Bobot Tugas
                                        {{--<span class="required"> * </span>--}}
                                    </label>
                                    <div class="md-radio-inline">&nbsp;&nbsp;&nbsp;
                                        <div class="md-radio">
                                            <input type="radio" id="checkbox1_21" name="bobot_tugas" value="Y" class="md-radiobtn">
                                            <label for="checkbox1_21">
                                                <span></span>
                                                <span class="check"></span>
                                                <span class="box"></span> Ya </label>
                                        </div>
                                        <div class="md-radio">
                                            <input type="radio" id="checkbox1_22" name="bobot_tugas" value="N" class="md-radiobtn">
                                            <label for="checkbox1_22">
                                                <span></span>
                                                <span class="check"></span>
                                                <span class="box"></span> Tidak </label>
                                        </div>
                                    </div>
                                    {{--<span class="help-block"> Provide your email address </span>--}}
                                </div>
                                <div class="row">
                                        {{--<span class="required"> * </span>--}}
                                    </label>
                                    <div class="col-md-6">
                                        <label class="control-label">Bobot SKS</label>
                                        <input type="number" class="form-control" name="bobot_sks">
                                    </div>
                                    <div class="col-md-6">
                                        <label class="control-label">Unit/ Jur/ Fak Pengelola</label>
                                        <input type="text" class="form-control" name="unit" value="JUR">
                                    </div>
                                    {{--<span class="help-block"> Provide your email address </span>--}}
                                </div>
                                <br>
                            <input type="submit" class="btn btn-primary" value="Simpan">
                            </form>
                        </div>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->

@stop
