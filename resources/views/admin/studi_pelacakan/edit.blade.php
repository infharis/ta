@extends('admin.layouts.index')
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- END PAGE BREADCRUMB -->
            <div class="row ">
                <div class="col-md-12">
                    <!-- BEGIN SAMPLE FORM PORTLET-->
                    <div class="portlet box purple ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-gift"></i> Form Edit Data Studi Pelacakan
                            </div>
                            <div class="pull-right" style="padding-top: 0.5%;">
                                <a href="{!! URL::to('mhs/evaluasi') !!}" class="btn btn-sm yellow dropdown-toggle">Back</a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <form role="form" action="{!! URL::to('mhs/evaluasi/update/'.$result->id) !!}" method="post">
                                {!! csrf_field() !!}
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="">Jenis Kemampuan</label><span class="required" aria-required="true"> * </span>
                                        <input type="text" class="form-control" name="jenis_kemampuan" value="{!! $result->jenis_kemampuan !!}">
                                    </div>
                                </div>
                                <!-- /.row -->
                                <br>

                                <div class="form-group">
                                    <label style="margin-left:35%"><b>Tanggapan Pihak Pengguna (%)</b></label>
                                    <div class="radio-list">
                                        <label class="radio-inline">Sangat Baik
                                            <input type="number" class="form-control" name="sangat_baik" value="{!! $result->sangat_baik !!}"></label>
                                        <label class="radio-inline">Baik
                                            <input type="number" class="form-control" name="baik" value="{!! $result->baik !!}"> </label>
                                        <label class="radio-inline">Cukup
                                            <input type="number" class="form-control" name="cukup" value="{!! $result->cukup !!}"> </label>
                                        <label class="radio-inline">Kurang
                                            <input type="number" class="form-control" name="kurang" value="{!! $result->kurang !!}">  </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="">Rencana Tindak Lanjut oleh Program Studi</label>
                                        <input type="text" class="form-control" name="rencana" value="{!! $result->rencana !!}">
                                    </div>
                                </div>
                                <br><br>
                                <input type="submit" class="btn btn-primary" value="Update">
                            </form>
                        </div>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->

@stop