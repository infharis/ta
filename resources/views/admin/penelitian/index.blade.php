@extends('admin.layouts.index')
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">@include('notif')
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light portlet-fit bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class=" icon-layers font-green"></i>
                                <span class="caption-subject font-green bold uppercase">Penelitian Dosen Bidang Keahliannya Sesuai PS</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="mt-element-step">
                                <div class="row step-default">
                                    <div class="col-md-6 bg-grey mt-step-col">
                                        {{--<div class="mt-step-number bg-white font-grey"></div>--}}
                                        <a href="{!! URL::to('penelitian/view') !!}" style="text-decoration:none">
                                            <div class="mt-step-title font-grey-cascade">Jumlah Penelitian</div>
                                            <div class="mt-step-content font-grey-cascade">Jumlah judul penelitian </div>
                                        </a>
                                    </div>
                                    <div class="col-md-6 bg-grey mt-step-col active">
                                        <a href="{!! URL::to('penelitian/mhs') !!}" style="text-decoration:none">
                                            <div class="mt-step-title font-grey-cascade">Penelitian Mahasiswa</div>
                                            <div class="mt-step-content font-grey-cascade">Keterlibatan mahasiswa Tugas Akhir dalam penelitian dosen </div>
                                        </a>
                                    </div>
                                    <div class="col-md-6 bg-grey mt-step-col active">
                                        {{--<div class="mt-step-number bg-white font-grey"></div>--}}
                                        <a href="{!! URL::to('penelitian/karya_dosen') !!}" style="text-decoration:none">
                                            <div class="mt-step-title font-grey-cascade">Karya Ilmiah</div>
                                            <div class="mt-step-content font-grey-cascade">Judul karya ilmiah yang dihasilkan dalam tahun akademik </div>
                                        </a>
                                    </div>
                                    <div class="col-md-6 bg-grey mt-step-col">
                                        {{--<div class="mt-step-number bg-white font-grey"></div>--}}
                                        <a href="{!! URL::to('penelitian/paten') !!}" style="text-decoration:none">
                                            <div class="mt-step-title font-grey-cascade">Hak Paten</div>
                                            <div class="mt-step-content font-grey-cascade">Karya dosen dan atau mahasiswa HaKI</div>
                                        </a>
                                    </div>

                                </div>
                                <br/>
                                <br/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
@stop


