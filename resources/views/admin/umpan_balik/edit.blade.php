@extends('admin.layouts.index')
@section('css')
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/select2/css/select2.min.css') !!}" rel="stylesheet" type="text/css" />
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/select2/css/select2-bootstrap.min.css')!!}" rel="stylesheet" type="text/css" />
@stop
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- END PAGE BREADCRUMB -->
            <div class="row ">
                <div class="col-md-12">
                    <!-- BEGIN SAMPLE FORM PORTLET-->
                    <div class="portlet box purple ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-gift"></i> Form Edit Umpan balik dari dosen, mahasiswa, alumni, dan pengguna lulusan
                            </div>
                            <div class="pull-right" style="padding-top: 0.5%;">
                                <a href="{!! URL::to('umpan_balik/view') !!}" class="btn btn-sm yellow dropdown-toggle">Back</a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <form role="form" method="post" action="{!! URL::to('umpan_balik/update/'.$result->id) !!}">
                                {!! csrf_field() !!}
                                <div class="row">
                                    <label class="control-label col-md-3">Umpan Balik
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-12">
                                        {{--<select id="multiple" class="form-control select2-multiple" multiple>--}}
                                        {{--<option value="1">Dosen</option>--}}
                                        {{--<option value="2">Mahasiswa</option>--}}
                                        {{--<option value="3">Alumni</option>--}}
                                        {{--<option value="4">pengguna lulusan</option>--}}
                                        {{--</select>--}}
                                        <input type="text" class="form-control" name="umpan_balik" value="{!! $result->umpan_balik !!}">
                                    </div>
                                </div>
                                <!-- /.row -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="">Isi</label><span class="required" aria-required="true"> * </span>
                                        <input type="text" class="form-control" name="isi" value="{!! $result->isi !!}">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="">Tindak Lanjut</label>
                                        <textarea name="tindak_lanjut" class="form-control" id="" cols="30" rows="5">
                                            {!! $result->tindak_lanjut !!}
                                        </textarea>
                                    </div>
                                </div>
                                <br>
                                <br><br>
                                <input type="submit" class="btn btn-primary" value="Update">
                            </form>
                        </div>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->

@stop
@section('js')
    <script src="{!! URL::to('local/resources/assets/assets/global/plugins/select2/js/select2.full.min.js') !!}" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="{!! URL::to('local/resources/assets/assets/global/scripts/app.min.js')!!}" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{!! URL::to('local/resources/assets/assets/pages/scripts/components-select2.min.js')!!}" type="text/javascript"></script>

@stop