@extends('admin.layouts.index')
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">@include('notif')
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light portlet-fit bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class=" icon-layers font-green"></i>
                                <span class="caption-subject font-green bold uppercase">KEGIATAN PELAYANAN/PENGABDIAN KEPADA MASYARAKAT (PKM)</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="mt-element-step">
                                <div class="row step-default">
                                    <div class="col-md-6 bg-grey mt-step-col">
                                        {{--<div class="mt-step-number bg-white font-grey"></div>--}}
                                        <a href="{!! URL::to('p2m/view/jumlah') !!}" style="text-decoration:none">
                                            <div class="mt-step-title font-grey-cascade">Jumlah P2m</div>
                                            <div class="mt-step-content font-grey-cascade">Jumlah Pelayanan Dan Pengabdian Masyarakat</div>
                                        </a>
                                    </div>
                                    <div class="col-md-6 bg-grey mt-step-col active">
                                        <a href="{!! URL::to('p2m/mhs/view') !!}" style="text-decoration:none">
                                            <div class="mt-step-title font-grey-cascade">P2m Mahasiswa</div>
                                            <div class="mt-step-content font-grey-cascade">Keterlibatan mahasiswa Tugas Akhir dalam p2m dosen </div>
                                        </a>
                                    </div>
                                </div>
                                <br/>
                                <br/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
@stop


