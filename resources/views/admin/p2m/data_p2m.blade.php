@extends('admin.layouts.index')
@section('css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/datatables.min.css')!!}" rel="stylesheet" type="text/css" />
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')!!}" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->
@stop
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">@include('notif')
        <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>Kegiatan Penelitan dan Pengabdian Masyarakat
                        {{--<small>dashboard & statistics</small>--}}
                    </h1>
                </div>
                <div class="pull-right">
                    <a href="{!! URL::to('pembiayaan/alokasi_dana') !!}" class="btn btn-sm yellow dropdown-toggle">Back</a>
                </div>
            </div>

            <!-- END PAGE BREADCRUMB -->
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption font-dark">
                                <i class="icon-settings font-dark"></i>
                                <span class="caption-subject bold"></span>
                            </div>
                            <div class="tools"> </div>
                        </div>
                        <div class="portlet-body">
                            <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th class="none">Dosen : </th>
                                    <th class="none">Mahasiswa : </th>
                                    <th>Judul P2m</th>
                                    {{--<th>Sumber Dan Jenis Dana</th>--}}
                                    <th>Jumlah Dana</th>
                                    <th>Tahun</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i=1;?>
                                @foreach($result as $value)
                                    <tr>
                                        <td>{!! $i++ !!}</td>
                                        <td>
                                            <?php $result_dosen=App\Models\Dosen\P2m::Dosen($value->id);?>
                                            @if(is_object($result_dosen) || is_array($result_dosen))
                                                @foreach($result_dosen as $dos)
                                                    {!! $dos->nama_dosen.', ' !!}
                                                @endforeach
                                            @endif
                                        </td>
                                        <td>
                                            <?php $result_mhs=App\Models\Dosen\P2m::Mhs($value->id); ?>
                                            @if(is_object($result_mhs) || is_array($result_mhs))
                                                @foreach($result_mhs as $m)
                                                    {!! $m->nama.', ' !!}
                                                @endforeach
                                            @endif
                                        </td>
                                        <td>{!! $value->judul_p2m !!}</td>
                                        <td>{!! number_format($value->jumlah_dana,2) !!}</td>
                                        <td>{!! $value->tahun_p2m !!}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>

            </div>





            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
@stop
@section('js')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{!! URL::to('local/resources/assets/assets/global/scripts/datatable.js')!!}" type="text/javascript"></script>
    <script src="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/datatables.min.js')!!}" type="text/javascript"></script>
    <script src="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')!!}" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="{!! URL::to('local/resources/assets/assets/global/scripts/app.min.js')!!}" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{!! URL::to('local/resources/assets/assets/pages/scripts/table-datatables-responsive.min.js')!!}" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
@stop