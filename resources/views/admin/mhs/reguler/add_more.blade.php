@extends('admin.layouts.index')
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>
                        Form Input Data Mahasiswa Reguler
                    </h1>
                </div>
                <div class="pull-right">
                    <a href="{!! URL::to('mhs/view/reguler') !!}" class="btn btn-sm green dropdown-toggle">Back</a>
                </div>
            </div>
            <!-- END PAGE HEAD-->
            <!-- BEGIN PAGE BREADCRUMB -->

            <!-- END PAGE BREADCRUMB -->
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption font-dark">
                                <i class="icon-settings font-dark"></i>
                                <span class="caption-subject bold"></span>
                            </div>
                            <div class="tools"></div>
                        </div>
                        <div class="portlet-body">
                            <form class="form-horizontal" role="form" method="post" action="{!! URL::to('mhs/view/reguler/addmore/'.$single->id) !!}">
                                {!! csrf_field() !!}
                                <div class="portlet-body flip-scroll">
                                    <table class="table table-bordered table-striped table-condensed flip-content">
                                        <thead class="flip-content">
                                        <tr>
                                            <th rowspan="2" class="text-center"> Tahun Akademik </th>
                                            <th colspan="3" class="text-center"> IPK Lulusan Reguler</th>
                                            <th colspan="3" class="text-center"> Presentase Lulusan Reguler dengan IPK (%) </th>
                                        </tr>
                                        <tr>
                                            <th colspan="1" class="text-center">Min</th>
                                            <th colspan="1" class="text-center">Rat</th>
                                            <th colspan="1" class="text-center">Mak</th>
                                            <th colspan="1" class="text-center">< 2,75</th>
                                            <th colspan="1" class="text-center">2,75 - 3,50</th>
                                            <th colspan="1" class="text-center">< 3,50</th>

                                        </tr>

                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>{!! $single->tahun_akademik !!}</td>
                                            <td>
                                                <input type="text" name="ipk_min" class="form-control" value="{!! $single->ipk_min !!}">
                                            </td>
                                            <td>
                                                <input type="text" name="ipk_rata" class="form-control" value="{!! $single->ipk_rata !!}">
                                            </td>
                                            <td>
                                                <input type="text" name="ipk_mak" class="form-control" value="{!! $single->ipk_mak !!}">
                                            </td>
                                            <td>
                                                <input type="number" name="p_rendah" class="form-control" value="{!! $single->p_rendah !!}">
                                            </td>
                                            <td>
                                                <input type="number" name="p_sedang" class="form-control" value="{!! $single->p_sedang !!}">
                                            </td>
                                            <td>
                                                <input type="number" name="p_tinggi" class="form-control" value="{!! $single->p_tinggi !!}">
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-10">
                                        <button type="submit" class="btn blue">Simpan</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>

            </div>





            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
@stop