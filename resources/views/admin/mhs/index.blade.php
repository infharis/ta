@extends('admin.layouts.index')
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">

            <!-- END PAGE BREADCRUMB -->
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light portlet-fit bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class=" icon-layers font-green"></i>
                                <span class="caption-subject font-green bold uppercase">Kelompok Data Mahasiswa</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="mt-element-step">
                                <div class="row step-default">
                                    <div class="col-md-6 bg-grey mt-step-col">
                                        <a href="{!! URL::to('mhs/view/reguler') !!}" style="text-decoration:none">
                                            <div class="mt-step-title  font-grey-cascade">Mahasiswa Reguler</div>
                                            <div class="mt-step-content font-grey-cascade">
                                                Data Seluruh Mahasiswa Reguler dan lulusannya
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-6 bg-grey mt-step-col active">
                                        {{--<div class="mt-step-number bg-white font-grey"></div>--}}
                                        <a href="{!! URL::to('mhs/view/non_reguler') !!}" style="text-decoration:none">
                                            <div class="mt-step-title font-grey-cascade">Mahasiswa Non Reguler</div>
                                            <div class="mt-step-content font-grey-cascade">
                                                Data Mahasiswa Non-Reguler
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-6 bg-grey mt-step-col active">
                                        {{--<div class="mt-step-number bg-white font-grey"></div>--}}
                                        <a href="{!! URL::to('mhs/view/jumlah_reguler') !!}" style="text-decoration:none">
                                            <div class="mt-step-title font-grey-cascade">Mahasiswa Reguler</div>
                                            <div class="mt-step-content font-grey-cascade">
                                                Data Jumlah Mahasiswa Reguler 7 tahun akademik terakhir
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-6 bg-grey mt-step-col">
                                        {{--<div class="mt-step-number bg-white font-grey"></div>--}}
                                        <a href="{!! URL::to('mhs/view/data') !!}" style="text-decoration:none">
                                            <div class="mt-step-title font-grey-cascade">Master Mahasiswa</div>
                                            <div class="mt-step-content font-grey-cascade">
                                                Biodata, Prestasi, Akademik, Tugas Akhir Mahasiswa/i
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <br/>
                                <br/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
@stop


