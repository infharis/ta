@extends('admin.layouts.index')
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>Form Edit Jumlah Mahasiswa Reguler
                    </h1>
                </div>
                <div class="pull-right">
                    <a href="{!! URL::to('mhs/view/jumlah_reguler') !!}" class="btn btn-sm green dropdown-toggle">Back</a>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption font-dark">
                                <i class="icon-settings font-dark"></i>
                                <span class="caption-subject bold"></span>
                            </div>
                            <div class="tools"></div>
                        </div>
                        <div class="portlet-body">
                            <form class="form-horizontal" role="form" method="post" action="{!! URL::to('mhs/update/jumlah_reguler/'.$result->id) !!}">
                                {!! csrf_field() !!}
                                <div class="portlet-body flip-scroll">
                                    <table class="table table-bordered table-striped table-condensed flip-content">
                                        <thead class="flip-content">
                                        <tr>
                                            <th rowspan="2" class="text-center">Tahun Masuk </th>
                                            <th colspan="6" class="text-center">Jumlah Mahasiswa Reguler per Angkatan pada Tahun</th>
                                        </tr>
                                        <tr>
                                            <th>TS-6</th>
                                            <th>TS-5</th>
                                            <th>TS-4</th>
                                            <th>TS-3</th>
                                            <th>TS-2</th>
                                            <th>TS-1</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>
                                                <input type="text" maxlength="4" name="tahun_masuk" class="form-control" value="{!! $result->tahun_masuk !!}">
                                            </td>
                                            <td>
                                                <input type="number" name="ts6" class="form-control"  value="{!! $result->ts6 !!}">
                                            </td>
                                            <td>
                                                <input type="number" name="ts5" class="form-control" value="{!! $result->ts5 !!}">
                                            </td>
                                            <td>
                                                <input type="number" name="ts4" class="form-control" value="{!! $result->ts4 !!}">
                                            </td>
                                            <td>
                                                <input type="number" name="ts3" class="form-control" value="{!! $result->ts3 !!}">
                                            </td>
                                            <td>
                                                <input type="number" name="ts2" class="form-control" value="{!! $result->ts2 !!}">
                                            </td>
                                            <td>
                                                <input type="number" name="ts1" class="form-control" value="{!! $result->ts1 !!}">
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-10">
                                        <button type="submit" class="btn blue">Update</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>

            </div>





            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
@stop