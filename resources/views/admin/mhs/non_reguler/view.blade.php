@extends('admin.layouts.index')
@section('css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/datatables.min.css') !!}" rel="stylesheet" type="text/css" />
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')!!}" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->
@stop
@section('content')
    {{-- begin content --}}
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">@include('notif')
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>
                        Data Mahasiswa Non Reguler
                    </h1>
                </div>
                <div class="pull-right">
                    <a href="{!! URL::to('mhs/view') !!}" class="btn btn-sm yellow dropdown-toggle">Back</a>
                    <a href="{!! URL::to('mhs/view/non_reguler_create') !!}" class="btn btn-sm green dropdown-toggle">Tambah</a>
                </div>
                <!-- END PAGE TITLE -->
                <!-- BEGIN PAGE TOOLBAR -->

                <!-- END PAGE TOOLBAR -->
            </div>
            <!-- END PAGE HEAD-->

            <!-- END PAGE BREADCRUMB -->
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption font-dark">
                                <i class="icon-settings font-dark"></i>
                                <span class="caption-subject bold uppercase"></span>
                            </div>
                            {{--<div class="tools"> </div>--}}
                        </div>
                        <div class="portlet-body">
                            <div class="portlet-body flip-scroll">
                                <table class="table table-bordered table-striped table-condensed flip-content" id="sample_1">
                                    <thead class="flip-content">
                                    <tr>
                                        <th rowspan="2" class="text-center">No</th>
                                        <th rowspan="2" class="text-center"> Tahun Akademik </th>
                                        <th rowspan="2" class="text-center"> Daya Tampung </th>
                                        <th colspan="2" class="text-center"> Jumlah Calon mhs </th>
                                        <th colspan="2" class="text-center">Jumlah mhs Baru</th>
                                        <th colspan="2" class="text-center">Jumlah total mhs</th>
                                        <th rowspan="2" class="text-center">Aksi</th>

                                    </tr>
                                    <tr>
                                        <th colspan="1" class="text-center">Ikut Seleksi</th>
                                        <th colspan="1" class="text-center">Lulus Seleksi</th>
                                        <th colspan="1" class="text-center">Non Reguler</th>
                                        <th colspan="1" class="text-center">Transfer</th>
                                        <th colspan="1" class="text-center">Non Reguler</th>
                                        <th colspan="1" class="text-center">Transfer</th>
                                    </tr>

                                    </thead>
                                    <tbody>
                                    <?php $i=1; ?>
                                    @foreach($result as $value)
                                        <tr>
                                            <td>{!! $i++ !!}</td>
                                            <td>{!! $value->tahun_akademik !!}</td>
                                            <td>{!! $value->daya_tampung !!}</td>
                                            <td>{!! $value->ikut_seleksi !!}</td>
                                            <td>{!! $value->lulus_seleksi !!}</td>
                                            <td>{!! $value->non_reguler !!}</td>
                                            <td>{!! $value->transfer !!}</td>
                                            <td>{!! $value->t_non_reguler !!}</td>
                                            <td>{!! $value->t_transfer !!}</td>
                                            <td>
                                                <a href="{!! URL::to('mhs/edit/non_reguler/'.$value->id) !!}" class="btn btn-primary">Edit</a>
                                                <a href="#" id="delete" value="{!! $value->id !!}" class="btn btn-danger">Delete</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    {{-- end content --}}

@stop
@section('js')
    <script type="text/javascript">
        $(document).ready(function(){

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $(document).on("click","#delete",function(){
                if(confirm('Are You Sure Delete This Data?'))
                {
                    var val=$(this).attr('value');
                    $.ajax({
                        type : 'DELETE',
                        url :'{!! URL::to('mhs/delete/non_reguler') !!}',
                        data :{
                            id:val
                        },
                        success:function(response)
                        {
                            if(response==1)
                            {
                                alert('Data Berhasil Dihapus');
                                location.reload();
                            }else {
                                alert('Gagal Menghapus Data');
                            }
                        }
                    });
                }else {
                    return false;
                }
            });
        });
    </script>
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{!! URL::to('local/resources/assets/assets/global/scripts/datatable.js') !!}" type="text/javascript"></script>
    <script src="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/datatables.min.js')!!}" type="text/javascript"></script>
    <script src="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')!!}" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src={!! URL::to('local/resources/assets/assets/pages/scripts/table-datatables-colreorder.min.js')!!} type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
@stop