@extends('admin.layouts.index')
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>Form Input Data Mahasiswa Non Reguler
                    </h1>
                </div>
                <div class="pull-right">
                    <a href="{!! URL::to('mhs/view/non_reguler') !!}" class="btn btn-sm green dropdown-toggle">Back</a>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption font-dark">
                                <i class="icon-settings font-dark"></i>
                                <span class="caption-subject bold"></span>
                            </div>
                            <div class="tools"></div>
                        </div>
                        <div class="portlet-body">
                            <form class="form-horizontal" role="form" action="{!! URL::to('mhs/store/non_reguler_create') !!}" method="post">
                                {!! csrf_field() !!}
                                <div class="portlet-body flip-scroll">
                                    <table class="table table-bordered table-striped table-condensed flip-content">
                                        <thead class="flip-content">
                                        <tr>
                                            <th rowspan="2" class="text-center"> Tahun Akademik </th>
                                            <th rowspan="2" class="text-center"> Daya Tampung </th>
                                            <th colspan="2" class="text-center"> Jumlah Calon mhs Reguler </th>
                                            <th colspan="2" class="text-center">Jumlah mhs Baru</th>
                                            <th colspan="2" class="text-center">Jumlah total mhs</th>
                                        </tr>
                                        <tr>
                                            <th colspan="1" class="text-center">Ikut Seleksi</th>
                                            <th colspan="1" class="text-center">Lulus Seleksi</th>
                                            <th colspan="1" class="text-center">Reguler</th>
                                            <th colspan="1" class="text-center">Transfer</th>
                                            <th colspan="1" class="text-center">Reguler</th>
                                            <th colspan="1" class="text-center">Transfer</th>

                                        </tr>

                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>
                                                <input type="text" maxlength="4" name="tahun_akademik" class="form-control">
                                            </td>
                                            <td>
                                                <input type="number" name="daya_tampung" class="form-control">
                                            </td>
                                            <td>
                                                <input type="number" name="ikut_seleksi" class="form-control">
                                            </td>
                                            <td>
                                                <input type="number" name="lulus_seleksi" class="form-control">
                                            </td>
                                            <td>
                                                <input type="number" name="non_reguler" class="form-control">
                                            </td>
                                            <td>
                                                <input type="number" name="transfer" class="form-control">
                                            </td>
                                            <td>
                                                <input type="number" class="form-control" name="t_non_reguler">
                                            </td>
                                            <td>
                                                <input type="number" class="form-control" name="t_transfer">
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-10">
                                        <button type="submit" class="btn blue">Simpan</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>

            </div>





            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
@stop