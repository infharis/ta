<?php

Excel::create($sheet1, function($excel)use($result,$sheet1,$name_file) {


    // 2.5 umpan balik
    $excel->sheet($sheet1, function($sheet)use($result,$name_file) {
        // center title and value title
        $sheet->cells('B3', function($cells) {
            $cells->setFontColor('#ffffff');
            $cells->setAlignment('center');
            $cells->setFontSize(24);
            $cells->setValue('Umpan balik dari dosen, mahasiswa, alumni, dan pengguna lulusan');
        });
        $sheet->cells('A1:A7', function($cells) {
           // $cells->setAlignment('center');
            $cells->setBackground('#1e422a');
        });
        $sheet->cells('B1:B7', function($cells) {
           // $cells->setAlignment('center');
            $cells->setBackground('#1e422a');
        }); $sheet->cells('C1:C7', function($cells) {
           // $cells->setAlignment('center');
            $cells->setBackground('#1e422a');
        });
        // end

        // heading border
        $sheet->setBorder('A8:C8', 'thick');
        // heading title
        $sheet->cell('A8', function($cell) {
            $cell->setFont(array(
                'family'     => 'Calibri',
                'size'       => '16',
                'bold'       =>  true
            ));
            $cell->setValue('Umpan Balik Dari');
        });
        $sheet->cell('B8', function($cell) {
            $cell->setFont(array(
                'family'     => 'Calibri',
                'size'       => '16',
                'bold'       =>  true
            ));
            $cell->setValue('Isi Umpan Balik');
        });
        $sheet->cell('C8', function($cell) {
            $cell->setFont(array(
                'family'     => 'Calibri',
                'size'       => '16',
                'bold'       =>  true
            ));
            $cell->setValue('Tindak Lanjut');
        });

        // values
        //$sheet->mergeCells('C9:C9');

        $a=9;$b=9;$c=9;
        foreach($result as $value){
            $sheet->setSize('A'.$a, 50, 50);
            $sheet->setSize('B'.$b, 50, 50);
            $sheet->setSize('C'.$c, 50, 50);
            $sheet->setBorder('A'.$a.':C'.$c, 'thin');

            $sheet->cell('A'.$a++, function($cell)use($value){
                $cell->setValignment('justify');
                // manipulate the cell
                $cell->setValue($value->umpan_balik);
            });

            $sheet->cell('B'.$b++, function($cell)use($value){
                $cell->setValignment('justify');
                // manipulate the cell
                $cell->setValue($value->isi);
            });

            $sheet->cell('C'.$c++, function($cell)use($value){
                $cell->setValignment('justify');
                // manipulate the cell
                $cell->setValue($value->tindak_lanjut);
            });
        }

    });


})->export('xls');