<?php

Excel::create($sheet1, function($excel)use($result,$sheet1,$name_file) {


    // 2.5 umpan balik
    $excel->sheet($sheet1, function($sheet)use($result,$name_file) {
        // center title and value title
        $sheet->cells('C3', function($cells) {
            $cells->setFontColor('#ffffff');
            $cells->setAlignment('center');
            $cells->setFontSize(24);
            $cells->setValue('Data Penelitian Dosen Jurusan Informatika');
        });
        $sheet->cells('A1:A4', function($cells) {
            // $cells->setAlignment('center');
            $cells->setBackground('#1e422a');
        });
        $sheet->cells('B1:B4', function($cells) {
            // $cells->setAlignment('center');
            $cells->setBackground('#1e422a');
        }); $sheet->cells('C1:C4', function($cells) {
            // $cells->setAlignment('center');
            $cells->setBackground('#1e422a');
        });
        $sheet->cells('D1:D4', function($cells) {
            // $cells->setAlignment('center');
            $cells->setBackground('#1e422a');
        });
        // end

        // set heading border
        $sheet->setBorder('A5:D5', 'thick');


        // heading title
        $sheet->cell('A5', function($cell) {
            $cell->setFont(array(
                'family'     => 'Calibri',
                'size'       => '16',
                'bold'       =>  true
            ));
            $cell->setValue('Tahun');
        });
        $sheet->cell('B5', function($cell) {
            $cell->setFont(array(
                'family'     => 'Calibri',
                'size'       => '16',
                'bold'       =>  true
            ));
            $cell->setValue('Judul Penelitian');
        });
        $sheet->cell('C5', function($cell) {
            $cell->setFont(array(
                'family'     => 'Calibri',
                'size'       => '16',
                'bold'       =>  true
            ));
            $cell->setValue('Sumber Dan Jenis Dana');
        });
        $sheet->cell('D5', function($cell) {
            $cell->setFont(array(
                'family'     => 'Calibri',
                'size'       => '16',
                'bold'       =>  true
            ));
            $cell->setValue('Jumlah Dana');
        });


        //$sheet->mergeCells('C9:C9');

        $a=6;$b=6;$c=6;$d=6;$i=1;
        for($i=0;$i<3;$i++){
            foreach(App\Models\Admin\DataPenelitian::PenelitianTs($result[$i]) as $value){
                $sheet->setSize('A'.$a, 10, 10); // width, height
                $sheet->setSize('B'.$b, 100, 100);
                $sheet->setSize('C'.$c,50, 20);
                $sheet->setSize('D'.$d, 50, 20);
                $sheet->setBorder('A'.$a.':D'.$d, 'thin');

                $sheet->cell('A'.$a++, function($cell)use($value){
                    $cell->setValignment('justify');
                    // manipulate the cell
                    $cell->setValue($value->tahun_penelitian);
                });

                $sheet->cell('B'.$b++, function($cell)use($value){
                    $cell->setValignment('justify');
                    // manipulate the cell
                        $cell->setValue($value->judul_penelitian);
                });


                $sheet->cell('C'.$c++, function($cell)use($value){
                    $cell->setValignment('justify');
                    // manipulate the cell
                    $sd=null;
                    foreach(App\Models\Admin\Dosen::SumberDana($value->id) as $jd){
                        $sd.=$jd->nama_jenis_dana.', ';
                    }
                    $cell->setValue(' '.$sd.'');
                });
                // set type data
                $sheet->setColumnFormat(array(
                    'D'.$d => '#,##0.00_-'
                ));
                $sheet->cell('D'.$d++, function($cell)use($value){
                    $cell->setValignment('justify');
                    // manipulate the cell
                    $cell->setValue($value->jumlah_dana);
                });
                $i++;
            }
        }

    });


})->export('xls');