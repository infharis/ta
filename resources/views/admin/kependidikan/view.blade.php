@extends('admin.layouts.index')
@section('css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/datatables.min.css') !!}" rel="stylesheet" type="text/css" />
    <link href="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')!!}" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->
@stop
@section('content')
    {{-- begin content --}}
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">@include('notif')
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>
                        Data Tenaga Kependidikan
                    </h1>
                </div>
                <div class="pull-right">
                    <a href="{!! URL::to('sdm/kependidikan/create') !!}" class="btn btn-sm green dropdown-toggle">Tambah</a>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption font-dark">
                                <i class="icon-settings font-dark"></i>
                                <span class="caption-subject bold uppercase"></span>
                            </div>
                            {{--<div class="tools"> </div>--}}
                        </div>
                        <div class="portlet-body">
                            <div class="portlet-body flip-scroll">
                                <table class="table table-bordered table-striped table-condensed flip-content" id="sample_2">
                                    <thead class="flip-content">
                                    <tr>
                                        <th rowspan="2">No</th>
                                        <th rowspan="2">Tenaga Kependidikan</th>
                                        <th colspan="10" class="text-center">Jumlah Tenaga Kependidikan dengan Pendidikan Terakhir</th>
                                        {{--<th rowspan="2">Unit Kerja</th>--}}
                                        <th rowspan="2">Total</th>
                                        <th rowspan="2">Aksi</th>
                                    </tr>
                                    <tr>
                                        <th colspan="1">S3</th>
                                        <th colspan="1">S2</th>
                                        <th colspan="1">S1</th>
                                        <th colspan="1">D4</th>
                                        <th colspan="1">D3</th>
                                        <th colspan="1">D2</th>
                                        <th colspan="1">D1</th>
                                        <th colspan="1">SMA/SMK</th>
                                        <th colspan="1">SMP</th>
                                        <th colspan="1">SD</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        $i=1;
                                        $s3=0;$s2=0;$s1=0;$d4=0;$d3=0;$d2=0;$d1=0;$sma=0;$smp=0;$sd=0;$total=0;
                                    ?>
                                    @foreach($result as $value)
                                        <tr>
                                            <td>{!! $i++ !!}</td>
                                            <td>{!! $value->nama !!}</td>
                                            <td>@if($s3=is_int(App\Models\Admin\TenagaKependidikan::CountTK(1,$value->id))) @endif </td>
                                            <td>@if($s2=is_int(App\Models\Admin\TenagaKependidikan::CountTK(2,$value->id))) @endif</td>
                                            <td> @if($s1=is_int(App\Models\Admin\TenagaKependidikan::CountTK(3,$value->id))) @endif</td>
                                            <td>@if($d4=is_int(App\Models\Admin\TenagaKependidikan::CountTK(4,$value->id))) @endif</td>
                                            <td>@if($d3=is_int(App\Models\Admin\TenagaKependidikan::CountTK(5,$value->id))) @endif</td>
                                            <td>@if($d2=is_int(App\Models\Admin\TenagaKependidikan::CountTK(6,$value->id))) @endif</td>
                                            <td>@if($d1=is_int(App\Models\Admin\TenagaKependidikan::CountTK(7,$value->id))) @endif</td>
                                            <td>@if($sma=is_int(App\Models\Admin\TenagaKependidikan::CountTK(8,$value->id))) @endif </td>
                                            <td>@if($smp=is_int(App\Models\Admin\TenagaKependidikan::CountTK(9,$value->id))) @endif</td>
                                            <td>@if($sd=is_int(App\Models\Admin\TenagaKependidikan::CountTK(10,$value->id))) @endif</td>
                                            <td>
                                                <?php $total+=$s3+$s2+$s1+$d4+$d3+$d2+$d1+$sma+$smp+$sd ?>
                                                {!!  $s3+$s2+$s1+$d4+$d3+$d2+$d1+$sma+$smp+$sd !!}
                                            </td>
                                            <td>
                                                <a href="{!! URL::to('sdm/kependidikan/detail/'.$value->id) !!}" class="btn btn-primary">Detail</a>
                                            </td>
                                            <?php
                                                $s3=0;$s2=0;$s1=0;$d4=0;$d3=0;$d2=0;$d1=0;$sma=0;$smp=0;$sd=0;
                                            ?>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tr>

                                        <td colspan="2" class="text-center"><b>Total</b></td>
                                        <td>{!! App\Models\Admin\TenagaKependidikan::CountPendidikan(1) !!}</td>
                                        <td>{!! App\Models\Admin\TenagaKependidikan::CountPendidikan(2) !!}</td>
                                        <td>{!! App\Models\Admin\TenagaKependidikan::CountPendidikan(3) !!}</td>
                                        <td>{!! App\Models\Admin\TenagaKependidikan::CountPendidikan(4) !!}</td>
                                        <td>{!! App\Models\Admin\TenagaKependidikan::CountPendidikan(5) !!}</td>
                                        <td>{!! App\Models\Admin\TenagaKependidikan::CountPendidikan(6) !!}</td>
                                        <td>{!! App\Models\Admin\TenagaKependidikan::CountPendidikan(7) !!}</td>
                                        <td>{!! App\Models\Admin\TenagaKependidikan::CountPendidikan(8) !!}</td>
                                        <td>{!! App\Models\Admin\TenagaKependidikan::CountPendidikan(9) !!}</td>
                                        <td>{!! App\Models\Admin\TenagaKependidikan::CountPendidikan(10) !!}</td>
                                        <td>{!! $total !!}</td>
                                        <td></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    {{-- end content --}}

@stop
@section('js')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{!! URL::to('local/resources/assets/assets/global/scripts/datatable.js') !!}" type="text/javascript"></script>
    <script src="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/datatables.min.js')!!}" type="text/javascript"></script>
    <script src="{!! URL::to('local/resources/assets/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')!!}" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src={!! URL::to('local/resources/assets/assets/pages/scripts/table-datatables-colreorder.min.js')!!} type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
@stop