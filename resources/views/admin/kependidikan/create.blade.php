@extends('admin.layouts.index')
@section('css')
    <link href="{!! URL::to('local/resources/assets/selectize/css/selectize.bootstrap3.css') !!}" rel="stylesheet">
@stop
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- END PAGE BREADCRUMB -->
            <div class="row ">
                <div class="col-md-12">
                    <!-- BEGIN SAMPLE FORM PORTLET-->
                    <div class="portlet box purple ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-gift"></i> Form Tambah Data Tenaga Kependidikan</div>
                            <div class="pull-right" style="padding-top: 0.5%;">
                                <a href="{!! URL::to('sdm/kependidikan/view') !!}" class="btn btn-sm yellow dropdown-toggle">Back</a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <form role="form" method="post" action="{!! URL::to('kependidikan/store') !!}">
                                {!! csrf_field() !!}
                                <!-- /.row -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="">Nama</label><span class="required" aria-required="true"> * </span>
                                        <input type="text" class="form-control" name="nama" required>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="">Tempat Lahir</label><span class="required" aria-required="true"> * </span>
                                        <input type="text" class="form-control" name="tempat_lahir">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="">Tanggal Lahir</label><span class="required" aria-required="true"> * </span>
                                        <input type="date" class="form-control" name="tgl_lahir">
                                    </div>
                                </div>
                                  <div class="row">
                                      <div class="col-md-12">
                                          <label for="">Alamat</label><span class="required" aria-required="true"> * </span>
                                          <input type="text" class="form-control" name="alamat">
                                      </div>
                                  </div>
                                    <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="">Sebagai</label>
                                        <div class="md-radio-inline">
                                            @foreach($tk as $value)
                                            <div class="md-radio">
                                                <input type="radio" id="checkbox{!! $value->id !!}" name="sebagai" value="{!! $value->id !!}" class="md-radiobtn">
                                                <label for="checkbox{!! $value->id !!}">
                                                    <span></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span>{!! $value->nama !!}</label>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label> Pendidikan Terakhir</label>
                                            <div class="md-radio-inline">
                                                @foreach($pt as $val)
                                                    <div class="md-radio">
                                                        <input  type="radio" id="checkbox1_{!! $val->id !!}" name="pendidikan" value="{!! $val->id !!}" class="md-radiobtn">
                                                        <label for="checkbox1_{!! $val->id !!}">
                                                            <span></span>
                                                            <span class="check"></span>
                                                            <span class="box"></span> {!! $val->nama_pendidikan !!} </label>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                <br>
                                <input type="submit" class="btn btn-primary" value="Simpan">
                            </form>
                        </div>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->

@stop
@section('js')
    <script src="{!! URL::to('local/resources/assets/selectize/common.min.js')!!}"></script>
    <script src="{!! URL::to('local/resources/assets/selectize/uikit_custom.min.js')!!}"></script>
    <script src="{!! URL::to('local/resources/assets/selectize/altair_admin_common.min.js')!!}"></script>
@stop