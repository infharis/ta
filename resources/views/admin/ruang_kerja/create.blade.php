@extends('admin.layouts.index')
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">@include('notif')
            <!-- END PAGE BREADCRUMB -->
            <div class="row ">
                <div class="col-md-12">
                    <!-- BEGIN SAMPLE FORM PORTLET-->
                    <div class="portlet box purple ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-gift"></i> Form Tambah Data Ruang Kerja
                            </div>
                            <div class="pull-right" style="padding-top: 0.5%;">
                                <a href="{!! URL::to('ruang_kerja/view') !!}" class="btn btn-sm yellow dropdown-toggle">Back</a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <form role="form" action="{!! URL::to('ruang_kerja/store') !!}" method="post">
                                {!! csrf_field() !!}
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="">Ruang Kerja</label><span class="required" aria-required="true"> * </span>
                                        <input type="text" class="form-control" name="ruang_kerja">
                                    </div>
                                </div>
                                <!-- /.row -->
                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="">Jumlah</label><span class="required" aria-required="true"> * </span>
                                        <input type="text" class="form-control" name="jumlah">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="">Luas</label><span class="required" aria-required="true"> * </span>
                                        <input type="text" class="form-control" name="luas">
                                    </div>
                                </div>
                                <br><br>
                                <input type="submit" class="btn btn-primary" value="Simpan">
                            </form>
                        </div>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->

@stop